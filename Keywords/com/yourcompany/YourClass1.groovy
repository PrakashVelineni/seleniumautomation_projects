package com.yourcompany

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI


import internal.GlobalVariable

public class YourClass1 {

	@Keyword
	def yourNewKeyword() {
		WebUI.comment("This is my new keyword...")
	}

	static void yourMethod() {
		WebUI.delay(1)

		WebUI.scrollToPosition(0, 500)

		WebUI.delay(2)

		if (WebUI.verifyElementPresent(findTestObject('Object Repository/Youth_ProductListingPage_OR1/Page_Thort SS youth white S  T-Shirts youth  Streetwear youth  Categories  Apparel Site UK/button_Add to bag'), 5)){

			WebUI.click(findTestObject('Object Repository/Youth_ProductListingPage_OR1/Page_Thort SS youth white S  T-Shirts youth  Streetwear youth  Categories  Apparel Site UK/button_Add to bag'))

			WebUI.delay(2)

			WebUI.click(findTestObject('Object Repository/Youth_ProductListingPage_OR1/Page_Thort SS youth white S  T-Shirts youth  Streetwear youth  Categories  Apparel Site UK/span_Added to Your Shopping Bag_glyphicon glyphicon-remove'))

			WebUI.delay(2)

			WebUI.click(findTestObject('Object Repository/Youth_ProductListingPage_OR1/Page_Thort SS youth white S  T-Shirts youth  Streetwear youth  Categories  Apparel Site UK/i_Added to Your Shopping Bag_far fa-heart'))

			WebUI.delay(1)

			WebUI.scrollToPosition(500, 1000)

			WebUI.delay(1)
		}

		else {

			WebUI.delay(1)

			WebUI.click(findTestObject('Object Repository/Youth_ProductListingPage_OR1/Page_Thort SS youth white S  T-Shirts youth  Streetwear youth  Categories  Apparel Site UK/i_Added to Your Shopping Bag_far fa-heart'))

			WebUI.delay(1)

			WebUI.scrollToPosition(500, 1000)

			WebUI.delay(1)
		}
	}
}

