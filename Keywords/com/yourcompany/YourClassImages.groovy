package com.yourcompany

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class YourClassImages {

	@Keyword
	def yourNewKeyword() {
		WebUI.comment("This is my new keyword...")
	}

	static void yourMethod() {

		WebUI.delay(1)

		WebUI.scrollToPosition(0, 500)

		WebUI.delay(1)

		WebUI.scrollToPosition(500, 1000)

		WebUI.delay(1)

		WebUI.scrollToPosition(1000, 1500)

		WebUI.delay(1)

		WebUI.scrollToPosition(1500, 2000)

		WebUI.delay(1)

		WebUI.scrollToPosition(2000, 2500)

		WebUI.delay(1)

		WebUI.scrollToPosition(2500, 3000)

		WebUI.delay(1)

		WebUI.scrollToPosition(3000, 3500)

		WebUI.delay(1)

		WebUI.scrollToPosition(3500, 4000)

		WebUI.delay(1)
	}
}



