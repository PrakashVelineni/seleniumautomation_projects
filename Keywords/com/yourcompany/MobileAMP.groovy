package com.yourcompany

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent

public class MobileAMP {

	@Keyword
	def yourNewKeyword() {
		WebUI.comment("This is my new keyword...")
	}

	static void yourMethod() {

		WebUI.delay(3)

		Robot rb = new Robot()

		rb.keyPress(KeyEvent.VK_CONTROL)

		rb.keyPress(KeyEvent.VK_SHIFT)

		rb.keyPress(KeyEvent.VK_J)

		WebUI.delay(3)

		rb.keyRelease(KeyEvent.VK_CONTROL)

		rb.keyRelease(KeyEvent.VK_SHIFT)

		rb.keyRelease(KeyEvent.VK_J)

		WebUI.delay(3)

		rb.keyPress(KeyEvent.VK_CONTROL)

		rb.keyPress(KeyEvent.VK_SHIFT)

		rb.keyPress(KeyEvent.VK_M)

		WebUI.delay(3)

		rb.keyRelease(KeyEvent.VK_CONTROL)

		rb.keyRelease(KeyEvent.VK_SHIFT)

		rb.keyRelease(KeyEvent.VK_M)

		WebUI.delay(3)
	}
}
