package com.apparel;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Window;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;

public class HomePage {
	WebDriver driver;
	
	 @BeforeTest
	  
	  public void beforeTest() {
		  System.setProperty("webdriver.chrome.driver","D:\\Prakash\\Selenium\\chromedriver_win32\\chromedriver.exe");
		  driver=new ChromeDriver();
		  driver.get("https://apparel-uk.hybrisdemo.sbpcorp.com/");
			 driver.manage().window().maximize();
 }
  @Test(description="Verify_headericons",priority=0, enabled=false)
  public void headericons() throws InterruptedException, AWTException {
	  System.out.println("Test Case 1 - Started  headericons");
	  driver.get("https://apparel-uk.hybrisdemo.sbpcorp.com/");
		 driver.manage().window().maximize();
		 Thread.sleep(5000);
		 driver.navigate().refresh();
		  Thread.sleep(5000);
		  driver.findElement(By.xpath("//*[@id=\"sticky-header\"]/nav[1]/div/div[2]/div/ul/li/a")).click();
		  Thread.sleep(2000);
	      driver.findElement(By.className("simple-banner")).click();
		  Thread.sleep(2000);
	      driver.findElement(By.id("js-site-search-input")).sendKeys("accessories");
		  Thread.sleep(2000);
		  Robot robot=new Robot();
		  robot.keyPress(KeyEvent.VK_ENTER);
		  Thread.sleep(4000);
		  driver.findElement(By.className("fa-user")).click();
		  Thread.sleep(4000);
	      driver.findElement(By.xpath("//*[@class='wish-list-count js-mini-cart-count number']")).click();
		  
		 System.out.println("1");
  
		  Thread.sleep(4000);
		  System.out.println("2");
	      driver.findElement(By.className("fa-cart-plus")).click();
		  Thread.sleep(2000);
		  System.out.println("3");
	      WebElement ele=driver.findElement(By.xpath("//*[@class='yCmsContentSlot componentContainer']"));
	      System.out.println("4");
		  Thread.sleep(2000);
	      Actions actions=new Actions(driver);
	      actions.moveToElement(ele).build().perform();
	      System.out.println("5");
		  Thread.sleep(2000);
	      driver.findElement(By.linkText("IMPORT SAVED CART")).click();
		  Thread.sleep(2000);
		  WebElement ele1=driver.findElement(By.xpath("//*[@class='yCmsContentSlot componentContainer']"));
	      Actions actions1=new Actions(driver);
	      actions1.moveToElement(ele1).build().perform();
	      driver.findElement(By.linkText("QUICK ORDER")).click();
		  Thread.sleep(2000);
	      driver.findElement(By.className("simple-banner")).click();
		  Thread.sleep(2000);
	      driver.findElement(By.xpath("//*[@title='Save Big on Select Streetwear']")).click();
		  Thread.sleep(2000);
		  driver.findElement(By.className("simple-banner")).click();
		  Thread.sleep(2000);
	      driver.findElement(By.xpath("//*[@title='Start Your Season']")).click();
		  Thread.sleep(2000);
		  driver.findElement(By.className("simple-banner")).click();
		  Thread.sleep(2000);
		  driver.findElement(By.xpath("//*[@title='Women']")).click();
		  driver.navigate().back();
		  Thread.sleep(2000);
		  driver.findElement(By.xpath("//*[@title='Men']")).click();
		  driver.navigate().back();
		  Thread.sleep(2000);
		  driver.findElement(By.xpath("//*[@title='Youth']")).click();
		  driver.navigate().back();
		  Thread.sleep(2000);
		  driver.findElement(By.xpath("//*[@title='Our brand range']")).click();
		  driver.navigate().back();
		  System.out.println("Test Case 1 - Completed headericons");
  }
 
		  
		  //Mouse hover on Best Selling Products
		  
		  @Test(description="Verify_Best_Selling_Products",priority=1, enabled=false)
		  public void best_selling_products() throws InterruptedException, AWTException {
			  System.out.println("Test Case 2 - Started Best_Selling_Products");
		  
		  Thread.sleep(2000);
		  WebElement ele2= driver.findElement(By.xpath("//*[@title='Snowboard Ski Tool Toko Ergo Multi Guide yellow']"));
		  Actions actions2=new Actions(driver);
	      actions2.moveToElement(ele2).build().perform();
	      ele2.click();
		  Thread.sleep(2000);
		  driver.navigate().back();
		  Thread.sleep(2000);
		  WebElement ele3=driver.findElement(By.xpath("//*[@title='Snowboard Ski Tool Toko Waxremover HC3 500ml']"));
		  Actions actions3=new Actions(driver);
	      actions3.moveToElement(ele3).build().perform();
	      ele3.click();
		  Thread.sleep(2000);
		  driver.navigate().back();
		  Thread.sleep(2000);
		  WebElement ele4 = driver.findElement(By.xpath("//*[@title='Helmet Women TSG Lotus Graphic Designs wms']"));
		  Actions actions4=new Actions(driver);
	      actions4.moveToElement(ele4).build().perform();
	      ele4.click();
		  Thread.sleep(2000);
		  driver.navigate().back();
		  Thread.sleep(2000);
		 
		  WebElement ele5 =  driver.findElement(By.xpath("//*[@title='Wallet Dakine Agent Leather Wallet brown']"));
		  Actions actions5=new Actions(driver);
	      actions5.moveToElement(ele5).build().perform();
	      ele5.click();
		  Thread.sleep(2000);
		  driver.navigate().back();
		  Thread.sleep(2000);
	
	     WebElement ele6 = driver.findElement(By.xpath("//*[@title='Shades Quiksilver Dinero black white red grey']"));
	      Actions actions6=new Actions(driver);
         actions6.moveToElement(ele6).build().perform();
         ele6.click();
     	  Thread.sleep(3000);
    	  driver.navigate().back();
  		  Thread.sleep(3000);
	
		  WebElement ele7 = driver.findElement(By.xpath("//*[@title='Snowboard Ski Tool Toko Ergo Speed Top 88�/89�']"));
		  Actions actions7=new Actions(driver);
	      actions7.moveToElement(ele7).build().perform();
	      ele7.click();
		  Thread.sleep(3000);
		  driver.navigate().back();
		  Thread.sleep(3000);
		 
		  WebElement ele8= driver.findElement(By.xpath("//*[@title='Shades Von Zipper Papa G white grey chrome']"));
		  Actions actions8=new Actions(driver);
	      actions8.moveToElement(ele8).build().perform();
	      ele8.click();
		  Thread.sleep(2000);
		  driver.navigate().back();
		  Thread.sleep(2000);
			  
			  System.out.println("Test Case 2 - Completed Best_Selling_Products");
		  }
			  
		  
		//Mouse hover on What's New
		  
			  
	 @Test(description="verify_whatsnew",priority=2, enabled=false)
	 public void whatsnew() throws InterruptedException, AWTException {
	 System.out.println("Test Case 3 - Started whatsnew");
				  
				  
		  Thread.sleep(2000);
		  WebElement ele9= driver.findElement(By.xpath("//*[@title='Shades Fox The Duncan polished black grey']"));
		  Actions actions9=new Actions(driver);
	      actions9.moveToElement(ele9).build().perform();
	      ele9.click();
		  Thread.sleep(2000);
		  driver.navigate().back();
		  Thread.sleep(2000);
		  WebElement ele10=driver.findElement(By.xpath("//*[@title='Shades Von Zipper Fernstein gold moss gradient']"));
		  Actions actions10=new Actions(driver);
	      actions10.moveToElement(ele10).build().perform();
	      ele10.click();
		  Thread.sleep(2000);
		  driver.navigate().back();
		  Thread.sleep(2000);
		  WebElement ele11 = driver.findElement(By.xpath("//*[@title='Shades Women Roxy Carla roxy black grey']"));
		  Actions actions11=new Actions(driver);
	      actions11.moveToElement(ele11).build().perform();
	      ele11.click();
		  Thread.sleep(2000);
		  driver.navigate().back();
		  Thread.sleep(2000);
		 
		  WebElement ele12 =  driver.findElement(By.xpath("//*[@title='Protector Dainese Waistcoat S7']"));
		  Actions actions12=new Actions(driver);
	      actions12.moveToElement(ele12).build().perform();
	      ele12.click();
		  Thread.sleep(2000);
		  driver.navigate().back();
		  Thread.sleep(2000);
		
		  WebElement ele13 = driver.findElement(By.xpath("//*[@title='Helmet Women TSG Lotus Graphic Designs wms']"));
		  Actions actions13=new Actions(driver);
	      actions13.moveToElement(ele13).build().perform();
	      ele13.click();
		  Thread.sleep(2000);
		  driver.navigate().back();
		  Thread.sleep(2000);

		  WebElement ele14= driver.findElement(By.xpath("//*[@title='Wallet Dakine Agent Leather Wallet brown']"));
		  Actions actions14=new Actions(driver);
	      actions14.moveToElement(ele14).build().perform();
	      ele14.click();
	      
		  Thread.sleep(2000);
		  driver.navigate().back();
		  Thread.sleep(4000);
		  System.out.println("1");
		  Thread.sleep(4000);
		  driver.findElement(By.xpath("/html/body/main/div[3]/div[4]/div[2]/div/div[2]/div[2]/div/div[2]/span")).click();
		  Thread.sleep(4000);
		  driver.findElement(By.xpath("/html/body/main/div[3]/div[4]/div[2]/div/div[2]/div[2]/div/div[2]/span")).click();
		  Thread.sleep(4000);
		  driver.findElement(By.xpath("/html/body/main/div[3]/div[4]/div[2]/div/div[2]/div[2]/div/div[2]/span")).click();
		  
		  Thread.sleep(4000);
		  WebElement ele15= driver.findElement(By.xpath("//*[@title='Snowboard Ski Tool Toko T8 800 Wachsb�geleisen f�r Hei�wachs']"));
		  Actions actions15=new Actions(driver);
	      actions15.moveToElement(ele15).build().perform();
	      ele15.click();
		  Thread.sleep(2000);
		  driver.navigate().back();
		  Thread.sleep(2000);
		  WebElement ele16= driver.findElement(By.xpath("//*[@title='Snowglasses adidas eyewear Yodai black matt orange mirror']"));
		  Actions actions16=new Actions(driver);
	      actions16.moveToElement(ele16).build().perform();
	      ele16.click();
		  Thread.sleep(2000);
		  driver.navigate().back();
		  Thread.sleep(2000);
		  WebElement ele17= driver.findElement(By.xpath("//*[@title='Cap Blue Tomato BT Snow Trucker Cap black']"));
		  Actions actions17=new Actions(driver);
	      actions17.moveToElement(ele17).build().perform();
	      ele17.click();
		  Thread.sleep(2000);
		  driver.navigate().back();
		  Thread.sleep(4000);
		  driver.findElement(By.xpath("/html/body/main/div[3]/div[4]/div[2]/div/div[2]/div[2]/div/div[2]/span")).click();
		  Thread.sleep(4000);
		  driver.findElement(By.xpath("/html/body/main/div[3]/div[4]/div[2]/div/div[2]/div[2]/div/div[1]/span")).click();
		  Thread.sleep(2000);
		  driver.findElement(By.className("simple-banner")).click();
		  Thread.sleep(2000);
		  WebElement ele18= driver.findElement(By.xpath("//*[@title='Cap Blue Tomato BT Snow Trucker Cap black']"));
		  Actions actions18=new Actions(driver);
	      actions18.moveToElement(ele18).build().perform();
	      ele18.click();
		  Thread.sleep(2000);
		  driver.navigate().back();
		  Thread.sleep(2000);
		  driver.findElement(By.xpath("//*[@title='Maguro Pu Belt plaid']")).click();
		  Thread.sleep(2000);
		  driver.navigate().back();
		  Thread.sleep(2000);
		  driver.findElement(By.xpath("//*[@title='Airline Bag Bluebird Uni']")).click();
		  Thread.sleep(2000);
		  driver.navigate().back();
		  Thread.sleep(2000);
		  driver.findElement(By.xpath("//*[@title='Adapt New Era Blacktop']")).click();
		  Thread.sleep(2000);
		  driver.navigate().back();
		  Thread.sleep(2000);
		  driver.findElement(By.xpath("//*[@title='Shades Von Zipper Papa G']")).click();
		  Thread.sleep(2000);
		  driver.navigate().back();
		  Thread.sleep(2000);
		  driver.findElement(By.xpath("//*[@title='Free Shipping on All Orders This Weekend']")).click();
		  
		  System.out.println("Test Case 3 - Completed whatsnew");
	 }
		  
		// Click on Footer Links
	 
	 @Test(description="verify_Footerlinks",priority=3, enabled=false)
	 public void footer_links() throws InterruptedException, AWTException {
	 System.out.println("Test Case 4 - Started footer_links");
		  
		  Thread.sleep(2000);
		  driver.findElement(By.xpath("//*[@title='FAQ']")).click();
		  Thread.sleep(2000);
		  driver.navigate().back();
		  Thread.sleep(2000);
		  System.out.println("1");
		  driver.findElement(By.xpath("//*[@title='About SAP Commerce Cloud']")).click();
		  Thread.sleep(2000);
		  ArrayList<String> tabs1= new ArrayList<String> (driver.getWindowHandles());
		  System.out.println(tabs1);
		  driver.switchTo().window(tabs1.get(1));
		  Thread.sleep(2000);
		  driver.close();
		  driver.switchTo().window(tabs1.get(0));
		  Thread.sleep(2000);
		  driver.findElement(By.xpath("//*[@title='Visit SAP']")).click();
		  Thread.sleep(2000);
		  ArrayList<String> tabs2= new ArrayList<String> (driver.getWindowHandles());
		  System.out.println(tabs2);
		  driver.switchTo().window(tabs2.get(1));
		  Thread.sleep(2000);
		  driver.close();
		  driver.switchTo().window(tabs1.get(0));
	      Thread.sleep(2000);
	      driver.findElement(By.xpath("//*[@title='Contact Us']")).click();
	      Thread.sleep(2000);
	      ArrayList<String> tabs3= new ArrayList<String> (driver.getWindowHandles());
		  System.out.println(tabs3);
		  driver.switchTo().window(tabs3.get(1));
	       Thread.sleep(2000);
	      driver.close();
	      driver.switchTo().window(tabs1.get(0));
		  Thread.sleep(2000);
		  driver.findElement(By.xpath("//*[@title='Twitter']")).click();
		  Thread.sleep(2000);
		  ArrayList<String> tabs4= new ArrayList<String> (driver.getWindowHandles());
		  System.out.println(tabs4);
		  driver.switchTo().window(tabs4.get(1));
	 	  Thread.sleep(2000);
	      driver.close();
	 	  driver.switchTo().window(tabs1.get(0));
		  Thread.sleep(2000);
	      driver.findElement(By.xpath("//*[@title='Agile Commerce Blog']")).click();
	      Thread.sleep(2000);
	      ArrayList<String> tabs5= new ArrayList<String> (driver.getWindowHandles());
		  System.out.println(tabs5);
		  driver.switchTo().window(tabs5.get(1));
	      Thread.sleep(2000);
	      driver.close();
	     driver.switchTo().window(tabs1.get(0));
		  Thread.sleep(2000);
		  driver.findElement(By.xpath("//*[@title='Linked In']")).click();

		  Thread.sleep(2000);
		  ArrayList<String> tabs6= new ArrayList<String> (driver.getWindowHandles());
		  System.out.println(tabs6);
		  driver.switchTo().window(tabs6.get(1));
		  Thread.sleep(2000);
		  driver.close();
		  driver.switchTo().window(tabs1.get(0));
		  Thread.sleep(2000);
		  driver.findElement(By.className("fa-google-plus")).click();
		  Thread.sleep(2000);
		  ArrayList<String> tabs7= new ArrayList<String> (driver.getWindowHandles());
		  System.out.println(tabs7);
		  driver.switchTo().window(tabs7.get(1));
		  Thread.sleep(2000);
		  driver.close();
		  driver.switchTo().window(tabs1.get(0));
		  Thread.sleep(2000);
		  driver.findElement(By.className("fa-twitter")).click();
		  Thread.sleep(2000);
		  ArrayList<String> tabs8= new ArrayList<String> (driver.getWindowHandles());
		  System.out.println(tabs8);
		  driver.switchTo().window(tabs8.get(1));
		  Thread.sleep(2000);
		  driver.close();
		  driver.switchTo().window(tabs1.get(0));
		  Thread.sleep(2000);
		  driver.findElement(By.className("fa-pinterest-square")).click();
		  Thread.sleep(2000);
		  ArrayList<String> tabs9= new ArrayList<String> (driver.getWindowHandles());
		  System.out.println(tabs9);
		  driver.switchTo().window(tabs9.get(1));
		  Thread.sleep(2000);
		  driver.close();
		  driver.switchTo().window(tabs1.get(0));
		  Thread.sleep(2000);
		  driver.findElement(By.className("fa-play")).click();
		  Thread.sleep(2000);
		  ArrayList<String> tabs10= new ArrayList<String> (driver.getWindowHandles());
		  System.out.println(tabs10);
		  driver.switchTo().window(tabs10.get(1));
		  Thread.sleep(2000);
		  driver.close();
		  driver.switchTo().window(tabs1.get(0));
		  Thread.sleep(2000);
		  driver.findElement(By.className("fa-facebook")).click();
		  Thread.sleep(2000);
		  ArrayList<String> tabs11= new ArrayList<String> (driver.getWindowHandles());
		  System.out.println(tabs11);
		  driver.switchTo().window(tabs11.get(1));
		  Thread.sleep(2000);
		  driver.close();
		  driver.switchTo().window(tabs1.get(0));
		  Thread.sleep(2000);
		  driver.findElement(By.className("fa-linkedin")).click();
		  Thread.sleep(2000);
		  ArrayList<String> tabs12= new ArrayList<String> (driver.getWindowHandles());
		  System.out.println(tabs12);
		  driver.switchTo().window(tabs12.get(1));
		  Thread.sleep(2000);
		  driver.close();
		  driver.switchTo().window(tabs1.get(0));
		  
		 System.out.println("Test Case 4 - Completed footer_links");
		  
		   }
	 
	 @Test(description="Verify_ImportSavedCart",priority=4, enabled=false)
	 public void orders() throws InterruptedException, AWTException {
	 System.out.println("Test Case 5 - Started ImportSavedCart");
	 
	 driver.findElement(By.className("simple-banner")).click();
	 WebElement ele=driver.findElement(By.xpath("//*[@class='yCmsContentSlot componentContainer']"));
     Thread.sleep(2000);
     Actions actions=new Actions(driver);
     actions.moveToElement(ele).build().perform();
     Thread.sleep(2000);
     driver.findElement(By.linkText("IMPORT SAVED CART")).click();
    driver.findElement(By.id("j_username")).sendKeys("prakashnew6@gmail.com");
 	Thread.sleep(3000);
 	driver.findElement(By.id("j_password")).sendKeys("prakash@12345678");
 	Thread.sleep(2000);
 	 JavascriptExecutor js = (JavascriptExecutor) driver;
 	  js.executeScript("window.scrollBy(0,250)");
 	driver.findElement(By.xpath("//*[@class='btn btn-primary btn-block']")).click();
 	Thread.sleep(5000);
 	WebElement ele11=driver.findElement(By.xpath("//*[@class='yCmsContentSlot componentContainer']"));
    Thread.sleep(2000);
    Actions actions11=new Actions(driver);
    actions11.moveToElement(ele11).build().perform();
    Thread.sleep(2000);
    driver.findElement(By.linkText("IMPORT SAVED CART")).click();
	  Thread.sleep(5000);
	 WebElement uploadElement = driver.findElement(By.id("csvFile"));
      uploadElement.sendKeys("C:\\Users\\PrakashVelineni\\Downloads\\cart.csv");
      System.out.println("1");
      Thread.sleep(3000);
      driver.findElement(By.id("importButton")).click();
      System.out.println("2");
      Thread.sleep(3000);
     WebElement ele1 = driver.findElement(By.id("import-csv-alerts"));
     String csvresult= ele1.getText();
     System.out.println(csvresult);
     System.out.println("3");
     Thread.sleep(3000);
     driver.findElement(By.className("closeAccAlert")).click();
     System.out.println("Test Case 5 - Completed ImportSavedCart");
	 }
     
	 
	 @Test(description="Verify_QuickOrders",priority=5, enabled=false)
	 public void QuickOrders () throws InterruptedException, AWTException {
		 System.out.println("Test Case 6 - Started QuickOrders");
	WebElement ele2=driver.findElement(By.xpath("//*[@class='yCmsContentSlot componentContainer']"));
     Thread.sleep(2000);
     Actions actions1=new Actions(driver);
     actions1.moveToElement(ele2).build().perform();
     driver.findElement(By.linkText("QUICK ORDER")).click();
     driver.findElement(By.xpath("//li[2]//div[1]//input[1]")).sendKeys("prakash1234@#$%");
     driver.findElement(By.xpath("//li[3]//div[1]//input[1]")).sendKeys("prakash1234@#$%");
     driver.findElement(By.xpath("//li[4]//div[1]//input[1]")).sendKeys("300466682");
     Thread.sleep(2000);
	  Robot robot=new Robot();
	  robot.keyPress(KeyEvent.VK_ENTER);
	  Thread.sleep(3000);
      driver.findElement(By.xpath("//input[@class='js-quick-order-qty form-control']")).clear();
      Thread.sleep(3000);
      driver.findElement(By.xpath("//input[@class='js-quick-order-qty form-control']")).sendKeys("200");
      Thread.sleep(3000);
      driver.findElement(By.xpath("//li[5]//div[1]//input[1]")).sendKeys("");
      Thread.sleep(3000);
      driver.findElement(By.linkText("The Reservoir Beanie red Uni")).click();
      Thread.sleep(3000);
      driver.switchTo().alert().accept();
      Thread.sleep(2000);
  	 JavascriptExecutor js1 = (JavascriptExecutor) driver;
  	  js1.executeScript("window.scrollBy(0,750)");
  	  Thread.sleep(5000);
  	  driver.navigate().back();
  	  Thread.sleep(5000);
  	  driver.findElement(By.xpath("//li[4]//div[1]//input[1]")).sendKeys("");
  	  driver.findElement(By.xpath("//li[5]//div[1]//input[1]")).sendKeys("");
  	  driver.findElement(By.xpath("//li[6]//div[1]//input[1]")).sendKeys("");
  	  driver.findElement(By.xpath("//li[7]//div[1]//input[1]")).sendKeys("");
  	  driver.findElement(By.xpath("//li[8]//div[1]//input[1]")).sendKeys("");
  	  driver.findElement(By.xpath("//li[9]//div[1]//input[1]")).sendKeys("");
  	  driver.findElement(By.xpath("//li[10]//div[1]//input[1]")).sendKeys("");
  	  driver.findElement(By.xpath("//li[11]//div[1]//input[1]")).sendKeys("");
  	  driver.findElement(By.xpath("//li[12]//div[1]//input[1]")).sendKeys("");
  	  driver.findElement(By.xpath("//li[13]//div[1]//input[1]")).sendKeys("");
  	  driver.findElement(By.xpath("//li[14]//div[1]//input[1]")).sendKeys("");
  	  driver.findElement(By.xpath("//li[15]//div[1]//input[1]")).sendKeys("");
  	  driver.findElement(By.xpath("//li[16]//div[1]//input[1]")).sendKeys("");
  	  driver.findElement(By.xpath("//li[17]//div[1]//input[1]")).sendKeys("");
  	  driver.findElement(By.xpath("//li[18]//div[1]//input[1]")).sendKeys(""); 
  	  driver.findElement(By.xpath("//li[19]//div[1]//input[1]")).sendKeys("");
  	  driver.findElement(By.xpath("//li[20]//div[1]//input[1]")).sendKeys("");
  	  driver.findElement(By.xpath("//li[21]//div[1]//input[1]")).sendKeys("");
  	  driver.findElement(By.xpath("//li[22]//div[1]//input[1]")).sendKeys("");
  	  driver.findElement(By.xpath("//li[23]//div[1]//input[1]")).sendKeys("");
  	  driver.findElement(By.xpath("//li[24]//div[1]//input[1]")).sendKeys("");
  	  driver.findElement(By.xpath("//li[25]//div[1]//input[1]")).sendKeys("");
  	 
 
  	 driver.findElement(By.xpath("//li[25]//div[2]//button[1]//span[1]")).click();
  	 driver.findElement(By.xpath("//li[24]//div[2]//button[1]//span[1]")).click();
  	 driver.findElement(By.xpath("//li[23]//div[2]//button[1]//span[1]")).click();
  	 driver.findElement(By.xpath("//li[22]//div[2]//button[1]//span[1]")).click();
  	 driver.findElement(By.xpath("//li[21]//div[2]//button[1]//span[1]")).click();
  	 driver.findElement(By.xpath("//li[20]//div[2]//button[1]//span[1]")).click();
  	 driver.findElement(By.xpath("//li[19]//div[2]//button[1]//span[1]")).click();
  	 driver.findElement(By.xpath("//li[18]//div[2]//button[1]//span[1]")).click();
  	 driver.findElement(By.xpath("//li[17]//div[2]//button[1]//span[1]")).click();
  	 driver.findElement(By.xpath("//li[16]//div[2]//button[1]//span[1]")).click();
  	 driver.findElement(By.xpath("//li[15]//div[2]//button[1]//span[1]")).click();
  	 driver.findElement(By.xpath("//li[14]//div[2]//button[1]//span[1]")).click();
  	 driver.findElement(By.xpath("//li[13]//div[2]//button[1]//span[1]")).click();
  	 driver.findElement(By.xpath("//li[12]//div[2]//button[1]//span[1]")).click();
  	 driver.findElement(By.xpath("//li[11]//div[2]//button[1]//span[1]")).click();
  	 driver.findElement(By.xpath("//li[10]//div[2]//button[1]//span[1]")).click();
  	 driver.findElement(By.xpath("//li[9]//div[2]//button[1]//span[1]")).click();
  	 driver.findElement(By.xpath("//li[8]//div[2]//button[1]//span[1]")).click();
  	 driver.findElement(By.xpath("//li[7]//div[2]//button[1]//span[1]")).click();
  	 driver.findElement(By.xpath("//li[6]//div[2]//button[1]//span[1]")).click();
  	 System.out.println("1");
  	 driver.findElement(By.xpath("//li[5]//div[2]//button[1]//span[1]")).click();
  	 System.out.println("2");
  	 driver.findElement(By.id("js-reset-quick-order-form-btn-bottom")).click();
  	Thread.sleep(3000);
    driver.switchTo().alert().accept();
    driver.findElement(By.xpath("//li[4]//div[1]//input[1]")).sendKeys("300466682");
    driver.findElement(By.xpath("//li[5]//div[1]//input[1]")).sendKeys("300466682");
      Thread.sleep(3000);
    driver.findElement(By.id("js-add-to-cart-quick-order-btn-bottom")).click();
    Thread.sleep(3000);
    driver.findElement(By.linkText("CONTINUE SHOPPING")).click();
    
    System.out.println("Test Case 6 - Completed QuickOrders");
	 }

	 
	 @Test(description="Verify_CartIcon",priority=6, enabled=false)
	 public void CartIcon () throws InterruptedException, AWTException, IOException {
		 System.out.println("Test Case 7 - Started CartIcon");
		  Thread.sleep(2000);
		driver.findElement(By.linkText("ACCESSORIES")).click();
		 Thread.sleep(2000);
		// driver.switchTo().alert().accept();
		 Thread.sleep(2000);
		WebElement ele = driver.findElement(By.linkText("Incision Leather Belt shadow grey SM"));
		Actions actions=new Actions(driver);
	    actions.moveToElement(ele).build().perform();
	    Thread.sleep(2000);
	    driver.findElement(By.xpath("//form[@id='addToCartForm300608309']//button[@class='fas fa-cart-plus custom-cart js-enable-btn']")).click();
	    Thread.sleep(2000);
	    driver.findElement(By.linkText("CHECK OUT")).click();
	    Thread.sleep(2000);
		 driver.findElement(By.linkText("ACCESSORIES")).click();
		 Thread.sleep(2000);
		 WebElement ele1 = driver.findElement(By.linkText("Granda Belt white L"));
		 Actions actions1=new Actions(driver);
		  actions1.moveToElement(ele1).build().perform();
		  Thread.sleep(2000);
		  driver.findElement(By.xpath("//form[@id='addToCartForm300604648']//button[@class='fas fa-cart-plus custom-cart js-enable-btn']")).click();
		  Thread.sleep(2000);
		    driver.findElement(By.linkText("CHECK OUT")).click();
		    Thread.sleep(2000);
			 driver.findElement(By.linkText("ACCESSORIES")).click();
			 Thread.sleep(3000);
			 WebElement ele2 = driver.findElement(By.className("fa-cart-plus"));
			 Actions actions2=new Actions(driver);
			  actions2.moveToElement(ele2).build().perform();
			  Thread.sleep(3000);
			  driver.findElement(By.linkText("CHECK OUT")).click();
			  Thread.sleep(3000);
			  driver.findElement(By.linkText("NEW CART")).click();
			  Thread.sleep(3000);
			  JavascriptExecutor js1 = (JavascriptExecutor) driver;
		 	  js1.executeScript("window.scrollBy(0,500)");
		 	  Thread.sleep(2000);
			  driver.findElement(By.linkText("Incision Leather Belt shadow grey SM")).click();
			  JavascriptExecutor js = (JavascriptExecutor) driver;
		 	  js.executeScript("window.scrollBy(0,500)");
		 	 Thread.sleep(3000);
		 	 driver.navigate().back();
		 	 Thread.sleep(3000);
			  driver.findElement(By.linkText("Granda Belt white L")).click();
			  JavascriptExecutor js2 = (JavascriptExecutor) driver;
		 	  js2.executeScript("window.scrollBy(0,500)");
		 	 Thread.sleep(3000);
		 	 driver.navigate().back();
		 	 Thread.sleep(3000);
		 	 driver.findElement(By.id("quantity_0")).clear();
		 	 Thread.sleep(3000);
		 	 driver.findElement(By.id("quantity_0")).sendKeys("0");
		 	 Thread.sleep(3000);
		 	 JavascriptExecutor js3 = (JavascriptExecutor) driver;
		 	  js3.executeScript("window.scrollBy(0,500)");
		 	 Thread.sleep(3000);
		 	 driver.findElement(By.id("quantity_0")).sendKeys("2");
		 	 Robot robot = new Robot();
		 	 robot.keyPress(KeyEvent.VK_ENTER);
		 	Thread.sleep(3000);
		 	 driver.findElement(By.id("quantity_1")).sendKeys("0");
		 	JavascriptExecutor js4 = (JavascriptExecutor) driver;
		 	 js4.executeScript("window.scrollBy(0,500)");
		 	 driver.findElement(By.id("quantity_1")).sendKeys("3");
		 	 Robot robot1 = new Robot();
		 	 robot1.keyPress(KeyEvent.VK_ENTER);
		 	JavascriptExecutor js6 = (JavascriptExecutor) driver;
		 	js6.executeScript("window.scrollTo(document.body.scrollHeight, 0)");
		 	driver.findElement(By.xpath("//div[@class='cart__actions border']//button[@class='btn btn-primary btn-block btn--continue-checkout js-continue-checkout-button'][contains(text(),'Check Out')]")).click();
            driver.navigate().back();
		 	 Thread.sleep(3000);
		 	 JavascriptExecutor js7 = (JavascriptExecutor)driver;
		 	 js7.executeScript("window.scrollBy(0,500)");
		 	 driver.findElement(By.xpath("//button[@id='editEntry_1']//span[@class='glyphicon glyphicon-option-vertical']")).click();
		 	 Thread.sleep(3000);
		 	 driver.findElement(By.linkText("Remove")).click();
		 	 Thread.sleep(3000);
		 	 WebElement ele3= driver.findElement(By.className("global-alerts"));
		 	 System.out.println(ele3.getText());
		 	 Thread.sleep(3000);
		 	 driver.findElement(By.id("js-voucher-code-text")).sendKeys("prakash123@34");
		   	 Thread.sleep(3000);
		 	 driver.findElement(By.id("js-voucher-apply-btn")).click();
		 	 Thread.sleep(3000);
		 	 WebElement ele4 = driver.findElement(By.xpath("//div[@class='js-voucher-validation-container help-block cart-voucher__help-block']"));
		 	 System.out.println(ele4.getText());
		 	 Thread.sleep(3000);
		 	 driver.findElement(By.id("js-voucher-code-text")).clear();
		 	 Thread.sleep(3000);
		 	 driver.findElement(By.id("js-voucher-code-text")).sendKeys("12345678");
		   	 Thread.sleep(3000);
		 	 driver.findElement(By.id("js-voucher-apply-btn")).click();
		 	 Thread.sleep(3000);
		 	 WebElement ele5 = driver.findElement(By.xpath("//div[@class='js-voucher-validation-container help-block cart-voucher__help-block']"));
		 	 System.out.println(ele5.getText());
		 	 Thread.sleep(3000);
		 	 driver.findElement(By.linkText("EXPORT CSV")).click();
		 	 Thread.sleep(3000);
		 	 driver.findElement(By.xpath("//div[@class='cart__actions border']//button[@class='btn btn-default btn-block btn--continue-shopping js-continue-shopping-button'][contains(text(),'Continue Shopping')]")).click();
		 	System.out.println("Test Case 7 - Completed CartIcon");
		 	

	 }
	 
	 @Test(description="Verify_WishlistIcon",priority=7, enabled=false)
	 public void WishlistIcon () throws InterruptedException, AWTException, IOException {
		 System.out.println("Test Case 8 - Started WishlistIcon");
		 Thread.sleep(3000);
	    driver.findElement(By.xpath("//*[@class='wish-list-count js-mini-cart-count number']")).click();
		 Thread.sleep(3000);
		 driver.findElement(By.linkText("ACCESSORIES")).click();
		 Thread.sleep(3000);
		  WebElement ele = driver.findElement(By.linkText("The Rubber Re-Run sky blue"));
		  Actions actions=new Actions(driver);
		  actions.moveToElement(ele).build().perform();
		  Thread.sleep(2000);
		  driver.findElement(By.className("wishlist-btn")).click();
		  Thread.sleep(3000);
		  driver.findElement(By.linkText("ACCESSORIES")).click();
		  Thread.sleep(3000);
		  JavascriptExecutor js= (JavascriptExecutor)driver;
		  js.executeScript("window.scrollBy(0,500)");
		  Thread.sleep(3000);
		  driver.findElement(By.linkText("The Rubber Re-Run sky blue")).click();
		  Thread.sleep(3000);
		  JavascriptExecutor js1= (JavascriptExecutor)driver;
		  js1.executeScript("window.scrollBy(0,500)");
		  Thread.sleep(3000);
		  driver.findElement(By.className("wishlist-btn")).click();
		  Thread.sleep(3000);
		  driver.findElement(By.id("j_username")).sendKeys("prakashnew6@gmail.com");
			Thread.sleep(3000);
			driver.findElement(By.id("j_password")).sendKeys("prakash@12345678");
			Thread.sleep(2000);
			 JavascriptExecutor js2 = (JavascriptExecutor) driver;
			  js2.executeScript("window.scrollBy(0,250)");
			driver.findElement(By.xpath("//*[@class='btn btn-primary btn-block']")).click();
			Thread.sleep(5000);
			 driver.findElement(By.xpath("//*[@class='wish-list-count js-mini-cart-count number']")).click();
			 Thread.sleep(3000);
			 driver.findElement(By.linkText("ACCESSORIES")).click();
			 Thread.sleep(3000);
			  WebElement ele1 = driver.findElement(By.linkText("The Rubber Re-Run sky blue"));
			  Actions actions1=new Actions(driver);
			  actions1.moveToElement(ele1).build().perform();
			  Thread.sleep(2000);
			  driver.findElement(By.className("wishlist-btn")).click();
			  Thread.sleep(3000);
			  driver.findElement(By.linkText("ACCESSORIES")).click();
			  Thread.sleep(3000);
			  JavascriptExecutor js3= (JavascriptExecutor)driver;
			  js3.executeScript("window.scrollBy(0,500)");
			  Thread.sleep(3000);
			  driver.findElement(By.linkText("The Rubber Re-Run sky blue")).click();
			  Thread.sleep(3000);
			  JavascriptExecutor js4= (JavascriptExecutor)driver;
			  js4.executeScript("window.scrollBy(0,500)");
			  Thread.sleep(3000);
			  driver.findElement(By.xpath("//*[@class='wish-list-count js-mini-cart-count number']")).click();
			  Thread.sleep(3000);
			  JavascriptExecutor js6= (JavascriptExecutor)driver;
			  js6.executeScript("window.scrollBy(0,500)");
			  Thread.sleep(3000);
			  driver.findElement(By.linkText("The Rubber Re-Run sky blue")).click();
			  Thread.sleep(3000);
			  JavascriptExecutor js7= (JavascriptExecutor)driver;
			  js7.executeScript("window.scrollBy(0,500)");
			  driver.findElement(By.id("addToCartButton")).click();
			  Thread.sleep(5000);
			  driver.findElement(By.id("cboxClose")).click();
			  Thread.sleep(3000);
			  driver.findElement(By.className("wishlist-btn")).click();
			  Thread.sleep(3000);
			  driver.findElement(By.xpath("//*[@class='wish-list-count js-mini-cart-count number']")).click();
			  Thread.sleep(3000);
			  driver.findElement(By.linkText("ACCESSORIES")).click();
			  Thread.sleep(3000);
			  JavascriptExecutor js5= (JavascriptExecutor)driver;
			  js5.executeScript("window.scrollBy(0,500)");
			  Thread.sleep(3000);
			  WebElement ele2 = driver.findElement(By.linkText("The Rubber Re-Run sky blue"));
			  Actions actions2=new Actions(driver);
			  actions2.moveToElement(ele2).build().perform();
			  Thread.sleep(2000);
			  driver.findElement(By.className("wishlist-btn")).click();
			  Thread.sleep(3000);
			  driver.findElement(By.linkText("ACCESSORIES")).click();
			  Thread.sleep(3000);
			  JavascriptExecutor js8= (JavascriptExecutor)driver;
			  js8.executeScript("window.scrollBy(0,500)");
			  Thread.sleep(3000);
			  driver.findElement(By.linkText("The Rubber Re-Run sky blue")).click();
			  Thread.sleep(3000);
			  JavascriptExecutor js9= (JavascriptExecutor)driver;
			  js9.executeScript("window.scrollBy(0,500)");
			  Thread.sleep(2000);
			  driver.findElement(By.className("wishlist-btn")).click();
			  Thread.sleep(2000);
			  driver.findElement(By.linkText("ACCESSORIES")).click();
			  JavascriptExecutor js10= (JavascriptExecutor)driver;
			  js10.executeScript("window.scrollBy(0,500)");
			  Thread.sleep(3000);
			 
			  
	 }
	 
	 @Test(description="Verify_SearchBar",priority=7, enabled=true)
	 public void searchbar () throws InterruptedException, AWTException, IOException {
		 System.out.println("Test Case 8 - Started Searchbar");
		 Thread.sleep(2000);
	      driver.findElement(By.className("simple-banner")).click();
		  Thread.sleep(2000);
		  driver.navigate().refresh();
		  Thread.sleep(2000);
	      driver.findElement(By.id("js-site-search-input")).sendKeys("accessories");
	      Thread.sleep(2000);
		  Robot robot = new Robot();
		  robot.keyPress(KeyEvent.VK_ENTER);
		  Thread.sleep(4000);
		  driver.findElement(By.id("list-view")).click();
		  Thread.sleep(4000);
		  driver.findElement(By.id("grid-view")).click();
		  JavascriptExecutor js1= (JavascriptExecutor)driver;
		  js1.executeScript("window.scrollBy(0,750)");
		  Thread.sleep(3000);
		  driver.findElement(By.id("wish-list-link")).click();
		  Thread.sleep(3000);
		  driver.navigate().back();
		  Thread.sleep(4000);
		  driver.navigate().refresh();
		  Thread.sleep(3000);
		  JavascriptExecutor js2= (JavascriptExecutor)driver;
		  js2.executeScript("window.scrollBy(0,500)");
		  WebElement ele1 = driver.findElement(By.linkText("The Rubber Re-Run sky blue"));
		  Actions actions1=new Actions(driver);
		  actions1.moveToElement(ele1).build().perform();
		  Thread.sleep(2000);
           driver.findElement(By.xpath("/html/body/main/div[3]/div[1]/div[2]/section/div/div/div/div[3]/div/div/ul/li[2]/div/div[3]/div/div/button")).click();
          Thread.sleep(3000);
		  driver.navigate().back();
		  Thread.sleep(3000);
		  driver.findElement(By.linkText("The Rubber Re-Run sky blue")).click();
		  JavascriptExecutor js3= (JavascriptExecutor)driver;
		  js3.executeScript("window.scrollBy(0,500)");
		  Thread.sleep(3000);
		  driver.findElement(By.className("wishlist-btn")).click();
		  Thread.sleep(3000);
		  driver.findElement(By.id("j_username")).sendKeys("prakashnew6@gmail.com");
			Thread.sleep(3000);
		  driver.findElement(By.id("j_password")).sendKeys("prakash@12345678");
			Thread.sleep(2000);
			 JavascriptExecutor js11= (JavascriptExecutor) driver;
			  js11.executeScript("window.scrollBy(0,250)");
			driver.findElement(By.xpath("//*[@class='btn btn-primary btn-block']")).click();
			Thread.sleep(5000);
		  driver.findElement(By.id("js-site-search-input")).sendKeys("accessories");
	      Thread.sleep(2000);
		  Robot robot1 = new Robot();
		  robot1.keyPress(KeyEvent.VK_ENTER);
		  Thread.sleep(4000);
		  JavascriptExecutor js12= (JavascriptExecutor)driver;
		  js12.executeScript("window.scrollBy(0,500)");
		  WebElement ele2= driver.findElement(By.linkText("The Rubber Re-Run sky blue"));
		  Actions actions2=new Actions(driver);
		  actions2.moveToElement(ele2).build().perform();
		  Thread.sleep(2000);
          driver.findElement(By.xpath("/html/body/main/div[3]/div[1]/div[2]/section/div/div/div/div[3]/div/div/ul/li[2]/div/div[3]/div/div/button")).click();
          Thread.sleep(3000);
          driver.findElement(By.linkText("The Rubber Re-Run sky blue")).click();
			  Thread.sleep(3000);
			  JavascriptExecutor js13= (JavascriptExecutor)driver;
			  js13.executeScript("window.scrollBy(0,500)");
			  Thread.sleep(2000);
			  driver.findElement(By.className("wishlist-btn")).click();
			  Thread.sleep(2000);
			  driver.navigate().back();
			  Thread.sleep(2000);
			  JavascriptExecutor js14 = (JavascriptExecutor)driver;
			  js14.executeScript("window.scrollBy(0,500)");
			  Thread.sleep(3000);
			  WebElement ele4= driver.findElement(By.linkText("The Rubber Re-Run sky blue"));
			  Actions actions4=new Actions(driver);
			  actions4.moveToElement(ele4).build().perform();
		 
		 
		  
		  
		  
	 }
	 
	 
	  @AfterMethod
	  public void tearDown(ITestResult result) {
		  if(ITestResult.FAILURE==result.getStatus())
		  {
			  Utility_Screenshot.capturescreenshot(driver, result.getName());
	  }
	  }
	 
	 
  @AfterTest
  public void afterTest() {
  }

}
