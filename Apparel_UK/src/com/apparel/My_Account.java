package com.apparel;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;

public class My_Account {
	static WebDriver driver;
	
	 @BeforeTest
	  public void beforeTest() throws InterruptedException {
		  System.setProperty("webdriver.ie.driver","D:\\Prakash\\Selenium\\IEDriverServer_x64_3.14.0\\IEDriverServer.exe");
		  
		  driver = new InternetExplorerDriver();
		 // driver=new ChromeDriver();
		 driver.get("https://apparel-uk.hybrisdemo.sbpcorp.com/");
		 driver.manage().window().maximize();
		 Thread.sleep(5000);
		 driver.navigate().refresh();
		  Thread.sleep(5000);
		  JavascriptExecutor js1= (JavascriptExecutor) driver;
		  js1.executeScript("window.scrollBy(0,500)");
//		 JavascriptExecutor js = (javascriptexecutor)driver;
		  driver.findElement(By.xpath("//*[@id=\"sticky-header\"]/nav[1]/div/div[2]/div/ul/li/a")).click();
		  driver.findElement(By.id("j_username")).sendKeys("prakashnew6@gmail.com");
			Thread.sleep(3000);
			driver.findElement(By.id("j_password")).sendKeys("prakash@12345678");
			Thread.sleep(2000);
			 JavascriptExecutor js = (JavascriptExecutor) driver;
			  js.executeScript("window.scrollBy(0,250)");
			driver.findElement(By.xpath("//*[@class='btn btn-primary btn-block']")).click();
			Thread.sleep(5000);
	 }
//			@Test
//			@Parameters("browser")
//			public static void cross_browser(String browser) {
//				if(browser.equalsIgnoreCase("Chrome"))
//				{
//					 System.setProperty("webdriver.chrome.driver","D:\\Prakash\\Selenium\\chromedriver_win32\\chromedriver.exe");
//					  driver=new ChromeDriver();
//					
//				}
//				else if(browser.equalsIgnoreCase("firefox")) 
//				{
//					 System.setProperty("webdriver.chrome.driver", "D:\\Prakash\\Selenium\\geckodriver-v0.25.0-win64\\geckodriver.exe");
//					  driver=new ChromeDriver();
//					
//				}
//				else if(browser.equalsIgnoreCase("IE")) 
//				{
//					 System.setProperty("webdriver.chrome.driver", "D:\\Prakash\\Selenium\\geckodriver-v0.25.0-win64\\geckodriver.exe");
//					  driver=new ChromeDriver();
//					
//					
//				}
//			    }
//		   
//	 

  
  @Test(description="Verify_AllLinks_in_MyAccount",priority=0, enabled=false)
  public void all_links_in_MyAccount() throws InterruptedException {
	  System.out.println("Test Case 1 - Started  all_links_in_MyAccount");
	  Thread.sleep(3000);
     driver.findElement(By.className("yCmsComponent")).click();
	 Thread.sleep(3000);
	 driver.findElement(By.linkText("Password")).click();
	 Thread.sleep(3000);
	 driver.navigate().back();
	 Thread.sleep(3000);
	 driver.findElement(By.className("yCmsComponent")).click();
	 driver.findElement(By.linkText("Payment Details")).click();
	 Thread.sleep(3000);
	 driver.navigate().back();
	 Thread.sleep(3000);
	 driver.findElement(By.className("yCmsComponent")).click();
	 driver.findElement(By.linkText("Email Address")).click();
	 Thread.sleep(3000);
	 driver.navigate().back();
	 Thread.sleep(3000);
	 driver.findElement(By.className("yCmsComponent")).click();
	 Thread.sleep(2000);
	 driver.findElement(By.linkText("Personal Details")).click();
	 Thread.sleep(3000);
	 driver.navigate().back();
	 Thread.sleep(3000);
	 driver.findElement(By.className("yCmsComponent")).click();
	 Thread.sleep(2000);
	 driver.findElement(By.linkText("Order History")).click();
	 Thread.sleep(3000);
	 driver.navigate().back();
	 Thread.sleep(3000);
	 driver.findElement(By.className("yCmsComponent")).click();
	 Thread.sleep(2000);
	 driver.findElement(By.linkText("Address Book")).click();
	 Thread.sleep(3000);
	 driver.navigate().back();
	 Thread.sleep(3000);
	 driver.findElement(By.className("yCmsComponent")).click();
	 Thread.sleep(2000);
	 driver.findElement(By.linkText("Saved Carts")).click();
	 Thread.sleep(3000);
	 driver.navigate().back();
	 Thread.sleep(3000);
	 driver.findElement(By.className("yCmsComponent")).click();
	 Thread.sleep(2000);
	 driver.findElement(By.linkText("Consent Management")).click();
	 Thread.sleep(3000);
	 driver.navigate().back();
	 Thread.sleep(3000);
	 driver.findElement(By.className("yCmsComponent")).click();
	 Thread.sleep(2000);
	 driver.findElement(By.linkText("Close Account")).click();
	 Thread.sleep(3000);
	 driver.navigate().back();
	 Thread.sleep(3000);
	 driver.findElement(By.className("yCmsComponent")).click();
	 Thread.sleep(2000);
	 driver.findElement(By.linkText("Support Tickets")).click();
	 Thread.sleep(3000);
	 driver.navigate().back();
	 Thread.sleep(3000);
	 driver.findElement(By.className("yCmsComponent")).click();
	 Thread.sleep(2000);
	 driver.findElement(By.linkText("Returns History")).click();
	 Thread.sleep(3000);
	 driver.navigate().back();
	 Thread.sleep(3000);
	 System.out.println("Test Case 1 - Completed  all_links_in_MyAccount");
	  }
  
  @Test(description="Verify_Password",priority=1, enabled=false)
  public void password() throws InterruptedException {
	  System.out.println("Test Case 2 - Started  password");
	     Thread.sleep(3000);
	     driver.findElement(By.className("yCmsComponent")).click();
		 Thread.sleep(3000);
		 driver.findElement(By.linkText("Password")).click();
		 Thread.sleep(3000);
		 driver.findElement(By.id("currentPassword")).sendKeys("prakash@12345678");
		 Thread.sleep(3000);
		 driver.findElement(By.id("newPassword")).sendKeys("prakash@12345678");
		 Thread.sleep(3000);
		 driver.findElement(By.id("checkNewPassword")).sendKeys("prakash@12345678");
		 Thread.sleep(3000);
		 driver.findElement(By.xpath("//button[@class='btn btn-primary btn-block']")).click();
		 Thread.sleep(3000); 
		 String str = driver.findElement(By.className("global-alerts")).getText();
		 System.out.println(str);
		 System.out.println("Test Case 2 - Completed  password");
	  
  }
  
  @Test(description="Verify_EmailAddress",priority=2, enabled=false)
  public void EmailAddress() throws InterruptedException {
	  System.out.println("Test Case 3 - Started  EmailAddress");
	     Thread.sleep(3000);
	     driver.findElement(By.className("yCmsComponent")).click();
		 Thread.sleep(3000);
		 driver.findElement(By.linkText("Email Address")).click();
		 Thread.sleep(3000);
		 driver.navigate().refresh();
		 Thread.sleep(5000);
		 driver.findElement(By.id("profile.email")).sendKeys("prakashnew112@gmail.com");
		 Thread.sleep(3000);
		 driver.findElement(By.id("profile.checkEmail")).sendKeys("prakashnew112@gmail.com");
		 Thread.sleep(3000);
		 driver.findElement(By.id("profile.pwd")).sendKeys("prakash@12345678");
		 Thread.sleep(3000);
		 driver.findElement(By.xpath("//button[@class='btn btn-primary btn-block']")).click();
		 Thread.sleep(3000); 
		 String str = driver.findElement(By.className("global-alerts")).getText();
		 System.out.println(str);
		 
	  System.out.println("Test Case 3 - Completed  EmailAddress");
  }
  
  @Test(description="Verify_PersonalDetails",priority=3, enabled=false)
  public void PersonalDetails() throws InterruptedException {
	  System.out.println("Test Case 4 - Started  PersonalDetails");
	     Thread.sleep(3000);
		 driver.findElement(By.className("yCmsComponent")).click();
		 Thread.sleep(2000);
		 driver.findElement(By.linkText("Personal Details")).click();
		 Thread.sleep(3000);
		 Select obj=new Select(driver.findElement(By.id("profile.title")));
		 obj.selectByIndex(1);
		 obj.selectByIndex(2);
		 obj.selectByIndex(3);
		 obj.selectByIndex(4);
		 obj.selectByIndex(5);
		 obj.selectByIndex(6);
		 Thread.sleep(3000);
		 driver.findElement(By.id("profile.firstName")).sendKeys("prakash");
		 Thread.sleep(3000);
		 driver.findElement(By.id("profile.lastName")).sendKeys("velineni");
		 Thread.sleep(3000);
		 driver.findElement(By.xpath("//button[@class='btn btn-primary btn-block']")).click();
		 Thread.sleep(3000);
		  String profile = driver.findElement(By.className("global-alerts")).getText();
		  System.out.println(profile);
		  System.out.println("Test Case 4 - Completed  EmailAddress");  
  }
  
  @Test(description="Verify_OrderHistory",priority=4, enabled=false)
  public void orderhistory() throws InterruptedException {
	  System.out.println("Test Case 5 - Started  orderhistory");
	     Thread.sleep(3000);
		 driver.findElement(By.className("yCmsComponent")).click();
		 Thread.sleep(2000);
		 driver.findElement(By.linkText("Order History")).click();
		 Thread.sleep(3000);
		 driver.findElement(By.xpath("/html/body/main/div[3]/div/div/div[2]/div/div/table/tbody/tr[2]/td[2]/a")).click();
		 Thread.sleep(3000);
		 JavascriptExecutor js1= (JavascriptExecutor) driver;
		  js1.executeScript("window.scrollBy(0,500)");
		  Thread.sleep(3000);
		  driver.findElement(By.linkText("The Life Beanie black Uni")).click();
		  Thread.sleep(3000);
		  JavascriptExecutor js2= (JavascriptExecutor) driver;
		  js2.executeScript("window.scrollBy(0,750)");
		  Thread.sleep(3000);
		  driver.navigate().back();
		  Thread.sleep(3000);
		  driver.navigate().back();
		  Thread.sleep(3000);
		  driver.findElement(By.xpath("/html/body/main/div[3]/div/div/div[2]/div/div/table/tbody/tr[3]/td[2]/a")).click();
		  Thread.sleep(3000);
			 JavascriptExecutor js3= (JavascriptExecutor) driver;
			  js3.executeScript("window.scrollBy(0,500)");
			  Thread.sleep(3000);
			  driver.findElement(By.linkText("The Rubber Re-Run sky blue")).click();
			  Thread.sleep(3000);
			  JavascriptExecutor js4= (JavascriptExecutor) driver;
			  js4.executeScript("window.scrollBy(0,750)");
			  Thread.sleep(3000);
			  driver.navigate().back();
			  Thread.sleep(3000);
			  driver.navigate().back();
			  Thread.sleep(3000);
			  System.out.println("Test Case 5 - Completed  orderhistory");
  }
		 
    @Test(description="Verify_SavedCarts",priority=5, enabled=false)
    public void savedcarts() throws InterruptedException {
	System.out.println("Test Case 6 - Started  savedcarts");
	 Thread.sleep(3000);
	 driver.findElement(By.className("yCmsComponent")).click();
	 Thread.sleep(2000);
	 driver.findElement(By.linkText("Saved Carts")).click();
	 Thread.sleep(3000);
	String savedcart = driver.findElement(By.xpath("//div[@class='account-section-content content-empty']")).getText();
	System.out.println(savedcart);
	 Thread.sleep(3000);	 
	 System.out.println("Test Case 6 - Completed  savedcarts"); 			 
		 
		
	  
  }
    
    @Test(description="Verify_ConsentManagement",priority=6, enabled=false)
    public void consent_management() throws InterruptedException {
	System.out.println("Test Case 7 - Started   consent_management");
	 Thread.sleep(3000);
	 driver.findElement(By.className("yCmsComponent")).click();
	 Thread.sleep(2000);
	 driver.findElement(By.linkText("Consent Management")).click();
	 Thread.sleep(5000);
	 driver.findElement(By.xpath("//div[@class='toggle-button__switch is-checked']")).click();
	 Thread.sleep(5000);
	 String consent=  driver.findElement(By.className("global-alerts")).getText();
 	 System.out.println(consent);
	 Thread.sleep(3000);
	 driver.findElement(By.className("closeAccAlert")).click();
	 Thread.sleep(4000);
	 driver.findElement(By.xpath("//div[@class='toggle-button__switch']")).click();
	 Thread.sleep(3000);
	 String consent1=  driver.findElement(By.className("global-alerts")).getText();
 	 System.out.println(consent1);
	 Thread.sleep(3000);
	 driver.findElement(By.className("closeAccAlert")).click();
	 Thread.sleep(3000);
	 driver.findElement(By.xpath("//label[@class='consent-management-list__label']")).click();
	 Thread.sleep(3000);
	 driver.findElement(By.xpath("//label[@class='consent-management-list__label']")).click();
	 Thread.sleep(3000);
	 System.out.println("Test Case 7 - Completed   consent_management");
    }
    
    @Test(description="Verify_CloseAccount",priority=7, enabled=false)
    public void close_account() throws InterruptedException {
	System.out.println("Test Case 8 - Started   close_account");
	 Thread.sleep(3000);
	 driver.findElement(By.className("yCmsComponent")).click();
	 Thread.sleep(2000);
	 driver.findElement(By.linkText("Close Account")).click();
	 Thread.sleep(3000);
	 JavascriptExecutor js1= (JavascriptExecutor) driver;
	  js1.executeScript("window.scrollBy(0,750)");
	  Thread.sleep(3000);
	  driver.findElement(By.xpath("//button[@class='btn btn-primary pull-right js-close-account-popup-button']")).click();
	  Thread.sleep(3000);
	  driver.findElement(By.linkText("CANCEL")).click();
	  Thread.sleep(3000);
	  System.out.println("Test Case 8 - Completed   close_account");
    }
    
    @Test(description="Verify_SupportTickets",priority=8, enabled=true)
    public void support_tickets() throws InterruptedException {
	System.out.println("Test Case 9 - Started   support_tickets");
	 Thread.sleep(3000);
	 driver.findElement(By.className("yCmsComponent")).click();
	 Thread.sleep(2000);
	 driver.findElement(By.linkText("Support Tickets")).click();
	 Thread.sleep(3000);
	 driver.findElement(By.id("add-support-ticket-btn")).click();
	 Thread.sleep(3000);
	 driver.findElement(By.id("createTicket-subject")).sendKeys("accessories");
	 Thread.sleep(3000);
	 driver.findElement(By.id("createTicket-message")).sendKeys("accessories");
	 Thread.sleep(3000);
	 WebElement upload =driver.findElement(By.id("attachmentFiles"));
	 upload.sendKeys("D:\\SBP Consulting Inc\\Hybris - C 4HANA - SAP Commerce\\03. QA\\ELiteKart-Screenshots\\Apparel\\D_01.PNG");
	 Thread.sleep(3000);
	 Select obj=new Select(driver.findElement(By.id("associatedTo")));
	 obj.selectByIndex(1);
	 Thread.sleep(2000);
	 obj.selectByIndex(2);
	 Thread.sleep(2000);
	 obj.selectByIndex(3);
	 Thread.sleep(2000);
	 obj.selectByIndex(4);
	 Thread.sleep(3000);
	 Select obj1=new Select(driver.findElement(By.id("ticketCategory")));
	 obj1.selectByIndex(1);
	 Thread.sleep(2000);
	 obj1.selectByIndex(2);
	 Thread.sleep(3000);
	 driver.findElement(By.id("addTicket")).click();
	 Thread.sleep(3000);
	 String ticket=driver.findElement(By.id("customer-ticketing-alerts")).getText();
	 System.out.println(ticket);
	 Thread.sleep(3000);
	 System.out.println("Test Case 9 - Completed   support_tickets");
    }
    
    
    @Test(description="Verify_ReturnsHistory",priority=9, enabled=true)
    public void returns_history() throws InterruptedException {
	System.out.println("Test Case 10 - Started   returns_history");
	 Thread.sleep(3000);
	 driver.findElement(By.className("yCmsComponent")).click();
	 Thread.sleep(2000);
	 driver.findElement(By.linkText("Returns History")).click();
	 Thread.sleep(3000);
	String returns = driver.findElement(By.xpath("//div[@class='account-section-content content-empty']")).getText();
	System.out.println(returns);
	System.out.println("Test Case 10 - Completed   returns_history");
	
    }

  @AfterTest
  public void afterTest() {
  }
}


