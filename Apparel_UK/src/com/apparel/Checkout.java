package com.apparel;

//import org.testng.annotations.Test;
//import org.testng.annotations.BeforeTest;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
//import org.testng.annotations.AfterTest;

public class Checkout {
	WebDriver driver;
 
  @Before
  public void beforeTest() {
	  System.setProperty("webdriver.chrome.driver","D:\\Prakash\\Selenium\\chromedriver_win32\\chromedriver.exe");
	  driver=new ChromeDriver();
	  driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
	  driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
	 
	  }
 /*@Test//(description="Verify_registration",priority=0, enabled=true)
  public void registration() throws InterruptedException {
	 System.out.println("Test Case 1 - Started  registration ");
	 driver.get("https://dev-apparel-uk.hybrisdemo.sbpcorp.com/");
	 driver.manage().window().maximize();
	 Thread.sleep(5000);
	 driver.navigate().refresh();
	  Thread.sleep(5000);
	  driver.findElement(By.xpath("//*[@id=\"sticky-header\"]/nav[1]/div/div[2]/div/ul/li/a")).click();
	  Thread.sleep(3000);
	Select obj= new Select(driver.findElement(By.id("register.title")));
	obj.selectByIndex(1);
	obj.selectByIndex(2);
	obj.selectByIndex(3);
	obj.selectByIndex(4);
	
	obj.selectByIndex(5);
	obj.selectByIndex(6);
	Thread.sleep(3000);
	driver.findElement(By.id("register.firstName")).sendKeys("prakash");
	Thread.sleep(3000);
	driver.findElement(By.id("register.lastName")).sendKeys("velineni");
	Thread.sleep(3000);
	driver.findElement(By.id("register.email")).sendKeys("prakashnews@gmail.com");
	Thread.sleep(3000);
	driver.findElement(By.id("password")).sendKeys("prakash@12345678");
	Thread.sleep(3000);
	driver.findElement(By.id("register.checkPwd")).sendKeys("prakash@12345678");
	Thread.sleep(3000);
	driver.findElement(By.id("consentForm.consentGiven1")).click();
	Thread.sleep(3000);
	driver.findElement(By.id("registerChkTermsConditions")).click();
	Thread.sleep(3000);
	driver.findElement(By.xpath("//*[@class='btn btn-default btn-block']")).click();
	Thread.sleep(5000);
	 System.out.println("Test Case 1 - Completed  registration ");
 }
  @Test//(description="Verify_login",priority=1, enabled=true)
  public void login() throws InterruptedException {
	  System.out.println("Test Case 2 - Started  login ");
	  driver.get("https://dev-apparel-uk.hybrisdemo.sbpcorp.com/");
	  driver.manage().window().maximize();
	  driver.findElement(By.xpath("//*[@id=\"sticky-header\"]/nav[1]/div/div[2]/div/ul/li/a")).click();
	  driver.findElement(By.id("j_username")).sendKeys("prakashnew6@gmail.com");
	  driver.findElement(By.id("j_password")).sendKeys("prakash@12345678");
	  JavascriptExecutor js = (JavascriptExecutor) driver;
	  js.executeScript("window.scrollBy(0,250)");
	  driver.findElement(By.xpath("//*[@class='btn btn-primary btn-block']")).click();
	 System.out.println("Test Case 2 - Completed  login ");
   } */
  @Test//(description="Verify_CheckoutProcess",priority=2, enabled=true)
  public void checkoutprocess() throws InterruptedException {
	  System.out.println("Test Case 3 - Started  checkoutprocess");
	  driver.get("https://apparel-uk.hybrisdemo.sbpcorp.com/");
	  driver.manage().window().maximize();
	  driver.findElement(By.xpath("//*[@id=\"sticky-header\"]/nav[1]/div/div[2]/div/ul/li/a")).click();
	  driver.findElement(By.id("j_username")).sendKeys("prakashdefault@gmail.com");
	  driver.findElement(By.id("j_password")).sendKeys("prakash@12345678");
	  JavascriptExecutor js = (JavascriptExecutor) driver;
	  js.executeScript("window.scrollBy(0,250)");
	  driver.findElement(By.xpath("//*[@class='btn btn-primary btn-block']")).click();
	  driver.findElement(By.linkText("ACCESSORIES")).click();
	  JavascriptExecutor js11 = (JavascriptExecutor) driver;
	  js11.executeScript("window.scrollBy(0,750)");
	  driver.findElement(By.xpath("/html[1]/body[1]/main[1]/div[3]/div[1]/div[2]/section[1]/div[1]/div[1]/div[2]/div[1]/div[1]/ul[1]/li[1]/div[1]/div[2]/a[1]/h4[1]/span[1]")).click();
	  JavascriptExecutor js1 = (JavascriptExecutor) driver;
	  js1.executeScript("window.scrollBy(0,500)");
	  driver.findElement(By.id("addToCartButton")).click();
      JavascriptExecutor js2 = (JavascriptExecutor) driver;
	  js2.executeScript("window.scrollBy(0,500)");
	  Set<String>handles = driver.getWindowHandles();
	  System.out.println(handles);
	  Thread.sleep(2000);
//      WebDriverWait wait = new WebDriverWait(driver, 40);
//	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[5]/div[1]/div[2]/div[2]/div[1]/div/a[1]")));
	  driver.findElement(By.xpath("/html/body/div[5]/div[1]/div[2]/div[2]/div[1]/div/a[1]")).click();
	   driver.findElement(By.xpath("//button[@class= 'btn btn-primary btn-block btn--continue-checkout js-continue-checkout-button']")).click();
	  Select obj= new Select(driver.findElement(By.id("address.country")));
		obj.selectByIndex(1);
		obj.selectByIndex(2);
		obj.selectByIndex(3);
		obj.selectByIndex(4);
		Thread.sleep(2000);
	  Select obj1= new Select(driver.findElement(By.id("address.title")));
		obj1.selectByIndex(1);
		obj1.selectByIndex(2);
		obj1.selectByIndex(3);
		obj1.selectByIndex(4);
		obj1.selectByIndex(5);
		obj1.selectByIndex(6);
	  driver.findElement(By.id("address.firstName")).sendKeys("prakash");
	  
	  driver.findElement(By.id("address.surname")).sendKeys("velineni");
	  
	  driver.findElement(By.id("address.line1")).sendKeys("Guntur");
	  
	  driver.findElement(By.id("address.line2")).sendKeys("Mutluru");
	 
	  driver.findElement(By.id("address.townCity")).sendKeys("andhrapradesh");
	 
	  driver.findElement(By.id("address.postcode")).sendKeys("522212");
	  
	  driver.findElement(By.id("address.phone")).sendKeys("9999912345");
	  
	  driver.findElement(By.id("saveAddressInMyAddressBook")).click();
	  
	  driver.findElement(By.id("addressSubmit")).click();
	 
	  Select obj2= new Select(driver.findElement(By.id("delivery_method")));
		obj2.selectByIndex(0);
		obj2.selectByIndex(1);
	  driver.findElement(By.id("deliveryMethodSubmit")).click();
	  Select obj3= new Select(driver.findElement(By.id("card_cardType")));
			obj3.selectByIndex(0);
			obj3.selectByIndex(1);
			obj3.selectByIndex(2);
			obj3.selectByIndex(3);
			obj3.selectByIndex(4);
	  driver.findElement(By.id("card_nameOnCard")).sendKeys("prakashvelineni");
	  driver.findElement(By.id("card_accountNumber")).sendKeys("1234567890123456");
	  Select obj4= new Select(driver.findElement(By.id("ExpiryMonth")));
		obj4.selectByIndex(0);
		obj4.selectByIndex(1);
		obj4.selectByIndex(2);
		obj4.selectByIndex(3);
		obj4.selectByIndex(4);
		obj4.selectByIndex(5);
		obj4.selectByIndex(6);
		obj4.selectByIndex(7);
		obj4.selectByIndex(8);
		obj4.selectByIndex(9);
		obj4.selectByIndex(10);
		obj4.selectByIndex(11);
		Select obj5= new Select(driver.findElement(By.id("ExpiryYear")));
		obj5.selectByIndex(0);
		obj5.selectByIndex(1);
		obj5.selectByIndex(2);
		obj5.selectByIndex(3);
		obj5.selectByIndex(4);
		obj5.selectByIndex(5);
		obj5.selectByIndex(6);
		obj5.selectByIndex(7);
		obj5.selectByIndex(8);
		obj5.selectByIndex(9);
		obj5.selectByIndex(10);
		driver.findElement(By.id("card_cvNumber")).sendKeys("123");
		driver.findElement(By.id("savePaymentMethod")).click();
		driver.findElement(By.xpath("//button[@class='btn btn-primary btn-block submit_silentOrderPostForm checkout-next']")).click();
		JavascriptExecutor js10 = (JavascriptExecutor) driver;
   	    js10.executeScript("window.scrollBy(0,250)");
		driver.findElement(By.id("Terms1")).click();
		driver.findElement(By.id("placeOrder")).click();
		WebElement order=driver.findElement(By.className("checkout-success__body__headline"));
		 String message = order.getText();
		 System.out.println(message);
		/* driver.findElement(By.className("yCmsComponent")).click();
		 driver.findElement(By.linkText("Payment Details")).click();
		 driver.findElement(By.xpath("//*[@class='action-links removePaymentDetailsButton']")).click();
		 JavascriptExecutor js12 = (JavascriptExecutor) driver;
     	  js12.executeScript("window.scrollBy(0,250)");
		WebElement ele = driver.findElement(By.xpath("//*[starts-with(@id,'removePaymentDetails')]//button[@class='btn btn-default btn-primary btn-block paymentsDeleteBtn'][contains(text(),'Delete')]"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", ele);
//		Thread.sleep(5000);
//		driver.findElement(By.xpath("//*[starts-with(@id,'removePaymentDetails')]//button[@class='btn btn-default btn-primary btn-block paymentsDeleteBtn'][contains(text(),'Delete')]")).click();
//		 WebDriverWait wait = new WebDriverWait(driver, 40);
//		 wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//button[contains(text(),'Delete') and @type='submit']"))));
//		driver.findElement(By.xpath("//button[contains(text(),'Delete' and @type='submit']")).click();
//		WebDriverWait wait1 = new WebDriverWait(driver, 30);
//		wait1.until(ExpectedConditions.elementToBeClickable(By.className("btn-primary")));
//    	Thread.sleep(5000);
//      	driver.findElement(By.className("btn-primary")).click();
	    	driver.findElement(By.linkText("DELETE")).click();
	      Thread.sleep(3000);
		  WebElement PaymentCard=driver.findElement(By.xpath("//*[@class='alert alert-info alert-dismissable getAccAlert']"));
		 String Removed =  PaymentCard.getText();
		 System.out.println(Removed);
		 Thread.sleep(3000);
        driver.findElement(By.xpath("//*[@class='close closeAccAlert']")).click();	
        driver.findElement(By.className("yCmsComponent")).click();
        Thread.sleep(3000);
        driver.findElement(By.linkText("Address Book")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("//*[@class='glyphicon glyphicon-pencil']")).click();	
        Thread.sleep(3000);
    	driver.findElement(By.className("btn-default")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("//*[@class='glyphicon glyphicon-pencil']")).click();
        Thread.sleep(3000);
       // driver.findElement(By.xpath("//*[@class='btn btn-primary btn-block change_address_button show_processing_message']")).click();
        Thread.sleep(3000);
        driver.findElement(By.linkText("Address Book")).click();
	    driver.findElement(By.xpath("//*[@class='action-links removeAddressFromBookButton']")).click();
	    WebDriverWait wait2 = new WebDriverWait(driver, 30);
    	wait2.until(ExpectedConditions.elementToBeClickable(By.className("btn-primary")));
		Thread.sleep(3000);
//		List<WebElement>  listele = driver.findElements(By.className("btn-primary"));
//		  int count =listele.size();
//		  System.out.println(count);
//	    Set<String> windows=driver.getWindowHandles();
//	    System.out.println(windows);
//		  Thread.sleep(4000);
//	   driver.findElement(By.className("btn-primary")).click();
	    Thread.sleep(5000);
	    driver.findElement(By.linkText("DELETE")).click();
    
	
		//driver.findElement(By.className("btn-primary")).click();
		 Thread.sleep(3000);
		  WebElement Address=driver.findElement(By.xpath("//*[@class='alert alert-info alert-dismissable getAccAlert']"));
		 String Removed1 = Address.getText();
		 System.out.println(Removed1);
		 Thread.sleep(3000);
       driver.findElement(By.xpath("//*[@class='close closeAccAlert']")).click();
       Thread.sleep(3000);*/
       
		  System.out.println("Test Case 3 - Completed  checkoutprocess ");
		 }
  
 
 
	  
	  
	 
	  
	  
 

      
      
	 
	  
	 
	  
	  
	  
	 
	  
	  
	  
	  
	  
	  
      
      
  
  
  
   @After
  public void afterTest() {
  }

}
