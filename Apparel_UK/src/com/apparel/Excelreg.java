package com.apparel;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import com.utility.excel.Xls_Reader1;

public class Excelreg {

	public static void main(String[] args) throws InterruptedException {
		//Webdriver code
		 System.setProperty("webdriver.chrome.driver","D:\\Prakash\\Selenium\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://qa-omnibus.hybrisdemo.sbpcorp.com/");
		driver.manage().window().maximize();
		
		//get test data from excel
		
		Xls_Reader1 reader= new Xls_Reader1("D:\\Prakash\\Prakash_Projects\\Apparel_UK\\src\\com\\testdata\\Regexcel.xlsx");  
		int rowCount = reader.getRowCount("Registration");
		
		reader.addColumn("Registration", "status");
		
		for(int rowNum=2;rowNum<=rowCount;rowNum++) {
			
		Thread.sleep(5000);
	    driver.findElement(By.xpath("//*[@id=\"sticky-header\"]/nav[1]/div/div[2]/div/ul/li/a")).click();	
		String title= reader.getCellData("Registration", "title", rowNum);
		System.out.println(title);
		String firstname = reader.getCellData("Registration", "firstname", rowNum);
		System.out.println(firstname);
		String lastname = reader.getCellData("Registration", "lastname", rowNum);
		System.out.println(lastname);
		String email = reader.getCellData("Registration", "email", rowNum);
		System.out.println(email);
		String password = reader.getCellData("Registration", "password", rowNum);
		System.out.println(password);
		String confirmpassword = reader.getCellData("Registration", "confirmpassword", rowNum);
		System.out.println(confirmpassword);
		Select obj= new Select(driver.findElement(By.id("register.title")));
		Thread.sleep(3000);
		List<WebElement> options = obj.getOptions();
		obj.selectByIndex(1);
		Thread.sleep(3000);
		obj.selectByIndex(2);
		Thread.sleep(3000);
		obj.selectByIndex(3);
		Thread.sleep(3000);
		obj.selectByIndex(4);
		Thread.sleep(3000);
		obj.selectByIndex(5);
		Thread.sleep(3000);
//		List<WebElement> options = driver.findElements(By.id("register.title"));
//
//		for(WebElement sample : options)
//
//		{
//
//		if(sample.getText().equals("text in your application"))
//
//		{
//
//		sample.click();
//
//		break;
//
//		}

	//	}
		
		
		
//		
//		Select dropdown = new Select(driver.findElement(By.id("register.title")));
//
//	    //Get all options
//	    List<WebElement> dd = dropdown.getOptions();
//
//	    //Get the length
//	    System.out.println(dd.size());
//
//	    // Loop to print one by one
//	    for (int j = 0; j < dd.size(); j++) {
//	        String select =(dd.get(j).getText());
//             select.click();
//	    }
		
		// enter data
		
		driver.findElement(By.id("register.firstName")).clear();
		driver.findElement(By.id("register.firstName")).sendKeys(firstname);
		Thread.sleep(3000);
		driver.findElement(By.id("register.lastName")).clear();
		driver.findElement(By.id("register.lastName")).sendKeys(lastname);
		Thread.sleep(3000);
		driver.findElement(By.id("register.email")).clear();
		driver.findElement(By.id("register.email")).sendKeys(email);
		Thread.sleep(3000);
		driver.findElement(By.id("password")).clear();
		driver.findElement(By.id("password")).sendKeys(password);
		Thread.sleep(3000);
		driver.findElement(By.id("register.checkPwd")).clear();
		driver.findElement(By.id("register.checkPwd")).sendKeys(confirmpassword );
		Thread.sleep(3000);
		driver.findElement(By.id("consentForm.consentGiven1")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("registerChkTermsConditions")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@class='btn btn-default btn-block']")).click();
		Thread.sleep(5000);
		
		if((driver.findElement(By.className("liOffcanvas"))).isDisplayed()) {
			driver.findElement(By.className("liOffcanvas")).click();
			Thread.sleep(5000);
			reader.setCellData("Registration", "status", rowNum,"Pass");
		}
		else
		{
			reader.setCellData("Registration", "status", rowNum,"Fail");
			
		}
	       System.out.println("Test Case 2 - Started  login ");
		    driver.findElement(By.id("j_username")).clear();
			driver.findElement(By.id("j_username")).sendKeys(email);
			Thread.sleep(3000);
			driver.findElement(By.id("j_password")).clear();
			driver.findElement(By.id("j_password")).sendKeys(password);
			Thread.sleep(2000);
			 JavascriptExecutor js = (JavascriptExecutor) driver;
			  js.executeScript("window.scrollBy(0,250)");
			 driver.findElement(By.xpath("//*[@class='btn btn-primary btn-block']")).click();
			Thread.sleep(5000);
		

			if((driver.findElement(By.className("liOffcanvas"))).isDisplayed()) {
				driver.findElement(By.className("liOffcanvas")).click();
				Thread.sleep(5000);
				reader.setCellData("Registration", "status", rowNum,"Pass");
			}
			else
			{
		reader.setCellData("Registration", "status", rowNum,"Fail");
		
		}
		
		 System.out.println("Test Case 2 - Completed  login ");	
		}
	}
}
