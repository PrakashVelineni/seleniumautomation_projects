$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("D:/Prakash/Prakash_Maven/Apparel-uk-maven/src/main/java/Features/Checkout.feature");
formatter.feature({
  "line": 1,
  "name": "eLitekart Apparel-Uk Functional Testing Feature",
  "description": "",
  "id": "elitekart-apparel-uk-functional-testing-feature",
  "keyword": "Feature"
});
formatter.before({
  "duration": 18537835200,
  "status": "passed"
});
formatter.scenario({
  "line": 3,
  "name": "eLitekart Apparel-Uk Registration Scenario",
  "description": "",
  "id": "elitekart-apparel-uk-functional-testing-feature;elitekart-apparel-uk-registration-scenario",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "user is already on home page1",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "user click on signin",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "user select title",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "user enters first name and last name",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "user enters email address and password",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "user enters confirm password",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "user select consent checkbox",
  "keyword": "Then "
});
formatter.step({
  "line": 12,
  "name": "user select terms and condition checkbox",
  "keyword": "Then "
});
formatter.step({
  "line": 13,
  "name": "click on Register",
  "keyword": "Then "
});
formatter.step({
  "line": 14,
  "name": "click on my account",
  "keyword": "Then "
});
formatter.step({
  "line": 15,
  "name": "click on signout",
  "keyword": "And "
});
formatter.match({
  "location": "CheckoutStepDefinition.user_is_already_on_home_page1()"
});
formatter.result({
  "duration": 75725200,
  "status": "passed"
});
formatter.match({
  "location": "CheckoutStepDefinition.user_click_on_signin()"
});
formatter.result({
  "duration": 1983808300,
  "status": "passed"
});
formatter.match({
  "location": "CheckoutStepDefinition.user_select_title()"
});
formatter.result({
  "duration": 528158500,
  "status": "passed"
});
formatter.match({
  "location": "CheckoutStepDefinition.user_enters_first_name_and_last_name()"
});
formatter.result({
  "duration": 172254700,
  "status": "passed"
});
formatter.match({
  "location": "CheckoutStepDefinition.user_enters_email_address_and_password()"
});
formatter.result({
  "duration": 266118200,
  "status": "passed"
});
formatter.match({
  "location": "CheckoutStepDefinition.user_enters_confirm_password()"
});
formatter.result({
  "duration": 96055200,
  "status": "passed"
});
formatter.match({
  "location": "CheckoutStepDefinition.user_select_consent_checkbox()"
});
formatter.result({
  "duration": 62713800,
  "status": "passed"
});
formatter.match({
  "location": "CheckoutStepDefinition.user_select_terms_and_condition_checkbox()"
});
formatter.result({
  "duration": 65558700,
  "status": "passed"
});
formatter.match({
  "location": "CheckoutStepDefinition.click_on_Register()"
});
formatter.result({
  "duration": 3703441800,
  "status": "passed"
});
formatter.match({
  "location": "CheckoutStepDefinition.click_on_my_account()"
});
formatter.result({
  "duration": 70217600,
  "status": "passed"
});
formatter.match({
  "location": "CheckoutStepDefinition.click_on_signout()"
});
formatter.result({
  "duration": 2256428100,
  "status": "passed"
});
formatter.after({
  "duration": 625876000,
  "status": "passed"
});
formatter.before({
  "duration": 8617973800,
  "status": "passed"
});
formatter.scenario({
  "line": 18,
  "name": "eLitekart Apparel-Uk Login Scenario",
  "description": "",
  "id": "elitekart-apparel-uk-functional-testing-feature;elitekart-apparel-uk-login-scenario",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 20,
  "name": "user enters username and password",
  "keyword": "When "
});
formatter.step({
  "line": 21,
  "name": "user clicks on login",
  "keyword": "Then "
});
formatter.step({
  "line": 22,
  "name": "verify my account",
  "keyword": "Then "
});
formatter.match({
  "location": "CheckoutStepDefinition.user_enters_username_and_password()"
});
formatter.result({
  "duration": 1805521700,
  "status": "passed"
});
formatter.match({
  "location": "CheckoutStepDefinition.user_clicks_on_login()"
});
formatter.result({
  "duration": 2466384500,
  "status": "passed"
});
formatter.match({
  "location": "CheckoutStepDefinition.verify_my_account()"
});
formatter.result({
  "duration": 48173700,
  "status": "passed"
});
formatter.after({
  "duration": 617478200,
  "status": "passed"
});
formatter.before({
  "duration": 8367199300,
  "status": "passed"
});
formatter.scenario({
  "line": 25,
  "name": "eLitekart Apparel-Uk Checkout Scenario",
  "description": "",
  "id": "elitekart-apparel-uk-functional-testing-feature;elitekart-apparel-uk-checkout-scenario",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 27,
  "name": "user click on accessories",
  "keyword": "When "
});
formatter.step({
  "line": 28,
  "name": "scrolls down",
  "keyword": "Then "
});
formatter.step({
  "line": 29,
  "name": "click on product",
  "keyword": "Then "
});
formatter.step({
  "line": 30,
  "name": "again scrolls down",
  "keyword": "Then "
});
formatter.step({
  "line": 31,
  "name": "click on add to bag",
  "keyword": "Then "
});
formatter.step({
  "line": 32,
  "name": "click on checkout on popup",
  "keyword": "Then "
});
formatter.step({
  "line": 33,
  "name": "click on checkout in cart page",
  "keyword": "Then "
});
formatter.step({
  "line": 34,
  "name": "select country in shipment/pickup location",
  "keyword": "Then "
});
formatter.step({
  "line": 35,
  "name": "select title in shipment/pickup location",
  "keyword": "Then "
});
formatter.step({
  "line": 36,
  "name": "user enters first name",
  "keyword": "Then "
});
formatter.step({
  "line": 37,
  "name": "user enters last name",
  "keyword": "Then "
});
formatter.step({
  "line": 38,
  "name": "user enters address line1",
  "keyword": "Then "
});
formatter.step({
  "line": 39,
  "name": "user enters address line2",
  "keyword": "Then "
});
formatter.step({
  "line": 40,
  "name": "user enters city",
  "keyword": "Then "
});
formatter.step({
  "line": 41,
  "name": "user enters postcode",
  "keyword": "Then "
});
formatter.step({
  "line": 42,
  "name": "user enters phone number",
  "keyword": "Then "
});
formatter.step({
  "line": 43,
  "name": "click on checkbox of save shipping address",
  "keyword": "Then "
});
formatter.step({
  "line": 44,
  "name": "click on next for shipment/pickup location",
  "keyword": "And "
});
formatter.step({
  "line": 45,
  "name": "select premium delivery in shipping method",
  "keyword": "Then "
});
formatter.step({
  "line": 46,
  "name": "click on next for shipping method",
  "keyword": "And "
});
formatter.step({
  "line": 47,
  "name": "select card type",
  "keyword": "Then "
});
formatter.step({
  "line": 48,
  "name": "user enters name on card",
  "keyword": "Then "
});
formatter.step({
  "line": 49,
  "name": "user enters card number",
  "keyword": "Then "
});
formatter.step({
  "line": 50,
  "name": "select month and year",
  "keyword": "Then "
});
formatter.step({
  "line": 51,
  "name": "user enters card verification number",
  "keyword": "Then "
});
formatter.step({
  "line": 52,
  "name": "click on checkbox of save payment info",
  "keyword": "Then "
});
formatter.step({
  "line": 53,
  "name": "click on next for payment details",
  "keyword": "And "
});
formatter.step({
  "line": 54,
  "name": "click on checkbox of place order",
  "keyword": "Then "
});
formatter.step({
  "line": 55,
  "name": "click on place order",
  "keyword": "And "
});
formatter.step({
  "line": 56,
  "name": "verify order",
  "keyword": "And "
});
formatter.match({
  "location": "CheckoutStepDefinition.user_click_on_accessories()"
});
formatter.result({
  "duration": 6453653000,
  "status": "passed"
});
formatter.match({
  "location": "CheckoutStepDefinition.scrolls_down()"
});
formatter.result({
  "duration": 20947900,
  "status": "passed"
});
formatter.match({
  "location": "CheckoutStepDefinition.click_on_product()"
});
formatter.result({
  "duration": 1597039500,
  "status": "passed"
});
formatter.match({
  "location": "CheckoutStepDefinition.again_scrolls_down()"
});
formatter.result({
  "duration": 8647300,
  "status": "passed"
});
formatter.match({
  "location": "CheckoutStepDefinition.click_on_add_to_bag()"
});
formatter.result({
  "duration": 88724600,
  "status": "passed"
});
formatter.match({
  "location": "CheckoutStepDefinition.click_on_checkout_on_popup()"
});
formatter.result({
  "duration": 4430303100,
  "status": "passed"
});
formatter.match({
  "location": "CheckoutStepDefinition.click_on_checkout_in_cart_page()"
});
formatter.result({
  "duration": 3345619800,
  "status": "passed"
});
formatter.match({
  "location": "CheckoutStepDefinition.select_country_in_shipment_pickup_location()"
});
formatter.result({
  "duration": 2415951200,
  "status": "passed"
});
formatter.match({
  "location": "CheckoutStepDefinition.select_title_in_shipment_pickup_location()"
});
formatter.result({
  "duration": 2514838600,
  "status": "passed"
});
formatter.match({
  "location": "CheckoutStepDefinition.user_enters_first_name()"
});
formatter.result({
  "duration": 86918100,
  "status": "passed"
});
formatter.match({
  "location": "CheckoutStepDefinition.user_enters_last_name()"
});
formatter.result({
  "duration": 74176500,
  "status": "passed"
});
formatter.match({
  "location": "CheckoutStepDefinition.user_enters_address_line1()"
});
formatter.result({
  "duration": 66022400,
  "status": "passed"
});
formatter.match({
  "location": "CheckoutStepDefinition.user_enters_address_line2()"
});
formatter.result({
  "duration": 72920000,
  "status": "passed"
});
formatter.match({
  "location": "CheckoutStepDefinition.user_enters_city()"
});
formatter.result({
  "duration": 85784800,
  "status": "passed"
});
formatter.match({
  "location": "CheckoutStepDefinition.user_enters_postcode()"
});
formatter.result({
  "duration": 66560400,
  "status": "passed"
});
formatter.match({
  "location": "CheckoutStepDefinition.user_enters_phone_number()"
});
formatter.result({
  "duration": 82048600,
  "status": "passed"
});
formatter.match({
  "location": "CheckoutStepDefinition.click_on_checkbox_of_save_shipping_address()"
});
formatter.result({
  "duration": 58554900,
  "status": "passed"
});
formatter.match({
  "location": "CheckoutStepDefinition.click_on_next_for_shipmentpickup_location()"
});
formatter.result({
  "duration": 3052780000,
  "status": "passed"
});
formatter.match({
  "location": "CheckoutStepDefinition.select_premium_delivery_in_shipping_method()"
});
formatter.result({
  "duration": 125721500,
  "status": "passed"
});
formatter.match({
  "location": "CheckoutStepDefinition.click_on_next_for_shipping_method()"
});
formatter.result({
  "duration": 1945951000,
  "status": "passed"
});
formatter.match({
  "location": "CheckoutStepDefinition.select_card_type()"
});
formatter.result({
  "duration": 433282000,
  "status": "passed"
});
formatter.match({
  "location": "CheckoutStepDefinition.user_enters_name_on_card()"
});
formatter.result({
  "duration": 128888800,
  "status": "passed"
});
formatter.match({
  "location": "CheckoutStepDefinition.user_enters_card_number()"
});
formatter.result({
  "duration": 115692500,
  "status": "passed"
});
formatter.match({
  "location": "CheckoutStepDefinition.select_month_and_year()"
});
formatter.result({
  "duration": 2209658300,
  "status": "passed"
});
formatter.match({
  "location": "CheckoutStepDefinition.user_enters_card_verification_number()"
});
formatter.result({
  "duration": 62844800,
  "status": "passed"
});
formatter.match({
  "location": "CheckoutStepDefinition.click_on_checkbox_of_save_payment_info()"
});
formatter.result({
  "duration": 57069100,
  "status": "passed"
});
formatter.match({
  "location": "CheckoutStepDefinition.click_on_next_for_payment_details()"
});
formatter.result({
  "duration": 3362540300,
  "status": "passed"
});
formatter.match({
  "location": "CheckoutStepDefinition.click_on_checkbox_of_place_order()"
});
formatter.result({
  "duration": 825430500,
  "status": "passed"
});
formatter.match({
  "location": "CheckoutStepDefinition.click_on_place_order()"
});
formatter.result({
  "duration": 1965102400,
  "status": "passed"
});
formatter.match({
  "location": "CheckoutStepDefinition.verify_order()"
});
formatter.result({
  "duration": 300658600,
  "status": "passed"
});
formatter.after({
  "duration": 715357400,
  "status": "passed"
});
});