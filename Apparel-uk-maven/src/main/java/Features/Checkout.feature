Feature: eLitekart Apparel-Uk Functional Testing Feature

Scenario: eLitekart Apparel-Uk Registration Scenario

Given user is already on home page1
Then user click on signin 
Then user select title
Then user enters first name and last name
Then user enters email address and password
Then user enters confirm password
Then user select consent checkbox
Then user select terms and condition checkbox
Then click on Register
Then click on my account 
And  click on signout  


Scenario: eLitekart Apparel-Uk Login Scenario

When user enters username and password
Then user clicks on login
Then verify my account 


Scenario: eLitekart Apparel-Uk Checkout Scenario

When user click on accessories
Then scrolls down 
Then click on product
Then again scrolls down
Then click on add to bag
Then click on checkout on popup
Then click on checkout in cart page
Then select country in shipment/pickup location
Then select title in shipment/pickup location
Then user enters first name
Then user enters last name
Then user enters address line1
Then user enters address line2
Then user enters city
Then user enters postcode
Then user enters phone number
Then click on checkbox of save shipping address
And click on next for shipment/pickup location
Then select premium delivery in shipping method
And click on next for shipping method
Then select card type
Then user enters name on card 
Then user enters card number
Then select month and year
Then user enters card verification number
Then click on checkbox of save payment info
And click on next for payment details
Then click on checkbox of place order
And click on place order
And verify order 







