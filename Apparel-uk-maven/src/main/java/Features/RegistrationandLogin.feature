Feature: eLitekart Apparel-Uk Registration and Login Feature

Scenario Outline: eLitekart Apparel-Uk Registration Scenario

Given user is already on home page2
Then user click on signin 
Then user select title
Then user enters names"<firstname>" and "<lastname>"
Then user enters email and password"<emailaddress>" and "<password>"
Then user enters confirmpassword"<confirmpassword>"
Then user select consent checkbox
Then user select terms and condition checkbox
Then click on Register
Then click on my account 
And  click on signout  


Examples:
   | firstname | lastname | emailaddress              | password       | confirmpassword |
   | prakash   | velineni |cume111@gmail.com |Prakash@12345678|Prakash@12345678 |
   | prakash   | velineni |shcucumbe121@gmail.com |Prakash@12345678|Prakash@12345678 |
  # | prakash   | velineni |rakashcucumbe131@gmail.com |Prakash@12345678|Prakash@12345678 |
  # | prakash   | velineni |rakashcucumbe4@gmail.com |Prakash@12345678|Prakash@12345678 |
  # | prakash   | velineni |rakashcucumbe5@gmail.com |Prakash@12345678|Prakash@12345678 |
  # | prakash   | velineni |rakashcucumbe6@gmail.com |Prakash@12345678|Prakash@12345678 |
    

Scenario Outline: eLitekart Apparel-Uk Login Scenario

When user enters username and password "<username>" and "<password>"
Then user clicks on login
Then verify my account 
Then click on my account in login scenario 
And  click on signout in login scenario 
Examples:
  | username                   | password         |
  |prakashcucume111@gmail.com | Prakash@12345678 |
  |prakashcucumber12@gmail.com | Prakash@12345678 |
 # |prakashcucumber13@gmail.com | Prakash@12345678 |
 # |prakashcucumber4@gmail.com  | Prakash@12345678 |
 # |prakashcucumber5@gmail.com  | Prakash@12345678 |
 # |prakashcucumber6@gmail.com  | Prakash@12345678 |
