package stepDefinitions;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class RegistrationandLoginStepDefinition {
	
	 private static WebDriver driver;

	       @Before
           public void setUp(){
			System.setProperty("webdriver.chrome.driver","D:\\Prakash\\OneDrive_2019-11-26\\05. Test\\PerfTest\\jmeter-setup\\shoppers-stop\\chromedriver\\chromedriver.exe");
			  driver=new ChromeDriver();
			  driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
			  driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
			  driver.get("https://qa-omnibus.hybrisdemo.sbpcorp.com/");
			  driver.manage().window().maximize();
			  driver.findElement(By.xpath("//*[@id=\"sticky-header\"]/nav[1]/div/div[2]/div/ul/li/a")).click();
			 }
	
	@Given("^user is already on home page2$")
	public void user_is_already_on_home_page2() {
//		System.setProperty("webdriver.chrome.driver","D:\\Prakash\\OneDrive_2019-11-26\\05. Test\\PerfTest\\jmeter-setup\\shoppers-stop\\chromedriver\\chromedriver.exe");
//		  driver=new ChromeDriver();
//		  driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
//		  driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
//		  driver.get("https://qa-omnibus.hybrisdemo.sbpcorp.com/");
//		  driver.manage().window().maximize();
	      driver.navigate().refresh();
		 }
	
	@Then("^user click on signin$")
	public void user_click_on_signin() {
		driver.navigate().refresh();
//		driver.findElement(By.xpath("//*[@id=\"sticky-header\"]/nav[1]/div/div[2]/div/ul/li/a")).click();
	}
	
	@Then("^user select title$")
	public void user_select_title() {
		Select obj= new Select(driver.findElement(By.id("register.title")));
		obj.selectByIndex(1);
     	obj.selectByIndex(2);
        obj.selectByIndex(3);
        obj.selectByIndex(4);
        obj.selectByIndex(5);
        obj.selectByIndex(6);
		}
	
	@Then("^user enters names\"([^\"]*)\" and \"([^\"]*)\"$")
	public void user_enters_names_and(String firstname, String lastname)  {
	 
		driver.findElement(By.id("register.firstName")).sendKeys(firstname);
     	driver.findElement(By.id("register.lastName")).sendKeys(lastname);
	}

	@Then("^user enters email and password\"([^\"]*)\" and \"([^\"]*)\"$")
	public void user_enters_email_and_password_and(String emailaddress, String password)  {
		
		driver.findElement(By.id("register.email")).sendKeys(emailaddress);
    	driver.findElement(By.id("password")).sendKeys(password);
	
	}

	@Then("^user enters confirmpassword\"([^\"]*)\"$")
	public void user_enters_confirmpassword(String confirmpassword)  {
    driver.findElement(By.id("register.checkPwd")).sendKeys(confirmpassword);
	
	}

	@Then("^user select consent checkbox$")
	public void user_select_consent_checkbox() {
		driver.findElement(By.id("consentForm.consentGiven1")).click();
	}

	@Then("^user select terms and condition checkbox$")
	public void user_select_terms_and_condition_checkbox(){
		driver.findElement(By.id("registerChkTermsConditions")).click();
	}

	@Then("^click on Register$")
	public void click_on_Register() {
		driver.findElement(By.xpath("//*[@class='btn btn-default btn-block']")).click();
	}

	@Then("^click on my account$")
	public void click_on_my_account() {
		 driver.findElement(By.className("yCmsComponent")).click();
	}

	@And("^click on signout$")
	public void click_on_signout(){
		driver.findElement(By.linkText("Sign Out")).click();
	}
	
	@When("^user enters username and password \"([^\"]*)\" and \"([^\"]*)\"$")
	public void user_enters_username_and_password_and(String username, String password)  {
	    driver.findElement(By.id("j_username")).sendKeys(username);
		driver.findElement(By.id("j_password")).sendKeys(password);
	}

	@Then("^user clicks on login$")
	public void user_clicks_on_login() {
		  driver.findElement(By.xpath("//*[@class='btn btn-primary btn-block']")).click();
	}

	@Then("^verify my account$")
	public void verify_my_account() {
		String title = driver.getTitle();
	     System.out.println(title);
		 Assert.assertEquals("Apparel Site UK | Homepage", title);
	}
	
	@Then("^click on my account in login scenario$")
	public void click_on_my_account_in_login_scenario() {
		 driver.findElement(By.className("yCmsComponent")).click();
	}

	@And("^click on signout in login scenario$")
	public void click_on_signout_in_login_scenario(){
		driver.findElement(By.linkText("Sign Out")).click();
	}
	
}

