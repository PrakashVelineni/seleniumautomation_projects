package stepDefinitions;

import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CheckoutStepDefinition {
	
	private static WebDriver driver;
	
	@Before
    public void setUp(){
		System.setProperty("webdriver.chrome.driver","D:\\Prakash\\OneDrive_2019-11-26\\05. Test\\PerfTest\\jmeter-setup\\shoppers-stop\\chromedriver\\chromedriver.exe");
		  driver=new ChromeDriver();
		  driver.manage().window().maximize();
		  driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		  driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		  driver.get("https://qa-omnibus.hybrisdemo.sbpcorp.com/");
    }
	
	  @After
	    public void cleanUp(){
	        driver.quit();
	    }
	
	@Given("^user is already on home page1$")
	public void user_is_already_on_home_page1() {
		String title = driver.getTitle();
	     System.out.println(title);
		 Assert.assertEquals("Apparel Site UK | Homepage", title);
		
		 }

	@Then("^user click on signin$")
	public void user_click_on_signin() {
		driver.findElement(By.xpath("//*[@id=\"sticky-header\"]/nav[1]/div/div[2]/div/ul/li/a")).click();
	}

	@Then("^user select title$")
	public void user_select_title() {
		Select obj= new Select(driver.findElement(By.id("register.title")));
		obj.selectByIndex(1);
		obj.selectByIndex(2);
		obj.selectByIndex(3);
		obj.selectByIndex(4);
		obj.selectByIndex(5);
		obj.selectByIndex(6);
	 
	}

	@Then("^user enters first name and last name$")
	public void user_enters_first_name_and_last_name() throws InterruptedException{
		driver.findElement(By.id("register.firstName")).sendKeys("prakash");
		driver.findElement(By.id("register.lastName")).sendKeys("velineni");
	}

	@Then("^user enters email address and password$")
	public void user_enters_email_address_and_password() {
		driver.findElement(By.id("register.email")).sendKeys("prakashcucumbern@gmail.com");
		driver.findElement(By.id("password")).sendKeys("Prakash@12345678");
	}

	@Then("^user enters confirm password$")
	public void user_enters_confirm_password(){
		driver.findElement(By.id("register.checkPwd")).sendKeys("Prakash@12345678");
	}

	@Then("^user select consent checkbox$")
	public void user_select_consent_checkbox() {
		driver.findElement(By.id("consentForm.consentGiven1")).click();
	}

	@Then("^user select terms and condition checkbox$")
	public void user_select_terms_and_condition_checkbox(){
		driver.findElement(By.id("registerChkTermsConditions")).click();
	}

	@Then("^click on Register$")
	public void click_on_Register() {
		driver.findElement(By.xpath("//*[@class='btn btn-default btn-block']")).click();
	}

	@Then("^click on my account$")
	public void click_on_my_account() {
		 driver.findElement(By.className("yCmsComponent")).click();
	}

	@And("^click on signout$")
	public void click_on_signout(){
		driver.findElement(By.linkText("Sign Out")).click();
	}
	
	@When("^user enters username and password$")
	public void user_enters_username_and_password(){
		  
		  driver.findElement(By.xpath("//*[@id=\"sticky-header\"]/nav[1]/div/div[2]/div/ul/li/a")).click();
		  driver.findElement(By.id("j_username")).sendKeys("prakashcucumber@gmail.com");
		  driver.findElement(By.id("j_password")).sendKeys("Prakash@12345678");
	}

	@Then("^user clicks on login$")
	public void user_clicks_on_login() {
		  driver.findElement(By.xpath("//*[@class='btn btn-primary btn-block']")).click();
	}

	@Then("^verify my account$")
	public void verify_my_account() {
		WebElement myaccount = driver.findElement(By.className("yCmsComponent"));
        if(myaccount.isDisplayed()) {
        	System.out.println("successfully logged in");
        }
        
        else
        {
        	System.out.println("login was not successful");	
        }
        
        // Assert.assertTrue("Verification failed: Element1 and Element2 are not same",myaccount.isDisplayed());
 }

	@When("^user click on accessories$")
	public void user_click_on_accessories() {
		  driver.findElement(By.xpath("//*[@id=\"sticky-header\"]/nav[1]/div/div[2]/div/ul/li/a")).click();
		  driver.findElement(By.id("j_username")).sendKeys("prakashcucumber@gmail.com");
		  driver.findElement(By.id("j_password")).sendKeys("Prakash@12345678");
		  driver.findElement(By.xpath("//*[@class='btn btn-primary btn-block']")).click();
		  driver.findElement(By.linkText("ACCESSORIES")).click();
	}

	@Then("^scrolls down$")
	public void scrolls_down(){
		  JavascriptExecutor js1 = (JavascriptExecutor) driver;
		  js1.executeScript("window.scrollBy(0,750)");
	}

	@Then("^click on product$")
	public void click_on_product() {
	driver.findElement(By.xpath("/html[1]/body[1]/main[1]/div[3]/div[1]/div[2]/section[1]/div[1]/div[1]/div[2]/div[1]/div[1]/ul[1]/li[1]/div[1]/div[2]/a[1]/h4[1]/span[1]")).click();
	}

	@Then("^again scrolls down$")
	public void again_scrolls_down() {
		 JavascriptExecutor js1 = (JavascriptExecutor) driver;
		  js1.executeScript("window.scrollBy(0,500)");
	}

	@Then("^click on add to bag$")
	public void click_on_add_to_bag() {
		 driver.findElement(By.id("addToCartButton")).click();
	}

	@Then("^click on checkout on popup$")
	public void click_on_checkout_on_popup() throws InterruptedException{
		 Thread.sleep(3000);
		 driver.findElement(By.xpath("/html/body/div[5]/div[1]/div[2]/div[2]/div[1]/div/a[1]")).click();
	}

	@Then("^click on checkout in cart page$")
	public void click_on_checkout_in_cart_page(){
		 driver.findElement(By.xpath("//button[@class= 'btn btn-primary btn-block btn--continue-checkout js-continue-checkout-button']")).click();
	}

	@Then("^select country in shipment/pickup location$")
	public void select_country_in_shipment_pickup_location() throws InterruptedException {
		  Thread.sleep(2000);
		  Select obj= new Select(driver.findElement(By.id("address.country")));
			obj.selectByIndex(1);
			obj.selectByIndex(2);
			obj.selectByIndex(3);
			obj.selectByIndex(4);
	}

	@Then("^select title in shipment/pickup location$")
	public void select_title_in_shipment_pickup_location() throws InterruptedException{
		 Thread.sleep(2000);
		 Select obj1= new Select(driver.findElement(By.id("address.title")));
			obj1.selectByIndex(1);
			obj1.selectByIndex(2);
			obj1.selectByIndex(3);
			obj1.selectByIndex(4);
			obj1.selectByIndex(5);
			obj1.selectByIndex(6);
	}

	@Then("^user enters first name$")
	public void user_enters_first_name(){
		 driver.findElement(By.id("address.firstName")).sendKeys("prakash");
	}

	@Then("^user enters last name$")
	public void user_enters_last_name(){
		 driver.findElement(By.id("address.surname")).sendKeys("velineni");
	}

	@Then("^user enters address line1$")
	public void user_enters_address_line1()  {
		  driver.findElement(By.id("address.line1")).sendKeys("Guntur");
	}
	
	@Then("^user enters address line2$")
	public void user_enters_address_line2()  {
		 driver.findElement(By.id("address.line2")).sendKeys("Mutluru");
	}

	@Then("^user enters city$")
	public void user_enters_city() {
		  driver.findElement(By.id("address.townCity")).sendKeys("andhrapradesh");
	    }

	@Then("^user enters postcode$")
	public void user_enters_postcode()  {
		  driver.findElement(By.id("address.postcode")).sendKeys("522212");
	}

	@Then("^user enters phone number$")
	public void user_enters_phone_number() {
		  driver.findElement(By.id("address.phone")).sendKeys("9999912345");

	}

	@Then("^click on checkbox of save shipping address$")
	public void click_on_checkbox_of_save_shipping_address()  {
		  driver.findElement(By.id("saveAddressInMyAddressBook")).click();
	 
	}

	@And("^click on next for shipment/pickup location$")
	public void click_on_next_for_shipmentpickup_location() {
		  driver.findElement(By.id("addressSubmit")).click();
	}

	@Then("^select premium delivery in shipping method$")
	public void select_premium_delivery_in_shipping_method()  {
		 Select obj2= new Select(driver.findElement(By.id("delivery_method")));
			obj2.selectByIndex(0);
			obj2.selectByIndex(1);
	   
	}
	
	@And("^click on next for shipping method$")
	public void click_on_next_for_shipping_method() {
		 driver.findElement(By.id("deliveryMethodSubmit")).click();
	}


	@Then("^select card type$")
	public void select_card_type()  {
		Select obj3= new Select(driver.findElement(By.id("card_cardType")));
		obj3.selectByIndex(0);
		obj3.selectByIndex(1);
		obj3.selectByIndex(2);
		obj3.selectByIndex(3);
		obj3.selectByIndex(4);
	   }

	@Then("^user enters name on card$")
	public void user_enters_name_on_card()  {
		  driver.findElement(By.id("card_nameOnCard")).sendKeys("prakashvelineni");
	   }

	@Then("^user enters card number$")
	public void user_enters_card_number()  {
		driver.findElement(By.id("card_accountNumber")).sendKeys("1234567890123456");
	  }

	@Then("^select month and year$")
	public void select_month_and_year() {
		 Select obj4= new Select(driver.findElement(By.id("ExpiryMonth")));
			obj4.selectByIndex(0);
			obj4.selectByIndex(1);
			obj4.selectByIndex(2);
			obj4.selectByIndex(3);
			obj4.selectByIndex(4);
			obj4.selectByIndex(5);
			obj4.selectByIndex(6);
			obj4.selectByIndex(7);
			obj4.selectByIndex(8);
			obj4.selectByIndex(9);
			obj4.selectByIndex(10);
			obj4.selectByIndex(11);
			Select obj5= new Select(driver.findElement(By.id("ExpiryYear")));
			obj5.selectByIndex(0);
			obj5.selectByIndex(1);
			obj5.selectByIndex(2);
			obj5.selectByIndex(3);
			obj5.selectByIndex(4);
			obj5.selectByIndex(5);
			obj5.selectByIndex(6);
			obj5.selectByIndex(7);
			obj5.selectByIndex(8);
			obj5.selectByIndex(9);
			obj5.selectByIndex(10);
	  
	}

	@Then("^user enters card verification number$")
	public void user_enters_card_verification_number()  {
		driver.findElement(By.id("card_cvNumber")).sendKeys("123");
	  }

	@Then("^click on checkbox of save payment info$")
	public void click_on_checkbox_of_save_payment_info() {
		driver.findElement(By.id("savePaymentMethod")).click();
	  
	}
	
	@And("^click on next for payment details$")
	public void click_on_next_for_payment_details() {
		driver.findElement(By.xpath("//button[@class='btn btn-primary btn-block submit_silentOrderPostForm checkout-next']")).click();
	}


	@Then("^click on checkbox of place order$")
	public void click_on_checkbox_of_place_order()  {
		driver.findElement(By.id("Terms1")).click();
	   }

	@Then("^click on place order$")
	public void click_on_place_order()  {
		driver.findElement(By.id("placeOrder")).click();
	  }

	@Then("^verify order$")
	public void verify_order()  {
		WebElement order=driver.findElement(By.className("checkout-success__body__headline"));
		 String message = order.getText();
		 System.out.println(message);
	 
	}



}
