<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ul_ProductPriceQtyTotal                                                                Product not found</name>
   <tag></tag>
   <elementGuidId>f39c646c-a932-4d8e-9864-a9700ae75fa8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='quickOrder']/div/div[3]/ul</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>ul</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>item__list item__list__cart quick-order__list js-ul-container</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
		
			
				Product
				
				
				Price
				Qty
				Total
				
			
		

        
                
                    
                    
					Product not found
                
                
                    
                        
                    
                
            
        
                
                    
                    
					Sku already exists in the form
                
                
                    
                        
                    
                
            
        
                
                    
                    
					
                
                
                    
                        
                    
                
            
        
                
                    
                    
					
                
                
                    
                        
                    
                
            </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;quickOrder&quot;)/div[@class=&quot;account-section-content&quot;]/div[@class=&quot;js-quick-order-container&quot;]/ul[@class=&quot;item__list item__list__cart quick-order__list js-ul-container&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='quickOrder']/div/div[3]/ul</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Reset Form'])[1]/following::ul[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add to bag'])[1]/following::ul[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div[3]/ul</value>
   </webElementXpaths>
</WebElementEntity>
