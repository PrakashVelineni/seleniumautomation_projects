<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_1951_wishlist-btn fa-heart custom-cart js-enable-btn fas</name>
   <tag></tag>
   <elementGuidId>f832176c-5ed4-461c-bc6b-ef62d19f87b6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div/div[3]/div/div/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-product-code</name>
      <type>Main</type>
      <value>300020294</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>wishlist-btn fa-heart custom-cart js-enable-btn fas</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;i-amphtml-singledoc i-amphtml-standalone&quot;]/body[@class=&quot;page-SearchResultsGridPage2 pageType-ContentPage template-pages-search-searchGridPage2 pageLabel-search smartedit-page-uid-SearchResultsGridPage2 smartedit-page-uuid-eyJpdGVtSWQiOiJTZWFyY2hSZXN1bHRzR3JpZFBhZ2UyIiwiY2F0YWxvZ0lkIjoiYXBwYXJlbC11a0NvbnRlbnRDYXRhbG9nIiwiY2F0YWxvZ1ZlcnNpb24iOiJPbmxpbmUifQ== smartedit-catalog-version-uuid-apparel-ukContentCatalog/Online language-en amp-mode-mouse scrolling-down&quot;]/main[1]/div[@class=&quot;main__inner-wrapper container&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-sm-12 col-md-9&quot;]/section[@class=&quot;module module-product-card&quot;]/div[@class=&quot;yCmsContentSlot search-grid-page-result-grid-slot&quot;]/div[@class=&quot;yCmsComponent search-grid-page-result-grid-component&quot;]/div[@class=&quot;product__list--wrapper&quot;]/div[@class=&quot;products-container&quot;]/div[@class=&quot;product__listing product__grid&quot;]/div[@class=&quot;row&quot;]/ul[@class=&quot;list-unstyled product-list&quot;]/li[@class=&quot;col-md-4 col-sm-6&quot;]/div[@class=&quot;item-block&quot;]/div[@class=&quot;product-actions&quot;]/div[@class=&quot;addtocart&quot;]/div[@class=&quot;actions-container-for-SearchResultsGrid pickup-in-store-available&quot;]/button[@class=&quot;wishlist-btn fa-heart custom-cart js-enable-btn fas&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='£19.51'])[1]/following::button[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Accessories'])[3]/following::button[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='The 51-30 matte black/gold'])[1]/preceding::button[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='£55.00'])[1]/preceding::button[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[3]/div/div/button</value>
   </webElementXpaths>
</WebElementEntity>
