<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_1   Shopping Cart   HD Hero2 Outdoor Edition uni Uni</name>
   <tag></tag>
   <elementGuidId>80a697d1-bbe9-42ca-8684-1f4768e804b4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='sticky-header']/nav[2]/div/div[2]/div[3]/ul/li[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                

			



    1
    



	
	    
    Shopping Cart
    
        
        
        
                
                    
                        
			

                
                
                    HD Hero2 Outdoor Edition uni Uni
                     300659388
                    
                                                        
                                        Uni
                            

                each £283.46
                1 items
                
                    Total   £283.46

                
            
        
    


    
        
            
                Check Out
        
        
            Total :
            £283.46
        
    





    &lt;h6 class=&quot;title&quot;>Shopping Cart&lt;/h6>
    &lt;div class=&quot;cart-overview&quot;>
        &lt;li class=&quot;mini-cart-item mini-cart-popup&quot;>
            &lt;div class=&quot;thumb&quot;>
                &lt;a href=&quot;{{url}}&quot;> &lt;img src=&quot;{{imageUrl}}&quot; alt=&quot;{{name}}&quot; title=&quot;{{name}}&quot;>&lt;/a>
            &lt;/div>
            &lt;div class=&quot;description&quot;>
                &lt;a class=&quot;product-title&quot; href=&quot;{{url}}&quot;>{{name}}&lt;/a>
            &lt;/div>
            &lt;span class=&quot;price&quot;>{{price}}&lt;/span>
            &lt;span class=&quot;qty&quot;>{{quantity}} items&lt;/span>

            &lt;div class=&quot;value&quot;>
                        &lt;span class=&quot;totalValue&quot;>Total {{totalPrice}}&lt;/span>
            &lt;/div>
        &lt;/li>
    &lt;/div>
    &lt;/div>



    &lt;div class=&quot;total-line total-amount&quot;>
        &lt;ul>
            &lt;li>
                &lt;span class=&quot;cart-total&quot;>Total: &lt;/span>
                &lt;span class=&quot;total-sum-cart&quot;>&lt;/span>
            &lt;/li>
            &lt;li>
                &lt;a href=&quot;/en/cart&quot; class=&quot;btn btn-sm btn-filled&quot;>
                    Check Out&lt;/a>
            &lt;/li>
        &lt;/ul>
    &lt;/div>

							</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;sticky-header&quot;)/nav[@class=&quot;navigation navigation--middle js-navigation--middle&quot;]/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;row desktop__nav container&quot;]/div[@class=&quot;nav__right col-xs-6 col-xs-6 hidden-xs col-sm-6 col-md-4&quot;]/ul[@class=&quot;nav__links nav__links--shop_info&quot;]/li[3]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='sticky-header']/nav[2]/div/div[2]/div[3]/ul/li[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Welcome prakash'])[1]/following::li[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quick Order'])[1]/following::li[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[3]</value>
   </webElementXpaths>
</WebElementEntity>
