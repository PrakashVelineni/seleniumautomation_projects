<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_846_glyphicon glyphicon-chevron-right</name>
   <tag></tag>
   <elementGuidId>5003ab4e-f0c6-419e-a8f9-0389e8571e6b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='£8.46'])[1]/following::span[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>glyphicon glyphicon-chevron-right</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;i-amphtml-singledoc i-amphtml-standalone&quot;]/body[@class=&quot;page-homepage pageType-ContentPage template-pages-layout-landingLayout2Page pageLabel-homepage smartedit-page-uid-homepage smartedit-page-uuid-eyJpdGVtSWQiOiJob21lcGFnZSIsImNhdGFsb2dJZCI6ImFwcGFyZWwtdWtDb250ZW50Q2F0YWxvZyIsImNhdGFsb2dWZXJzaW9uIjoiT25saW5lIn0= smartedit-catalog-version-uuid-apparel-ukContentCatalog/Online language-en amp-mode-mouse scrolling-down&quot;]/main[1]/div[@class=&quot;main__inner-wrapper container&quot;]/div[@class=&quot;yCmsContentSlot row no-margin&quot;]/div[@class=&quot;yCmsComponent no-space yComponentWrapper&quot;]/div[@class=&quot;carousel__component&quot;]/div[@class=&quot;carousel__component--carousel js-owl-carousel js-owl-default owl-carousel owl-theme&quot;]/div[@class=&quot;owl-controls clickable&quot;]/div[@class=&quot;owl-buttons&quot;]/div[@class=&quot;owl-next&quot;]/span[@class=&quot;glyphicon glyphicon-chevron-right&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='£8.46'])[1]/following::span[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cap Blue Tomato BT Snow Trucker Cap black'])[1]/following::span[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Accelerator'])[1]/preceding::span[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='FAQ'])[1]/preceding::span[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[2]/div[2]/div/div[2]/span</value>
   </webElementXpaths>
</WebElementEntity>
