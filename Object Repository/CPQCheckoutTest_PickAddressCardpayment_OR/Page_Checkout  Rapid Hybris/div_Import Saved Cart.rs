<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Import Saved Cart</name>
   <tag></tag>
   <elementGuidId>53432c0f-6e96-4816-8da4-c75d143bfa09</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='sticky-header']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>custom-sticky sticky</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>sticky-header</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        
              
                  

                  
                  
                      
                          
                              
                      
                  
              
          
    
      

      
    
    
      
        
          
            
              
                
                  
                    
                  
                

                
                  
                      
                    
                  

                


        
            
                        Import Saved Cart
                
                        Order Forms
                
                        Quick Order
                
    


	
		
         		    1
             		
         		
         		

         	
         
         
            
          
        
                  

                      
                          
                              
                                  
                                      
                                  
                              
                              
                                  
                                      

	
		
			
			 
						
					
				
		
	



                              
                          
                      
                      
                              
                                  

	
		
                      
                      
                            
                               
                              
                                          
                                              
                                          


                                  
                                            Welcome PowerDrills

                                  
                                  
                              
                                

                              
                                


        
            
                        Import Saved Cart
                
                        Order Forms
                
                        Quick Order
                
    


	
		
         		    1
             		
         		
         		

         	
         
         
                            
          
        
      
    
  

     
                    
                        
                
  
    		
                    
    		
    			
                       
               	
    				
    						
Angle Grinders
    				
    						
Screwdrivers
    				
    						
Power Drills
    				
    						
Sanders
    				
    						
Measuring &amp; Layout Tools
    				
    						
Hand Tools
    							
    								Back
    								
    									
    									        
    													Hand Tools
    													
    														
Nut Drivers
Hand Saws
Rotary Hammers
Jigsaws
Stripping Tools
Punches
    												
    											
    									    
    							
    						
    				
    						
Safety
    							
    								Back
    								
    									
    									        
    													Footwear
    													
    														
Men's
Women's
    												
    											
    									    
    							
    						
    				
    		
    	

		
	
		Home
	

	
					Checkout
				
			Shipping Method
			

</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;sticky-header&quot;)</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='sticky-header']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Skip to navigation menu'])[1]/following::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Skip to content'])[1]/following::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//header/div</value>
   </webElementXpaths>
</WebElementEntity>
