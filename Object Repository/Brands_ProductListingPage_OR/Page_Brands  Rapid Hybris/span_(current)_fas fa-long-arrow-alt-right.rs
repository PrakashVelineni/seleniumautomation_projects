<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_(current)_fas fa-long-arrow-alt-right</name>
   <tag></tag>
   <elementGuidId>e744d87e-ce75-47ea-ab44-4a8d981f3e98</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[4]/div/div[2]/div/div[2]/ul/li[7]/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fas fa-long-arrow-alt-right</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;page-productGrid2 pageType-CategoryPage template-pages-category-productGridPage2 smartedit-page-uid-productGrid2 smartedit-page-uuid-eyJpdGVtSWQiOiJwcm9kdWN0R3JpZDIiLCJjYXRhbG9nSWQiOiJhcHBhcmVsLXVrQ29udGVudENhdGFsb2ciLCJjYXRhbG9nVmVyc2lvbiI6Ik9ubGluZSJ9 smartedit-catalog-version-uuid-apparel-ukContentCatalog/Online language-en scrolling-down&quot;]/main[1]/div[@class=&quot;main__inner-wrapper container&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-sm-12 col-md-9&quot;]/section[@class=&quot;module module-product-card&quot;]/div[@class=&quot;yCmsContentSlot product-grid-right-result-slot&quot;]/div[@class=&quot;yCmsComponent product__list--wrapper yComponentWrapper product-grid-right-result-component&quot;]/div[@class=&quot;pagination-bar bottom&quot;]/div[@class=&quot;pagination-toolbar&quot;]/div[@class=&quot;sort-refine-bar&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-6 col-md-5 pagination-wrap&quot;]/ul[@class=&quot;pagination&quot;]/li[@class=&quot;pagination-next disabled&quot;]/span[@class=&quot;fas fa-long-arrow-alt-right&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='(current)'])[2]/following::span[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Grid'])[2]/following::span[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Refine'])[2]/preceding::span[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Back'])[6]/preceding::span[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div[2]/div/div[2]/ul/li[7]/span</value>
   </webElementXpaths>
</WebElementEntity>
