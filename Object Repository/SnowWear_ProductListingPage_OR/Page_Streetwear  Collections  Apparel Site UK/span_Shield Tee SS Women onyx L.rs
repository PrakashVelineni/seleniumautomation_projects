<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Shield Tee SS Women onyx L</name>
   <tag></tag>
   <elementGuidId>5e1c3e16-08e3-4a0e-8624-45b6cadb0034</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//li[13]/div/div[2]/a/h4/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Shield Tee SS Women onyx L</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;i-amphtml-singledoc i-amphtml-standalone&quot;]/body[@class=&quot;page-productGrid2 pageType-CategoryPage template-pages-category-productGridPage2 smartedit-page-uid-productGrid2 smartedit-page-uuid-eyJpdGVtSWQiOiJwcm9kdWN0R3JpZDIiLCJjYXRhbG9nSWQiOiJhcHBhcmVsLXVrQ29udGVudENhdGFsb2ciLCJjYXRhbG9nVmVyc2lvbiI6Ik9ubGluZSJ9 smartedit-catalog-version-uuid-apparel-ukContentCatalog/Online language-en amp-mode-mouse scrolling-down&quot;]/main[1]/div[@class=&quot;main__inner-wrapper container&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-sm-12 col-md-9&quot;]/section[@class=&quot;module module-product-card&quot;]/div[@class=&quot;yCmsContentSlot product-grid-right-result-slot&quot;]/div[@class=&quot;yCmsComponent product__list--wrapper yComponentWrapper product-grid-right-result-component&quot;]/div[@class=&quot;products-container&quot;]/div[@class=&quot;product__listing product__grid&quot;]/div[@class=&quot;row&quot;]/ul[@class=&quot;list-unstyled product-list&quot;]/li[@class=&quot;col-md-4 col-sm-6&quot;]/div[@class=&quot;item-block&quot;]/div[@class=&quot;product-description&quot;]/a[@class=&quot;name&quot;]/h4[1]/span[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='£153.86'])[1]/following::span[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Panorama Pants Women beet red S'])[1]/following::span[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='£28.31'])[4]/preceding::span[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Nightlife T-Shirt Women violet M'])[1]/preceding::span[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[13]/div/div[2]/a/h4/span</value>
   </webElementXpaths>
</WebElementEntity>
