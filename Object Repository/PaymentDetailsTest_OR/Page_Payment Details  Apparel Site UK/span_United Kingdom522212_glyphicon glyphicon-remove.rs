<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_United Kingdom522212_glyphicon glyphicon-remove</name>
   <tag></tag>
   <elementGuidId>cc4bcb62-4a2d-4c87-b6e3-961505297584</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[2]/div/div/div/div/a/span</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>/html[@class=&quot;i-amphtml-singledoc i-amphtml-standalone&quot;]/body[@class=&quot;page-payment-details pageType-ContentPage template-pages-account-accountLayoutPage pageLabel-payment-details smartedit-page-uid-payment-details smartedit-page-uuid-eyJpdGVtSWQiOiJwYXltZW50LWRldGFpbHMiLCJjYXRhbG9nSWQiOiJhcHBhcmVsLXVrQ29udGVudENhdGFsb2ciLCJjYXRhbG9nVmVyc2lvbiI6Ik9ubGluZSJ9 smartedit-catalog-version-uuid-apparel-ukContentCatalog/Online language-en amp-mode-mouse&quot;]/main[1]/div[@class=&quot;main__inner-wrapper container&quot;]/div[@class=&quot;account-section&quot;]/div[@class=&quot;yCmsContentSlot account-section-content&quot;]/div[@class=&quot;account-paymentdetails account-list&quot;]/div[@class=&quot;account-cards card-select&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-6 col-md-4 card&quot;]/div[@class=&quot;account-cards-actions pull-left&quot;]/a[@class=&quot;action-links removePaymentDetailsButton&quot;]/span[@class=&quot;glyphicon glyphicon-remove&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>glyphicon glyphicon-remove</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;i-amphtml-singledoc i-amphtml-standalone&quot;]/body[@class=&quot;page-payment-details pageType-ContentPage template-pages-account-accountLayoutPage pageLabel-payment-details smartedit-page-uid-payment-details smartedit-page-uuid-eyJpdGVtSWQiOiJwYXltZW50LWRldGFpbHMiLCJjYXRhbG9nSWQiOiJhcHBhcmVsLXVrQ29udGVudENhdGFsb2ciLCJjYXRhbG9nVmVyc2lvbiI6Ik9ubGluZSJ9 smartedit-catalog-version-uuid-apparel-ukContentCatalog/Online language-en amp-mode-mouse&quot;]/main[1]/div[@class=&quot;main__inner-wrapper container&quot;]/div[@class=&quot;account-section&quot;]/div[@class=&quot;yCmsContentSlot account-section-content&quot;]/div[@class=&quot;account-paymentdetails account-list&quot;]/div[@class=&quot;account-cards card-select&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-6 col-md-4 card&quot;]/div[@class=&quot;account-cards-actions pull-left&quot;]/a[@class=&quot;action-links removePaymentDetailsButton&quot;]/span[@class=&quot;glyphicon glyphicon-remove&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='United Kingdom 522212'])[1]/following::span[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div/a/span</value>
   </webElementXpaths>
</WebElementEntity>
