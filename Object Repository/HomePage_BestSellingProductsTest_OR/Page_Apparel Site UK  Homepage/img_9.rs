<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>img_9</name>
   <tag></tag>
   <elementGuidId>97fa8dfd-4a2b-4841-9c7e-c8018784eb4b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='£50.96'])[1]/following::img[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>img</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>/medias/sys_master/images/images/h87/hca/8797908828190/8797908828190.jpg</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>alt</name>
      <type>Main</type>
      <value>Snowboard Ski Tool Toko Waxremover HC3 500ml</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Snowboard Ski Tool Toko Waxremover HC3 500ml</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;i-amphtml-singledoc i-amphtml-standalone&quot;]/body[@class=&quot;page-homepage pageType-ContentPage template-pages-layout-landingLayout2Page pageLabel-homepage smartedit-page-uid-homepage smartedit-page-uuid-eyJpdGVtSWQiOiJob21lcGFnZSIsImNhdGFsb2dJZCI6ImFwcGFyZWwtdWtDb250ZW50Q2F0YWxvZyIsImNhdGFsb2dWZXJzaW9uIjoiT25saW5lIn0= smartedit-catalog-version-uuid-apparel-ukContentCatalog/Online language-en amp-mode-mouse scrolling-down&quot;]/main[1]/div[@class=&quot;main__inner-wrapper container&quot;]/div[@class=&quot;yCmsContentSlot row no-margin&quot;]/div[@class=&quot;yCmsComponent no-space yComponentWrapper&quot;]/div[@class=&quot;carousel__component&quot;]/div[@class=&quot;carousel__component--carousel js-owl-carousel js-owl-default owl-carousel owl-theme&quot;]/div[@class=&quot;owl-wrapper-outer&quot;]/div[@class=&quot;owl-wrapper&quot;]/div[@class=&quot;owl-item&quot;]/div[@class=&quot;carousel__item&quot;]/a[1]/div[@class=&quot;carousel__item--thumb&quot;]/img[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='£50.96'])[1]/following::img[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Snowboard Ski Tool Toko Ergo Multi Guide yellow'])[1]/following::img[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Snowboard Ski Tool Toko Waxremover HC3 500ml'])[1]/preceding::img[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='£12.86'])[1]/preceding::img[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:img</name>
      <type>Main</type>
      <value>//img[@alt='Snowboard Ski Tool Toko Waxremover HC3 500ml']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/a/div/img</value>
   </webElementXpaths>
</WebElementEntity>
