<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>img_Thank you for your review_lazyOwl lazy</name>
   <tag></tag>
   <elementGuidId>969087ac-6adc-493e-b28f-46189f1b06e7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Maguro Pu Belt'])[1]/following::img[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>img</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>lazyOwl lazy</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>/medias/sys_master/images/h10/hb9/8862392877086</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>alt</name>
      <type>Main</type>
      <value>Maguro Pu Belt plaid</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;page-productDetails pageType-ProductPage template-pages-product-productLayout1Page smartedit-page-uid-productDetails smartedit-page-uuid-eyJpdGVtSWQiOiJwcm9kdWN0RGV0YWlscyIsImNhdGFsb2dJZCI6ImFwcGFyZWwtdWtDb250ZW50Q2F0YWxvZyIsImNhdGFsb2dWZXJzaW9uIjoiT25saW5lIn0= smartedit-catalog-version-uuid-apparel-ukContentCatalog/Online language-en scrolling-down&quot;]/main[1]/div[@class=&quot;main__inner-wrapper container&quot;]/div[@class=&quot;row custom-details-block&quot;]/div[@class=&quot;col-xs-10 col-xs-push-1 col-sm-6 col-sm-push-0 col-lg-6&quot;]/div[@class=&quot;image-gallery js-gallery row&quot;]/div[@class=&quot;carousel gallery-carousel js-gallery-carousel hidden-xs hidden-sm custom-carousel-right owl-carousel owl-theme&quot;]/div[@class=&quot;owl-wrapper-outer&quot;]/div[@class=&quot;owl-wrapper&quot;]/div[@class=&quot;owl-item&quot;]/a[@class=&quot;item&quot;]/img[@class=&quot;lazyOwl lazy&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Maguro Pu Belt'])[1]/following::img[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Maguro Pu Belt plaid'])[1]/preceding::img[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Write a Review'])[1]/preceding::img[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:img</name>
      <type>Main</type>
      <value>(//img[@alt='Maguro Pu Belt plaid'])[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div[2]/div/div/div/a/img</value>
   </webElementXpaths>
</WebElementEntity>
