<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>It contains all tests in my account</description>
   <name>MyAccountSuite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>7bb0a39c-4129-4781-b91f-b3d9489938d6</testSuiteGuid>
   <testCaseLink>
      <guid>1eb4b486-e21e-4352-b9c6-3c595619a69b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MyAccount/Click_On_AllLinks_Test</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cc99821e-5b84-460f-a3b9-5afe7b0ed675</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MyAccount/CloseAccountTest</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9a80743a-49da-4185-86e8-17df0c3f86ff</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MyAccount/ConsentManagementTest</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f1955818-8136-4ca0-a55d-3a91e8e14cf1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MyAccount/EmailAddressTest</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8e8740a5-b606-4ff9-8fd7-7ab60777db04</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MyAccount/OrderHistoryTest</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>603c4cd1-f0bc-494f-a4fc-1249202ece67</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MyAccount/PasswordTest</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6ac347b2-bc11-4f26-83f7-f2b488ec7312</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MyAccount/ReturnsHistoryTest</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7b36804b-7985-45ba-87ce-789cda6c20bb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MyAccount/PersonalDetailsTest</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>690c1257-919d-4599-a0f9-dcf73ff4e999</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MyAccount/SavedCartsTest</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>662be020-571b-4d86-8789-a20a662c35e7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MyAccount/SupportTicketsTest</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
