<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Verifying the functionality of Registration,Login,Checkout</description>
   <name>RegLoginCheckoutTestSuite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient>prakash.velineni95@gmail.com;</mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>5241353a-ee0b-4394-a609-4583ef743f80</testSuiteGuid>
   <testCaseLink>
      <guid>76c6b0d1-15f9-4d8d-b211-d4c3ce1030c2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RegistrationTest</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ebf59918-94a8-4a2e-91f0-a9e8e7bb8a95</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/LoginTest</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a4c4168f-b518-4a04-b7fc-df74fdb99622</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CheckoutTest</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
