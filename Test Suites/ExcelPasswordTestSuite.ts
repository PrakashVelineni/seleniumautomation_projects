<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Verifying password functionality with different data</description>
   <name>ExcelPasswordTestSuite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>963c83d7-5dde-44a5-b057-d6ed57d824ac</testSuiteGuid>
   <testCaseLink>
      <guid>233b58ed-36a2-43eb-bc2d-53af2cdc89eb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ExcelTests_DiffrentData/ExcelPasswordTest</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>b01976cd-fc6f-461c-a804-8f2ea2088102</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/PasswordExcelDataFile</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>b01976cd-fc6f-461c-a804-8f2ea2088102</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>currentpassword</value>
         <variableId>b0d75779-1b84-4be4-86fe-49e07c98c8bb</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>b01976cd-fc6f-461c-a804-8f2ea2088102</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>newpassword</value>
         <variableId>7bd7cd9d-37ec-4cf7-aa66-f7fcba043e04</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>b01976cd-fc6f-461c-a804-8f2ea2088102</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>confirmnewpassword</value>
         <variableId>ec3234a0-c2bd-4841-96e0-e00ea916d124</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
