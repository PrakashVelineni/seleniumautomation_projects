<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Verifying Login functionality with different data</description>
   <name>ExcelLoginTestSuite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>ad416994-0178-477f-b5e0-89bc3b6ec407</testSuiteGuid>
   <testCaseLink>
      <guid>427fb338-17cd-4f50-a0a5-c7c4b3672981</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ExcelTests_DiffrentData/ExcelLoginTest</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>34239b7c-d121-4051-9ade-d978195eee7a</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/LoginExcelDataFile</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>34239b7c-d121-4051-9ade-d978195eee7a</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>email</value>
         <variableId>733b260c-7864-4425-906a-540df41ea461</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>34239b7c-d121-4051-9ade-d978195eee7a</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>ab04c5f9-9eb5-4860-9516-339e1568fa34</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
