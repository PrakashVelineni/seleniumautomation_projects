<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Verifying email functionality with different data</description>
   <name>ExcelEmailAddressTest</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>b31f4d50-095c-4b9e-9baa-e92f0ec10d60</testSuiteGuid>
   <testCaseLink>
      <guid>a8ae3e25-baf3-41d0-8628-6c99770e389a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ExcelTests_DiffrentData/ExcelEmailAddressTest</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>755638fa-2152-40c8-bdf9-270a14fe8f0f</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/EmailAddressExcelDataFile</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>755638fa-2152-40c8-bdf9-270a14fe8f0f</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>NewEmailAddress</value>
         <variableId>fd1e9682-3cd7-41c9-867a-9a78b40af48c</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>755638fa-2152-40c8-bdf9-270a14fe8f0f</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>ConfirmNewEmailAddress</value>
         <variableId>18597e7a-dcb0-4711-bc57-22029d73fe75</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>755638fa-2152-40c8-bdf9-270a14fe8f0f</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>38cb0948-9063-49a9-9d9c-8460f5c14655</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
