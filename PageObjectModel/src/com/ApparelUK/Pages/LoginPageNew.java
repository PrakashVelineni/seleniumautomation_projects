package com.ApparelUK.Pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class LoginPageNew {
	
	WebDriver driver;
	
	public LoginPageNew(WebDriver ldriver)
	{
		this.driver = ldriver;
	}
	
	@FindBy(xpath="//*[@id=\"sticky-header\"]/nav[1]/div/div[2]/div/ul/li/a")
	WebElement signinlink;
	
	@FindBy(how=How.ID, using="j_username")
	WebElement username;
	
	@FindBy(id="j_password")
	WebElement password;
	
	@FindBy(xpath="//*[@class='btn btn-primary btn-block']")
	WebElement Loginbtn;
	
	@FindBy(className="yCmsComponent")
	WebElement myaccount;
	
	@FindBy(linkText="Sign Out")
	WebElement signoutbtn;
	
	
	
public void login_to_appareluk(String uid,String pwd) throws InterruptedException {
		 
	signinlink.click();
	username.sendKeys(uid);
	password.sendKeys(pwd);
	JavascriptExecutor js1= (JavascriptExecutor) driver;
	  js1.executeScript("window.scrollBy(0,250)");
	  Loginbtn.click();
	myaccount.click();
	Thread.sleep(2000);
	signoutbtn.click();
		
		
		
	}
	
	
	
	
	
	

}
