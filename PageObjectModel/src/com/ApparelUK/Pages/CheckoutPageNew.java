/**
 * 
 */
package com.ApparelUK.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

/**
 * @author PrakashVelineni
 *
 */
public class CheckoutPageNew {

	WebDriver driver;
	
	public CheckoutPageNew (WebDriver driverl)
	{
		this.driver = driverl;
	}
	
	@FindBy(xpath="//*[@id=\"sticky-header\"]/nav[1]/div/div[2]/div/ul/li/a")
	WebElement signinlink;
	
	@FindBy(how=How.ID, using="j_username")
	WebElement username;
	
	@FindBy(id="j_password")
	WebElement password;
	
	@FindBy(xpath="//*[@class='btn btn-primary btn-block']")
	WebElement loginbtn;
	

	@FindBy(linkText="ACCESSORIES")
	WebElement accessorieslink;
	

	@FindBy(xpath="/html[1]/body[1]/main[1]/div[3]/div[1]/div[2]/section[1]/div[1]/div[1]/div[2]/div[1]/div[1]/ul[1]/li[1]/div[1]/div[2]/a[1]/h4[1]/span[1]")
	WebElement clickonproduct;
	
	@FindBy(id="addToCartButton")
	WebElement addtobagbtn;
	
	@FindBy(xpath="/html/body/div[5]/div[1]/div[2]/div[2]/div[1]/div/a[1]")
	WebElement checkoutonpopup;
	

	@FindBy(xpath="//button[@class= 'btn btn-primary btn-block btn--continue-checkout js-continue-checkout-button']")
	WebElement checkoutbtn;
	

	@FindBy(id="address.country")
	WebElement selectcountry;
	

	@FindBy(id="address.title")
	WebElement selecttitle;
	

	@FindBy(id="address.firstName")
	WebElement firstname;
	
	@FindBy(id="address.surname")
	WebElement lastname;
	
	@FindBy(id="address.line1")
	WebElement addressline1;
	
	@FindBy(id="address.line2")
	WebElement addressline2;
	
	@FindBy(id="address.townCity")
	WebElement cityname;
	
	@FindBy(id="address.postcode")
	WebElement postcodenumber;
	
	@FindBy(id="address.phone")
	WebElement phonenumber;
	
	@FindBy(id="saveAddressInMyAddressBook")
	WebElement saveaddresscheckbox;
	
	@FindBy(id="addressSubmit")
	WebElement Nextbtnshipment;
	
	@FindBy(id="delivery_method")
	WebElement selectdelivery;
	
	@FindBy(id="deliveryMethodSubmit")
	WebElement Nextbtndelivery;
	
	@FindBy(id="card_cardType")
	WebElement selectcard;
	
	@FindBy(id="card_nameOnCard")
	WebElement nameoncard;
	
	@FindBy(id="card_accountNumber")
	WebElement cardnumber;
	
	@FindBy(id="ExpiryMonth")
	WebElement selectmonth;
	
	@FindBy(id="ExpiryYear")
	WebElement selectyear;
	
	@FindBy(id="card_cvNumber")
	WebElement cvvnumber;
	
	@FindBy(id="savePaymentMethod")
	WebElement savepaymentcheckbox;
	
	@FindBy(xpath="//button[@class='btn btn-primary btn-block submit_silentOrderPostForm checkout-next']")
	WebElement Nextbtnpayment;
	
	@FindBy(xpath="//div//div//div//div//div//div//label[1]//input[1]")
	WebElement termscheckbox;
	
	@FindBy(id="placeOrder")
	WebElement placeorderbtn;
	
	@FindBy(className="checkout-success__body__headline")
	WebElement thankyoutext;

	public void placeorder(String uid,String pwd,String fstname, String lstname,String address1,String address2, String city,String postcode,String phone,String cardname,String cardnum,String cvvnum ) throws InterruptedException {
	
		signinlink.click();
		
		username.sendKeys(uid);
		
		password.sendKeys(pwd);
		
		loginbtn.click();
		
		accessorieslink.click();
		
		JavascriptExecutor js1= (JavascriptExecutor) driver;
		  js1.executeScript("window.scrollBy(0,750)");
		
		clickonproduct.click();
		
		JavascriptExecutor js2= (JavascriptExecutor) driver;
		  js2.executeScript("window.scrollBy(0,750)");
		
		 addtobagbtn.click();
		 
		 Thread.sleep(2000);
		 
		 checkoutonpopup.click();
		 
		 checkoutbtn.click();
		 
		 Select obj= new Select(selectcountry);
			obj.selectByIndex(1);
			obj.selectByIndex(2);
			obj.selectByIndex(3);
			obj.selectByIndex(4);
			Thread.sleep(2000);
			
		 Select obj1= new Select(selecttitle);
			obj1.selectByIndex(1);
			obj1.selectByIndex(2);
			obj1.selectByIndex(3);
			obj1.selectByIndex(4);
			obj1.selectByIndex(5);
			obj1.selectByIndex(6);
			
		firstname.sendKeys(fstname);
		
		lastname.sendKeys(lstname);
		
		addressline1.sendKeys(address1);
		
		addressline2.sendKeys(address2);
		
		cityname.sendKeys(city);
		
		postcodenumber.sendKeys(postcode);
		
		phonenumber.sendKeys(phone);
		
		saveaddresscheckbox.click();
		
		Nextbtnshipment.click();
		
		Select obj2= new Select(selectdelivery);
		obj2.selectByIndex(0);
		obj2.selectByIndex(1);
		
		Nextbtndelivery.click();
		
		 Select obj3= new Select(selectcard);
			obj3.selectByIndex(0);
			obj3.selectByIndex(1);
			obj3.selectByIndex(2);
			obj3.selectByIndex(3);
			obj3.selectByIndex(4);
			
			nameoncard.sendKeys(cardname);
			
			cardnumber.sendKeys(cardnum);
			
			  Select obj4= new Select(selectmonth);
				obj4.selectByIndex(0);
				obj4.selectByIndex(1);
				obj4.selectByIndex(2);
				obj4.selectByIndex(3);
				obj4.selectByIndex(4);
				obj4.selectByIndex(5);
				obj4.selectByIndex(6);
				obj4.selectByIndex(7);
				obj4.selectByIndex(8);
				obj4.selectByIndex(9);
				obj4.selectByIndex(10);
				obj4.selectByIndex(11);
				
				Select obj5= new Select(selectyear);
				obj5.selectByIndex(0);
				obj5.selectByIndex(1);
				obj5.selectByIndex(2);
				obj5.selectByIndex(3);
				obj5.selectByIndex(4);
				obj5.selectByIndex(5);
				obj5.selectByIndex(6);
				obj5.selectByIndex(7);
				obj5.selectByIndex(8);
				obj5.selectByIndex(9);
				obj5.selectByIndex(10);
				
				cvvnumber.sendKeys(cvvnum);
				
				savepaymentcheckbox.click();
				
				Nextbtnpayment.click();
				
                JavascriptExecutor js3= (JavascriptExecutor) driver;
				  js3.executeScript("window.scrollBy(0,500)");
				
				  Thread.sleep(3000);
				  
				termscheckbox.click();
				
				placeorderbtn.click();
				
				 String message = thankyoutext.getText();
				 System.out.println(message);
				
				
		
		
		
}
	
}

