/**
 * 
 */
package com.ApparelUK.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

/**
 * @author PrakashVelineni
 *
 */
public class RegistrationPageNew {
	
	WebDriver driver;
	
	public RegistrationPageNew (WebDriver ldriver)
	{
		this.driver = ldriver;
	}
	
	@FindBy(xpath="//*[@id=\"sticky-header\"]/nav[1]/div/div[2]/div/ul/li/a")
	WebElement signinlink;

	@FindBy(id="register.title")
	WebElement title;
	

	@FindBy(id="register.firstName")
	WebElement firstname;
	
	@FindBy(id="register.lastName")
	WebElement lastname;
	
	@FindBy(id="register.email")
	WebElement email;
	

	@FindBy(id="password")
	WebElement password;
	

	@FindBy(id="register.checkPwd")
	WebElement confirmpassword;
	

	@FindBy(id="consentForm.consentGiven1")
	WebElement consentcheckbox;
	

	@FindBy(id="registerChkTermsConditions")
	WebElement termscheckbox;
	
	@FindBy(xpath="//*[@class='btn btn-default btn-block']")
	WebElement registerbtn;
	
	@FindBy(className="yCmsComponent")
	WebElement myaccount;
	
	@FindBy(linkText="Sign Out")
	WebElement signoutbtn;
	
	public void register_to_appareluk(String fstname,String lstname,String emailid, String pwd, String confirmpwd) throws InterruptedException {
		 
		signinlink.click();
		
		Select obj= new Select(title);
		obj.selectByIndex(1);
		obj.selectByIndex(2);
		obj.selectByIndex(3);
		obj.selectByIndex(4);
		
		firstname.sendKeys(fstname);
		
		lastname.sendKeys(lstname);
		
		email.sendKeys(emailid);
		
		password.sendKeys(pwd);
		
		confirmpassword.sendKeys(confirmpwd);
		
		consentcheckbox.click();
		
		termscheckbox.click();
		
		registerbtn.click();
		
		myaccount.click();
		
		Thread.sleep(2000);
		
		signoutbtn.click();
		
			
	}

}