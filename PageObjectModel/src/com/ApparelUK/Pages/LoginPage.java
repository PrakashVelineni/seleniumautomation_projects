/**
 * 
 */
package com.ApparelUK.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * @author PrakashVelineni
 *
 */
public class LoginPage {
	
	WebDriver driver;
	
	
	
	By signinlink = By.xpath("//*[@id=\"sticky-header\"]/nav[1]/div/div[2]/div/ul/li/a");
	By username= By.id("j_username");
	By password= By.id("j_password");
	By loginbtn= By.xpath("//*[@class='btn btn-primary btn-block']");
	
	public LoginPage(WebDriver driver)
	{
		this.driver = driver;
	}
	
	public void login_to_appareluk(String uid,String pwd) {
		
		driver.findElement(signinlink).click();
		driver.findElement(username).sendKeys(uid);
		driver.findElement(password).sendKeys(pwd);
		driver.findElement(loginbtn).click();
		
		
		
	}
}
