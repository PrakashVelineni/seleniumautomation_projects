/**
 * 
 */
package com.ApparelUK.Testcases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import com.ApparelUK.Pages.CheckoutPageNew;
import com.ApparelUK.Pages.LoginPage;
import com.ApparelUK.Pages.LoginPageNew;
import com.ApparelUK.Pages.RegistrationPageNew;

import Helper.BrowserFactory;

/**
 * @author PrakashVelineni
 *
 */
public class Totalscript {
	
	@Test
	public void verifyTotalscript() throws InterruptedException {
	
	WebDriver driver = BrowserFactory.startBrowser("chrome", "https://qa-omnibus.hybrisdemo.sbpcorp.com/");
	RegistrationPageNew  registerpage = PageFactory.initElements(driver, 	RegistrationPageNew.class);
	registerpage.register_to_appareluk("prakash", "velineni","prakashpageobjectmodel9@gmail.com", "Prakash@12345678", "Prakash@12345678");
    Thread.sleep(5000);
    LoginPageNew loginpage = PageFactory.initElements(driver, LoginPageNew.class);
	loginpage.login_to_appareluk("prakashpageobjectmodel8@gmail.com", "Prakash@12345678");
    CheckoutPageNew checkoutpage = PageFactory.initElements(driver, CheckoutPageNew.class);
	checkoutpage.placeorder("prakashpageobjectmodel8@gmail.com", "Prakash@12345678", "prakash", "velineni", "hyderabad", "krishnanagar", "andhrapradesh", "522212", "9999912345", "prakashvelineni", "1234567890123456","123");

	 }
	
   }




