/**
 * 
 */
package com.ApparelUK.Testcases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import com.ApparelUK.Pages.LoginPage;

/**
 * @author PrakashVelineni
 *
 */
public class Login {
	
	@Test
	public void verifylogin() {
		System.setProperty("webdriver.chrome.driver","D:\\Prakash\\OneDrive_2019-11-26\\05. Test\\PerfTest\\jmeter-setup\\shoppers-stop\\chromedriver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://qa-omnibus.hybrisdemo.sbpcorp.com/");
		LoginPage login = new LoginPage (driver);
		login.login_to_appareluk("prakashnew@gmail.com", "prakash@12345678");
		driver.quit();
	}
	

}
