/**
 * 
 */
package Helper;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

/**
 * @author PrakashVelineni
 *
 */
public class BrowserFactory {
	  static  WebDriver driver;
	public static WebDriver  startBrowser(String browserName,String url)
	{
		if(browserName.equals("chrome"))
		{
		 System.setProperty("webdriver.chrome.driver","D:\\Prakash\\OneDrive_2019-11-26\\05. Test\\PerfTest\\jmeter-setup\\shoppers-stop\\chromedriver\\chromedriver.exe");
			driver = new ChromeDriver();
			
		}
		else if (browserName.equals("firefox")) 
			
		{
		 System.setProperty("webdriver.chrome.driver", "D:\\Prakash\\Selenium\\geckodriver-v0.25.0-win64\\geckodriver.exe");
			driver = new FirefoxDriver();
			
		}
		else if(browserName.equalsIgnoreCase("IE"))
		{
		 System.setProperty("webdriver.chrome.driver", "D:\\Prakash\\Selenium\\geckodriver-v0.25.0-win64\\geckodriver.exe");
			driver = new InternetExplorerDriver();
			
		}
		
		driver.manage().window().maximize();
		driver.get(url);
		return driver;
		
	}
			
	}

