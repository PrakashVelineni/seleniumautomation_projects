import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import static com.yourcompany.ConnectionNotPrivate.*
import com.yourcompany.ConnectionNotPrivate as ConnectionNotPrivate

//ConnectionNotPrivate.yourMethod()
WebUI.openBrowser('')

WebUI.navigateToUrl("http://$GlobalVariable.Hostname")

WebUI.maximizeWindow()

WebUI.delay(2)

WebUI.mouseOver(findTestObject('Object Repository/ImportSavedCart_Test_OR/Page_Login  Apparel Site UK/div_Import Saved Cart                                        Quick Order'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ImportSavedCart_Test_OR/Page_Login  Apparel Site UK/a_Import Saved Cart'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/LoginTest_OR/Page_Login  Apparel Site UK/input_Email Address_j_username'), 
    "$GlobalVariable.Email")

WebUI.setEncryptedText(findTestObject('Object Repository/LoginTest_OR/Page_Login  Apparel Site UK/input_Password_j_password'), 
    "$GlobalVariable.Password")

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/LoginTest_OR/Page_Login  Apparel Site UK/button_Log In'))

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('Object Repository/ImportSavedCart_Test_OR/Page_Apparel Site UK  Homepage/div_Import Saved Cart                                        Quick Order'), 
    0)

WebUI.mouseOver(findTestObject('Object Repository/ImportSavedCart_Test_OR/Page_Apparel Site UK  Homepage/div_Import Saved Cart                                        Quick Order'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ImportSavedCart_Test_OR/Page_Apparel Site UK  Homepage/a_Import Saved Cart'))

WebUI.delay(2)

WebUI.sendKeys(findTestObject('Object Repository/ImportSavedCart_Test_OR/Page_Import Saved Cart  Apparel Site UK/div_Choose file'), 
    'C:\\\\Users\\\\PrakashVelineni\\\\Downloads\\\\cart (1).csv')

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ImportSavedCart_Test_OR/Page_Import Saved Cart  Apparel Site UK/button_Import'))

WebUI.delay(2)

WebUI.verifyElementPresent(findTestObject('Object Repository/ImportSavedCart_Test_OR/Page_Import Saved Cart  Apparel Site UK/div_         Imported file must be a text file with CSV extension'), 
    0)

WebUI.delay(2)

String savedcart = WebUI.getText(findTestObject('Object Repository/ImportSavedCart_Test_OR/Page_Import Saved Cart  Apparel Site UK/div_         Imported file must be a text file with CSV extension'))

KeywordLogger logger = new KeywordLogger()

logger.logInfo(savedcart)

WebUI.scrollToPosition(250, 0)

WebUI.click(findTestObject('ImportSavedCart_Test_OR/Page_Import Saved Cart  Apparel Site UK/button_'))

WebUI.delay(2)

