import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import static com.yourcompany.ConnectionNotPrivate.*
import com.yourcompany.ConnectionNotPrivate as ConnectionNotPrivate

//ConnectionNotPrivate.yourMethod()
WebUI.openBrowser('')

WebUI.navigateToUrl("http://$GlobalVariable.Hostname")

WebUI.maximizeWindow()

WebUI.delay(2)

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/SubmittingReviewsTest_OR/Page_Rapid Hybris  Homepage/div_Wallet Dakine Agent Leather Wallet brown'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/SubmittingReviewsTest_OR/Page_Wallet Dakine Agent Leather Wallet brown  Dakine  Brands  Rapid Hybris/span_Write a Review'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/SubmittingReviewsTest_OR/Page_Wallet Dakine Agent Leather Wallet brown  Dakine  Brands  Rapid Hybris/input_Review Title_headline'), 
    'test')

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/SubmittingReviewsTest_OR/Page_Wallet Dakine Agent Leather Wallet brown  Dakine  Brands  Rapid Hybris/textarea_Review Description_comment'), 
    'test')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/SubmittingReviewsTest_OR/Page_Wallet Dakine Agent Leather Wallet brown  Dakine  Brands  Rapid Hybris/span_Your Rating_js-ratingIcon glyphicon glyphicon-star lh active'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/SubmittingReviewsTest_OR/Page_Wallet Dakine Agent Leather Wallet brown  Dakine  Brands  Rapid Hybris/input_(optional)_alias'), 
    'test')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/SubmittingReviewsTest_OR/Page_Wallet Dakine Agent Leather Wallet brown  Dakine  Brands  Rapid Hybris/button_Submit Review'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/SubmittingReviewsTest_OR/Page_Wallet Dakine Agent Leather Wallet brown  Dakine  Brands  Rapid Hybris/button_'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/SubmittingReviewsTest_OR/Page_Wallet Dakine Agent Leather Wallet brown  Dakine  Brands  Rapid Hybris/i_Added to Your Shopping Bag_far fa-heart'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/SubmittingReviewsTest_OR/Page_Login  Rapid Hybris/img_Quick Order_lazy'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/SubmittingReviewsTest_OR/Page_Rapid Hybris  Homepage/div_Shades Quiksilver Dinero black white red grey'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/SubmittingReviewsTest_OR/Page_Shades Quiksilver Dinero black white red grey  Quiksilver  Brands  Rapid Hybris/span_Write a Review'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/SubmittingReviewsTest_OR/Page_Shades Quiksilver Dinero black white red grey  Quiksilver  Brands  Rapid Hybris/input_Review Title_headline'), 
    'test2')

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/SubmittingReviewsTest_OR/Page_Shades Quiksilver Dinero black white red grey  Quiksilver  Brands  Rapid Hybris/textarea_Review Description_comment'), 
    'test2')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/SubmittingReviewsTest_OR/Page_Shades Quiksilver Dinero black white red grey  Quiksilver  Brands  Rapid Hybris/span_Your Rating_js-ratingIcon glyphicon glyphicon-star lh active'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/SubmittingReviewsTest_OR/Page_Shades Quiksilver Dinero black white red grey  Quiksilver  Brands  Rapid Hybris/input_(optional)_alias'), 
    'test2')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/SubmittingReviewsTest_OR/Page_Shades Quiksilver Dinero black white red grey  Quiksilver  Brands  Rapid Hybris/button_Submit Review'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/SubmittingReviewsTest_OR/Page_Shades Quiksilver Dinero black white red grey  Quiksilver  Brands  Rapid Hybris/button_'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/SubmittingReviewsTest_OR/Page_Shades Quiksilver Dinero black white red grey  Quiksilver  Brands  Rapid Hybris/button_Add to bag'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/SubmittingReviewsTest_OR/Page_Shades Quiksilver Dinero black white red grey  Quiksilver  Brands  Rapid Hybris/a_Continue Shopping'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/SubmittingReviewsTest_OR/Page_Shades Quiksilver Dinero black white red grey  Quiksilver  Brands  Rapid Hybris/i_Added to Your Shopping Bag_far fa-heart'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/SubmittingReviewsTest_OR/Page_Login  Rapid Hybris/img_Quick Order_lazy'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/SubmittingReviewsTest_OR/Page_Rapid Hybris  Homepage/a_Accessories'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/SubmittingReviewsTest_OR/Page_Accessories  Categories  Rapid Hybris/span_BT Airhole Splatter Facemask white LXL'))

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('WishlistPDP1_OR/Page_Maguro Pu Belt plaid  Belts  Accessories  Categories  Rapid Hybris/a_Write a Review'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/SubmittingReviewsTest_OR/Page_BT Airhole Splatter Facemask white  Bandanas  Accessories  Categories  Rapid Hybris/input_Review Title_headline'), 
    'test3')

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/SubmittingReviewsTest_OR/Page_BT Airhole Splatter Facemask white  Bandanas  Accessories  Categories  Rapid Hybris/textarea_Review Description_comment'), 
    'test3')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/SubmittingReviewsTest_OR/Page_BT Airhole Splatter Facemask white  Bandanas  Accessories  Categories  Rapid Hybris/span_Your Rating_js-ratingIcon glyphicon glyphicon-star lh active'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/SubmittingReviewsTest_OR/Page_BT Airhole Splatter Facemask white  Bandanas  Accessories  Categories  Rapid Hybris/input_(optional)_alias'), 
    'test3')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/SubmittingReviewsTest_OR/Page_BT Airhole Splatter Facemask white  Bandanas  Accessories  Categories  Rapid Hybris/button_Submit Review'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/SubmittingReviewsTest_OR/Page_BT Airhole Splatter Facemask white  Bandanas  Accessories  Categories  Rapid Hybris/button_'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/SubmittingReviewsTest_OR/Page_BT Airhole Splatter Facemask white  Bandanas  Accessories  Categories  Rapid Hybris/button_Add to bag'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/SubmittingReviewsTest_OR/Page_BT Airhole Splatter Facemask white  Bandanas  Accessories  Categories  Rapid Hybris/span_Added to Your Shopping Bag_glyphicon glyphicon-remove'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/SubmittingReviewsTest_OR/Page_BT Airhole Splatter Facemask white  Bandanas  Accessories  Categories  Rapid Hybris/i_Added to Your Shopping Bag_far fa-heart'))

WebUI.delay(2)

