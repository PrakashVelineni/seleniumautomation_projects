import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent
import static com.yourcompany.ConnectionNotPrivate.*
import com.yourcompany.ConnectionNotPrivate as ConnectionNotPrivate

//ConnectionNotPrivate.yourMethod()
WebUI.openBrowser('')

WebUI.navigateToUrl("http://$GlobalVariable.Hostname")

WebUI.maximizeWindow()

WebUI.click(findTestObject('CheckOutTest_OR/Page_Apparel Site UK  Homepage/span_Quick Order_far fa-user'))

WebUI.setText(findTestObject('Object Repository/WishlistTest_WithLogin_OR/Page_Login  Apparel Site UK/input_Email Address_j_username'), 
    "$GlobalVariable.Email")

WebUI.setEncryptedText(findTestObject('Object Repository/WishlistTest_WithLogin_OR/Page_Login  Apparel Site UK/input_Password_j_password'), 
    "$GlobalVariable.Password")

WebUI.scrollToPosition(0, 250)

WebUI.click(findTestObject('Object Repository/WishlistTest_WithLogin_OR/Page_Login  Apparel Site UK/button_Log In'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistTest_WithLogin_OR/Page_Apparel Site UK  Homepage/span_1'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistTest_WithLogin_OR/Page_Wish List Page  Apparel Site UK/a_Accessories'))

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistTest_WithLogin_OR/Page_Accessories  Categories  Apparel Site UK/button_2021_wishlist-btn far fa-heart custom-cart js-enable-btn'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistTest_WithLogin_OR/Page_Accessories  Categories  Apparel Site UK/span_Assortment Web Belt blue combo Uni'))

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.back()

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistTest_WithLogin_OR/Page_Accessories  Categories  Apparel Site UK/button_2021_wishlist-btn fa-heart custom-cart js-enable-btn fas'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistTest_WithLogin_OR/Page_Accessories  Categories  Apparel Site UK/span_Assortment Web Belt blue combo Uni'))

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistTest_WithLogin_OR/Page_Assortment Web Belt blue combo Uni  Belts  Accessories  Categories  Apparel Site UK/i_Added to Your Shopping Bag_far fa-heart'))

WebUI.delay(2)

WebUI.back()

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistTest_WithLogin_OR/Page_Accessories  Categories  Apparel Site UK/span_Assortment Web Belt blue combo Uni'))

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistTest_WithLogin_OR/Page_Assortment Web Belt blue combo Uni  Belts  Accessories  Categories  Apparel Site UK/i_Added to Your Shopping Bag_fa-heart fas'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/WishlistTest_WithLogin_OR/Page_Accessories  Categories  Apparel Site UK/input_Quick Order_text'), 
    'accessories')

WebUI.delay(3)

Robot robot = new Robot()

robot.keyPress(KeyEvent.VK_ENTER)

robot.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/WishlistTest_WithLogin_OR/Page_Search accessories  Apparel Site UK/span_0'))

WebUI.delay(2)

WebUI.back()

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistTest_WithLogin_OR/Page_Search accessories  Apparel Site UK/button_1951_wishlist-btn far fa-heart custom-cart js-enable-btn'))

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistTest_WithLogin_OR/Page_Search accessories  Apparel Site UK/span_Shade Accessories Spy Omega Lens persimmon'))

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.back()

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistTest_WithLogin_OR/Page_Search accessories  Apparel Site UK/button_1951_wishlist-btn fa-heart custom-cart js-enable-btn fas'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistTest_WithLogin_OR/Page_Search accessories  Apparel Site UK/span_Shade Accessories Spy Omega Lens persimmon'))

WebUI.scrollToPosition(0, 500)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/WishlistTest_WithLogin_OR/Page_Shade Accessories Spy Omega Lens persimmon  Spy  Brands  Apparel Site UK/i_Added to Your Shopping Bag_far fa-heart'))

WebUI.back()

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistTest_WithLogin_OR/Page_Search accessories  Apparel Site UK/span_Shade Accessories Spy Omega Lens persimmon'))

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistTest_WithLogin_OR/Page_Shade Accessories Spy Omega Lens persimmon  Spy  Brands  Apparel Site UK/i_Added to Your Shopping Bag_fa-heart fas'))

WebUI.back()

WebUI.scrollToPosition(0, 500)

