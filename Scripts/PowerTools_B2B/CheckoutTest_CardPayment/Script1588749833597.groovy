import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import static com.yourcompany.ConnectionNotPrivate.*
import com.yourcompany.ConnectionNotPrivate as ConnectionNotPrivate

//ConnectionNotPrivate.yourMethod()

WebUI.openBrowser('')

WebUI.navigateToUrl("https://$GlobalVariable.Hostname")

WebUI.maximizeWindow()
WebUI.click(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Rapid Hybris B2B  Homepage/span_Quick Order_far fa-user'))

WebUI.setText(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Login  Rapid Hybris B2B/input_Email Address_j_username'), 
    "$GlobalVariable.Email")

WebUI.setEncryptedText(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Login  Rapid Hybris B2B/input_Password_j_password'), 
    "$GlobalVariable.Password")

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Login  Rapid Hybris B2B/button_Log In'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Rapid Hybris B2B  Homepage/a_Screwdrivers'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Screwdrivers  Tools  Open Catalogue  Rapid Hybris B2B/a_CD12CE'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_CD12CE ScrewdriversToolsOpen CatalogueRapid Hybris B2B/button_Add to cart'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_CD12CE ScrewdriversToolsOpen CatalogueRapid Hybris B2B/a_Check Out'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Your Shopping Cart  Rapid Hybris B2B/button_Check Out'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CheckoutTest_Pickaddresses_OR/Page_Checkout  Rapid Hybris B2B/input_Payment Type_paymentType'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CheckoutTest_Pickaddresses_OR/Page_Checkout  Rapid Hybris B2B/div_Payment Type'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/input_PO No_purchaseOrderNumber'), 
    '123456')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/button_Next'))

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/select_Country                United States'), 
    'US', true)

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Company', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Mr', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Miss', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Mrs', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Ms', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Rev.', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Prof.', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Dr.', true)

WebUI.setText(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/input_First Name_firstName'), 
    'testb2bpowertools')

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/input_Last Name_lastName'), 
    'testb2bpowertools')

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/input_Address Line 1_line1'), 
    'testb2bpowertools')

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/input_(optional)_line2'), 
    'testb2bpowertools')

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/input_TownCity_townCity'), 
    'testb2bpowertools')

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/select_State  ProvinceAlabama'), 
    'US-AL', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/select_State  ProvinceAlabama'), 
    'US-HI', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/select_State  ProvinceAlabama'), 
    'US-VA', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/select_State  ProvinceAlabama'), 
    'US-WY', true)

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/input_Zip  Postal Code_postcode'), 
    '123456')

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/input_(optional)_phone'), 
    '1234567890')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/input_Save Shipping Address_saveInAddressBook'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/button_Next_1'))

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/select_Standard Delivery-3-5 business'), 
    'premium-gross', true)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/button_Next_2'))

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/select_Please select a card typeAmerican'), 
    '003', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/select_Please select a card typeAmerican'), 
    '024', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/select_Please select a card typeAmerican'), 
    '001', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/select_Please select a card typeAmerican'), 
    '002', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/select_Please select a card typeAmerican'), 
    '005', true)

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/input_(optional)_card_nameOnCard'), 
    'testb2bpowertools')

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/input_Card number_card_accountNumber'), 
    '1234567890123456')

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/select_Month                010203040506070809101112'), 
    '1', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/select_Month                010203040506070809101112'), 
    '2', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/select_Month                010203040506070809101112'), 
    '3', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/select_Month                010203040506070809101112'), 
    '4', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/select_Month                010203040506070809101112'), 
    '6', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/select_Month                010203040506070809101112'), 
    '5', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/select_Month                010203040506070809101112'), 
    '7', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/select_Month                010203040506070809101112'), 
    '8', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/select_Month                010203040506070809101112'), 
    '9', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/select_Month                010203040506070809101112'), 
    '10', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/select_Month                010203040506070809101112'), 
    '11', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/select_Month                010203040506070809101112'), 
    '12', true)

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/select_Year2020202120222023'), 
    '2020', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/select_Year2020202120222023'), 
    '2021', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/select_Year2020202120222023'), 
    '2022', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/select_Year2020202120222023'), 
    '2023', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/select_Year2020202120222023'), 
    '2024', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/select_Year2020202120222023'), 
    '2025', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/select_Year2020202120222023'), 
    '2026', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/select_Year2020202120222023'), 
    '2027', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/select_Year2020202120222023'), 
    '2028', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/select_Year2020202120222023'), 
    '2029', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/select_Year2020202120222023'), 
    '2030', true)

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/input_Card Verification Number_card_cvNumber'), 
    '123')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/input_Save Payment Info_savePaymentInfo'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/button_Next_3'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/input_By placing the order I am'))

WebUI.delay(2)

/*WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CheckoutTest_CardPayment/Page_Checkout  Rapid Hybris B2B/button_Place Order'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CheckoutTest_AccountPayment_OR/Page_Order Confirmation  Rapid Hybris B2B/div_Thank you for your OrderYour Order'))

WebUI.delay(2)

WebUI.verifyElementPresent(findTestObject('Object Repository/CheckoutTest_AccountPayment_OR/Page_Order Confirmation  Rapid Hybris B2B/div_Thank you for your OrderYour Order'), 
    0)

String order = WebUI.getText(findTestObject('Object Repository/CheckoutTest_AccountPayment_OR/Page_Order Confirmation  Rapid Hybris B2B/div_Thank you for your OrderYour Order'))

KeywordLogger logger = new KeywordLogger()

logger.logInfo(order)*/

WebUI.delay(2)

WebUI.click(findTestObject('CheckoutTest_AccountPayment_OR/Page_Checkout  Rapid Hybris B2B/button_Schedule Replenishment'))

WebUI.delay(2)

WebUI.click(findTestObject('CheckoutTest_AccountPayment_OR/Page_Checkout  Rapid Hybris B2B/i_Start auto-replenishment on_glyphicon'))

WebUI.delay(2)

WebUI.click(findTestObject('CheckoutTest_AccountPayment_OR/Page_Checkout  Rapid Hybris B2B/a_8'))

WebUI.delay(2)

WebUI.click(findTestObject('CheckoutTest_AccountPayment_OR/Page_Checkout  Rapid Hybris B2B/input_Daily_replenishmentRecurrence'))

WebUI.delay(2)

WebUI.click(findTestObject('CheckoutTest_AccountPayment_OR/Page_Checkout  Rapid Hybris B2B/select_123456789101112'))

WebUI.delay(2)

WebUI.click(findTestObject('CheckoutTest_AccountPayment_OR/Page_Checkout  Rapid Hybris B2B/input_On the following days_nDaysOfWeek'))

WebUI.delay(2)

WebUI.click(findTestObject('CheckoutTest_AccountPayment_OR/Page_Checkout  Rapid Hybris B2B/input_Monday_nDaysOfWeek'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CheckoutTest_Pickaddresses_OR/Page_Checkout  Rapid Hybris B2B/button_Schedule Replenishment_1'))

WebUI.delay(2)

WebUI.click(findTestObject('CheckoutTest_CardPayment/Page_Replenishment Confirmation  Rapid Hybris B2B/div_Thank you for your scheduled Replenishment Order'))

WebUI.delay(2)

WebUI.verifyElementPresent(findTestObject('CheckoutTest_CardPayment/Page_Replenishment Confirmation  Rapid Hybris B2B/div_Thank you for your scheduled Replenishment Order'), 
    0)

String order = WebUI.getText(findTestObject('CheckoutTest_CardPayment/Page_Replenishment Confirmation  Rapid Hybris B2B/div_Thank you for your scheduled Replenishment Order'))

KeywordLogger logger = new KeywordLogger()

logger.logInfo(order)

