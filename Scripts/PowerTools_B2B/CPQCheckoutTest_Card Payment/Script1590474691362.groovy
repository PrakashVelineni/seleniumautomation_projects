import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import static com.yourcompany.ConnectionNotPrivate.*
import com.yourcompany.ConnectionNotPrivate as ConnectionNotPrivate

//ConnectionNotPrivate.yourMethod()
WebUI.openBrowser('')

WebUI.navigateToUrl("https://$GlobalVariable.Hostname")

WebUI.maximizeWindow()

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_Rapid Hybris B2B  Homepage/span_Quick Order_far fa-user'))

WebUI.setText(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_Login  Rapid Hybris B2B/input_Email Address_j_username'), 
    "$GlobalVariable.Email")

WebUI.setEncryptedText(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_Login  Rapid Hybris B2B/input_Password_j_password'), 
    "$GlobalVariable.Password")

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_Login  Rapid Hybris B2B/button_Log In'))

WebUI.setText(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_Rapid Hybris B2B  Homepage/input_Quick Order_text'), 
    'megamachines')

WebUI.delay(5)

Robot robot = new Robot()

robot.keyPress(KeyEvent.VK_ENTER)

robot.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(5)

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.mouseOver(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_Search megamachines/button_Configure'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_Search megamachines/button_Configure'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_MegaMachines Excavator/span_Please select'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_MegaMachines Excavator/div_100'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_MegaMachines Excavator/input_Transmission_groups'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_MegaMachines Excavator/input_Drive_groups0cstics2value'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_MegaMachines Excavator/div_02CAB'))

WebUI.delay(2)

WebUI.scrollToPosition(1000, 600)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_MegaMachines Excavator/input_seats_groups1cstics0value'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_MegaMachines Excavator/input_HVAC Options_groups1cstics1'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_MegaMachines Excavator/div_03OPTIONS'))

WebUI.delay(3)

WebUI.scrollToPosition(1400, 1100)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_MegaMachines Excavator/input_options_groups2cstics0value'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_MegaMachines Excavator/span_Please select_1'))

WebUI.delay(2)

WebUI.mouseOver(findTestObject('Titles_OR/Page_MegaMachines Excavator Model 500  Powertools Site/div_8Newrt'))

WebUI.delay(2)

WebUI.click(findTestObject('Titles_OR/Page_MegaMachines Excavator Model 500  Powertools Site/div_8Newrt'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_MegaMachines Excavator/div_04ATTACH'))

WebUI.delay(2)

WebUI.scrollToPosition(1250, 1000)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_MegaMachines Excavator/input_Buckets_groups3cstics0value'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_MegaMachines Excavator/input_Augers_groups3cstics1value'))

WebUI.delay(2)

WebUI.scrollToPosition(1000, 2000)

WebUI.delay(2)

WebUI.click(findTestObject('Titles_OR/Page_MegaMachines Excavator Model 500  Rapid Hybris B2B/button_Add to cartN'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('Titles_OR/Page_MegaMachines Excavator Model 500  Rapid Hybris B2B/button_CHECKOUTN'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('Titles_OR/Page_Your Shopping Cart  Rapid Hybris B2B/button_Check OutN'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CheckoutTest_Pickaddresses_OR/Page_Checkout  Rapid Hybris B2B/input_Payment Type_paymentType'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CheckoutTest_Pickaddresses_OR/Page_Checkout  Rapid Hybris B2B/div_Payment Type'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_Checkout  Rapid Hybris B2B/input_PO No_purchaseOrderNumber'), 
    '123456')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_Checkout  Rapid Hybris B2B/button_Next'))

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_Checkout  Rapid Hybris B2B/select_Country                United States'), 
    'US', true)

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Company', true)

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_Checkout  Rapid Hybris B2B/input_First Name_firstName'), 
    'test')

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_Checkout  Rapid Hybris B2B/input_Last Name_lastName'), 
    'test')

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_Checkout  Rapid Hybris B2B/input_Address Line 1_line1'), 
    'test')

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_Checkout  Rapid Hybris B2B/input_(optional)_line2'), 
    'test')

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_Checkout  Rapid Hybris B2B/input_TownCity_townCity'), 
    'test')

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_Checkout  Rapid Hybris B2B/select_State'), 
    'US-AL', true)

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_Checkout  Rapid Hybris B2B/input_Zip  Postal Code_postcode'), 
    '123456')

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_Checkout  Rapid Hybris B2B/input_(optional)_phone'), 
    '1234567890')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_Checkout  Rapid Hybris B2B/input_Save Shipping Address'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_Checkout  Rapid Hybris B2B/input_Save Shipping Address'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_Checkout  Rapid Hybris B2B/input_Save Shipping Address'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_Checkout  Rapid Hybris B2B/button_Next_1'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_Checkout  Rapid Hybris B2B/button_Next_2'))

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_Checkout  Rapid Hybris B2B/select_Please select a card type'), 
    '003', true)

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_Checkout  Rapid Hybris B2B/input_(optional)_card_nameOnCard'), 
    'test')

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_Checkout  Rapid Hybris B2B/input_Card number_card_accountNumber'), 
    '1234567890123456')

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_Checkout  Rapid Hybris B2B/select_Month 010203040506070809101112'), 
    '1', true)

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_Checkout  Rapid Hybris B2B/select_Year 2020'), 
    '2030', true)

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_Checkout  Rapid Hybris B2B/input_Card Verification Number_card_cvNumber'), 
    '123')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_Checkout  Rapid Hybris B2B/input_Save Payment Info_savePaymentInfo'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_Checkout  Rapid Hybris B2B/button_Next_3'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_Checkout  Rapid Hybris B2B/input_By placing the order'))

WebUI.scrollToPosition(0, 150)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_CardPayment_OR/Page_Checkout  Rapid Hybris B2B/button_Place Order'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CheckoutTest_AccountPayment_OR/Page_Order Confirmation  Rapid Hybris B2B/div_Thank you for your OrderYour Order'))

WebUI.delay(2)

WebUI.verifyElementPresent(findTestObject('Object Repository/CheckoutTest_AccountPayment_OR/Page_Order Confirmation  Rapid Hybris B2B/div_Thank you for your OrderYour Order'), 
    0)

String order = WebUI.getText(findTestObject('Object Repository/CheckoutTest_AccountPayment_OR/Page_Order Confirmation  Rapid Hybris B2B/div_Thank you for your OrderYour Order'))

KeywordLogger logger = new KeywordLogger()

logger.logInfo(order)

