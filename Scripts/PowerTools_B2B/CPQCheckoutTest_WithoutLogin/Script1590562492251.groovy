import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import static com.yourcompany.ConnectionNotPrivate.*
import com.yourcompany.ConnectionNotPrivate as ConnectionNotPrivate

//ConnectionNotPrivate.yourMethod()
WebUI.openBrowser('')

WebUI.navigateToUrl("http://$GlobalVariable.Hostname")

WebUI.maximizeWindow()

WebUI.setText(findTestObject('Object Repository/CPQCheckouTest_WithoutLogin/Page_Rapid Hybris B2B  Homepage/input_Quick Order_text'), 
    'megamachines')

WebUI.delay(5)

Robot robot = new Robot()

robot.keyPress(KeyEvent.VK_ENTER)

robot.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(5)

WebUI.scrollToPosition(0, 750)

WebUI.delay(3)

WebUI.mouseOver(findTestObject('Object Repository/CPQCheckouTest_WithoutLogin/Page_Search megamachines/button_Configure'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/CPQCheckouTest_WithoutLogin/Page_Search megamachines/button_Configure'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckouTest_WithoutLogin/Page_MegaMachines/span_Please select'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckouTest_WithoutLogin/Page_MegaMachines/div_100'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckouTest_WithoutLogin/Page_MegaMachines/input_Transmission_groups0cstics1value'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckouTest_WithoutLogin/Page_MegaMachines/input_Drive_groups0cstics2value'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckouTest_WithoutLogin/Page_MegaMachines/div_02CAB'))

WebUI.delay(2)

WebUI.scrollToPosition(750, 500)

WebUI.click(findTestObject('Object Repository/CPQCheckouTest_WithoutLogin/Page_MegaMachines/input_seats_groups1cstics0value'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckouTest_WithoutLogin/Page_MegaMachines/input_HVAC Options_groups1cstics1value'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckouTest_WithoutLogin/Page_MegaMachines/div_03OPTIONS'))

WebUI.delay(3)

WebUI.scrollToPosition(1400, 1000)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/CPQCheckouTest_WithoutLogin/Page_MegaMachines/input_options_groups2cstics0value'))

WebUI.delay(2)

WebUI.click(findTestObject('Titles_OR/Page_MegaMachines Excavator Model 500  Powertools Site/span_Please select'))

WebUI.delay(2)

WebUI.mouseOver(findTestObject('Titles_OR/Page_MegaMachines Excavator Model 500  Powertools Site/div_8Newrt'))

WebUI.delay(2)

WebUI.click(findTestObject('Titles_OR/Page_MegaMachines Excavator Model 500  Powertools Site/div_8Newrt'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckouTest_WithoutLogin/Page_MegaMachines/div_04ATTACH'))

WebUI.delay(2)

WebUI.scrollToPosition(1250, 1000)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckouTest_WithoutLogin/Page_MegaMachines/input_Buckets_groups3cstics0value'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckouTest_WithoutLogin/Page_MegaMachines/input_Augers_groups3cstics1value'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckouTest_WithoutLogin/Page_MegaMachines/button_Add to cart'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckouTest_WithoutLogin/Page_MegaMachines/button_CHECKOUT'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckouTest_WithoutLogin/Page_Your Shopping Cart  Rapid Hybris B2B/button_Check Out'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CPQCheckouTest_WithoutLogin/Page_Proceed to Checkout  Rapid Hybris B2B/input_Email Address_j_username'), 
    "$GlobalVariable.Email")

WebUI.delay(2)

WebUI.setEncryptedText(findTestObject('Object Repository/CPQCheckouTest_WithoutLogin/Page_Proceed to Checkout  Rapid Hybris B2B/input_Password_j_password'), 
    "$GlobalVariable.Password")

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckouTest_WithoutLogin/Page_Proceed to Checkout  Rapid Hybris B2B/button_Log In and Check Out'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckouTest_WithoutLogin/Page_Checkout  Rapid Hybris B2B/input_Payment Type_paymentType'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckouTest_WithoutLogin/Page_Checkout  Rapid Hybris B2B/button_Next'))

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/CPQCheckouTest_WithoutLogin/Page_Checkout  Rapid Hybris B2B/select_Country  United States'), 
    'US', true)

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Company', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Mr', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Miss', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Mrs', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Ms', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Rev.', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Prof.', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Dr.', true)

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CPQCheckouTest_WithoutLogin/Page_Checkout  Rapid Hybris B2B/input_First Name_firstName'), 
    'test')

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CPQCheckouTest_WithoutLogin/Page_Checkout  Rapid Hybris B2B/input_Last Name_lastName'), 
    'test')

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CPQCheckouTest_WithoutLogin/Page_Checkout  Rapid Hybris B2B/input_Address Line 1_line1'), 
    'test')

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CPQCheckouTest_WithoutLogin/Page_Checkout  Rapid Hybris B2B/input_(optional)_line2'), 
    'test')

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CPQCheckouTest_WithoutLogin/Page_Checkout  Rapid Hybris B2B/input_TownCity_townCity'), 
    'test')

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/CPQCheckouTest_WithoutLogin/Page_Checkout  Rapid Hybris B2B/select_State  Province'), 
    'US-AL', true)

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/CPQCheckouTest_WithoutLogin/Page_Checkout  Rapid Hybris B2B/select_State  Province'), 
    'US-KS', true)

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CPQCheckouTest_WithoutLogin/Page_Checkout  Rapid Hybris B2B/input_Zip  Postal Code_postcode'), 
    '123456')

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CPQCheckouTest_WithoutLogin/Page_Checkout  Rapid Hybris B2B/input_(optional)_phone'), 
    '1234567890')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckouTest_WithoutLogin/Page_Checkout  Rapid Hybris B2B/input_Save Shipping Address_saveInAddressBook'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckouTest_WithoutLogin/Page_Checkout  Rapid Hybris B2B/button_Next_1'))

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/CPQCheckouTest_WithoutLogin/Page_Checkout  Rapid Hybris B2B/select_Standard'), 
    'premium-gross', true)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckouTest_WithoutLogin/Page_Checkout  Rapid Hybris B2B/button_Next_2'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckouTest_WithoutLogin/Page_Checkout  Rapid Hybris B2B/button_Use a saved card'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckouTest_WithoutLogin/Page_Checkout  Rapid Hybris B2B/button_Use these payment details'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckouTest_WithoutLogin/Page_Checkout  Rapid Hybris B2B/input_By placing the order'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckouTest_WithoutLogin/Page_Checkout  Rapid Hybris B2B/button_Place Order'))

WebUI.delay(2)

