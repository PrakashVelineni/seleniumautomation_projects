import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import java.util.concurrent.TimeUnit as TimeUnit
import org.openqa.selenium.WebDriver as WebDriver
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent

WebUI.openBrowser('')

WebUI.navigateToUrl("https://$GlobalVariable.Hostname")

WebUI.waitForPageLoad(30)

WebDriver driver = DriverFactory.getWebDriver()

driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS)

WebUI.maximizeWindow()

WebUI.mouseOver(findTestObject('Object Repository/ImportSavedCartTestB_OR/Page_Rapid Hybris B2B  Homepage/div_Quick Import Saved Cart'))

WebUI.click(findTestObject('ImportSavedCartTestB_OR/Page_Powertools Site  Homepage/a_Import Saved CartN'))

WebUI.setText(findTestObject('Object Repository/ImportSavedCartTestB_OR/Page_Login  Rapid Hybris B2B/input_Email Address_j_username'), 
    "$GlobalVariable.Email")

WebUI.setEncryptedText(findTestObject('Object Repository/ImportSavedCartTestB_OR/Page_Login  Rapid Hybris B2B/input_Password_j_password'), 
    "$GlobalVariable.Password")

WebUI.click(findTestObject('Object Repository/ImportSavedCartTestB_OR/Page_Login  Rapid Hybris B2B/button_Log In'))

WebUI.mouseOver(findTestObject('Object Repository/ImportSavedCartTestB_OR/Page_Rapid Hybris B2B  Homepage/div_Quick Import Saved Cart'))

WebUI.click(findTestObject('ImportSavedCartTestB_OR/Page_Powertools Site  Homepage/a_Import Saved CartN'))

WebUI.sendKeys(findTestObject('Object Repository/ImportSavedCart_Test_OR/Page_Import Saved Cart  Apparel Site UK/div_Choose file'), 
    'C:\\\\Users\\\\PrakashVelineni\\\\Downloads\\\\cart (1).csv')

WebUI.click(findTestObject('Object Repository/ImportSavedCartTestB_OR/Page_Import Saved Cart  Rapid Hybris B2B/button_Import'))

WebUI.click(findTestObject('Object Repository/ImportSavedCartTestB_OR/Page_Import Saved Cart  Rapid Hybris B2B/a_saved carts'))

