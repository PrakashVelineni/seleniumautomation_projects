import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import java.util.concurrent.TimeUnit as TimeUnit
import org.openqa.selenium.WebDriver as WebDriver
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent

WebUI.openBrowser('')

WebUI.navigateToUrl("https://$GlobalVariable.Hostname")

WebUI.waitForPageLoad(30)

WebDriver driver = DriverFactory.getWebDriver()

driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS)

WebUI.maximizeWindow()

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Rapid Hybris B2B  Homepage/a_Power Drills'))

WebUI.scrollToPosition(0, 500)

WebUI.mouseOver(findTestObject('Object Repository/QuotePageTest_OR/Page_Power Drills  Tools  Open Catalogue  Rapid Hybris B2B/button_7900_fas fa-cart-plus custom-cart js-enable-btn rh_cart'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Power Drills  Tools  Open Catalogue  Rapid Hybris B2B/button_7900_fas fa-cart-plus custom-cart js-enable-btn rh_cart'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Power Drills  Tools  Open Catalogue  Rapid Hybris B2B/span_Added to Your Shopping Cart_glyphicon glyphicon-remove'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Power Drills  Tools  Open Catalogue  Rapid Hybris B2B/a_Screwdrivers'))

WebUI.scrollToPosition(0, 500)

WebUI.mouseOver(findTestObject('Object Repository/QuotePageTest_OR/Page_Screwdrivers  Tools  Open Catalogue  Rapid Hybris B2B/button_3400_fas fa-cart-plus custom-cart js-enable-btn rh_cart'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Screwdrivers  Tools  Open Catalogue  Rapid Hybris B2B/button_3400_fas fa-cart-plus custom-cart js-enable-btn rh_cart'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Screwdrivers  Tools  Open Catalogue  Rapid Hybris B2B/span_Added to Your Shopping Cart_glyphicon glyphicon-remove'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Screwdrivers  Tools  Open Catalogue  Rapid Hybris B2B/a_Angle Grinders'))

WebUI.scrollToPosition(0, 500)

WebUI.mouseOver(findTestObject('Object Repository/QuotePageTest_OR/Page_Angle Grinders  Tools  Open Catalogue  Rapid Hybris B2B/button_4300_fas fa-cart-plus custom-cart js-enable-btn rh_cart'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Angle Grinders  Tools  Open Catalogue  Rapid Hybris B2B/button_4300_fas fa-cart-plus custom-cart js-enable-btn rh_cart'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Angle Grinders  Tools  Open Catalogue  Rapid Hybris B2B/a_Check Out'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Your Shopping Cart  Rapid Hybris B2B/button_Request a Quote'))

WebUI.setText(findTestObject('Object Repository/QuotePageTest_OR/Page_Login  Rapid Hybris B2B/input_Email Address_j_username'), 
    'powerdrillslover@pronto-hw.com')

WebUI.setEncryptedText(findTestObject('Object Repository/QuotePageTest_OR/Page_Login  Rapid Hybris B2B/input_Password_j_password'), 
    'aeHFOx8jV/A=')

WebUI.scrollToPosition(0, 250)

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Login  Rapid Hybris B2B/button_Log In'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Your Shopping Cart  Rapid Hybris B2B/span_Import Saved Cart_glyphicon fas fa-cart-plus'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Your Shopping Cart  Rapid Hybris B2B/a_Check Out'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Your Shopping Cart  Rapid Hybris B2B/a_New Cart'))

WebUI.setText(findTestObject('Object Repository/QuotePageTest_OR/Page_Your Shopping Cart  Rapid Hybris B2B/input_Name_name'), 
    'test')

WebUI.setText(findTestObject('Object Repository/QuotePageTest_OR/Page_Your Shopping Cart  Rapid Hybris B2B/textarea_Description_description'), 
    'test')

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Your Shopping Cart  Rapid Hybris B2B/button_Save'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Your Shopping Cart  Rapid Hybris B2B/button_'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Your Shopping Cart  Rapid Hybris B2B/a_Sanders'))

WebUI.scrollToPosition(0, 500)

WebUI.mouseOver(findTestObject('Object Repository/QuotePageTest_OR/Page_Sanders  Tools  Open Catalogue  Rapid Hybris B2B/button_5100_fas fa-cart-plus custom-cart js-enable-btn rh_cart'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Sanders  Tools  Open Catalogue  Rapid Hybris B2B/button_5100_fas fa-cart-plus custom-cart js-enable-btn rh_cart'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Sanders  Tools  Open Catalogue  Rapid Hybris B2B/span_Added to Your Shopping Cart_glyphicon glyphicon-remove'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Sanders  Tools  Open Catalogue  Rapid Hybris B2B/a_Measuring  Layout Tools'))

WebUI.scrollToPosition(0, 500)

WebUI.mouseOver(findTestObject('Object Repository/QuotePageTest_OR/Page_Measuring  Layout Tools  Tools  Open Catalogue  Rapid Hybris B2B/button_2350_fas fa-cart-plus custom-cart js-enable-btn rh_cart'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Measuring  Layout Tools  Tools  Open Catalogue  Rapid Hybris B2B/button_2350_fas fa-cart-plus custom-cart js-enable-btn rh_cart'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Measuring  Layout Tools  Tools  Open Catalogue  Rapid Hybris B2B/span_Added to Your Shopping Cart_glyphicon glyphicon-remove'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Measuring  Layout Tools  Tools  Open Catalogue  Rapid Hybris B2B/a_Safety'))

WebUI.scrollToPosition(0, 500)

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Safety  Open Catalogue  Rapid Hybris B2B/a_Hand Tools'))

WebUI.scrollToPosition(0, 500)

WebUI.mouseOver(findTestObject('Object Repository/QuotePageTest_OR/Page_Tools  Open Catalogue  Rapid Hybris B2B/button_7900_fas fa-cart-plus custom-cart js-enable-btn rh_cart'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Tools  Open Catalogue  Rapid Hybris B2B/button_7900_fas fa-cart-plus custom-cart js-enable-btn rh_cart'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Tools  Open Catalogue  Rapid Hybris B2B/span_Added to Your Shopping Cart_glyphicon glyphicon-remove'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Tools  Open Catalogue  Rapid Hybris B2B/span_Import Saved Cart_glyphicon fas fa-cart-plus'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Tools  Open Catalogue  Rapid Hybris B2B/a_Check Out'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Your Shopping Cart  Rapid Hybris B2B/button_Request a Quote'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Quote Edit  Rapid Hybris B2B/a_New Cart'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Your Shopping Cart  Rapid Hybris B2B/button_'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Your Shopping Cart  Rapid Hybris B2B/a_Power Drills'))

WebUI.scrollToPosition(0, 500)

WebUI.mouseOver(findTestObject('Object Repository/QuotePageTest_OR/Page_Power Drills  Tools  Open Catalogue  Rapid Hybris B2B/button_7900_fas fa-cart-plus custom-cart js-enable-btn rh_cart'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Power Drills  Tools  Open Catalogue  Rapid Hybris B2B/button_7900_fas fa-cart-plus custom-cart js-enable-btn rh_cart'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Power Drills  Tools  Open Catalogue  Rapid Hybris B2B/a_Check Out'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Your Shopping Cart  Rapid Hybris B2B/button_Request a Quote'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Quote Edit  Rapid Hybris B2B/a_Quotes (39)'))

WebUI.back()

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Quote Edit  Rapid Hybris B2B/a_Saved Carts (3)'))

WebUI.back()

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Quote Edit  Rapid Hybris B2B/button_Continue Shopping'))

WebUI.back()

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Quote Edit  Rapid Hybris B2B/button_Cancel Quote'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Quote Edit  Rapid Hybris B2B/span_Cancel Quote 0005623035_glyphicon glyphicon-remove'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Quote Edit  Rapid Hybris B2B/button_Submit Quote'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Quote Edit  Rapid Hybris B2B/span_Confirm Requested Quote 0005623035_glyphicon glyphicon-remove'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Quote Edit  Rapid Hybris B2B/button_Quote ID_quote__form--toggle js-quote-toggle-btn'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Quote Edit  Rapid Hybris B2B/button_Quote ID_quote__form--toggle js-quote-toggle-btn open collapsed'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Quote Edit  Rapid Hybris B2B/button_000_quote__comments--toggle collapsed js-quote-comments-btn'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Quote Edit  Rapid Hybris B2B/button_000_quote__comments--toggle collapsed js-quote-comments-btn open'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Quote Edit  Rapid Hybris B2B/button_000_quote__comments--toggle js-quote-comments-btn'))

WebUI.setText(findTestObject('Object Repository/QuotePageTest_OR/Page_Quote Edit  Rapid Hybris B2B/textarea_Comments(optional)_comment'), 
    'test')

WebUI.delay(2)

Robot robot5 = new Robot()

robot5.keyPress(KeyEvent.VK_ENTER)

robot5.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Quote Edit  Rapid Hybris B2B/a_Export CSV'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Quote Edit  Rapid Hybris B2B/a_Export CSV'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Quote Edit  Rapid Hybris B2B/span_7950_glyphicon glyphicon-option-vertical'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Quote Edit  Rapid Hybris B2B/a_Comment'))

WebUI.setText(findTestObject('Object Repository/QuotePageTest_OR/Page_Quote Edit  Rapid Hybris B2B/textarea_7950_entryComment_2'), 
    'test')

WebUI.delay(2)

Robot robot1 = new Robot()

robot1.keyPress(KeyEvent.VK_ENTER)

robot1.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/QuotePageTest_OR/Page_Quote Edit  Rapid Hybris B2B/input_Import Saved Cart_text'), 
    'megamachines')

WebUI.delay(2)

Robot robot3 = new Robot()

robot3.keyPress(KeyEvent.VK_ENTER)

robot3.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(2)

WebUI.scrollToPosition(0, 1000)

WebUI.delay(2)

WebUI.mouseOver(findTestObject('Object Repository/QuotePageTest_OR/Page_Search megamachines  Rapid Hybris B2B/button_9874950_fas fa-cart-plus custom-cart js-enable-btn rh_cart'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Search megamachines  Rapid Hybris B2B/button_9874950_fas fa-cart-plus custom-cart js-enable-btn rh_cart'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Search megamachines  Rapid Hybris B2B/a_View Quote'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Quote Edit  Rapid Hybris B2B/span_9875000_glyphicon glyphicon-option-vertical'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Quote Edit  Rapid Hybris B2B/a_Comment_2'))

WebUI.setText(findTestObject('Object Repository/QuotePageTest_OR/Page_Quote Edit  Rapid Hybris B2B/textarea_Change configuration_entryComment_3'), 
    'test')

WebUI.delay(2)

Robot robot4 = new Robot()

robot4.keyPress(KeyEvent.VK_ENTER)

robot4.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Quote Edit  Rapid Hybris B2B/span_9875000_glyphicon glyphicon-option-vertical'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Quote Edit  Rapid Hybris B2B/a_Copy'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Quote Edit  Rapid Hybris B2B/button_'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Quote Edit  Rapid Hybris B2B/span_9875000_glyphicon glyphicon-option-vertical_1'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Quote Edit  Rapid Hybris B2B/a_Remove'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Quote Edit  Rapid Hybris B2B/button_'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Quote Edit  Rapid Hybris B2B/span_Import Saved Cart_glyphicon fas fa-cart-plus'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Quote Edit  Rapid Hybris B2B/a_View Quote'))

WebUI.back()

WebUI.mouseOver(findTestObject('QuotePageTest_OR/Page_Rapid Hybris B2B  Homepage/div_Quick Order Order Forms Import Saved Cart'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Quote Edit  Rapid Hybris B2B/a_Quick Order'))

WebUI.back()

WebUI.mouseOver(findTestObject('QuotePageTest_OR/Page_Rapid Hybris B2B  Homepage/div_Quick Order Order Forms Import Saved Cart'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Quote Edit  Rapid Hybris B2B/a_Order Forms'))

WebUI.back()

WebUI.mouseOver(findTestObject('QuotePageTest_OR/Page_Rapid Hybris B2B  Homepage/div_Quick Order Order Forms Import Saved Cart'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Quote Edit  Rapid Hybris B2B/a_Import Saved Cart'))

WebUI.back()

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Quote Edit  Rapid Hybris B2B/span_Welcome Power Drills_glyphicon glyphicon-map-marker'))

WebUI.back()

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Quote Edit  Rapid Hybris B2B/span_Import Saved Cart_far fa-user'))

WebUI.click(findTestObject('Object Repository/QuotePageTest_OR/Page_Quote Edit  Rapid Hybris B2B/span_Import Saved Cart_far fa-user_1'))

