import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import java.util.concurrent.TimeUnit as TimeUnit
import org.openqa.selenium.WebDriver as WebDriver
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent

WebUI.openBrowser('')

WebUI.navigateToUrl("https://$GlobalVariable.Hostname")

WebUI.waitForPageLoad(30)

WebDriver driver = DriverFactory.getWebDriver()

driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS)

WebUI.maximizeWindow()

WebUI.click(findTestObject('Object Repository/QuickOrderTestB_OR/Page_Rapid Hybris B2B  Homepage/span_Import Saved Cart_far fa-user'))

WebUI.setText(findTestObject('Object Repository/QuickOrderTestB_OR/Page_Login  Rapid Hybris B2B/input_Email Address_j_username'), 
    "$GlobalVariable.Email")

WebUI.setEncryptedText(findTestObject('Object Repository/QuickOrderTestB_OR/Page_Login  Rapid Hybris B2B/input_Password_j_password'), 
    "$GlobalVariable.Password")

WebUI.click(findTestObject('Object Repository/QuickOrderTestB_OR/Page_Login  Rapid Hybris B2B/button_Log In'))

WebUI.click(findTestObject('Object Repository/QuickOrderTestB_OR/Page_Rapid Hybris B2B  Homepage/div_Quick Order  Order Forms'))

WebUI.click(findTestObject('OrderFormsTest_OR/Page_Rapid Hybris B2B  Homepage/a_Quick OrderN'))

WebUI.setText(findTestObject('Object Repository/QuickOrderTestB_OR/Page_Quick Order  Rapid Hybris B2B/input_Total_js-sku-input-field form-control'), 
    'test12!@')

WebUI.setText(findTestObject('Object Repository/QuickOrderTestB_OR/Page_Quick Order  Rapid Hybris B2B/input_Product not found_js-sku-input-field form-control'), 
    'test12!@')

WebUI.setText(findTestObject('Object Repository/QuickOrderTestB_OR/Page_Quick Order  Rapid Hybris B2B/input_Sku already exists in the form_js-sku-input-field form-control'), 
    '3794514')

WebUI.delay(2)

Robot robot1 = new Robot()

robot1.keyPress(KeyEvent.VK_ENTER)

robot1.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/QuickOrderTestB_OR/Page_Quick Order  Rapid Hybris B2B/button_Add to cart'))

WebUI.click(findTestObject('Object Repository/QuickOrderTestB_OR/Page_Quick Order  Rapid Hybris B2B/span_Added to Your Shopping Cart_glyphicon glyphicon-remove'))

WebUI.delay(2)

WebUI.setText(findTestObject('QuickOrderTestB_OR/Page_Quick Order  Rapid Hybris B2B/input_Sku already exists in the form_js-sku-input-field form-control'), 
    '3794514')

WebUI.delay(2)

Robot robot2 = new Robot()

robot2.keyPress(KeyEvent.VK_ENTER)

robot2.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(2)

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/QuickOrderTestB_OR/Page_Quick Order  Rapid Hybris B2B/input_Item price_js-quick-order-qty form-control'), 
    '15')

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

Robot robot3 = new Robot()

robot3.keyPress(KeyEvent.VK_ENTER)

robot3.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/QuickOrderTestB_OR/Page_Quick Order  Rapid Hybris B2B/button_Add to cart'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/QuickOrderTestB_OR/Page_Quick Order  Rapid Hybris B2B/span_Added to Your Shopping Cart_glyphicon glyphicon-remove'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/QuickOrderTestB_OR/Page_Quick Order  Rapid Hybris B2B/input_Sku already exists in the form_js-sku-input-field form-control has-error'), 
    '3794514')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/QuickOrderTestB_OR/Page_Quick Order  Rapid Hybris B2B/button_Reset Form'))

WebUI.acceptAlert()

WebUI.delay(2)

WebUI.setText(findTestObject('QuickOrderTestB_OR/Page_Quick Order  Rapid Hybris B2B/input_Sku already exists in the form_js-sku-input-field form-control'), 
    '3794514')

WebUI.delay(2)

Robot robot4 = new Robot()

robot4.keyPress(KeyEvent.VK_ENTER)

robot4.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/QuickOrderTestB_OR/Page_Quick Order  Rapid Hybris B2B/span_18V Auto Select cordless drill'))

WebUI.acceptAlert()

WebUI.back()

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/QuickOrderTestB_OR/Page_Quick Order  Rapid Hybris B2B/span_Sku already exists'))

WebUI.click(findTestObject('Object Repository/QuickOrderTestB_OR/Page_Quick Order  Rapid Hybris B2B/span_Sku already exists'))

