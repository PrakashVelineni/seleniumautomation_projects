import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import java.util.concurrent.TimeUnit as TimeUnit
import org.openqa.selenium.WebDriver as WebDriver
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent

WebUI.openBrowser('')

WebUI.navigateToUrl("https://$GlobalVariable.Hostname")

WebUI.waitForPageLoad(30)

WebDriver driver = DriverFactory.getWebDriver()

driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS)

WebUI.maximizeWindow()

WebUI.click(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Rapid Hybris B2B  Homepage/span_Import Saved Cart_far fa-user'))

WebUI.setText(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Login  Rapid Hybris B2B/input_Email Address_j_username'), 
    "$GlobalVariable.Email")

WebUI.setEncryptedText(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Login  Rapid Hybris B2B/input_Password_j_password'), 
    "$GlobalVariable.Password")

WebUI.click(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Login  Rapid Hybris B2B/button_Log In'))

WebUI.click(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Rapid Hybris B2B  Homepage/span_Import Saved Cart_far fa-user_1'))

WebUI.click(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Rapid Hybris B2B  Homepage/a_Payment Details'))

WebUI.click(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Payment Details  Rapid Hybris B2B/a_Add Payment Details'))

WebUI.selectOptionByValue(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/select_State  Province'), 
    'AL', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/select_State  Province'), 
    'FI', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/select_State  Province'), 
    'FR', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/select_Please selectMsDrRevProf'), 
    'Mr', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/select_Please selectMsDrRevProf'), 
    'Mrs', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/select_Please selectMsDrRevProf'), 
    'Miss', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/select_Please selectMsDrRevProf'), 
    'Company', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/select_Please selectMsDrRevProf'), 
    'Ms', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/select_Please selectMsDrRevProf'), 
    'Dr.', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/select_Please selectMsDrRevProf'), 
    'Rev.', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/select_Please selectMsDrRevProf'), 
    'Prof.', true)

WebUI.setText(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/input_First Name_firstName'), 
    'test')

WebUI.setText(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/input_Last Name_lastName'), 
    'test')

WebUI.setText(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/input_Address Line 1_line1'), 
    'test')

WebUI.setText(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/input_(optional)_line2'), 
    'test')

WebUI.setText(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/input_TownCity_townCity'), 
    'test')

WebUI.setText(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/input_Zip  Postal Code_postcode'), 
    '123456')

WebUI.setText(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/input_(optional)_phone'), 
    '1234567890')

WebUI.click(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/input_Make this my default address_defaultAddress'))

WebUI.selectOptionByValue(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/select_Please select'), 
    'amex', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/select_Please select'), 
    'maestro', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/select_Please select'), 
    'switch', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/select_Please select'), 
    'visa', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/select_Please select'), 
    'master', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/select_Please select'), 
    'mastercard_eurocard', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/select_Please select'), 
    'diners', true)

WebUI.setText(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/input_(optional)_nameOnCard'), 
    'test')

WebUI.setText(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/input_Card Number_cardNumber'), 
    '1234567890123456')

WebUI.selectOptionByValue(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/select_Month'), 
    '1', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/select_Month'), 
    '2', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/select_Month'), 
    '3', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/select_Month'), 
    '4', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/select_Month'), 
    '5', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/select_Month'), 
    '6', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/select_Month'), 
    '7', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/select_Month'), 
    '8', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/select_Month'), 
    '9', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/select_Month'), 
    '10', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/select_Month'), 
    '11', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/select_Month'), 
    '12', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/select_Year 202820292030'), 
    '2020', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/select_Year 202820292030'), 
    '2021', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/select_Year 202820292030'), 
    '2022', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/select_Year 202820292030'), 
    '2023', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/select_Year 202820292030'), 
    '2024', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/select_Year 202820292030'), 
    '2025', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/select_Year 202820292030'), 
    '2026', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/select_Year 202820292030'), 
    '2027', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/select_Year 202820292030'), 
    '2028', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/select_Year 202820292030'), 
    '2029', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/select_Year 202820292030'), 
    '2030', true)

WebUI.setText(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/input_Card Verification'), 
    '123')

WebUI.click(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Add Payment Details  Rapid Hybris B2B/button_Save'))

WebUI.click(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Payment Details  Rapid Hybris B2B/span_France123456_glyphicon glyphicon-pencil'))

WebUI.click(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Edit Payment Details  Rapid Hybris B2B/button_Update'))

WebUI.click(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Payment Details  Rapid Hybris B2B/button_'))

WebUI.click(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Payment Details  Rapid Hybris B2B/span_France123456_glyphicon glyphicon-pencil'))

WebUI.click(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Edit Payment Details  Rapid Hybris B2B/button_Cancel'))

WebUI.click(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Rapid Hybris B2B  Homepage/span_Import Saved Cart_far fa-user_2'))

WebUI.click(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Rapid Hybris B2B  Homepage/a_Payment Details'))

WebUI.click(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Payment Details  Rapid Hybris B2B/span_France123456_glyphicon glyphicon-remove'))

WebUI.click(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Payment Details  Rapid Hybris B2B/a_Cancel'))

WebUI.click(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Payment Details  Rapid Hybris B2B/span_France123456_glyphicon glyphicon-remove'))

WebUI.click(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Payment Details  Rapid Hybris B2B/span_Delete Payment_glyphicon glyphicon-remove'))

WebUI.click(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Payment Details  Rapid Hybris B2B/span_France123456_glyphicon glyphicon-remove'))

WebUI.click(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Payment Details  Rapid Hybris B2B/button_Delete'))

WebUI.click(findTestObject('Object Repository/PaymentDetailsTestB_OR/Page_Payment Details  Rapid Hybris B2B/button_'))

