import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import java.util.concurrent.TimeUnit as TimeUnit
import org.openqa.selenium.WebDriver as WebDriver
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent


WebUI.openBrowser('')

WebUI.navigateToUrl("https://$GlobalVariable.Hostname")

WebUI.waitForPageLoad(30)

WebDriver driver = DriverFactory.getWebDriver()

driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS)

WebUI.maximizeWindow()

WebUI.click(findTestObject('Object Repository/SupportTicketsTestB1_OR/Page_Rapid Hybris B2B  Homepage/span_Import Saved Cart_far fa-user'))

WebUI.setText(findTestObject('Object Repository/SupportTicketsTestB1_OR/Page_Login  Rapid Hybris B2B/input_Email Address_j_username'), 
    "$GlobalVariable.Email")

WebUI.setEncryptedText(findTestObject('Object Repository/SupportTicketsTestB1_OR/Page_Login  Rapid Hybris B2B/input_Password_j_password'), 
    "$GlobalVariable.Password")

WebUI.click(findTestObject('Object Repository/SupportTicketsTestB1_OR/Page_Login  Rapid Hybris B2B/button_Log In'))

WebUI.click(findTestObject('Object Repository/SupportTicketsTestB1_OR/Page_Rapid Hybris B2B  Homepage/span_Import Saved Cart_far fa-user_1'))

WebUI.click(findTestObject('Object Repository/SupportTicketsTestB1_OR/Page_Rapid Hybris B2B  Homepage/a_Support Tickets'))

WebUI.click(findTestObject('Object Repository/SupportTicketsTestB1_OR/Page_Support Tickets listing  Rapid Hybris B2B/a_Add new'))

WebUI.setText(findTestObject('Object Repository/SupportTicketsTestB1_OR/Page_Add Support Ticket  Rapid Hybris B2B/input_Subject_subject'), 
    'test')

WebUI.click(findTestObject('Object Repository/SupportTicketsTestB1_OR/Page_Add Support Ticket  Rapid Hybris B2B/div_Subject Message Select a'))

WebUI.setText(findTestObject('Object Repository/SupportTicketsTestB1_OR/Page_Add Support Ticket  Rapid Hybris B2B/input_Subject_subject'), 
    'test')

WebUI.setText(findTestObject('Object Repository/SupportTicketsTestB1_OR/Page_Add Support Ticket  Rapid Hybris B2B/textarea_Message_message'), 
    'test')

WebUI.sendKeys(findTestObject('SupportTicketsTest_OR/Page_Add Support Ticket  Apparel Site UK/div_Choose file'), 'D:\\SBP Consulting Inc\\Hybris - C 4HANA - SAP Commerce\\03. QA\\ELiteKart-Screenshots\\Apparel\\D_01.PNG')

WebUI.selectOptionByValue(findTestObject('Object Repository/SupportTicketsTestB1_OR/Page_Add Support Ticket  Rapid Hybris B2B/select_EnquiryComplaintProblem'), 
    'COMPLAINT', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/SupportTicketsTestB1_OR/Page_Add Support Ticket  Rapid Hybris B2B/select_EnquiryComplaintProblem'), 
    'PROBLEM', true)

WebUI.click(findTestObject('Object Repository/SupportTicketsTestB1_OR/Page_Add Support Ticket  Rapid Hybris B2B/button_Submit'))

WebUI.click(findTestObject('Object Repository/SupportTicketsTestB1_OR/Page_Support Tickets listing  Rapid Hybris B2B/button_'))

WebUI.selectOptionByValue(findTestObject('Object Repository/SupportTicketsTestB1_OR/Page_Support Tickets listing  Rapid Hybris B2B/select_Sort by Date UpdateTicket ID'), 
    'byTicketId', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/SupportTicketsTestB1_OR/Page_Support Tickets listing  Rapid Hybris B2B/select_Sort by Date UpdateTicket ID'), 
    'byDate', true)

WebUI.click(findTestObject('Object Repository/SupportTicketsTestB1_OR/Page_Support Tickets listing  Rapid Hybris B2B/a_00046000'))

WebUI.click(findTestObject('Object Repository/SupportTicketsTestB1_OR/Page_Update Support Ticket  Rapid Hybris B2B/a_Add Message'))

WebUI.setText(findTestObject('Object Repository/SupportTicketsTestB1_OR/Page_Update Support Ticket  Rapid Hybris B2B/textarea_Message_message'), 
    'test')

WebUI.selectOptionByValue(findTestObject('Object Repository/SupportTicketsTestB1_OR/Page_Update Support Ticket  Rapid Hybris B2B/select_Open (current status)Closed'), 
    'CLOSED', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/SupportTicketsTestB1_OR/Page_Update Support Ticket  Rapid Hybris B2B/select_Open (current status)Closed'), 
    'OPEN', true)

WebUI.click(findTestObject('Object Repository/SupportTicketsTestB1_OR/Page_Update Support Ticket  Rapid Hybris B2B/button_Submit'))

WebUI.click(findTestObject('Object Repository/SupportTicketsTestB1_OR/Page_Support Tickets listing  Rapid Hybris B2B/button_'))

