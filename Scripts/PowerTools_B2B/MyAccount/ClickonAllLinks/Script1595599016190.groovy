import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import java.util.concurrent.TimeUnit
import org.openqa.selenium.WebDriver


import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent




WebUI.openBrowser('')

WebUI.navigateToUrl("https://$GlobalVariable.Hostname")

WebUI.waitForPageLoad(30)

WebDriver driver = DriverFactory.getWebDriver()

driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS)

WebUI.maximizeWindow()

WebUI.click(findTestObject('Object Repository/ClickonAllLinksTest_OR/Page_Rapid Hybris B2B  Homepage/span_Import Saved Cart_far fa-user'))

WebUI.setText(findTestObject('Object Repository/ClickonAllLinksTest_OR/Page_Login  Rapid Hybris B2B/input_Email Address_j_username'), 
    "$GlobalVariable.Email")

WebUI.setEncryptedText(findTestObject('Object Repository/ClickonAllLinksTest_OR/Page_Login  Rapid Hybris B2B/input_Password_j_password'), 
    "$GlobalVariable.Password")

WebUI.click(findTestObject('Object Repository/ClickonAllLinksTest_OR/Page_Login  Rapid Hybris B2B/button_Log In'))

WebUI.click(findTestObject('Object Repository/ClickonAllLinksTest_OR/Page_Rapid Hybris B2B  Homepage/span_Import Saved Cart_far fa-user_1'))

WebUI.click(findTestObject('Object Repository/ClickonAllLinksTest_OR/Page_Rapid Hybris B2B  Homepage/a_Payment Details'))

WebUI.click(findTestObject('Object Repository/ClickonAllLinksTest_OR/Page_Payment Details  Rapid Hybris B2B/span_Import Saved Cart_far fa-user'))

WebUI.click(findTestObject('Object Repository/ClickonAllLinksTest_OR/Page_Payment Details  Rapid Hybris B2B/a_Personal Details'))

WebUI.click(findTestObject('Object Repository/ClickonAllLinksTest_OR/Page_Update Profile  Rapid Hybris B2B/span_Import Saved Cart_far fa-user'))

WebUI.click(findTestObject('Object Repository/ClickonAllLinksTest_OR/Page_Update Profile  Rapid Hybris B2B/a_Password'))

WebUI.click(findTestObject('Object Repository/ClickonAllLinksTest_OR/Page_Update Forgotten Password  Rapid Hybris B2B/span_Import Saved Cart_far fa-user'))

WebUI.click(findTestObject('Object Repository/ClickonAllLinksTest_OR/Page_Update Forgotten Password  Rapid Hybris B2B/a_Email Address'))

WebUI.click(findTestObject('Object Repository/ClickonAllLinksTest_OR/Page_Update Email  Rapid Hybris B2B/span_Import Saved Cart_far fa-user'))

WebUI.click(findTestObject('Object Repository/ClickonAllLinksTest_OR/Page_Update Email  Rapid Hybris B2B/a_Order History'))

WebUI.click(findTestObject('Object Repository/ClickonAllLinksTest_OR/Page_Order History  Rapid Hybris B2B/span_Import Saved Cart_far fa-user'))

WebUI.click(findTestObject('Object Repository/ClickonAllLinksTest_OR/Page_Order History  Rapid Hybris B2B/a_Quotes'))

WebUI.click(findTestObject('Object Repository/ClickonAllLinksTest_OR/Page_Quotes  Rapid Hybris B2B/span_Import Saved Cart_far fa-user'))

WebUI.click(findTestObject('Object Repository/ClickonAllLinksTest_OR/Page_Quotes  Rapid Hybris B2B/a_Address Book'))

WebUI.click(findTestObject('Object Repository/ClickonAllLinksTest_OR/Page_Address Book  Rapid Hybris B2B/span_Import Saved Cart_far fa-user'))

WebUI.click(findTestObject('Object Repository/ClickonAllLinksTest_OR/Page_Address Book  Rapid Hybris B2B/a_Replenishment Orders'))

WebUI.click(findTestObject('Object Repository/ClickonAllLinksTest_OR/Page_Replenishment Orders  Rapid Hybris B2B/span_Import Saved Cart_far fa-user'))

WebUI.click(findTestObject('Object Repository/ClickonAllLinksTest_OR/Page_Replenishment Orders  Rapid Hybris B2B/a_Saved Carts'))

WebUI.click(findTestObject('Object Repository/ClickonAllLinksTest_OR/Page_Saved Carts  Rapid Hybris B2B/span_Import Saved Cart_far fa-user'))

WebUI.click(findTestObject('Object Repository/ClickonAllLinksTest_OR/Page_Saved Carts  Rapid Hybris B2B/a_Returns History'))

WebUI.click(findTestObject('Object Repository/ClickonAllLinksTest_OR/Page_Returns Listing  Rapid Hybris B2B/span_Import Saved Cart_far fa-user'))

WebUI.click(findTestObject('Object Repository/ClickonAllLinksTest_OR/Page_Returns Listing  Rapid Hybris B2B/a_Support Tickets'))

WebUI.click(findTestObject('Object Repository/ClickonAllLinksTest_OR/Page_Support Tickets listing  Rapid Hybris B2B/span_Import Saved Cart_far fa-user'))

