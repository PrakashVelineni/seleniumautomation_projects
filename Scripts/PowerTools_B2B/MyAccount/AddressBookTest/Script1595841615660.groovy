import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import java.util.concurrent.TimeUnit as TimeUnit
import org.openqa.selenium.WebDriver as WebDriver
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent

WebUI.openBrowser('')

WebUI.navigateToUrl("https://$GlobalVariable.Hostname")

WebUI.waitForPageLoad(30)

WebDriver driver = DriverFactory.getWebDriver()

driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS)

WebUI.maximizeWindow()

WebUI.click(findTestObject('Object Repository/AddressBookTestB_OR/Page_Rapid Hybris B2B  Homepage/span_Import Saved Cart_far fa-user'))

WebUI.setText(findTestObject('Object Repository/AddressBookTestB_OR/Page_Login  Rapid Hybris B2B/input_Email Address_j_username'), 
    "$GlobalVariable.Email")

WebUI.setEncryptedText(findTestObject('Object Repository/AddressBookTestB_OR/Page_Login  Rapid Hybris B2B/input_Password_j_password'), 
    "$GlobalVariable.Password")

WebUI.click(findTestObject('Object Repository/QuoteTestB1_OR/Page_Login  Rapid Hybris B2B/button_Log In'))

WebUI.click(findTestObject('Object Repository/AddressBookTestB_OR/Page_Rapid Hybris B2B  Homepage/span_Import Saved Cart_far fa-user_1'))

WebUI.click(findTestObject('Object Repository/AddressBookTestB_OR/Page_Rapid Hybris B2B  Homepage/a_Address Book'))

WebUI.click(findTestObject('Object Repository/AddressBookTestB_OR/Page_Address Book  Rapid Hybris B2B/a_Add Address'))

WebUI.selectOptionByValue(findTestObject('Object Repository/AddressBookTestB_OR/Page_AddEdit Address  Rapid Hybris B2B/select_CountryHolySee'), 
    'AD', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/AddressBookTestB_OR/Page_AddEdit Address  Rapid Hybris B2B/select_CountryHolySee'), 
    'EE', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/AddressBookTestB_OR/Page_AddEdit Address  Rapid Hybris B2B/select_CountryHolySee'), 
    'GL', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/AddressBookTestB_OR/Page_AddEdit Address  Rapid Hybris B2B/select_None MrMrsMissCompanyMsDrRevProf'), 
    'Mr', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/AddressBookTestB_OR/Page_AddEdit Address  Rapid Hybris B2B/select_None MrMrsMissCompanyMsDrRevProf'), 
    'Mrs', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/AddressBookTestB_OR/Page_AddEdit Address  Rapid Hybris B2B/select_None MrMrsMissCompanyMsDrRevProf'), 
    'Miss', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/AddressBookTestB_OR/Page_AddEdit Address  Rapid Hybris B2B/select_None MrMrsMissCompanyMsDrRevProf'), 
    'Company', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/AddressBookTestB_OR/Page_AddEdit Address  Rapid Hybris B2B/select_None MrMrsMissCompanyMsDrRevProf'), 
    'Ms', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/AddressBookTestB_OR/Page_AddEdit Address  Rapid Hybris B2B/select_None MrMrsMissCompanyMsDrRevProf'), 
    'Dr.', true)

WebUI.click(findTestObject('Object Repository/AddressBookTestB_OR/Page_AddEdit Address  Rapid Hybris B2B/div_Title MrMrsMissCompanyMs'))

WebUI.selectOptionByValue(findTestObject('Object Repository/AddressBookTestB_OR/Page_AddEdit Address  Rapid Hybris B2B/select_None MrMrsMissCompanyMsDrRevProf'), 
    'Rev.', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/AddressBookTestB_OR/Page_AddEdit Address  Rapid Hybris B2B/select_None MrMrsMissCompanyMsDrRevProf'), 
    'Prof.', true)

WebUI.setText(findTestObject('Object Repository/AddressBookTestB_OR/Page_AddEdit Address  Rapid Hybris B2B/input_First Name_firstName'), 
    'test')

WebUI.setText(findTestObject('Object Repository/AddressBookTestB_OR/Page_AddEdit Address  Rapid Hybris B2B/input_Last Name_lastName'), 
    'test')

WebUI.setText(findTestObject('Object Repository/AddressBookTestB_OR/Page_AddEdit Address  Rapid Hybris B2B/input_Address Line 1_line1'), 
    'test')

WebUI.setText(findTestObject('Object Repository/AddressBookTestB_OR/Page_AddEdit Address  Rapid Hybris B2B/input_(optional)_line2'), 
    'test')

WebUI.setText(findTestObject('Object Repository/AddressBookTestB_OR/Page_AddEdit Address  Rapid Hybris B2B/input_TownCity_townCity'), 
    'test')

WebUI.setText(findTestObject('Object Repository/AddressBookTestB_OR/Page_AddEdit Address  Rapid Hybris B2B/input_Postcode_postcode'), 
    '123456')

WebUI.setText(findTestObject('Object Repository/AddressBookTestB_OR/Page_AddEdit Address  Rapid Hybris B2B/input_(optional)_phone'), 
    '1234567890')

WebUI.click(findTestObject('Object Repository/AddressBookTestB_OR/Page_AddEdit Address  Rapid Hybris B2B/input_Make this'))

WebUI.click(findTestObject('Object Repository/AddressBookTestB_OR/Page_AddEdit Address  Rapid Hybris B2B/button_Save'))

WebUI.click(findTestObject('Object Repository/AddressBookTestB_OR/Page_Address Book  Rapid Hybris B2B/button_'))

WebUI.click(findTestObject('Object Repository/AddressBookTestB_OR/Page_Address Book  Rapid Hybris B2B/span_Greenland123456_glyphicon glyphicon-pencil'))

WebUI.click(findTestObject('Object Repository/AddressBookTestB_OR/Page_AddEdit Address  Rapid Hybris B2B/button_Save'))

WebUI.click(findTestObject('Object Repository/AddressBookTestB_OR/Page_Address Book  Rapid Hybris B2B/button_'))

WebUI.click(findTestObject('Object Repository/AddressBookTestB_OR/Page_Address Book  Rapid Hybris B2B/span_Greenland123456_glyphicon glyphicon-remove'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/AddressBookTestB_OR/Page_Address Book  Rapid Hybris B2B/a_Cancel'))

WebUI.click(findTestObject('Object Repository/AddressBookTestB_OR/Page_Address Book  Rapid Hybris B2B/span_Greenland123456_glyphicon glyphicon-remove'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/AddressBookTestB_OR/Page_Address Book  Rapid Hybris B2B/span_Delete Address_glyphicon glyphicon-remove'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/AddressBookTestB_OR/Page_Address Book  Rapid Hybris B2B/span_Greenland123456_glyphicon glyphicon-remove'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/AddressBookTestB_OR/Page_Address Book  Rapid Hybris B2B/span_Delete Address_glyphicon glyphicon-remove'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/AddressBookTestB_OR/Page_Address Book  Rapid Hybris B2B/span_Greenland123456_glyphicon glyphicon-pencil'))

WebUI.scrollToPosition(0, 1000)

WebUI.delay(2)

WebUI.click(findTestObject('AddressBookTestB_OR/Page_AddEdit Address  Rapid Hybris B2B/a_CancelN'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/AddressBookTestB_OR/Page_Address Book  Rapid Hybris B2B/span_Greenland123456_glyphicon glyphicon-remove'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/AddressBookTestB_OR/Page_Address Book  Rapid Hybris B2B/a_Delete'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/AddressBookTestB_OR/Page_Address Book  Rapid Hybris B2B/button_'))

