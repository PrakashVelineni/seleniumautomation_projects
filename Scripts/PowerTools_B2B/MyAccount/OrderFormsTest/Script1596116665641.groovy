import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import java.util.concurrent.TimeUnit as TimeUnit
import org.openqa.selenium.WebDriver as WebDriver
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent

WebUI.openBrowser('')

WebUI.navigateToUrl("https://$GlobalVariable.Hostname")

WebUI.waitForPageLoad(30)

WebDriver driver = DriverFactory.getWebDriver()

driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS)

WebUI.maximizeWindow()

WebUI.click(findTestObject('Object Repository/OrderFormsTest_OR/Page_Rapid Hybris B2B  Homepage/span_Import Saved Cart_far fa-user'))

WebUI.setText(findTestObject('Object Repository/OrderFormsTest_OR/Page_Login  Rapid Hybris B2B/input_Email Address_j_username'), 
    "$GlobalVariable.Email")

WebUI.setEncryptedText(findTestObject('Object Repository/OrderFormsTest_OR/Page_Login  Rapid Hybris B2B/input_Password_j_password'), 
    "$GlobalVariable.Password")

WebUI.click(findTestObject('Object Repository/OrderFormsTest_OR/Page_Login  Rapid Hybris B2B/button_Log In'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/OrderFormsTest_OR/Page_Rapid Hybris B2B  Homepage/div_Quick Order'))

WebUI.delay(2)

WebUI.click(findTestObject('OrderFormsTest_OR/Page_Rapid Hybris B2B  Homepage/a_Order FormsN'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/OrderFormsTest_OR/Page_Order Forms  Rapid Hybris B2B/input_Keyword Search_keywords'), 
    '3794514')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/OrderFormsTest_OR/Page_Order Forms  Rapid Hybris B2B/button_Search'))

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/OrderFormsTest_OR/Page_Order Forms  Rapid Hybris B2B/select_Sort by'), 
    'topRated', true)

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/OrderFormsTest_OR/Page_Order Forms  Rapid Hybris B2B/select_Sort by'), 
    'name-asc', true)

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/OrderFormsTest_OR/Page_Order Forms  Rapid Hybris B2B/select_Sort by'), 
    'name-desc', true)

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/OrderFormsTest_OR/Page_Order Forms  Rapid Hybris B2B/select_Sort by'), 
    'price-asc', true)

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/OrderFormsTest_OR/Page_Order Forms  Rapid Hybris B2B/select_Sort by'), 
    'price-desc', true)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/OrderFormsTest_OR/Page_Order Forms  Rapid Hybris B2B/div_18V Auto Select cordless drill'))

WebUI.delay(2)

WebUI.back()

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/OrderFormsTest_OR/Page_Order Forms  Rapid Hybris B2B/input_Qty_cartEntries0quantity'), 
    '1')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/OrderFormsTest_OR/Page_Order Forms  Rapid Hybris B2B/div_Qty            1350'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/OrderFormsTest_OR/Page_Order Forms  Rapid Hybris B2B/input_Qty_cartEntries1quantity'), 
    '1')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/OrderFormsTest_OR/Page_Order Forms  Rapid Hybris B2B/div_128'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/OrderFormsTest_OR/Page_Order Forms  Rapid Hybris B2B/button_Add to cart'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/OrderFormsTest_OR/Page_Order Forms  Rapid Hybris B2B/a_Check Out'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/OrderFormsTest_OR/Page_Your Shopping Cart  Rapid Hybris B2B/div_Quick Order'))

WebUI.delay(2)

WebUI.click(findTestObject('OrderFormsTest_OR/Page_Rapid Hybris B2B  Homepage/a_Order FormsN'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/OrderFormsTest_OR/Page_Order Forms  Rapid Hybris B2B/input_Keyword Search_keywords'), 
    '3794514')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/OrderFormsTest_OR/Page_Order Forms  Rapid Hybris B2B/button_Search'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/OrderFormsTest_OR/Page_Order Forms  Rapid Hybris B2B/input_Order Form_searchResultType'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/OrderFormsTest_OR/Page_Order Forms  Rapid Hybris B2B/button_Search'))

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/OrderFormsTest_OR/Page_Order Forms  Rapid Hybris B2B/select_Sort by'), 
    'topRated', true)

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/OrderFormsTest_OR/Page_Order Forms  Rapid Hybris B2B/select_Sort by'), 
    'name-asc', true)

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/OrderFormsTest_OR/Page_Order Forms  Rapid Hybris B2B/select_Sort by'), 
    'name-desc', true)

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/OrderFormsTest_OR/Page_Order Forms  Rapid Hybris B2B/select_Sort by'), 
    'price-asc', true)

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/OrderFormsTest_OR/Page_Order Forms  Rapid Hybris B2B/select_Sort by'), 
    'price-desc', true)

WebUI.delay(2)

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/OrderFormsTest_OR/Page_Order Forms  Rapid Hybris B2B/input_Item price_js-checkbox-sku-id'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/OrderFormsTest_OR/Page_Order Forms  Rapid Hybris B2B/button_Create Order Form'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/OrderFormsTest_OR/Page_Order Forms  Rapid Hybris B2B/input_Qty_cartEntries0quantity'), 
    '1')

WebUI.delay(2)

WebUI.click(findTestObject('OrderFormsTest_OR/Page_Order Forms  Rapid Hybris B2B/li_Subtotal  13500N'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/OrderFormsTest_OR/Page_Order Forms  Rapid Hybris B2B/button_Add to cart'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/OrderFormsTest_OR/Page_Order Forms  Rapid Hybris B2B/span_Added to Your Shopping Cart_glyphicon glyphicon-remove'))

WebUI.delay(5)

WebUI.scrollToPosition(500, 0)

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/OrderFormsTest_OR/Page_Order Forms  Rapid Hybris B2B/input_Order Form_searchResultType'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/OrderFormsTest_OR/Page_Order Forms  Rapid Hybris B2B/button_Search'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/OrderFormsTest_OR/Page_Order Forms  Rapid Hybris B2B/button_Create Order Form'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.setText(findTestObject('OrderFormsTest_OR/Page_Order Forms  Rapid Hybris B2B/input_Qty_cartEntries0quantity'), '1')

WebUI.delay(2)

WebUI.click(findTestObject('OrderFormsTest_OR/Page_Order Forms  Rapid Hybris B2B/li_Subtotal  13500N'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/OrderFormsTest_OR/Page_Order Forms  Rapid Hybris B2B/button_Add to cart_1'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/OrderFormsTest_OR/Page_Order Forms  Rapid Hybris B2B/span_Added to Your Shopping Cart_glyphicon glyphicon-remove'))

WebUI.delay(2)

WebUI.scrollToPosition(500, 0)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/OrderFormsTest_OR/Page_Order Forms  Rapid Hybris B2B/input_Product IDs Only_onlyProductIds'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/OrderFormsTest_OR/Page_Order Forms  Rapid Hybris B2B/input_Product IDs Only_onlyProductIds'))

WebUI.delay(2)

