import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import java.util.concurrent.TimeUnit
import org.openqa.selenium.WebDriver


import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent




WebUI.openBrowser('')

WebUI.navigateToUrl("https://$GlobalVariable.Hostname")

WebUI.waitForPageLoad(30)

WebDriver driver = DriverFactory.getWebDriver()

driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS)

WebUI.maximizeWindow()

WebUI.click(findTestObject('Object Repository/PasswordTestB_OR/Page_Rapid Hybris B2B  Homepage/span_Import Saved Cart_far fa-user'))

WebUI.setText(findTestObject('Object Repository/PasswordTestB_OR/Page_Login  Rapid Hybris B2B/input_Email Address_j_username'), 
    "$GlobalVariable.Email")

WebUI.setEncryptedText(findTestObject('Object Repository/PasswordTestB_OR/Page_Login  Rapid Hybris B2B/input_Password_j_password'), 
    "$GlobalVariable.Password")

WebUI.click(findTestObject('Object Repository/PasswordTestB_OR/Page_Login  Rapid Hybris B2B/button_Log In'))

WebUI.click(findTestObject('Object Repository/PasswordTestB_OR/Page_Rapid Hybris B2B  Homepage/span_Import Saved Cart_far fa-user_1'))

WebUI.click(findTestObject('Object Repository/PasswordTestB_OR/Page_Rapid Hybris B2B  Homepage/a_Password'))

WebUI.setEncryptedText(findTestObject('Object Repository/PasswordTestB_OR/Page_Update Forgotten Password  Rapid Hybris B2B/input_Current Password_currentPassword'), 
    "$GlobalVariable.Password")

WebUI.setEncryptedText(findTestObject('Object Repository/PasswordTestB_OR/Page_Update Forgotten Password  Rapid Hybris B2B/input_New Password_newPassword'), 
    "$GlobalVariable.Password")

WebUI.setEncryptedText(findTestObject('Object Repository/PasswordTestB_OR/Page_Update Forgotten Password  Rapid Hybris B2B/input_Confirm New Password_checkNewPassword'), 
    "$GlobalVariable.Password")

WebUI.click(findTestObject('Object Repository/PasswordTestB_OR/Page_Update Forgotten Password  Rapid Hybris B2B/button_Update'))

WebUI.click(findTestObject('Object Repository/PasswordTestB_OR/Page_Update Forgotten Password  Rapid Hybris B2B/button_'))

WebUI.click(findTestObject('Object Repository/PasswordTestB_OR/Page_Update Forgotten Password  Rapid Hybris B2B/button_Cancel'))

