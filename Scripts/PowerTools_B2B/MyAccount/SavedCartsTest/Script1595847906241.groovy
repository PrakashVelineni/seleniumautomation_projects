import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import java.util.concurrent.TimeUnit as TimeUnit
import org.openqa.selenium.WebDriver as WebDriver
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent

WebUI.openBrowser('')

WebUI.navigateToUrl("https://$GlobalVariable.Hostname")

WebUI.waitForPageLoad(30)

WebDriver driver = DriverFactory.getWebDriver()

driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS)

WebUI.maximizeWindow()

WebUI.click(findTestObject('Object Repository/SavedCartTestB_OR/Page_Rapid Hybris B2B  Homepage/span_Import Saved Cart_far fa-user'))

WebUI.setText(findTestObject('Object Repository/SavedCartTestB_OR/Page_Login  Rapid Hybris B2B/input_Email Address_j_username'), 
    "$GlobalVariable.Email")

WebUI.setEncryptedText(findTestObject('Object Repository/SavedCartTestB_OR/Page_Login  Rapid Hybris B2B/input_Password_j_password'), 
    "$GlobalVariable.Password")

WebUI.click(findTestObject('Object Repository/SavedCartTestB_OR/Page_Login  Rapid Hybris B2B/button_Log In'))

WebUI.click(findTestObject('Object Repository/SavedCartTestB_OR/Page_Rapid Hybris B2B  Homepage/span_Import Saved Cart_far fa-user_1'))

WebUI.click(findTestObject('Object Repository/SavedCartTestB_OR/Page_Rapid Hybris B2B  Homepage/a_Saved Carts'))

WebUI.selectOptionByValue(findTestObject('Object Repository/SavedCartTestB_OR/Page_Saved Carts  Rapid Hybris B2B/select_Sort by Date Modified Date Saved'), 
    'byDateSaved', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/SavedCartTestB_OR/Page_Saved Carts  Rapid Hybris B2B/select_Sort by Date Modified Date Saved'), 
    'byName', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/SavedCartTestB_OR/Page_Saved Carts  Rapid Hybris B2B/select_Sort by Date Modified Date Saved'), 
    'byCode', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/SavedCartTestB_OR/Page_Saved Carts  Rapid Hybris B2B/select_Sort by Date Modified Date Saved'), 
    'byTotal', true)

WebUI.click(findTestObject('Object Repository/SavedCartTestB_OR/Page_Saved Carts  Rapid Hybris B2B/a_1593584991503'))

WebUI.click(findTestObject('Object Repository/SavedCartTestB_OR/Page_Saved Cart Details  Rapid Hybris B2B/button_Edit'))

WebUI.click(findTestObject('Object Repository/SavedCartTestB_OR/Page_Saved Cart Details  Rapid Hybris B2B/span_Edit Saved Cart_glyphicon glyphicon-remove'))

WebUI.back()

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/SavedCartTestB_OR/Page_Saved Carts  Rapid Hybris B2B/a_2'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/SavedCartTestB_OR/Page_Saved Carts  Rapid Hybris B2B/a_1'))

WebUI.delay(2)

WebUI.click(findTestObject('SavedCartTestB_OR/Page_Saved Carts  Powertools Site/a_(current)_glyphicon glyphicon-chevron-right'))

WebUI.delay(2)

WebUI.click(findTestObject('SavedCartTestB_OR/Page_Saved Carts  Powertools Site/a_Grid_glyphicon glyphicon-chevron-left'))

