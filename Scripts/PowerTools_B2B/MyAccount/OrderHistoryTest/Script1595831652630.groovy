import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import java.util.concurrent.TimeUnit as TimeUnit
import org.openqa.selenium.WebDriver as WebDriver
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent

WebUI.openBrowser('')

WebUI.navigateToUrl("https://$GlobalVariable.Hostname")

WebUI.waitForPageLoad(30)

WebDriver driver = DriverFactory.getWebDriver()

driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS)

WebUI.maximizeWindow()

WebUI.click(findTestObject('Object Repository/OrderHistoryTestB_OR/Page_Rapid Hybris B2B  Homepage/span_Import Saved Cart_far fa-user'))

WebUI.setText(findTestObject('Object Repository/OrderHistoryTestB_OR/Page_Login  Rapid Hybris B2B/input_Email Address_j_username'), 
    "$GlobalVariable.Email")

WebUI.setEncryptedText(findTestObject('Object Repository/OrderHistoryTestB_OR/Page_Login  Rapid Hybris B2B/input_Password_j_password'), 
    "$GlobalVariable.Password")

WebUI.click(findTestObject('Object Repository/OrderHistoryTestB_OR/Page_Login  Rapid Hybris B2B/button_Log In'))

WebUI.click(findTestObject('Object Repository/OrderHistoryTestB_OR/Page_Rapid Hybris B2B  Homepage/span_Import Saved Cart_far fa-user_1'))

WebUI.delay(3)

WebUI.click(findTestObject('OrderHistoryTestB_OR/Page_Rapid Hybris B2B  Homepage/a_Order History'))

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('Object Repository/OrderHistoryTestB_OR/Page_Order History  Rapid Hybris B2B/select_Sort by  Date Order Number'), 
    'byOrderNumber', true)

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('Object Repository/OrderHistoryTestB_OR/Page_Order History  Rapid Hybris B2B/select_Sort by  Date Order Number'), 
    'byDate', true)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/OrderHistoryTestB_OR/Page_Order History  Rapid Hybris B2B/a_0005622915'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/OrderHistoryTestB_OR/Page_Order Details  Rapid Hybris B2B/button_Reorder'))

WebUI.back()

WebUI.click(findTestObject('Object Repository/OrderHistoryTestB_OR/Page_Order Details  Rapid Hybris B2B/button_Cancel Order'))

WebUI.back()

WebUI.back()

WebUI.scrollToPosition(0, 500)

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('Object Repository/OrderHistoryTestB_OR/Page_Order History  Rapid Hybris B2B/select_Sort by  Date  Order Number_1'), 
    'byOrderNumber', true)

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('Object Repository/OrderHistoryTestB_OR/Page_Order History  Rapid Hybris B2B/select_Sort  Date  Order Number_2'), 
    'byDate', true)

WebUI.delay(3)

WebUI.scrollToPosition(500, 0)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/OrderHistoryTestB_OR/Page_Order History  Rapid Hybris B2B/a_(current)_glyphicon glyphicon-chevron-right'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/OrderHistoryTestB_OR/Page_Order History  Rapid Hybris B2B/a_Grid_glyphicon glyphicon-chevron-left'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/OrderHistoryTestB_OR/Page_Order History  Rapid Hybris B2B/a_2'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/OrderHistoryTestB_OR/Page_Order History  Rapid Hybris B2B/a_1'))

WebUI.scrollToPosition(0, 500)

/*WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/OrderHistoryTestB_OR/Page_Order History  Rapid Hybris B2B/a_(current)_glyphicon glyphicon-chevron-right'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/OrderHistoryTestB_OR/Page_Order History  Rapid Hybris B2B/a_Grid_glyphicon glyphicon-chevron-left'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/OrderHistoryTestB_OR/Page_Order History  Rapid Hybris B2B/a_2'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/OrderHistoryTestB_OR/Page_Order History  Rapid Hybris B2B/a_1'))

WebUI.delay(3)*/
WebUI.delay(3)

WebUI.click(findTestObject('OrderHistoryTestB_OR/Page_Order History  Rapid Hybris B2B/a_(current)_glyphicon glyphicon-chevron-rightN'))

WebUI.delay(3)

WebUI.click(findTestObject('OrderHistoryTestB_OR/Page_Order History  Rapid Hybris B2B/a_Grid_glyphicon glyphicon-chevron-leftN'))

WebUI.delay(3)

WebUI.click(findTestObject('OrderHistoryTestB_OR/Page_Order History  Rapid Hybris B2B/a_2N'))

WebUI.delay(3)

WebUI.click(findTestObject('OrderHistoryTestB_OR/Page_Order History  Rapid Hybris B2B/a_1N'))

