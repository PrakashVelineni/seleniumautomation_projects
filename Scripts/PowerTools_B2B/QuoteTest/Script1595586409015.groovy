import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys


import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import java.util.concurrent.TimeUnit
import org.openqa.selenium.WebDriver


import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent




WebUI.openBrowser('')

WebUI.navigateToUrl("https://$GlobalVariable.Hostname")

WebUI.waitForPageLoad(30)

WebDriver driver = DriverFactory.getWebDriver()

driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS)

WebUI.maximizeWindow()

WebUI.click(findTestObject('Object Repository/QuoteTest_OR/Page_Rapid Hybris B2B  Homepage/span_Import Saved Cart_far fa-user'))

WebUI.setText(findTestObject('Object Repository/QuoteTest_OR/Page_Login  Rapid Hybris B2B/input_Email Address_j_username'), 
    "$GlobalVariable.Email")

WebUI.setEncryptedText(findTestObject('Object Repository/QuoteTest_OR/Page_Login  Rapid Hybris B2B/input_Password_j_password'), 
    "$GlobalVariable.Password")

WebUI.scrollToPosition(0, 250)

WebUI.click(findTestObject('Object Repository/QuoteTest_OR/Page_Login  Rapid Hybris B2B/button_Log In'))

WebUI.click(findTestObject('Object Repository/QuoteTest_OR/Page_Rapid Hybris B2B  Homepage/a_Power Drills'))

WebUI.scrollToPosition(0, 500)

WebUI.mouseOver(findTestObject('Object Repository/QuoteTest_OR/Page_Power Drills  Tools  Open Catalogue  Rapid Hybris B2B/button_7900_fas fa-cart-plus custom-cart js-enable-btn rh_cart'))

WebUI.click(findTestObject('Object Repository/QuoteTest_OR/Page_Power Drills  Tools  Open Catalogue  Rapid Hybris B2B/button_7900_fas fa-cart-plus custom-cart js-enable-btn rh_cart'))

WebUI.click(findTestObject('Object Repository/QuoteTest_OR/Page_Power Drills  Tools  Open Catalogue  Rapid Hybris B2B/a_Check Out'))

WebUI.click(findTestObject('Object Repository/QuoteTest_OR/Page_Your Shopping Cart  Rapid Hybris B2B/button_Request a Quote'))

WebUI.click(findTestObject('Object Repository/QuoteTest_OR/Page_Quote Edit  Rapid Hybris B2B/button_Cancel Quote'))

WebUI.click(findTestObject('Object Repository/QuoteTest_OR/Page_Quote Edit  Rapid Hybris B2B/span_Cancel Quote 0005622920_glyphicon glyphicon-remove'))

WebUI.click(findTestObject('Object Repository/QuoteTest_OR/Page_Quote Edit  Rapid Hybris B2B/button_Cancel Quote'))

WebUI.click(findTestObject('Object Repository/QuoteTest_OR/Page_Quote Edit  Rapid Hybris B2B/button_No'))

WebUI.click(findTestObject('Object Repository/QuoteTest_OR/Page_Quote Edit  Rapid Hybris B2B/button_Cancel Quote'))

WebUI.click(findTestObject('Object Repository/QuoteTest_OR/Page_Quote Edit  Rapid Hybris B2B/button_Yes'))

WebUI.click(findTestObject('Object Repository/QuoteTest_OR/Page_Quotes  Rapid Hybris B2B/button_'))

WebUI.click(findTestObject('Object Repository/QuoteTest_OR/Page_Quotes  Rapid Hybris B2B/a_Quote 0005622920'))

WebUI.click(findTestObject('Object Repository/QuoteTest_OR/Page_My Quote Details  Rapid Hybris B2B/button_Requote'))

WebUI.scrollToPosition(0, 750)

WebUI.delay(5)

WebUI.setText(findTestObject('Object Repository/QuoteTest_OR/Page_Quote Edit  Rapid Hybris B2B/input_Import Saved Cart_text'), 
    'megamachines')

WebUI.delay(5)

Robot robot = new Robot()

robot.keyPress(KeyEvent.VK_ENTER)

robot.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(5)

WebUI.scrollToPosition(0, 750)

WebUI.mouseOver(findTestObject('Object Repository/QuoteTest_OR/Page_Search megamachines  Rapid Hybris B2B/button_9874950_fas fa-cart-plus custom-cart js-enable-btn rh_cart'))

WebUI.click(findTestObject('Object Repository/QuoteTest_OR/Page_Search megamachines  Rapid Hybris B2B/button_9874950_fas fa-cart-plus custom-cart js-enable-btn rh_cart'))

WebUI.click(findTestObject('Object Repository/QuoteTest_OR/Page_Search megamachines  Rapid Hybris B2B/a_View Quote'))

WebUI.click(findTestObject('Object Repository/QuoteTest_OR/Page_Quote Edit  Rapid Hybris B2B/button_Submit Quote'))

WebUI.click(findTestObject('Object Repository/QuoteTest_OR/Page_Quote Edit  Rapid Hybris B2B/span_Confirm Requested Quote 0005622924_glyphicon glyphicon-remove'))

WebUI.click(findTestObject('Object Repository/QuoteTest_OR/Page_Quote Edit  Rapid Hybris B2B/button_Submit Quote'))

WebUI.click(findTestObject('Object Repository/QuoteTest_OR/Page_Quote Edit  Rapid Hybris B2B/button_No_1'))

WebUI.click(findTestObject('Object Repository/QuoteTest_OR/Page_Quote Edit  Rapid Hybris B2B/button_Submit Quote'))

WebUI.click(findTestObject('Object Repository/QuoteTest_OR/Page_Quote Edit  Rapid Hybris B2B/button_Yes_1'))

WebUI.click(findTestObject('Object Repository/QuoteTest_OR/Page_Quotes  Rapid Hybris B2B/button_'))

WebUI.click(findTestObject('Object Repository/QuoteTest_OR/Page_Quotes  Rapid Hybris B2B/a_Quote 0005622924'))

WebUI.click(findTestObject('Object Repository/QuoteTest_OR/Page_Angle Grinders  Tools  Open Catalogue  Rapid Hybris B2B/a_Power Drills'))

WebUI.scrollToPosition(0, 500)

WebUI.mouseOver(findTestObject('Object Repository/QuoteTest_OR/Page_Power Drills  Tools  Open Catalogue  Rapid Hybris B2B/button_13500_fas fa-cart-plus custom-cart js-enable-btn rh_cart'))

WebUI.click(findTestObject('Object Repository/QuoteTest_OR/Page_Power Drills  Tools  Open Catalogue  Rapid Hybris B2B/button_13500_fas fa-cart-plus custom-cart js-enable-btn rh_cart'))

WebUI.click(findTestObject('Object Repository/QuoteTest_OR/Page_Power Drills  Tools  Open Catalogue  Rapid Hybris B2B/a_Check Out'))

WebUI.click(findTestObject('Object Repository/QuoteTest_OR/Page_Your Shopping Cart  Rapid Hybris B2B/button_Request a Quote'))

WebUI.click(findTestObject('Object Repository/QuoteTest_OR/Page_Quote Edit  Rapid Hybris B2B/button_Submit Quote'))

WebUI.click(findTestObject('Object Repository/QuoteTest_OR/Page_Quote Edit  Rapid Hybris B2B/button_Yes_2'))

WebUI.click(findTestObject('Object Repository/QuoteTest_OR/Page_Quote Edit  Rapid Hybris B2B/button_'))

WebUI.click(findTestObject('Object Repository/QuoteTest_OR/Page_Quote Edit  Rapid Hybris B2B/a_Quotes (4)'))

WebUI.click(findTestObject('Object Repository/QuoteTest_OR/Page_Quotes  Rapid Hybris B2B/span_Import Saved Cart_far fa-user'))

WebUI.click(findTestObject('Object Repository/QuoteTest_OR/Page_Quotes  Rapid Hybris B2B/a_Quotes'))

