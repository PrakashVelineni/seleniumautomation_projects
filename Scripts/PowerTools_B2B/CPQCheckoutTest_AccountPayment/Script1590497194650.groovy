import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import static com.yourcompany.ConnectionNotPrivate.*
import com.yourcompany.ConnectionNotPrivate as ConnectionNotPrivate

//ConnectionNotPrivate.yourMethod()
WebUI.openBrowser('')

WebUI.navigateToUrl("http://$GlobalVariable.Hostname")

WebUI.maximizeWindow()

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_AccountPayment_OR/Page_Rapid Hybris B2B  Homepage/span_Quick Order_far fa-user'))

WebUI.setText(findTestObject('Object Repository/CPQCheckoutTest_AccountPayment_OR/Page_Login  Rapid Hybris B2B/input_Email Address_j_username'), 
    "$GlobalVariable.Email")

WebUI.setEncryptedText(findTestObject('Object Repository/CPQCheckoutTest_AccountPayment_OR/Page_Login  Rapid Hybris B2B/input_Password_j_password'), 
    "$GlobalVariable.Password")

WebUI.scrollToPosition(0, 100)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_AccountPayment_OR/Page_Login  Rapid Hybris B2B/button_Log In'))

WebUI.setText(findTestObject('Object Repository/CPQCheckoutTest_AccountPayment_OR/Page_Rapid Hybris B2B  Homepage/input_Quick Order_text'), 
    'megamachines')

WebUI.delay(2)

WebUI.mouseOver(findTestObject('Object Repository/CPQCheckoutTest_AccountPayment_OR/Page_Rapid Hybris B2B  Homepage/img'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_AccountPayment_OR/Page_Rapid Hybris B2B  Homepage/img'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_AccountPayment_OR/Page_MegaMachines Excavator Model 500  Rapid Hybris B2B/button_Configure'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_AccountPayment_OR/Page_MegaMachines Excavator Model 500  Rapid Hybris B2B/div_Please select'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_AccountPayment_OR/Page_MegaMachines Excavator Model 500  Rapid Hybris B2B/span_350'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_AccountPayment_OR/Page_MegaMachines Excavator Model 500  Rapid Hybris B2B/input_one speed_groups0cstics1value'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_AccountPayment_OR/Page_MegaMachines Excavator Model 500  Rapid Hybris B2B/input_tracks_groups0cstics2value'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_AccountPayment_OR/Page_MegaMachines Excavator Model 500  Rapid Hybris B2B/div_02CAB'))

WebUI.delay(2)

WebUI.scrollToPosition(750, 500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_AccountPayment_OR/Page_MegaMachines Excavator Model 500  Rapid Hybris B2B/input_suspension seats_groups1cstics0value'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_AccountPayment_OR/Page_MegaMachines Excavator Model 500  Rapid Hybris B2B/input_heating_groups1cstics1value'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_AccountPayment_OR/Page_MegaMachines Excavator Model 500  Rapid Hybris B2B/div_03OPTIONS'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_AccountPayment_OR/Page_MegaMachines Excavator Model 500  Rapid Hybris B2B/input_horn_groups2cstics0value'))

WebUI.delay(2)

WebUI.click(findTestObject('CPQCheckoutTest_AccountPayment_OR/Page_MegaMachines Excavator Model 500  Powertools Site/span_Please select'))

WebUI.delay(2)

WebUI.mouseOver(findTestObject('Titles_OR/Page_MegaMachines Excavator Model 500  Powertools Site/div_8Newrt'))

WebUI.delay(2)

WebUI.click(findTestObject('Titles_OR/Page_MegaMachines Excavator Model 500  Powertools Site/div_8Newrt'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_AccountPayment_OR/Page_MegaMachines Excavator Model 500  Rapid Hybris B2B/div_04ATTACH'))

WebUI.delay(2)

WebUI.scrollToPosition(1250, 1000)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_AccountPayment_OR/Page_MegaMachines Excavator Model 500  Rapid Hybris B2B/input_concat(36   Trenching)_groups3cstics0value'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_AccountPayment_OR/Page_MegaMachines Excavator Model 500  Rapid Hybris B2B/input_concat(36   Auger)_groups3cstics1value'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('CPQCheckoutTest_AccountPayment_OR/Page_MegaMachines Excavator Model 500  Powertools Site/button_Add to cart'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('CPQCheckoutTest_AccountPayment_OR/Page_MegaMachines Excavator Model 500  Powertools Site/button_CHECKOUT'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('CPQCheckoutTest_AccountPayment_OR/Page_Your Shopping Cart  Powertools Site/button_Check Out'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_AccountPayment_OR/Page_Checkout  Rapid Hybris B2B/input_Card Payment_paymentType'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CPQCheckoutTest_AccountPayment_OR/Page_Checkout  Rapid Hybris B2B/input_PO No_purchaseOrderNumber'), 
    '123456')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_AccountPayment_OR/Page_Checkout  Rapid Hybris B2B/button_Next'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_AccountPayment_OR/Page_Checkout  Rapid Hybris B2B/button_Address Book'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_AccountPayment_OR/Page_Checkout  Rapid Hybris B2B/button_Use this Address'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_AccountPayment_OR/Page_Checkout  Rapid Hybris B2B/button_Next_1'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_AccountPayment_OR/Page_Checkout  Rapid Hybris B2B/input_By placing the order'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_AccountPayment_OR/Page_Checkout  Rapid Hybris B2B/button_Place Order'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CheckoutTest_AccountPayment_OR/Page_Order Confirmation  Rapid Hybris B2B/div_Thank you for your OrderYour Order'))

WebUI.delay(2)

WebUI.verifyElementPresent(findTestObject('Object Repository/CheckoutTest_AccountPayment_OR/Page_Order Confirmation  Rapid Hybris B2B/div_Thank you for your OrderYour Order'), 
    0)

String order = WebUI.getText(findTestObject('Object Repository/CheckoutTest_AccountPayment_OR/Page_Order Confirmation  Rapid Hybris B2B/div_Thank you for your OrderYour Order'))

KeywordLogger logger = new KeywordLogger()

logger.logInfo(order)

