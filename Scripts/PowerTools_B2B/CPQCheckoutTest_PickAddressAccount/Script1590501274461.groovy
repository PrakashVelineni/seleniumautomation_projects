import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent

import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import static com.yourcompany.ConnectionNotPrivate.*
import com.yourcompany.ConnectionNotPrivate as ConnectionNotPrivate

//ConnectionNotPrivate.yourMethod()

WebUI.openBrowser('')

WebUI.navigateToUrl("http://$GlobalVariable.Hostname")

WebUI.maximizeWindow()

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_PickAddressAccount_OR/Page_Rapid Hybris B2B  Homepage/span_Quick Order_far fa-user'))

WebUI.setText(findTestObject('Object Repository/CPQCheckoutTest_PickAddressAccount_OR/Page_Login  Rapid Hybris B2B/input_Email Address_j_username'), 
    "$GlobalVariable.Email")

WebUI.setEncryptedText(findTestObject('Object Repository/CPQCheckoutTest_PickAddressAccount_OR/Page_Login  Rapid Hybris B2B/input_Password_j_password'), 
    "$GlobalVariable.Password")

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_PickAddressAccount_OR/Page_Login  Rapid Hybris B2B/button_Log In'))

WebUI.setText(findTestObject('Object Repository/CPQCheckoutTest_PickAddressAccount_OR/Page_Rapid Hybris B2B  Homepage/input_Quick Order_text'), 
    'megamachines')

WebUI.delay(5)

Robot robot = new Robot()

(robot.keyPress(KeyEvent.VK_ENTER))
(robot.keyRelease(KeyEvent.VK_ENTER))

WebUI.delay(5)

WebUI.scrollToPosition(0,500)

WebUI.delay(2)

WebUI.mouseOver(findTestObject('Object Repository/CPQCheckoutTest_PickAddressAccount_OR/Page_Search megamachines/button_200000_fas fa-cart'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_PickAddressAccount_OR/Page_Search megamachines/button_200000_fas fa-cart'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_PickAddressAccount_OR/Page_Search megamachines/a_Check Out'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_PickAddressAccount_OR/Page_Your Shopping Cart/button_Check Out'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_PickAddressAccount_OR/Page_Checkout  Rapid Hybris B2B/input_Card Payment_paymentType'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CPQCheckoutTest_PickAddressAccount_OR/Page_Checkout  Rapid Hybris B2B/input_PO No_purchaseOrderNumber'), 
    '123456')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_PickAddressAccount_OR/Page_Checkout  Rapid Hybris B2B/button_Next'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_PickAddressAccount_OR/Page_Checkout  Rapid Hybris B2B/button_Address Book'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_PickAddressAccount_OR/Page_Checkout  Rapid Hybris B2B/button_Use this Address'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_PickAddressAccount_OR/Page_Checkout  Rapid Hybris B2B/button_Next_1'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_PickAddressAccount_OR/Page_Checkout  Rapid Hybris B2B/input_By placing the order'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQCheckoutTest_PickAddressAccount_OR/Page_Checkout  Rapid Hybris B2B/button_Place Order'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CheckoutTest_AccountPayment_OR/Page_Order Confirmation  Rapid Hybris B2B/div_Thank you for your OrderYour Order'))

WebUI.delay(2)

WebUI.verifyElementPresent(findTestObject('Object Repository/CheckoutTest_AccountPayment_OR/Page_Order Confirmation  Rapid Hybris B2B/div_Thank you for your OrderYour Order'), 
    0)

String order = WebUI.getText(findTestObject('Object Repository/CheckoutTest_AccountPayment_OR/Page_Order Confirmation  Rapid Hybris B2B/div_Thank you for your OrderYour Order'))

KeywordLogger logger = new KeywordLogger()

logger.logInfo(order)



/*WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CheckoutTest_Pickaddresses_OR/Page_Checkout  Rapid Hybris B2B/button_Schedule Replenishment'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CheckoutTest_Pickaddresses_OR/Page_Checkout  Rapid Hybris B2B/i_Start auto-replenishment'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CheckoutTest_Pickaddresses_OR/Page_Checkout  Rapid Hybris B2B/a_20'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CheckoutTest_Pickaddresses_OR/Page_Checkout  Rapid Hybris B2B/input_Weekly_replenishmentRecurrence'))

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckoutTest_Pickaddresses_OR/Page_Checkout  Rapid Hybris B2B/select_123456789101112131415'),
	'20', true)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CheckoutTest_Pickaddresses_OR/Page_Checkout  Rapid Hybris B2B/button_Schedule Replenishment_1'))

WebUI.delay(2)

WebUI.click(findTestObject('CheckoutTest_CardPayment/Page_Replenishment Confirmation  Rapid Hybris B2B/div_Thank you for your scheduled Replenishment Order'))

WebUI.delay(2)

WebUI.verifyElementPresent(findTestObject('CheckoutTest_CardPayment/Page_Replenishment Confirmation  Rapid Hybris B2B/div_Thank you for your scheduled Replenishment Order'),
	0)

String order = WebUI.getText(findTestObject('CheckoutTest_CardPayment/Page_Replenishment Confirmation  Rapid Hybris B2B/div_Thank you for your scheduled Replenishment Order'))

KeywordLogger logger = new KeywordLogger()

logger.logInfo(order)*/



