import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import static com.yourcompany.ConnectionNotPrivate.*
import com.yourcompany.ConnectionNotPrivate as ConnectionNotPrivate

//ConnectionNotPrivate.yourMethod()

WebUI.openBrowser('')

WebUI.navigateToUrl("http://$GlobalVariable.Hostname")

WebUI.maximizeWindow()
WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_Rapid Hybris B2B  Homepage/span_Quick Order_far fa-user'))

WebUI.setText(findTestObject('Object Repository/CPQProductTest_OR/Page_Login  Rapid Hybris B2B/input_Email Address_j_username'), 
    "$GlobalVariable.Email")

WebUI.setEncryptedText(findTestObject('Object Repository/CPQProductTest_OR/Page_Login  Rapid Hybris B2B/input_Password_j_password'), 
    "$GlobalVariable.Password")

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_Login  Rapid Hybris B2B/button_Log In'))

WebUI.setText(findTestObject('Object Repository/CPQProductTest_OR/Page_Rapid Hybris B2B  Homepage/input_Quick Order_text'), 
    'megamachines')

WebUI.delay(5)

Robot robot = new Robot()

robot.keyPress(KeyEvent.VK_ENTER)

robot.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_Search megamachines/button_List'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_Search megamachines/button_Grid'))

WebUI.delay(2)

WebUI.scrollToPosition(0,750)

WebUI.delay(2)

WebUI.click(findTestObject('CPQProductTest_OR/Page_Search megamachines  Rapid Hybris B2B/button_Listb'))

WebUI.delay(2)

WebUI.click(findTestObject('CPQProductTest_OR/Page_Search megamachines  Rapid Hybris B2B/button_Gridb'))

WebUI.delay(2)

WebUI.scrollToPosition(750, 0)

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/CPQProductTest_OR/Page_Search megamachines/select_Sort by'), 
    'topRated', true)

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/CPQProductTest_OR/Page_Search Results Grid Page  Rapid Hybris B2B/select_Sort by'), 
    'name-asc', true)

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/CPQProductTest_OR/Page_Search Results Grid Page  Rapid Hybris B2B/select_Sort by'), 
    'name-desc', true)

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/CPQProductTest_OR/Page_Search Results Grid Page  Rapid Hybris B2B/select_Sort by'), 
    'price-asc', true)

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/CPQProductTest_OR/Page_Search Results Grid Page  Rapid Hybris B2B/select_Sort by'), 
    'price-desc', true)

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/CPQProductTest_OR/Page_Search Results Grid Page  Rapid Hybris B2B/select_Sort by (4)'), 
    'relevance', true)

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/CPQProductTest_OR/Page_Search Results Grid Page  Rapid Hybris B2B/select_Sort by (2)'), 
    'topRated', true)

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/CPQProductTest_OR/Page_Search Results Grid Page  Rapid Hybris B2B/select_Sort by (3)'), 
    'name-asc', true)

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/CPQProductTest_OR/Page_Search Results Grid Page  Rapid Hybris B2B/select_Sort by   '), 
    'name-desc', true)

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/CPQProductTest_OR/Page_Search Results Grid Page  Rapid Hybris B2B/select_Sort by (5)'), 
    'price-asc', true)

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/CPQProductTest_OR/Page_Search Results Grid Page  Rapid Hybris B2B/select_Sort by (6)'), 
    'price-desc', true)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_Search Results Grid Page  Rapid Hybris B2B/div_Shop by Price'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_Search Results Grid Page  Rapid Hybris B2B/div_Shop by Price'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_Search Results Grid Page  Rapid Hybris B2B/span_Shop by Price_facet'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_Search Results Grid Page  Rapid Hybris B2B/div_Applied Facets'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_Search Results Grid Page  Rapid Hybris B2B/div_Applied Facets'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_Search Results Grid Page  Rapid Hybris B2B/span_1000-100000_glyphicon'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_Search Results Grid Page  Rapid Hybris B2B/a_MegaMachines Excavator Model 500'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator/a_Write a Review'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator/form_Review Title'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator/input_Review Title_headline'), 
    'test')

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator/textarea_Review Description_comment'), 
    'test')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator/span_Your Rating_js-ratingIcon'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator/input_(optional)_alias'), 
    'test')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator/button_Submit Review'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator/button_'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator/button_Write a Review_btn btn-default js-qty-selector-plus'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator/button_Write a Review_btn btn-default js-qty-selector-plus'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator/button_Write a Review_btn btn-default js-qty-selector-plus'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator/button_Write a Review_btn btn-default js-qty-selector-plus'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator/button_Write a Review_btn btn-default js-qty-selector-plus'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator/button_Write a Review_btn btn-default js-qty-selector-minus'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator/button_Write a Review_btn btn-default js-qty-selector-minus'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator/button_Write a Review_btn btn-default js-qty-selector-minus'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator/button_Write a Review_btn btn-default js-qty-selector-minus'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator/button_Write a Review_btn btn-default js-qty-selector-minus'))

WebUI.delay(2)

WebUI.scrollToPosition(250, 500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator/a_Specs'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator/a_Reviews'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator/button_Write a Review'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator/button_Write a Review'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator/a_Delivery'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator/button_Configure'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator/span_Please select'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator/span_100'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator/button_Add to cart'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator/button_Overview_cpq-back-button'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator/span_100_1'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator/div_100'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator/span_100_2'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator/div_150'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator/button_Update Cart'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator Model 500  Rapid Hybris B2B/div_Refine by Option'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator Model 500  Rapid Hybris B2B/div_Refine by Option'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator Model 500  Rapid Hybris B2B/div_Refine by Group'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator Model 500  Rapid Hybris B2B/div_Refine by Group'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator Model 500  Rapid Hybris B2B/div_Refine by Option_facet__list__mark col-xs-1'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator Model 500  Rapid Hybris B2B/div_Refine by Group_facet__list__mark col-xs-1'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator Model 500  Rapid Hybris B2B/div_Applied Filters'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator Model 500  Rapid Hybris B2B/div_Applied Filters'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator Model 500  Rapid Hybris B2B/span_My Selections_glyphicon glyphicon-remove'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator Model 500  Rapid Hybris B2B/span_Applied Filters_glyphicon glyphicon-remove'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator Model 500  Rapid Hybris B2B/button_Skip This Step'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_Your Shopping Cart  Rapid Hybris B2B/a_Change configuration'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator Model 500  Rapid Hybris B2B/span_100'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator Model 500  Rapid Hybris B2B/span_350'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator Model 500  Rapid Hybris B2B/button_Update Cart'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator Model 500  Rapid Hybris B2B/button_CHECKOUT'))

WebUI.delay(2)

WebUI.back()

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator Model 500  Rapid Hybris B2B/button_Back to Configuration'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator Model 500  Rapid Hybris B2B/span_350_1'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator Model 500  Rapid Hybris B2B/div_225'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator Model 500  Rapid Hybris B2B/button_Update Cart'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator Model 500  Rapid Hybris B2B/button_CHECKOUT'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_Your Shopping Cart  Rapid Hybris B2B/button_Check Out'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_Checkout  Rapid Hybris B2B/input_Card Payment_paymentType'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CPQProductTest_OR/Page_Checkout  Rapid Hybris B2B/input_PO No_purchaseOrderNumber'), 
    '123456')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_Checkout  Rapid Hybris B2B/button_Next'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_Checkout  Rapid Hybris B2B/button_Address Book'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_Checkout  Rapid Hybris B2B/button_Use this Address'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_Checkout  Rapid Hybris B2B/button_Next_1'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_Checkout  Rapid Hybris B2B/input_By placing the order'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_Checkout  Rapid Hybris B2B/button_Place Order'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_Order Confirmation  Rapid Hybris B2B/a_View Configuration'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CPQProductTest_OR/Page_MegaMachines Excavator Model 500  Rapid Hybris B2B/button_Back to Order'))

WebUI.delay(2)

