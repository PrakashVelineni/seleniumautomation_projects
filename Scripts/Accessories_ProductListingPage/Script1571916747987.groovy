import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.yourcompany.YourClass1.*
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.yourcompany.YourClass1 as YourClass1
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import static com.yourcompany.ConsoleRobot.*
import com.yourcompany.ConsoleRobot as ConsoleRobot

WebUI.openBrowser('')

//WebUI.navigateToUrl('https://qa-omnibus.hybrisdemo.sbpcorp.com/en/')
WebUI.navigateToUrl("http://$GlobalVariable.Hostname")

WebUI.maximizeWindow()

WebUI.delay(1)

WebUI.click(findTestObject('CheckOutTest_OR/Page_Apparel Site UK  Homepage/span_Quick Order_far fa-user'))

WebUI.setText(findTestObject('Object Repository/ReturnsHistoryTest_OR/Page_Login  Apparel Site UK/input_Email Address_j_username'), 
    "$GlobalVariable.Email")

WebUI.setEncryptedText(findTestObject('Object Repository/ReturnsHistoryTest_OR/Page_Login  Apparel Site UK/input_Password_j_password'), 
    "$GlobalVariable.Password")

WebUI.scrollToPosition(0, 250)

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Login  Apparel Site UK/button_Log In'))

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Apparel Site UK  Homepage/a_Accessories'))

WebUI.waitForElementVisible(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/select_Sort by  Relevance Top Rated'), 
    30)

WebUI.selectOptionByValue(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/select_Sort by  Relevance Top Rated'), 
    'topRated', true)

WebUI.delay(2)

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.scrollToPosition(500, 1000)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Apparel Site UK  Homepage/a_Accessories'))

WebUI.waitForElementVisible(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/select_Sort by  Relevance Top Rated'), 
    30)

WebUI.selectOptionByValue(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/select_Sort by  Relevance Top Rated'), 
    'name-asc', true)

WebUI.delay(2)

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.scrollToPosition(500, 1000)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Apparel Site UK  Homepage/a_Accessories'))

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/select_Sort by  Relevance Top Rated'), 
    'name-desc', true)

WebUI.delay(2)

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.scrollToPosition(500, 1000)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Apparel Site UK  Homepage/a_Accessories'))

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/select_Sort by  Relevance Top Rated'), 
    'price-asc', true)

WebUI.delay(2)

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.scrollToPosition(500, 1000)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Apparel Site UK  Homepage/a_Accessories'))

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/select_Sort by  Relevance Top Rated'), 
    'price-desc', true)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/button_List'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/button_Grid'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_HD Hero2 Outdoor Edition uni Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_The Rotolog TEAK Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_The Rotolog all blackwhite Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.delay(2)

WebUI.scrollToPosition(750, 1000)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_The Player matte blackmatte gunmeta Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_The Sentry all black Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_The Private silverbrown Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.delay(2)

WebUI.scrollToPosition(1000, 1250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_The Sentry gunship'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_The Lodown rasta Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_The Rubber Re-Run sky blue'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.delay(2)

WebUI.scrollToPosition(1250, 1750)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_The 51-30 matte blackgold'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Incision Leather Belt shadow grey LXL'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Granda Belt white L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.delay(2)

WebUI.scrollToPosition(1750, 2250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_El Moro Hat all black LXL'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Heritage Pipe Glove white L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Adapt New Era blacktop 7 14'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.delay(2)

WebUI.scrollToPosition(2250, 2750)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Spectre Glove prep L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Handshake Belt field green M'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Enterprize Cap blackred LXL'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.delay(2)

WebUI.scrollToPosition(2750, 3500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Ginga Cap black LXL'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Studded Belt cardinal L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(3500, 4000)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/a_2'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Trope Beanie purple label Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Cascata Beanie blackred Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_The Stripee Fresh Beanie yellow blue Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.delay(2)

WebUI.scrollToPosition(250, 750)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_BT Airhole Splatter Facemask black LXL'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Whats Your Niner astro turf Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Sadplant Pipe Glove blackorange M'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(750, 1250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Element Flat Cap celtic LXL'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Fade Beanie blue 23 Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Maguro Pu Belt print LXL'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.delay(2)

WebUI.scrollToPosition(1250, 1750)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Transplant Flexfit Cap white combo LXL'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Buffer II Sock cardinal L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_BT Airhole Helgasons Facemask tiedie LXL'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.delay(2)

WebUI.scrollToPosition(1750, 2250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Lenus Belt kelly Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Assortment Web Belt drip red Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Party Sock ski-zee L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.delay(2)

WebUI.scrollToPosition(2250, 2750)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Torbacula Belt white SM'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Easy Beanie black Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Camper Beanie black Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.delay(2)

WebUI.scrollToPosition(0, 3500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_The Life Beanie black Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_The Reservoir Beanie royal blue Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/a_(current)_fas fa-long-arrow-alt-right'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_The Brick Beanie ski high Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_BT Contrast Cap black Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Faded Droid Cap white Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.delay(2)

WebUI.scrollToPosition(500, 1000)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_BT Full Splatter Cap white Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.delay(2)

WebUI.scrollToPosition(0, 1500)

WebUI.delay(2)

WebUI.click(findTestObject('Accessories_ProductListingPage_OR/Page_Accessories  Categories  Rapid Hybris/button_Listbottom'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 50)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/button_Grid'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 50)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/a_1'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 50)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/a_2'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 50)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/a_3'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 50)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_(current)_fas fa-long-arrow-alt-right'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 50)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/a_1'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 50)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Grid_fas fa-long-arrow-alt-left'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 50)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/button_List'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 50)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/button_Grid'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 50)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/a_2'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 50)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/a_3'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 50)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_(current)_fas fa-long-arrow-alt-right_1'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 50)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/a_1'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 50)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Grid_fas fa-long-arrow-alt-left_1'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 50)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/img'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 50)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Apparel Site UK  Homepage/img_Browse All _js-responsive-image'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

