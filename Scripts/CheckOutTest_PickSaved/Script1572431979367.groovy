import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import static com.yourcompany.ConnectionNotPrivate.*
import com.yourcompany.ConnectionNotPrivate as ConnectionNotPrivate

import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger

//ConnectionNotPrivate.yourMethod()
WebUI.openBrowser('')

WebUI.navigateToUrl("http://$GlobalVariable.Hostname")

WebUI.maximizeWindow()

WebUI.waitForPageLoad(30)

WebUI.click(findTestObject('CheckOutTest_OR/Page_Apparel Site UK  Homepage/span_Quick Order_far fa-user'))

WebUI.setText(findTestObject('ConsoleErrorsTest_OR/CheckOutTest_PickSaved_OR/Page_Login  Apparel Site UK/input_Email Address_j_username'), 
    "$GlobalVariable.Email")

WebUI.setEncryptedText(findTestObject('ConsoleErrorsTest_OR/CheckOutTest_PickSaved_OR/Page_Login  Apparel Site UK/input_Password_j_password'), 
    "$GlobalVariable.Password")

WebUI.scrollToPosition(0, 250)

WebUI.click(findTestObject('ConsoleErrorsTest_OR/CheckOutTest_PickSaved_OR/Page_Login  Apparel Site UK/button_Log In'))

WebUI.click(findTestObject('ConsoleErrorsTest_OR/CheckOutTest_PickSaved_OR/Page_Apparel Site UK  Homepage/a_Accessories'))

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Checkout_Guest_OR1/Page_Accessories  Categories  Apparel Site UK/span_Granda Belt white L'))

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('ConsoleErrorsTest_OR/CheckOutTest_PickSaved_OR/Page_Handshake Belt field green L  Quiksilver  Brands  Apparel Site UK/button_Add to bag'))

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('ConsoleErrorsTest_OR/CheckOutTest_PickSaved_OR/Page_Handshake Belt field green L  Quiksilver  Brands  Apparel Site UK/a_Check Out'))

WebUI.click(findTestObject('Object Repository/CheckOutTest_PickSaved_OR/Page_Your Shopping Bag  Apparel Site UK/button_Check Out'))

WebUI.delay(5)

WebUI.click(findTestObject('ConsoleErrorsTest_OR/CheckOutTest_PickSaved_OR/Page_Checkout  Apparel Site UK/button_Address Book'))

WebUI.scrollToPosition(0, 1000)

WebUI.delay(5)

WebUI.click(findTestObject('ConsoleErrorsTest_OR/CheckOutTest_PickSaved_OR/Page_Checkout  Apparel Site UK/button_Use this Address'))

//WebUI.delay(10)
WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('ConsoleErrorsTest_OR/CheckOutTest_PickSaved_OR/Page_Checkout  Apparel Site UK/button_Next'))

WebUI.delay(5)

WebUI.click(findTestObject('ConsoleErrorsTest_OR/CheckOutTest_PickSaved_OR/Page_Checkout  Apparel Site UK/button_Use a saved card'))

WebUI.scrollToPosition(0, 250)

WebUI.delay(5)

WebUI.click(findTestObject('ConsoleErrorsTest_OR/CheckOutTest_PickSaved_OR/Page_Checkout  Apparel Site UK/button_Use these payment details'))

WebUI.scrollToPosition(0, 250)

WebUI.click(findTestObject('ConsoleErrorsTest_OR/CheckOutTest_PickSaved_OR/Page_Checkout  Apparel Site UK/input_By placing the order I am confirming that I have read and agree with the_termsCheck'))

WebUI.click(findTestObject('ConsoleErrorsTest_OR/CheckOutTest_PickSaved_OR/Page_Checkout  Apparel Site UK/button_Place Order'))

WebUI.delay(2)

WebUI.click(findTestObject('CheckOutTest_OR/Page_Order Confirmation  Rapid Hybris/div_Thank you for your Order'))

WebUI.delay(2)

WebUI.verifyElementPresent(findTestObject('CheckOutTest_OR/Page_Order Confirmation  Rapid Hybris/div_Thank you for your Order'), 
    0)

String order = WebUI.getText(findTestObject('CheckOutTest_OR/Page_Order Confirmation  Rapid Hybris/div_Thank you for your Order'))

KeywordLogger logger = new KeywordLogger()

logger.logInfo(order)

