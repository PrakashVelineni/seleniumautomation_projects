import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.yourcompany.ConsoleRobot.*
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.yourcompany.ConsoleRobot as ConsoleRobot
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent



WebUI.openBrowser('')

WebUI.navigateToUrl("http://${GlobalVariable.Hostname}")

WebUI.maximizeWindow()

WebUI.click(findTestObject('Object Repository/ConsoleErrorsTest_OR/Page_Rapid Hybris  Homepage/img_Quick Order_lazy'))

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Apparel Site UK  Homepage/a_Accessories'))

WebUI.waitForElementVisible(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/select_Sort by  Relevance Top Rated'),
	30)

WebUI.selectOptionByValue(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/select_Sort by  Relevance Top Rated'),
	'topRated', true)

WebUI.delay(3)

WebUI.scrollToPosition(0, 500)

WebUI.delay(3)

WebUI.scrollToPosition(500, 1000)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Apparel Site UK  Homepage/a_Accessories'))

WebUI.waitForElementVisible(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/select_Sort by  Relevance Top Rated'),
	30)

WebUI.selectOptionByValue(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/select_Sort by  Relevance Top Rated'),
	'name-asc', true)

WebUI.delay(3)

WebUI.scrollToPosition(0, 500)

WebUI.delay(3)

WebUI.scrollToPosition(500, 1000)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Apparel Site UK  Homepage/a_Accessories'))

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/select_Sort by  Relevance Top Rated'),
	'name-desc', true)

WebUI.delay(3)

WebUI.scrollToPosition(0, 500)

WebUI.delay(3)

WebUI.scrollToPosition(500, 1000)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Apparel Site UK  Homepage/a_Accessories'))

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/select_Sort by  Relevance Top Rated'),
	'price-asc', true)

WebUI.delay(3)

WebUI.scrollToPosition(0, 500)

WebUI.delay(3)

WebUI.scrollToPosition(500, 1000)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Apparel Site UK  Homepage/a_Accessories'))

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/select_Sort by  Relevance Top Rated'),
	'price-desc', true)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/button_List'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/button_Grid'))

WebUI.delay(3)

WebUI.scrollToPosition(0, 750)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_HD Hero2 Outdoor Edition uni Uni'))

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_The Rotolog TEAK Uni'))

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_The Rotolog all blackwhite Uni'))

ConsoleRobot.yourMethod()

WebUI.delay(3)

WebUI.scrollToPosition(750, 1000)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_The Player matte blackmatte gunmeta Uni'))

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_The Sentry all black Uni'))

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_The Private silverbrown Uni'))

ConsoleRobot.yourMethod()

WebUI.delay(3)

WebUI.scrollToPosition(1000, 1250)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_The Sentry gunship'))

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_The Lodown rasta Uni'))

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_The Rubber Re-Run sky blue'))

ConsoleRobot.yourMethod()

WebUI.delay(3)

ConsoleRobot.yourMethod()

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_The 51-30 matte blackgold'))

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Incision Leather Belt shadow grey LXL'))

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Granda Belt white L'))

ConsoleRobot.yourMethod()

WebUI.delay(3)

WebUI.scrollToPosition(1750, 2250)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_El Moro Hat all black LXL'))

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Heritage Pipe Glove white L'))

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Adapt New Era blacktop 7 14'))

ConsoleRobot.yourMethod()

WebUI.delay(3)

WebUI.scrollToPosition(2250, 2750)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Spectre Glove prep L'))

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Handshake Belt field green M'))

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Enterprize Cap blackred LXL'))

ConsoleRobot.yourMethod()

WebUI.delay(3)

WebUI.scrollToPosition(2750, 3500)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Ginga Cap black LXL'))

ConsoleRobot.yourMethod()

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Studded Belt cardinal L'))

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(3500, 4000)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/a_2'))

not_run: WebUI.delay(3)

WebUI.scrollToPosition(0, 250)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Trope Beanie purple label Uni'))

ConsoleRobot.yourMethod()

WebUI.delay(3)

not_run: WebUI.scrollToPosition(0, 250)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Cascata Beanie blackred Uni'))

ConsoleRobot.yourMethod()

WebUI.delay(3)

not_run: WebUI.scrollToPosition(0, 50)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_The Stripee Fresh Beanie yellow blue Uni'))

ConsoleRobot.yourMethod()

WebUI.delay(3)

WebUI.scrollToPosition(250, 750)

//WebUI.scrollToElement(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_BT Airhole Splatter Facemask black LXL'),
//   5)
WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_BT Airhole Splatter Facemask black LXL'))

ConsoleRobot.yourMethod()

not_run: WebUI.delay(3)

not_run: WebUI.scrollToPosition(0, 250)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Whats Your Niner astro turf Uni'))

ConsoleRobot.yourMethod()

not_run: WebUI.delay(3)

not_run: WebUI.scrollToPosition(0, 50)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Sadplant Pipe Glove blackorange M'))

ConsoleRobot.yourMethod()

not_run: WebUI.delay(3)

WebUI.scrollToPosition(750, 1250)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Element Flat Cap celtic LXL'))

ConsoleRobot.yourMethod()

not_run: WebUI.delay(3)

not_run: WebUI.scrollToPosition(0, 250)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Fade Beanie blue 23 Uni'))

ConsoleRobot.yourMethod()

not_run: WebUI.delay(3)

not_run: WebUI.scrollToPosition(0, 50)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Maguro Pu Belt print LXL'))

ConsoleRobot.yourMethod()

WebUI.delay(3)

WebUI.scrollToPosition(1250, 1750)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Transplant Flexfit Cap white combo LXL'))

ConsoleRobot.yourMethod()

not_run: WebUI.delay(3)

not_run: WebUI.scrollToPosition(0, 250)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Buffer II Sock cardinal L'))

ConsoleRobot.yourMethod()

not_run: WebUI.delay(3)

not_run: WebUI.scrollToPosition(0, 50)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_BT Airhole Helgasons Facemask tiedie LXL'))

ConsoleRobot.yourMethod()

WebUI.delay(3)

WebUI.scrollToPosition(1750, 2250)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Lenus Belt kelly Uni'))

ConsoleRobot.yourMethod()

not_run: WebUI.delay(3)

not_run: WebUI.scrollToPosition(0, 250)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Assortment Web Belt drip red Uni'))

ConsoleRobot.yourMethod()

not_run: WebUI.delay(3)

not_run: WebUI.scrollToPosition(0, 50)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Party Sock ski-zee L'))

ConsoleRobot.yourMethod()

WebUI.delay(3)

WebUI.scrollToPosition(2250, 2750)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Torbacula Belt white SM'))

ConsoleRobot.yourMethod()

not_run: WebUI.delay(3)

not_run: WebUI.scrollToPosition(0, 250)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Easy Beanie black Uni'))

ConsoleRobot.yourMethod()

not_run: WebUI.delay(3)

not_run: WebUI.scrollToPosition(0, 50)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Camper Beanie black Uni'))

ConsoleRobot.yourMethod()

WebUI.delay(3)

WebUI.scrollToPosition(2750, 3500)

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_The Life Beanie black Uni'))

ConsoleRobot.yourMethod()

not_run: WebUI.delay(3)

not_run: WebUI.scrollToPosition(0, 250)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_The Reservoir Beanie royal blue Uni'))

ConsoleRobot.yourMethod()

not_run: WebUI.delay(3)

not_run: WebUI.scrollToPosition(0, 250)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/a_(current)_fas fa-long-arrow-alt-right'))

WebUI.delay(3)

WebUI.scrollToPosition(0, 500)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_The Brick Beanie ski high Uni'))

ConsoleRobot.yourMethod()

WebUI.delay(3)

not_run: WebUI.scrollToPosition(0, 50)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_BT Contrast Cap black Uni'))

ConsoleRobot.yourMethod()

WebUI.delay(3)

not_run: WebUI.scrollToPosition(0, 50)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Faded Droid Cap white Uni'))

ConsoleRobot.yourMethod()

WebUI.delay(3)

WebUI.scrollToPosition(500, 1000)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_BT Full Splatter Cap white Uni'))

ConsoleRobot.yourMethod()

WebUI.delay(3)

not_run: WebUI.scrollToPosition(0, 50)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/button_List'))

WebUI.delay(3)

WebUI.scrollToPosition(0, 50)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/button_Grid'))

WebUI.delay(3)

WebUI.scrollToPosition(0, 50)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/a_1'))

WebUI.delay(3)

WebUI.scrollToPosition(0, 50)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/a_2'))

WebUI.delay(3)

WebUI.scrollToPosition(0, 50)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/a_3'))

WebUI.delay(3)

WebUI.scrollToPosition(0, 50)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_(current)_fas fa-long-arrow-alt-right'))

WebUI.delay(3)

WebUI.scrollToPosition(0, 50)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/a_1'))

WebUI.delay(3)

WebUI.scrollToPosition(0, 50)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Grid_fas fa-long-arrow-alt-left'))

WebUI.delay(3)

WebUI.scrollToPosition(0, 50)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/button_List'))

WebUI.delay(3)

WebUI.scrollToPosition(0, 50)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/button_Grid'))

WebUI.delay(3)

WebUI.scrollToPosition(0, 50)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/a_2'))

WebUI.delay(3)

WebUI.scrollToPosition(0, 50)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/a_3'))

WebUI.delay(3)

WebUI.scrollToPosition(0, 50)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_(current)_fas fa-long-arrow-alt-right_1'))

WebUI.delay(3)

WebUI.scrollToPosition(0, 50)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/a_1'))

WebUI.delay(3)

WebUI.scrollToPosition(0, 50)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/span_Grid_fas fa-long-arrow-alt-left_1'))

WebUI.delay(3)

WebUI.scrollToPosition(0, 50)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Accessories  Categories  Apparel Site UK/img'))

WebUI.delay(3)

WebUI.scrollToPosition(0, 50)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Accessories_ProductListingPage_OR/Page_Apparel Site UK  Homepage/img_Browse All _js-responsive-image'))

ConsoleRobot.yourMethod()







