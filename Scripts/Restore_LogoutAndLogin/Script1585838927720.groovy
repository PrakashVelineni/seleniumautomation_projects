import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import static com.yourcompany.ConnectionNotPrivate.*
import com.yourcompany.ConnectionNotPrivate as ConnectionNotPrivate

//ConnectionNotPrivate.yourMethod()
WebUI.openBrowser('')

WebUI.navigateToUrl("http://$GlobalVariable.Hostname")

WebUI.maximizeWindow()

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Restore_LoginAndLogout/Page_Rapid Hybris  Homepage/span_Quick Order_far fa-user'))

WebUI.selectOptionByValue(findTestObject('Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Company', true)

WebUI.selectOptionByValue(findTestObject('Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Mr', true)

WebUI.selectOptionByValue(findTestObject('Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Miss', true)

WebUI.selectOptionByValue(findTestObject('Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Mrs', true)

WebUI.selectOptionByValue(findTestObject('Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Ms', true)

WebUI.selectOptionByValue(findTestObject('Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Rev.', true)

WebUI.selectOptionByValue(findTestObject('Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Prof.', true)

WebUI.selectOptionByValue(findTestObject('Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Dr.', true)

WebUI.setText(findTestObject('Object Repository/Restore_LoginAndLogout/Page_Login  Rapid Hybris/input_First Name_firstName'), 
    'robert')

WebUI.setText(findTestObject('Object Repository/Restore_LoginAndLogout/Page_Login  Rapid Hybris/input_Last Name_lastName'), 
    'reviewer')

WebUI.setText(findTestObject('RegistrationTest_OR/Page_Registration Apparel Site UK/input_Email Address_email'), "$GlobalVariable.Email")

WebUI.setEncryptedText(findTestObject('RegistrationTest_OR/Page_Registration Apparel Site UK/input_Password_pwd'), "$GlobalVariable.Password")

WebUI.setEncryptedText(findTestObject('RegistrationTest_OR/Page_Registration Apparel Site UK/input_Confirm Password_checkPwd'), 
    "$GlobalVariable.Password")

WebUI.click(findTestObject('Object Repository/Restore_LoginAndLogout/Page_Login  Rapid Hybris/label_This is a sample consent description that will need to be updated or replaced based on the valid registration consent required'))

WebUI.click(findTestObject('Object Repository/Restore_LoginAndLogout/Page_Login  Rapid Hybris/input_I am confirming that I have read and agreed with the_termsCheck'))

WebUI.click(findTestObject('Object Repository/Restore_LoginAndLogout/Page_Login  Rapid Hybris/button_Register'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Restore_LoginAndLogout/Page_Rapid Hybris  Homepage/button_'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Restore_LoginAndLogout/Page_Rapid Hybris  Homepage/a_Accessories'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Restore_LoginAndLogout/Page_Accessories  Categories  Rapid Hybris/button_28346_fas fa-cart-plus custom-cart js-enable-btn'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Restore_LoginAndLogout/Page_Accessories  Categories  Rapid Hybris/a_Check Out'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Restore_LoginAndLogout/Page_Your Shopping Bag  Rapid Hybris/a_New Cart'))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/Restore_LoginAndLogout/Page_Your Shopping Bag  Rapid Hybris/input_Name_name'), 
    'test1')

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Restore_LoginAndLogout/Page_Your Shopping Bag  Rapid Hybris/textarea_Description_description'), 
    'test2')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Restore_LoginAndLogout/Page_Your Shopping Bag  Rapid Hybris/button_Save'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Restore_LoginAndLogout/Page_Your Shopping Bag  Rapid Hybris/a_Streetwear'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Restore_LoginAndLogout/Page_Streetwear  Collections  Rapid Hybris/button_3074_fas fa-cart-plus custom-cart js-enable-btn'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Restore_LoginAndLogout/Page_Streetwear  Collections  Rapid Hybris/a_Check Out'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Restore_LoginAndLogout/Page_Your Shopping Bag  Rapid Hybris/a_New Cart'))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/Restore_LoginAndLogout/Page_Your Shopping Bag  Rapid Hybris/input_Name_name'), 
    'test2')

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Restore_LoginAndLogout/Page_Your Shopping Bag  Rapid Hybris/textarea_Description_description'), 
    'test2')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Restore_LoginAndLogout/Page_Your Shopping Bag  Rapid Hybris/button_Save'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Restore_LoginAndLogout/Page_Your Shopping Bag  Rapid Hybris/a_Saved Carts (2)'))

WebUI.delay(2)

WebUI.click(findTestObject('Restore_LoginAndLogout/Page_Saved Carts  Rapid Hybris/span_Restorenew'))

WebUI.delay(2)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Restore_LoginAndLogout/Page_Saved Carts  Rapid Hybris/input_Keep a copy of this cart on saved list_keepRestoredCart'))

WebUI.delay(2)

WebUI.click(findTestObject('Restore_LoginAndLogout/Page_Saved Carts  Rapid Hybris/button_Restore'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Restore_LoginAndLogout/Page_Your Shopping Bag  Rapid Hybris/a_Saved Carts (1)'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Restore_LoginAndLogout/Page_Saved Carts  Rapid Hybris/span_Restorenew'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Restore_LoginAndLogout/Page_Saved Carts  Rapid Hybris/input_Keep a copy of this cart on saved list_keepRestoredCart'))

WebUI.delay(2)

WebUI.click(findTestObject('Restore_LoginAndLogout/Page_Saved Carts  Rapid Hybris/button_RestoreNew'))

WebUI.delay(3)

WebUI.click(findTestObject('Restore_LoginAndLogout/Page_Your Shopping Bag  Rapid Hybris/span_Quick Order_far fa-user'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Restore_LoginAndLogout/Page_Your Shopping Bag  Rapid Hybris/a_Sign Out'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Restore_LoginAndLogout/Page_Your Shopping Bag  Rapid Hybris/span_Quick Order_far fa-user_1'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/SavedCartsTest_OR1/Page_Login  Rapid Hybris/input_Email Address_j_username'), 
    "$GlobalVariable.Email")

WebUI.delay(2)

WebUI.setEncryptedText(findTestObject('Object Repository/SavedCartsTest_OR1/Page_Login  Rapid Hybris/input_Password_j_password'), 
    "$GlobalVariable.Password")

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Restore_LoginAndLogout/Page_Login  Rapid Hybris/button_Log In'))

WebUI.delay(2)

