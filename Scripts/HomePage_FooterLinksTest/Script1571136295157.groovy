import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

//WebUI.navigateToUrl('https://apparel-uk.hybrisdemo.sbpcorp.com/')
WebUI.navigateToUrl("http://$GlobalVariable.Hostname")

WebUI.maximizeWindow()

WebUI.delay(2)

WebUI.scrollToPosition(0, 3000)

WebUI.delay(2)

WebUI.click(findTestObject('HomePage_FooterLinksTest_OR/Page_Rapid Hybris  Homepage/a_SAP Cloud Services'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 3000)

WebUI.delay(2)

WebUI.closeWindowIndex(1)

WebUI.switchToWindowTitle('Rapid Hybris | Homepage')

WebUI.delay(2)

WebUI.click(findTestObject('HomePage_FooterLinksTest_OR/Page_Rapid Hybris  Homepage/a_Ecommerce Services'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 3000)

WebUI.delay(2)

WebUI.closeWindowIndex(1)

WebUI.switchToWindowTitle('Rapid Hybris | Homepage')

WebUI.delay(2)

WebUI.click(findTestObject('HomePage_FooterLinksTest_OR/Page_Rapid Hybris  Homepage/a_SAP Services'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 3000)

WebUI.delay(2)

WebUI.closeWindowIndex(1)

WebUI.switchToWindowTitle('Rapid Hybris | Homepage')

WebUI.delay(2)

WebUI.click(findTestObject('HomePage_FooterLinksTest_OR/Page_Rapid Hybris  Homepage/a_SmartCart'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 3000)

WebUI.delay(2)

WebUI.closeWindowIndex(1)

WebUI.switchToWindowTitle('Rapid Hybris | Homepage')

WebUI.delay(2)

WebUI.click(findTestObject('HomePage_FooterLinksTest_OR/Page_Rapid Hybris  Homepage/a_Rapid Hybris'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 3000)

WebUI.delay(2)

WebUI.closeWindowIndex(1)

WebUI.switchToWindowTitle('Rapid Hybris | Homepage')

WebUI.delay(2)

WebUI.click(findTestObject('HomePage_FooterLinksTest_OR/Page_Rapid Hybris  Homepage/a_Marketing Cockpit'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 3000)

WebUI.delay(2)

WebUI.closeWindowIndex(1)

WebUI.switchToWindowTitle('Rapid Hybris | Homepage')

WebUI.delay(2)

WebUI.click(findTestObject('HomePage_FooterLinksTest_OR/Page_Rapid Hybris  Homepage/a_Company Overview'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 3000)

WebUI.delay(2)

WebUI.closeWindowIndex(1)

WebUI.switchToWindowTitle('Rapid Hybris | Homepage')

WebUI.delay(2)

WebUI.click(findTestObject('HomePage_FooterLinksTest_OR/Page_Rapid Hybris  Homepage/a_Our Partners'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 3000)

WebUI.delay(2)

WebUI.closeWindowIndex(1)

WebUI.switchToWindowTitle('Rapid Hybris | Homepage')

WebUI.delay(2)

WebUI.click(findTestObject('HomePage_FooterLinksTest_OR/Page_Rapid Hybris  Homepage/a_Contact Us'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 3000)

WebUI.delay(2)

WebUI.closeWindowIndex(1)

WebUI.switchToWindowTitle('Rapid Hybris | Homepage')

WebUI.delay(2)

WebUI.click(findTestObject('HomePage_FooterLinksTest_OR/Page_Rapid Hybris  Homepage/span_ Copyright 2020 SBP Consulting  All Rights Reserved  Privacy Policy  Terms of Use_fa fa-facebook'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 3000)

WebUI.delay(2)

WebUI.closeWindowIndex(1)

WebUI.switchToWindowTitle('Rapid Hybris | Homepage')

WebUI.delay(2)

WebUI.click(findTestObject('HomePage_FooterLinksTest_OR/Page_Rapid Hybris  Homepage/span_ Copyright 2020 SBP Consulting  All Rights Reserved  Privacy Policy  Terms of Use_fa fa-linkedin'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 3000)

WebUI.delay(2)

WebUI.closeWindowIndex(1)

WebUI.switchToWindowTitle('Rapid Hybris | Homepage')

WebUI.delay(2)

WebUI.click(findTestObject('HomePage_FooterLinksTest_OR/Page_Rapid Hybris  Homepage/span_ Copyright 2020 SBP Consulting  All Rights Reserved  Privacy Policy  Terms of Use_fa fa-twitter'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 3000)

WebUI.delay(2)

WebUI.closeWindowIndex(1)

