import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.yourcompany.YourClass1.*
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.yourcompany.YourClass1 as YourClass1
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import static com.yourcompany.ConsoleRobot.*
import com.yourcompany.ConsoleRobot as ConsoleRobot

WebUI.openBrowser('')

WebUI.navigateToUrl("http://$GlobalVariable.Hostname")

WebUI.maximizeWindow()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Rapid Hybris  Homepage/a_Brands'))

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Quick Order_far fa-user'))

WebUI.setText(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Login  Rapid Hybris/input_Email Address_j_username'), 
    "$GlobalVariable.Email")

WebUI.setEncryptedText(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Login  Rapid Hybris/input_Password_j_password'), 
    "$GlobalVariable.Password")

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Login  Rapid Hybris/button_Log In'))

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Rapid Hybris  Homepage/a_Brands'))

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Snowboard Ski Tool Toko Side Edge Tuning Angle Pro 87 Grad'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Snowboard Ski Tool Toko Ergo Multi Guide yellow'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Shades Von Zipper Papa G white grey chrome'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(500, 1000)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Snowboard Ski Tool Toko Scraper Sharpener World Cup'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Shades Von Zipper Papa G black gloss black glossgrey'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Snowboard Ski Tool Toko Plexiklinge 3 mmflexibel'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(1000, 1500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Snowboard Ski Tool Toko Waxremover HC3 500ml'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Panorama Pants Women beet red S'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Basis Pant Women defender L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(1500, 2000)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_The Dial Women shocking pink'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Biarritz Women black'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_The Chalet Women dark wood'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(2000, 2500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_The Rubber Re-Run sky blue'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_The Sentry all black Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_The Rotolog all blackwhite Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(2500, 3000)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_The Rotolog TEAK Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_The Lodown rasta Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Shield Tee SS Women onyx L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(3000, 3500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Jackson Tee SS marine heather XL'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Solo Parenth SS black L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(3500, 4000)

WebUI.delay(2) 

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/a_2'))

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Eclipse SS youth white L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Asterisk SS youth black XS'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Flow Tee SS Youth royal L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(500, 1000)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Ekeel 63 GO Fish uni 63'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Selva Dress Women island greenpink lemonad XL'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Frickin Too Short 22 black S'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(1000, 1500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Skull 80 SS youth white XL'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Method Jacket Women risque M'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_System Tee SS royal blue L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(1500, 2000)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Gary Polo SS Youth white XS'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Stratus Dress Women seaweed S'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Laundry Chino Short dark grey XS'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(2000, 2500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Pledge Short total eclipse XS'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Control Short red S'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Maguro Print Boardshort Youth shadow grey XXS'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(2500, 3000)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Gusty Tote Ball Bag Women desert point Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Rachel Women black Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Seizure Satchel brown Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(3000, 3500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_AC Trefoil Shop Bag white whitebluebird Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Airline Bag white Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(3500, 4000)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/a_3'))

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Handshake Belt field green L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Crammed SS bright white S'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Quad Tee SS light yellow XL'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(500, 1000)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Slip On Vans Classic Slip on black white checkerwhite 95'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Helmet Women TSG Lotus Graphic Designs wms Butterfly LXL'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Protector Pro Tec IPS Back Pads blackred XL'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(1000, 1500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_T-Shirt Men Playboard Flower SS black L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Sneakers Vans Old Skool blackwhite 100'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_T-Shirt Men Playboard Logo Tee red S'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(1500, 2000)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Snowboard Binding F2-FTWO Mondo LT black L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_T-Shirt Men Playboard Raster SS black L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Protector Dainese Waistcoat S7 blacksilver M'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(2000, 2500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Last Mission Jacket snapper L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Kampai Jacket blackblackdark copper L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Andover Jacket cardinal S'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(2500, 3000)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_DMT Diamond File red'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Repair Candle 6 mm graphite Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Base Brush Combi NylonCopper -'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(3000, 3500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Custom Est 1112 red L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_HD Hero2 Outdoor Edition uni Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(3500, 4000)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/a_4'))

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Easy Beanie black Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_The Reservoir Beanie caribbean blue Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_The Life Beanie black Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(500, 1000)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Snowboard Ski Tool Toko Thermo Pad'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Shades Fox The Duncan polished black grey'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Snowboard Ski Tool Toko Skibrste Kombi NylonKupfer'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(1000, 1500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Shade Accessories Spy Omega Lens persimmon'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Snowboard Ski Tool FBI 6'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Wax Toko S3 Hydro Carbon Set 120 g yellowred'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(1500, 2000)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Shades Von Zipper Fernstein gold moss gradient'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Snowboard Ski Tool Toko Base File Radial'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Snowboard Ski Tool Toko Multi Base File Control'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(2000, 2500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Snowboard Ski Tool Toko T8 800 Wachsbgeleisen fr Heiwachs'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Shades Anon Convict black fade grey gradient'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Shades Quiksilver Dinero black white red grey'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(2500, 3000)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Shades Anon Legion crystal  black grey'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Shades Anon Allie brown tortoise brown'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Surf Book Periplus ACIon Guides Surfing Australia'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(3000, 3500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Shades Fox The Condition polished black grey'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Cap Rip Curl D Cap vetiver'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(3500, 4000)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/a_5'))

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_T-Shirt Men Playboard Skull SS black S'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Protector Dainese Shield 8 Evo white L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_BT Airhole Splatter Facemask black LXL'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(500, 1000)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Whats Your Niner team blue Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Sadplant Pipe Glove blackorange M'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Buffer II Sock cardinal L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(1000, 1500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Landen Insulated Pant medieval blue L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Machete 158 1112 uni 1580'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_5 Pocket Freedom Pant burnt greens M'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(1500, 2000)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Team 153 1112 uni 1530'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Monkeywrench 157 1112 uni 1540'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Twc Standard 156W 1112 uni 1560'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(2000, 2500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Tracker Insulated Jacket Youth asteroid plaid L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Amped Jacket youth mascot trippleo S'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Minishred Illusion 1 Piece yth honeydew S'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(2500, 3000)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Sweetart Pant youth lemonhead jumanji M'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Fornax Pant Insulated youth black L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Tabloid Jacket Women academy L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(3000, 3500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Dandridge Down Jacket Women lady luck groupie plaid L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Twain Pant Women peacock M'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(3500, 4000)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/a_6'))

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Strybal Dress Women heron blue S'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Stella Dress Women blue checked L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Nature 19 Boardshort youth girls white M'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(500, 1000)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Forever Creedlers Women black 100'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Blocked Boardshort youth quik red XL'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Boardshort youth other square M'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(1000, 1500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Overlay Cushion Women zebra 100'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Rocking 2 Creedlers Women mix 100'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Wind Jammer Tote Beach Bag Women sand Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(1500, 2000)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Baby II Bag Women cloud Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Dream Team II Skate Backpack black Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Vintage 88 black havana brown'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(2000, 2500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Seizure Bag clay court Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Vintage 72 red white silver mirror'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Overtime Belt Women trashed black Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(2500, 3000)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Override Reversible Belt Women black L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Incision Leather Belt shadow grey SM'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Granda Belt white L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(3000, 3500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Bike Bones SS Youth black L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Brands_ProductListingPage_OR/Page_Brands  Rapid Hybris/span_Eat Sleep Dream SS F black L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(3500, 4000)

WebUI.delay(2)

