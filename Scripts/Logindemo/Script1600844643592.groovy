import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.waitForPageLoad(30)

WebUI.navigateToUrl('https://dev-omnibus.hybrisdemo.sbpcorp.com/')

WebUI.maximizeWindow()

WebUI.click(findTestObject('Object Repository/Logindemo_OR/Page_Rapid Hybris  Homepage/span_Quick Order_far fa-user'))

WebUI.setText(findTestObject('Object Repository/Logindemo_OR/Page_Login  Rapid Hybris/input_Email Address_j_username'), 
    'prakashdev4@test.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Logindemo_OR/Page_Login  Rapid Hybris/input_Password_j_password'), 
    'zXMD8VQinzNakze1o1dv8w==')

WebUI.click(findTestObject('Object Repository/Logindemo_OR/Page_Login  Rapid Hybris/button_Log In'))

WebUI.click(findTestObject('Object Repository/Logindemo_OR/Page_Rapid Hybris  Homepage/span_Quick Order'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Logindemo_OR/Page_Rapid Hybris  Homepage/a_Sign Out'))

