import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.yourcompany.YourClass1.*
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.yourcompany.YourClass1 as YourClass1
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import static com.yourcompany.ConsoleRobot.*
import com.yourcompany.ConsoleRobot as ConsoleRobot

WebUI.openBrowser('')

//WebUI.navigateToUrl('https://apparel-uk.hybrisdemo.sbpcorp.com/')
WebUI.navigateToUrl("http://$GlobalVariable.Hostname")

WebUI.maximizeWindow()

WebUI.click(findTestObject('CheckOutTest_OR/Page_Apparel Site UK  Homepage/span_Quick Order_far fa-user'))

WebUI.click(findTestObject('Object Repository/Youth_ProductListingPage_OR1/Page_Login  Apparel Site UK/div_Email Address'))

WebUI.setText(findTestObject('Object Repository/Youth_ProductListingPage_OR1/Page_Login  Apparel Site UK/input_Email Address_j_username'), 
    "$GlobalVariable.Email")

WebUI.setEncryptedText(findTestObject('Object Repository/Youth_ProductListingPage_OR1/Page_Login  Apparel Site UK/input_Password_j_password'), 
    "$GlobalVariable.Password")

WebUI.scrollToPosition(0, 250)

WebUI.click(findTestObject('Object Repository/Youth_ProductListingPage_OR1/Page_Login  Apparel Site UK/button_Log In'))

WebUI.click(findTestObject('Object Repository/Youth_ProductListingPage_OR1/Page_Apparel Site UK  Homepage/a_Youth'))

WebUI.scrollToPosition(0, 500)

WebUI.click(findTestObject('Object Repository/Youth_ProductListingPage_OR1/Page_Streetwear youth  Categories  Apparel Site UK/span_Thort SS youth white S'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Youth_ProductListingPage_OR1/Page_Streetwear youth  Categories  Apparel Site UK/span_Del Chaos Tee SS Youth white L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Youth_ProductListingPage_OR1/Page_Streetwear youth  Categories  Apparel Site UK/span_Boxed Out Boardshort youth multi XL'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(500, 1000)

WebUI.click(findTestObject('Object Repository/Youth_ProductListingPage_OR1/Page_Streetwear youth  Categories  Apparel Site UK/span_Alex Boardshort Youth neo orange S'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Youth_ProductListingPage_OR1/Page_Streetwear youth  Categories  Apparel Site UK/span_Supremacy Boardshort Youth charcoal XS'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Youth_ProductListingPage_OR1/Page_Streetwear youth  Categories  Apparel Site UK/span_Eclipse SS youth red fire XS'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(1000, 1500)

WebUI.click(findTestObject('Object Repository/Youth_ProductListingPage_OR1/Page_Streetwear youth  Categories  Apparel Site UK/span_Asterisk SS youth dirty plum M'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Youth_ProductListingPage_OR1/Page_Streetwear youth  Categories  Apparel Site UK/span_Gary Polo SS Youth lime S'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Youth_ProductListingPage_OR1/Page_Streetwear youth  Categories  Apparel Site UK/span_Nature 19 Boardshort youth girls white M'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(1500, 2000)

WebUI.click(findTestObject('Object Repository/Youth_ProductListingPage_OR1/Page_Streetwear youth  Categories  Apparel Site UK/span_Blocked Boardshort youth quik red XL'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Youth_ProductListingPage_OR1/Page_Streetwear youth  Categories  Apparel Site UK/span_Boardshort youth other square M'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Youth_ProductListingPage_OR1/Page_Streetwear youth  Categories  Apparel Site UK/span_Bike Bones SS Youth black L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(2000, 2500)

WebUI.click(findTestObject('Object Repository/Youth_ProductListingPage_OR1/Page_Streetwear youth  Categories  Apparel Site UK/span_Skull 80 SS youth navy S'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Youth_ProductListingPage_OR1/Page_Streetwear youth  Categories  Apparel Site UK/span_Monster SS Youth Girls fuchsiapink L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Youth_ProductListingPage_OR1/Page_Streetwear youth  Categories  Apparel Site UK/span_Flow Tee SS Youth royal L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(2500, 3000)

WebUI.click(findTestObject('Object Repository/Youth_ProductListingPage_OR1/Page_Streetwear youth  Categories  Apparel Site UK/span_Maguro Print Boardshort Youth shadow grey XS'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Youth_ProductListingPage_OR1/Page_Streetwear youth  Categories  Apparel Site UK/span_Mountain SS Basic Tee Logo youth trigo XL'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(3000, 3500)

WebUI.delay(3)

WebUI.click(findTestObject('Accessories_ProductListingPage_OR/Page_Accessories  Categories  Rapid Hybris/button_Listbottom'))

WebUI.delay(3)

WebUI.click(findTestObject('SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Rapid Hybris/button_Gridbottom'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Youth_ProductListingPage_OR1/Page_Streetwear youth  Categories  Apparel Site UK/select_Sort by  Relevance  Top Rated'), 
    'topRated', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Youth_ProductListingPage_OR1/Page_Streetwear youth  Categories  Apparel Site UK/select_Sort by  Relevance  Top Rated'), 
    'name-asc', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Youth_ProductListingPage_OR1/Page_Streetwear youth  Categories  Apparel Site UK/select_Sort by  Relevance  Top Rated'), 
    'name-desc', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Youth_ProductListingPage_OR1/Page_Streetwear youth  Categories  Apparel Site UK/select_Sort by  Relevance  Top Rated'), 
    'price-asc', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Youth_ProductListingPage_OR1/Page_Streetwear youth  Categories  Apparel Site UK/select_Sort by  Relevance  Top Rated'), 
    'price-desc', true)

