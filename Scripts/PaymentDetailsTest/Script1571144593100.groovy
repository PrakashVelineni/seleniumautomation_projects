import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import static com.yourcompany.ConnectionNotPrivate.*
import com.yourcompany.ConnectionNotPrivate as ConnectionNotPrivate

//ConnectionNotPrivate.yourMethod()
WebUI.openBrowser('')

WebUI.navigateToUrl("http://$GlobalVariable.Hostname")

WebUI.maximizeWindow()

WebUI.click(findTestObject('CheckOutTest_OR/Page_Apparel Site UK  Homepage/span_Quick Order_far fa-user'))

WebUI.setText(findTestObject('Object Repository/PaymentDetailsTest_OR/Page_Login  Apparel Site UK/input_Email Address_j_username'), 
    "$GlobalVariable.Email")

WebUI.setEncryptedText(findTestObject('Object Repository/PaymentDetailsTest_OR/Page_Login  Apparel Site UK/input_Password_j_password'), 
    "$GlobalVariable.Password")

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/PaymentDetailsTest_OR/Page_Login  Apparel Site UK/button_Log In'))

WebUI.delay(2)

WebUI.click(findTestObject('CheckOutTest_OR/Page_Apparel Site UK  Homepage/span_Quick Order_far fa-user'))

WebUI.click(findTestObject('Object Repository/PaymentDetailsTest_OR/Page_Apparel Site UK  Homepage/a_Payment Details'))

WebUI.scrollToPosition(0, 100)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/PaymentDetailsTest_OR/Page_Payment Details  Apparel Site UK/button_Set as default'))

WebUI.delay(2)

WebUI.verifyElementPresent(findTestObject('Object Repository/PaymentDetailsTest_OR/Page_Payment Details  Apparel Site UK/div_Your default payment was updated'), 
    0)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/PaymentDetailsTest_OR/Page_Payment Details  Apparel Site UK/button_'))

WebUI.delay(2)

WebUI.click(findTestObject('PaymentDetailsTest_OR/Page_Payment Details  Apparel Site UK/a_United Kingdom522212_action-links removePaymentDetailsButton'))

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('PaymentDetailsTest_OR/Page_Payment Details  Apparel Site UK/button_Delete'))

WebUI.delay(2)

WebUI.verifyElementPresent(findTestObject('Object Repository/PaymentDetailsTest_OR/Page_Payment Details  Apparel Site UK/div_Payment Card removed successfully'), 
    0)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/PaymentDetailsTest_OR/Page_Payment Details  Apparel Site UK/button_'))

WebUI.delay(2)

WebUI.click(findTestObject('PaymentDetailsTest_OR/Page_Payment Details  Apparel Site UK/span_United Kingdom522212_glyphicon glyphicon-pencil'))

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('PaymentDetailsTest_OR/Page_Edit Payment Details  Apparel Site UK/select_6010203040506070809101112'), 
    '10', true)

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('PaymentDetailsTest_OR/Page_Edit Payment Details  Apparel Site UK/select_202820202021202220232024202520262027202820292030'), 
    '2023', true)

WebUI.delay(2)

WebUI.click(findTestObject('PaymentDetailsTest_OR/Page_Edit Payment Details  Apparel Site UK/button_Update'))

WebUI.click(findTestObject('PaymentDetailsTest_OR/Page_Payment Details  Apparel Site UK/span_United Kingdom522212_glyphicon glyphicon-pencil'))

WebUI.click(findTestObject('PaymentDetailsTest_OR/Page_Edit Payment Details  Apparel Site UK/button_Cancel'))

