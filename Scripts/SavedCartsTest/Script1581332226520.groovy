import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import static com.yourcompany.ConnectionNotPrivate.*
import com.yourcompany.ConnectionNotPrivate as ConnectionNotPrivate

//ConnectionNotPrivate.yourMethod()
WebUI.openBrowser('')

WebUI.navigateToUrl("http://$GlobalVariable.Hostname")

WebUI.maximizeWindow()

WebUI.click(findTestObject('Object Repository/SavedCartsTest_OR1/Page_Rapid Hybris  Homepage/span_Quick Order_far fa-user'))

WebUI.setText(findTestObject('Object Repository/SavedCartsTest_OR1/Page_Login  Rapid Hybris/input_Email Address_j_username'), 
    "$GlobalVariable.Email")

WebUI.setEncryptedText(findTestObject('Object Repository/SavedCartsTest_OR1/Page_Login  Rapid Hybris/input_Password_j_password'), 
    "$GlobalVariable.Password")

WebUI.click(findTestObject('Object Repository/SavedCartsTest_OR1/Page_Login  Rapid Hybris/button_Log In'))

WebUI.click(findTestObject('Object Repository/SavedCartsTest_OR1/Page_Rapid Hybris  Homepage/a_Accessories'))

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/SavedCartsTest_OR1/Page_Accessories  Categories  Rapid Hybris/button_1616_fas fa-cart-plus custom-cart js-enable-btn'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/SavedCartsTest_OR1/Page_Accessories  Categories  Rapid Hybris/button_Added to Your Shopping Bag_cboxClose'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/SavedCartsTest_OR1/Page_Accessories  Categories  Rapid Hybris/button_1454_fas fa-cart-plus custom-cart js-enable-btn'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/SavedCartsTest_OR1/Page_Accessories  Categories  Rapid Hybris/button_Added to Your Shopping Bag_cboxClose'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/SavedCartsTest_OR1/Page_Accessories  Categories  Rapid Hybris/span_Welcome kenneth_glyphicon fas fa-cart-plus'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/SavedCartsTest_OR1/Page_Your Shopping Bag  Rapid Hybris/a_New Cart'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/SavedCartsTest_OR1/Page_Your Shopping Bag  Rapid Hybris/input_Name_name'), 
    'prakash')

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/SavedCartsTest_OR1/Page_Your Shopping Bag  Rapid Hybris/textarea_Description_description'), 
    'prakash')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/SavedCartsTest_OR1/Page_Your Shopping Bag  Rapid Hybris/button_Save'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/SavedCartsTest_OR1/Page_Your Shopping Bag  Rapid Hybris/a_Saved Carts (1)'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/SavedCartsTest_OR1/Page_Saved Carts  Rapid Hybris/a_prakash'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/SavedCartsTest_OR1/Page_Saved Cart Details  Rapid Hybris/button_Edit'))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/SavedCartsTest_OR1/Page_Saved Cart Details  Rapid Hybris/input_Name_name'), 
    'prakash1')

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/SavedCartsTest_OR1/Page_Saved Cart Details  Rapid Hybris/textarea_prakash'), 
    'prakash1')

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/SavedCartsTest_OR1/Page_Saved Cart Details  Rapid Hybris/button_Save'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/SavedCartsTest_OR1/Page_Saved Cart Details  Rapid Hybris/span_Welcome kenneth_glyphicon fas fa-cart-plus'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/SavedCartsTest_OR1/Page_Your Shopping Bag  Rapid Hybris/a_Saved Carts (1)'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/SavedCartsTest_OR1/Page_Saved Carts  Rapid Hybris/span_Restore'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/SavedCartsTest_OR1/Page_Saved Carts  Rapid Hybris/input_Keep a copy of this cart on saved list_keepRestoredCart'))

WebUI.delay(2)

WebUI.click(findTestObject('SavedCartsTest_OR1/Page_Saved Carts  Rapid Hybris/button_Restore'))

WebUI.delay(2)

WebUI.refresh()

WebUI.delay(2)

WebUI.click(findTestObject('SavedCartsTest_OR1/Page_Your Shopping Bag  Rapid Hybris/span_Quick Order_far fa-userN2'))

WebUI.delay(2)

WebUI.click(findTestObject('SavedCartsTest_OR1/Page_Your Shopping Bag  Rapid Hybris/a_Saved CartsN2'))

WebUI.delay(2)

