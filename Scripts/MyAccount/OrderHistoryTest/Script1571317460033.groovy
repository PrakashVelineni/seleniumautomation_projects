import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys


import static com.yourcompany.ConnectionNotPrivate.*
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.yourcompany.ConnectionNotPrivate as ConnectionNotPrivate




//ConnectionNotPrivate.yourMethod()

WebUI.openBrowser('')

WebUI.navigateToUrl("http://$GlobalVariable.Hostname")

WebUI.maximizeWindow()

WebUI.delay(2)

WebUI.click(findTestObject('CheckOutTest_OR/Page_Apparel Site UK  Homepage/span_Quick Order_far fa-user'))

WebUI.delay(2)

WebUI.setText(findTestObject('OrderHistoryTest_OR/Page_Login  Apparel Site UK/input_Email Address_j_username'), "$GlobalVariable.Email")

WebUI.delay(2)

WebUI.setEncryptedText(findTestObject('OrderHistoryTest_OR/Page_Login  Apparel Site UK/input_Password_j_password'), "$GlobalVariable.Password")

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('OrderHistoryTest_OR/Page_Login  Apparel Site UK/button_Log In'))

WebUI.delay(2)

  WebUI.click(findTestObject('CheckOutTest_OR/Page_Apparel Site UK  Homepage/span_Quick Order_far fa-user'))

WebUI.delay(2)

WebUI.click(findTestObject('OrderHistoryTest_OR/Page_Apparel Site UK  Homepage/a_Order History'))

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/OrderHistoryTest_OR/Page_Order History  Apparel Site UK/select_Sort by   Date   Order Number'), 
    'byOrderNumber', true)

//WebUI.click(findTestObject('OrderHistoryTest_OR/Page_Order History  Apparel Site UK/select_Sort by  Date Order Number'))
WebUI.delay(2)

WebUI.click(findTestObject('OrderHistoryTest_OR/Page_Order History  Apparel Site UK/a_00035007'))

WebUI.delay(2)

WebUI.back()

WebUI.delay(2)

WebUI.click(findTestObject('OrderHistoryTest_OR/Page_Order History  Apparel Site UK/a_2'))

WebUI.delay(2)

WebUI.click(findTestObject('OrderHistoryTest_OR/Page_Order History  Apparel Site UK/a_Sort by_fas fa-long-arrow-alt-left'))

WebUI.delay(2)

WebUI.click(findTestObject('OrderHistoryTest_OR/Page_Order History  Apparel Site UK/a_(current)_fas fa-long-arrow-alt-right'))

WebUI.delay(2)

