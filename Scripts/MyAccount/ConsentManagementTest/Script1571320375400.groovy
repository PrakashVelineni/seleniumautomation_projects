import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger


import static com.yourcompany.ConnectionNotPrivate.*
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.yourcompany.ConnectionNotPrivate as ConnectionNotPrivate



//ConnectionNotPrivate.yourMethod()

WebUI.openBrowser('')

WebUI.navigateToUrl("http://$GlobalVariable.Hostname")

WebUI.maximizeWindow()

WebUI.delay(2)

WebUI.click(findTestObject('CheckOutTest_OR/Page_Apparel Site UK  Homepage/span_Quick Order_far fa-user'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/ConsentManagementTest_OR/Page_Login  Apparel Site UK/input_Email Address_j_username'), 
    "$GlobalVariable.Email")

WebUI.delay(2)

WebUI.setEncryptedText(findTestObject('Object Repository/ConsentManagementTest_OR/Page_Login  Apparel Site UK/input_Password_j_password'), 
    "$GlobalVariable.Password")

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ConsentManagementTest_OR/Page_Login  Apparel Site UK/button_Log In'))

WebUI.delay(2)

WebUI.click(findTestObject('CheckOutTest_OR/Page_Apparel Site UK  Homepage/span_Quick Order_far fa-user'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ConsentManagementTest_OR/Page_Apparel Site UK  Homepage/a_Consent Management'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ConsentManagementTest_OR/Page_Consent Management Page  Apparel Site UK/div_I approve to this sample consent_toggle-button__switch is-checked'))

WebUI.delay(2)

WebUI.verifyElementPresent(findTestObject('Object Repository/ConsentManagementTest_OR/Page_Consent Management Page  Apparel Site UK/div_Your consent has been withdrawn'), 
    0)

String consentwithdrawn = WebUI.getText(findTestObject('Object Repository/ConsentManagementTest_OR/Page_Consent Management Page  Apparel Site UK/div_Your consent has been withdrawn'))

KeywordLogger logger = new KeywordLogger()

logger.logInfo(consentwithdrawn)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ConsentManagementTest_OR/Page_Consent Management Page  Apparel Site UK/button_'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ConsentManagementTest_OR/Page_Consent Management Page  Apparel Site UK/div_I approve to this sample consent_toggle-button__switch'))

WebUI.delay(2)

WebUI.verifyElementPresent(findTestObject('Object Repository/ConsentManagementTest_OR/Page_Consent Management Page  Apparel Site UK/div_Your consent has been given'), 
    0)

String consentgiven = WebUI.getText(findTestObject('Object Repository/ConsentManagementTest_OR/Page_Consent Management Page  Apparel Site UK/div_Your consent has been given'))

KeywordLogger logger1 = new KeywordLogger()

logger1.logInfo(consentgiven)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ConsentManagementTest_OR/Page_Consent Management Page  Apparel Site UK/button_'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ConsentManagementTest_OR/Page_Consent Management Page  Apparel Site UK/label_I approve to this sample consent'))

WebUI.delay(2)

