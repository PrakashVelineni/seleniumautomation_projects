import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import static com.yourcompany.ConnectionNotPrivate.*
import com.yourcompany.ConnectionNotPrivate as ConnectionNotPrivate

//ConnectionNotPrivate.yourMethod()
WebUI.openBrowser('')

WebUI.navigateToUrl("http://$GlobalVariable.Hostname")

WebUI.maximizeWindow()

WebUI.delay(2)

WebUI.click(findTestObject('CheckOutTest_OR/Page_Apparel Site UK  Homepage/span_Quick Order_far fa-user'))

WebUI.delay(2)

WebUI.setText(findTestObject('SupportTicketsTest_OR/Page_Login  Apparel Site UK/input_Email Address_j_username'), "$GlobalVariable.Email")

WebUI.delay(2)

WebUI.setEncryptedText(findTestObject('SupportTicketsTest_OR/Page_Login  Apparel Site UK/input_Password_j_password'), "$GlobalVariable.Password")

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('SupportTicketsTest_OR/Page_Login  Apparel Site UK/button_Log In'))

WebUI.delay(2)

WebUI.click(findTestObject('CheckOutTest_OR/Page_Apparel Site UK  Homepage/span_Quick Order_far fa-user'))

WebUI.delay(2)

WebUI.click(findTestObject('SupportTicketsTest_OR/Page_Apparel Site UK  Homepage/a_Support Tickets'))

WebUI.delay(2)

WebUI.verifyElementPresent(findTestObject('SupportTicketsTest_OR/Page_Support Tickets listing  Apparel Site UK/div_You have no support tickets'), 
    0)

WebUI.delay(2)

String SupportTickets = WebUI.getText(findTestObject('SupportTicketsTest_OR/Page_Support Tickets listing  Apparel Site UK/div_You have no support tickets'))

KeywordLogger logger = new KeywordLogger()

logger.logInfo(SupportTickets)

WebUI.delay(2)

WebUI.click(findTestObject('SupportTicketsTest_OR/Page_Support Tickets listing  Apparel Site UK/a_Add new'))

WebUI.delay(2)

WebUI.setText(findTestObject('SupportTicketsTest_OR/Page_Add Support Ticket  Apparel Site UK/input_Subject_subject'), 'prakashkatalon')

WebUI.delay(2)

WebUI.setText(findTestObject('SupportTicketsTest_OR/Page_Add Support Ticket  Apparel Site UK/textarea_Message_message'), 
    'verifying support tickets functionality')

WebUI.delay(2)

WebUI.scrollToPosition(0, 100)

WebUI.sendKeys(findTestObject('SupportTicketsTest_OR/Page_Add Support Ticket  Apparel Site UK/div_Choose file'), 'D:\\SBP Consulting Inc\\Hybris - C 4HANA - SAP Commerce\\03. QA\\ELiteKart-Screenshots\\Apparel\\D_01.PNG')

WebUI.delay(2)

WebUI.scrollToPosition(0, 1000)

WebUI.delay(2)

WebUI.scrollToPosition(0, 1000)

WebUI.selectOptionByValue(findTestObject('SupportTicketsTest_OR/Page_Add Support Ticket  Apparel Site UK/select_EnquiryComplaintProblem'), 
    'COMPLAINT', true)

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('SupportTicketsTest_OR/Page_Add Support Ticket  Apparel Site UK/select_EnquiryComplaintProblem'), 
    'PROBLEM', true)

WebUI.delay(2)

WebUI.click(findTestObject('SupportTicketsTest_OR/Page_Add Support Ticket  Apparel Site UK/button_Submit'))

WebUI.scrollToPosition(250, 0)

WebUI.delay(2)

WebUI.verifyElementPresent(findTestObject('SupportTicketsTest_OR/Page_Support Tickets listing  Apparel Site UK/div_Thank you'), 
    0)

String SupportTickets1 = WebUI.getText(findTestObject('SupportTicketsTest_OR/Page_Support Tickets listing  Apparel Site UK/div_Thank you'))

KeywordLogger logger1 = new KeywordLogger()

logger1.logInfo(SupportTickets1)

WebUI.delay(2)

WebUI.click(findTestObject('SupportTicketsTest_OR/Page_Add Support Ticket  Apparel Site UK/button_'))

WebUI.delay(2)

