import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger


import static com.yourcompany.ConnectionNotPrivate.*
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.yourcompany.ConnectionNotPrivate as ConnectionNotPrivate




//ConnectionNotPrivate.yourMethod()


WebUI.openBrowser('')

WebUI.navigateToUrl("http://$GlobalVariable.Hostname")

WebUI.maximizeWindow()

WebUI.delay(2)

  WebUI.click(findTestObject('CheckOutTest_OR/Page_Apparel Site UK  Homepage/span_Quick Order_far fa-user'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/PasswordTest_OR/Page_Login  Apparel Site UK/input_Email Address_j_username'), 
    "$GlobalVariable.Email")

WebUI.delay(2)

WebUI.setEncryptedText(findTestObject('Object Repository/PasswordTest_OR/Page_Login  Apparel Site UK/input_Password_j_password'), 
    "$GlobalVariable.Password")

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/PasswordTest_OR/Page_Login  Apparel Site UK/button_Log In'))

WebUI.delay(2)

  WebUI.click(findTestObject('CheckOutTest_OR/Page_Apparel Site UK  Homepage/span_Quick Order_far fa-user'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/PasswordTest_OR/Page_Apparel Site UK  Homepage/a_Password'))

WebUI.delay(2)

WebUI.setEncryptedText(findTestObject('Object Repository/PasswordTest_OR/Page_Update Forgotten Password  Apparel Site UK/input_Current Password_currentPassword'), 
    "$GlobalVariable.Password")

WebUI.delay(2)

WebUI.setEncryptedText(findTestObject('Object Repository/PasswordTest_OR/Page_Update Forgotten Password  Apparel Site UK/input_New Password_newPassword'), 
    "$GlobalVariable.Password")

WebUI.delay(2)

WebUI.setEncryptedText(findTestObject('Object Repository/PasswordTest_OR/Page_Update Forgotten Password  Apparel Site UK/input_Confirm New Password_checkNewPassword'), 
    "$GlobalVariable.Password")

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/PasswordTest_OR/Page_Update Forgotten Password  Apparel Site UK/button_Update'))

WebUI.delay(2)

WebUI.verifyElementPresent(findTestObject('Object Repository/PasswordTest_OR/Page_Update Forgotten Password  Apparel Site UK/div_Your password has been changed'), 
    0)

String PasswordUpdate = WebUI.getText(findTestObject('Object Repository/PasswordTest_OR/Page_Update Forgotten Password  Apparel Site UK/div_Your password has been changed'))

KeywordLogger logger = new KeywordLogger()

logger.logInfo(PasswordUpdate)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/PasswordTest_OR/Page_Update Forgotten Password  Apparel Site UK/button_'))

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/PasswordTest_OR/Page_Update Forgotten Password  Apparel Site UK/button_Cancel'))

WebUI.delay(2)

