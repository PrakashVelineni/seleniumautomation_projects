import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://qa-omnibus.hybrisdemo.sbpcorp.com/')

WebUI.maximizeWindow()

WebUI.delay(2)

WebUI.click(findTestObject('CheckOutTest_OR/Page_Apparel Site UK  Homepage/span_Quick Order_far fa-user'))

WebUI.delay(2)

for (def rowNum = 1; rowNum <= findTestData('LoginExcelDataFile').getRowNumbers(); rowNum++) {
    WebUI.setText(findTestObject('Object Repository/ExcelLoginTest_OR/Page_Login  Apparel Site UK/input_Email Address_j_username'), 
        findTestData('LoginExcelDataFile').getValue('email', rowNum))

    WebUI.delay(2)

    WebUI.setText(findTestObject('Object Repository/ExcelLoginTest_OR/Page_Login  Apparel Site UK/input_Password_j_password'), 
        findTestData('LoginExcelDataFile').getValue('password', rowNum))

    WebUI.scrollToPosition(0, 250)

    WebUI.delay(2)

    WebUI.click(findTestObject('Object Repository/ExcelLoginTest_OR/Page_Login  Apparel Site UK/button_Log In'))

    WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ExcelLoginTest_OR/Page_Apparel Site UK  Homepage/a_Accessories'))

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ExcelLoginTest_OR/Page_Accessories  Categories  Apparel Site UK/span_HD Hero2 Outdoor Edition uni Uni'))

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ExcelLoginTest_OR/Page_HD Hero2 Outdoor Edition uni Uni  Videocamera  Accessories  Categories  Apparel Site UK/button_Add to bag'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ExcelLoginTest_OR/Page_HD Hero2 Outdoor Edition uni Uni  Videocamera  Accessories  Categories  Apparel Site UK/a_Check Out'))

if (WebUI.verifyElementPresent(findTestObject('CheckOutTest_OR/Page_Apparel Site UK  Homepage/span_Quick Order_far fa-user'),
	0)) {
	WebUI.delay(2)

   WebUI.click(findTestObject('CheckOutTest_OR/Page_Apparel Site UK  Homepage/span_Quick Order_far fa-user'))

    WebUI.delay(2)

	WebUI.click(findTestObject('CheckOutTest_OR/Page_Apparel Site UK  Homepage/span_Quick Order_far fa-user'))


	WebUI.delay(2)
}
}


