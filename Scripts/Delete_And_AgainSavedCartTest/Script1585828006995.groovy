import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import static com.yourcompany.ConnectionNotPrivate.*
import com.yourcompany.ConnectionNotPrivate as ConnectionNotPrivate

//ConnectionNotPrivate.yourMethod()
WebUI.openBrowser('')

WebUI.navigateToUrl("http://$GlobalVariable.Hostname")

WebUI.maximizeWindow()

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Rapid Hybris  Homepage/span_Quick Order_far fa-user'))

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Company', true)

WebUI.selectOptionByValue(findTestObject('Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Mr', true)

WebUI.selectOptionByValue(findTestObject('Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Miss', true)

WebUI.selectOptionByValue(findTestObject('Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Mrs', true)

WebUI.selectOptionByValue(findTestObject('Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Ms', true)

WebUI.selectOptionByValue(findTestObject('Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Rev.', true)

WebUI.selectOptionByValue(findTestObject('Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Prof.', true)

WebUI.selectOptionByValue(findTestObject('Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Dr.', true)

WebUI.setText(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Login  Rapid Hybris/input_First Name_firstName'), 
    'prakash')

WebUI.setText(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Login  Rapid Hybris/input_Last Name_lastName'), 
    'velineni')

WebUI.setText(findTestObject('RegistrationTest_OR/Page_Registration Apparel Site UK/input_Email Address_email'), "$GlobalVariable.Email")

WebUI.setEncryptedText(findTestObject('RegistrationTest_OR/Page_Registration Apparel Site UK/input_Password_pwd'), "$GlobalVariable.Password")

WebUI.setEncryptedText(findTestObject('RegistrationTest_OR/Page_Registration Apparel Site UK/input_Confirm Password_checkPwd'), 
    "$GlobalVariable.Password")

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Login  Rapid Hybris/consent'))

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Login  Rapid Hybris/input_I am confirming that I have read and agreed with the_termsCheck'))

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Login  Rapid Hybris/button_Register'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Rapid Hybris  Homepage/button_'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Rapid Hybris  Homepage/a_Accessories'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Accessories  Categories  Rapid Hybris/button_28346_fas fa-cart-plus custom-cart js-enable-btn'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Accessories  Categories  Rapid Hybris/button_Added to Your Shopping Bag_cboxClose'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Accessories  Categories  Rapid Hybris/button_1616_fas fa-cart-plus custom-cart js-enable-btn'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Accessories  Categories  Rapid Hybris/a_Continue Shopping'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Accessories  Categories  Rapid Hybris/span_Welcome prakash_glyphicon fas fa-cart-plus'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Your Shopping Bag  Rapid Hybris/a_New Cart'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Your Shopping Bag  Rapid Hybris/input_Name_name'), 
    'test1')

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Your Shopping Bag  Rapid Hybris/textarea_Description_description'), 
    'test1')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Your Shopping Bag  Rapid Hybris/button_Save'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Your Shopping Bag  Rapid Hybris/a_Streetwear'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Streetwear  Collections  Rapid Hybris/button_1778_fas fa-cart-plus custom-cart js-enable-btn'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Streetwear  Collections  Rapid Hybris/span_Added to Your Shopping Bag_glyphicon glyphicon-remove'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Streetwear  Collections  Rapid Hybris/button_3074_fas fa-cart-plus custom-cart js-enable-btn'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Streetwear  Collections  Rapid Hybris/a_Continue Shopping'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Streetwear  Collections  Rapid Hybris/span_System Tee SS dirty plum L'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_System Tee SS dirty plum  T-Shirts men  T-Shirts  Clothes  Categories  Rapid Hybris/button_Add to bag'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_System Tee SS dirty plum  T-Shirts men  T-Shirts  Clothes  Categories  Rapid Hybris/a_Check Out'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Your Shopping Bag  Rapid Hybris/a_New Cart'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Your Shopping Bag  Rapid Hybris/input_Name_name'), 
    'testdelete')

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Your Shopping Bag  Rapid Hybris/textarea_Description_description'), 
    'testdelete')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Your Shopping Bag  Rapid Hybris/button_Save'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Your Shopping Bag  Rapid Hybris/button_'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Your Shopping Bag  Rapid Hybris/a_Saved Carts (2)'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Saved Carts  Rapid Hybris/span_Restore_glyphicon glyphicon-remove'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Saved Carts  Rapid Hybris/button_Delete'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Saved Carts  Rapid Hybris/span_Restore'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Saved Carts  Rapid Hybris/input_Keep a copy of this cart on saved list_keepRestoredCart'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Saved Carts  Rapid Hybris/button_Restore'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Your Shopping Bag  Rapid Hybris/a_New Cart'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Your Shopping Bag  Rapid Hybris/input_Name_name'), 
    'test2')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Your Shopping Bag  Rapid Hybris/div_Your cart will  Characters Left  255'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Your Shopping Bag  Rapid Hybris/input_Name_name'), 
    'test2')

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Your Shopping Bag  Rapid Hybris/textarea_Description_description'), 
    'test2')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Your Shopping Bag  Rapid Hybris/button_Save'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Your Shopping Bag  Rapid Hybris/button_'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Your Shopping Bag  Rapid Hybris/a_Saved Carts (1)'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Saved Carts  Rapid Hybris/span_Restore_glyphicon glyphicon-remove'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Saved Carts  Rapid Hybris/button_Delete'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Saved Carts  Rapid Hybris/a_Brands'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Brands  Rapid Hybris/button_5096_fas fa-cart-plus custom-cart js-enable-btn'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Brands  Rapid Hybris/a_Check Out'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Your Shopping Bag  Rapid Hybris/a_New Cart'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Your Shopping Bag  Rapid Hybris/input_Name_name'), 
    'test4')

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Your Shopping Bag  Rapid Hybris/textarea_Description_description'), 
    'test4')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Your Shopping Bag  Rapid Hybris/button_Save'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Your Shopping Bag  Rapid Hybris/a_Youth'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Streetwear youth  Categories  Rapid Hybris/button_1859_fas fa-cart-plus custom-cart js-enable-btn'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Streetwear youth  Categories  Rapid Hybris/a_Check Out'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Your Shopping Bag  Rapid Hybris/a_Saved Carts (1)'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Saved Carts  Rapid Hybris/span_Restore'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Saved Carts  Rapid Hybris/input_Keep a copy of this cart on saved list_keepRestoredCart'))

WebUI.delay(2)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Saved Carts  Rapid Hybris/button_Restore'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Your Shopping Bag  Rapid Hybris/a_Youth'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Streetwear youth  Categories  Rapid Hybris/button_2021_fas fa-cart-plus custom-cart js-enable-btn'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Streetwear youth  Categories  Rapid Hybris/a_Check Out'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Your Shopping Bag  Rapid Hybris/span_Welcome prakash_glyphicon fas fa-cart-plus'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Your Shopping Bag  Rapid Hybris/a_New Cart'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Your Shopping Bag  Rapid Hybris/input_Name_name'), 
    'test5')

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Your Shopping Bag  Rapid Hybris/textarea_Description_description'), 
    'test5')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Your Shopping Bag  Rapid Hybris/button_Save'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Your Shopping Bag  Rapid Hybris/button_'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Your Shopping Bag  Rapid Hybris/span_Quick Order_far fa-user'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Your Shopping Bag  Rapid Hybris/a_Sign Out'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Your Shopping Bag  Rapid Hybris/span_Quick Order_far fa-user_1'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CartTest_WithLogin_OR/Page_Login  Apparel Site UK/input_Email Address_j_username'), 
    "$GlobalVariable.Email")

WebUI.delay(2)

WebUI.setEncryptedText(findTestObject('Object Repository/CartTest_WithLogin_OR/Page_Login  Apparel Site UK/input_Password_j_password'), 
    "$GlobalVariable.Password")

WebUI.delay(2)

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Login  Rapid Hybris/button_Log In'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Your Shopping Bag  Rapid Hybris/a_Streetwear'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Streetwear  Collections  Rapid Hybris/button_1778_fas fa-cart-plus custom-cart js-enable-btn'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Streetwear  Collections  Rapid Hybris/a_Check Out'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Your Shopping Bag  Rapid Hybris/a_Saved Carts (1)'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Saved Carts  Rapid Hybris/span_Restore'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Saved Carts  Rapid Hybris/input_Keep a copy of this cart on saved list_keepRestoredCart'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Saved Carts  Rapid Hybris/button_Restore'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Your Shopping Bag  Rapid Hybris/span_Quick Order_far fa-user'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Your Shopping Bag  Rapid Hybris/a_Sign Out'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Your Shopping Bag  Rapid Hybris/span_Quick Order_far fa-user_2'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CartTest_WithLogin_OR/Page_Login  Apparel Site UK/input_Email Address_j_username'), 
    "$GlobalVariable.Email")

WebUI.delay(2)

WebUI.setEncryptedText(findTestObject('Object Repository/CartTest_WithLogin_OR/Page_Login  Apparel Site UK/input_Password_j_password'), 
    "$GlobalVariable.Password")

WebUI.delay(2)

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Delete_AndSavedCartTest_OR/Page_Login  Rapid Hybris/button_Log In'))

WebUI.delay(2)

