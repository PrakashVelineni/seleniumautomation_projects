import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

//WebUI.navigateToUrl('https://ec2-18-214-70-23.compute-1.amazonaws.com:9002/yacceleratorstorefront/?site=apparel-uk')

import internal.GlobalVariable as GlobalVariable

WebUI.navigateToUrl("http://${GlobalVariable.Hostname}")

WebUI.maximizeWindow()

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/ExcelRegistrationTest_OR/Page_Apparel Site UK  Homepage/a_Sign in  Register'))

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('Object Repository/ExcelRegistrationTest_OR/Page_Login  Apparel Site UK/select_Please select                MrMissMrsMsDrRev'), 
    'mr', true)

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('Object Repository/ExcelRegistrationTest_OR/Page_Login  Apparel Site UK/select_Please select                MrMissMrsMsDrRev'), 
    'miss', true)

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('Object Repository/ExcelRegistrationTest_OR/Page_Login  Apparel Site UK/select_Please select                MrMissMrsMsDrRev'), 
    'mrs', true)

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('Object Repository/ExcelRegistrationTest_OR/Page_Login  Apparel Site UK/select_Please select                MrMissMrsMsDrRev'), 
    'ms', true)

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('Object Repository/ExcelRegistrationTest_OR/Page_Login  Apparel Site UK/select_Please select                MrMissMrsMsDrRev'), 
    'dr', true)

WebUI.delay(3)

WebUI.selectOptionByValue(findTestObject('Object Repository/ExcelRegistrationTest_OR/Page_Login  Apparel Site UK/select_Please select                MrMissMrsMsDrRev'), 
    'rev', true)

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/ExcelRegistrationTest_OR/Page_Login  Apparel Site UK/input_First Name_firstName'), 
    firstname)

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/ExcelRegistrationTest_OR/Page_Login  Apparel Site UK/input_Last Name_lastName'), 
    lastname)

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/ExcelRegistrationTest_OR/Page_Login  Apparel Site UK/input_Email Address_email'), 
    email)

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/ExcelRegistrationTest_OR/Page_Login  Apparel Site UK/input_Password_pwd'), 
    password)

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/ExcelRegistrationTest_OR/Page_Login  Apparel Site UK/input_Confirm Password_checkPwd'), 
    confirmpassword)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/ExcelRegistrationTest_OR/Page_Login  Apparel Site UK/input_Confirm Password_consentFormconsentGiven'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/ExcelRegistrationTest_OR/Page_Login  Apparel Site UK/input_I am confirming that I have read and agreed with the_termsCheck'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/ExcelRegistrationTest_OR/Page_Login  Apparel Site UK/button_Register'))

WebUI.delay(3)

WebUI.refresh()

if (WebUI.verifyElementPresent(findTestObject('Object Repository/ExcelRegistrationTest_OR/Page_Apparel Site UK  Homepage/div_My Account'), 
    0)) {
    WebUI.delay(3)

    WebUI.click(findTestObject('Object Repository/ExcelRegistrationTest_OR/Page_Apparel Site UK  Homepage/div_Thank you for registering'))

    WebUI.click(findTestObject('Object Repository/ExcelRegistrationTest_OR/Page_Apparel Site UK  Homepage/div_My Account'))

    WebUI.delay(3)

    WebUI.click(findTestObject('Object Repository/ExcelRegistrationTest_OR/Page_Apparel Site UK  Homepage/a_Sign Out'))
}

WebUI.delay(3)

