import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys



import static com.yourcompany.ConnectionNotPrivate.*
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.yourcompany.ConnectionNotPrivate as ConnectionNotPrivate




ConnectionNotPrivate.yourMethod()

/*WebUI.openBrowser('')

//WebUI.navigateToUrl('https://qa-omnibus.hybrisdemo.sbpcorp.com/')

import internal.GlobalVariable as GlobalVariable

WebUI.navigateToUrl("http://${GlobalVariable.Hostname}")

WebUI.maximizeWindow()*/

WebUI.delay(3)

WebUI.click(findTestObject('CheckOutTest_OR/Page_Apparel Site UK  Homepage/span_Quick Order_far fa-user'))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/LoginTest_OR/Page_Login  Apparel Site UK/input_Email Address_j_username'), 
    "$GlobalVariable.Email")

WebUI.setEncryptedText(findTestObject('Object Repository/LoginTest_OR/Page_Login  Apparel Site UK/input_Password_j_password'), 
    "$GlobalVariable.Password")

WebUI.scrollToPosition(0, 250)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/ExcelEmailAddressTest_OR/Page_Login  Apparel Site UK/button_Log In'))

WebUI.delay(3)

WebUI.click(findTestObject('CheckOutTest_OR/Page_Apparel Site UK  Homepage/span_Quick Order_far fa-user'))


WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/ExcelEmailAddressTest_OR/Page_Apparel Site UK  Homepage/a_Email Address'))

WebUI.delay(3)

for (def rowNum = 1; rowNum <= findTestData('EmailAddressExcelDataFile').getRowNumbers(); rowNum++) {

WebUI.setText(findTestObject('Object Repository/ExcelEmailAddressTest_OR/Page_Update Email Page  Apparel Site UK/input_New Email Address_email'), 
    findTestData('EmailAddressExcelDataFile').getValue('NewEmailAddress', rowNum))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/ExcelEmailAddressTest_OR/Page_Update Email Page  Apparel Site UK/input_Confirm New Email Address_chkEmail'), 
    findTestData('EmailAddressExcelDataFile').getValue('ConfirmNewEmailAddress', rowNum))

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/ExcelEmailAddressTest_OR/Page_Update Email Page  Apparel Site UK/input_Password_password'), 
    findTestData('EmailAddressExcelDataFile').getValue('Password', rowNum))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/ExcelEmailAddressTest_OR/Page_Update Email Page  Apparel Site UK/button_Update'))

WebUI.delay(3)

WebUI.scrollToPosition(0, 250)

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/ExcelEmailAddressTest_OR/Page_Update Email Page  Apparel Site UK/button_Cancel'))
}
