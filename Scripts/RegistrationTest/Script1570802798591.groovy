import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys


import static com.yourcompany.ConnectionNotPrivate.*
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.yourcompany.ConnectionNotPrivate as ConnectionNotPrivate




//ConnectionNotPrivate.yourMethod()

WebUI.openBrowser('')

WebUI.navigateToUrl("http://$GlobalVariable.Hostname")

WebUI.maximizeWindow()

WebUI.delay(2)

WebUI.click(findTestObject('RegistrationTest_OR/Page_Apparel Site UK  Homepage/span_Quick Order_far fa-user'))

WebUI.selectOptionByValue(findTestObject('RegistrationTest_OR/Page_Registration Apparel Site UK/select_Please select                MrMissMrsMsDrRev'), 
    'mr', true)

WebUI.selectOptionByValue(findTestObject('RegistrationTest_OR/Page_Registration Apparel Site UK/select_Please select                MrMissMrsMsDrRev'), 
    'miss', true)

WebUI.selectOptionByValue(findTestObject('RegistrationTest_OR/Page_Registration Apparel Site UK/select_Please select                MrMissMrsMsDrRev'), 
    'mrs', true)

WebUI.selectOptionByValue(findTestObject('RegistrationTest_OR/Page_Registration Apparel Site UK/select_Please select                MrMissMrsMsDrRev'), 
    'ms', true)

WebUI.selectOptionByValue(findTestObject('RegistrationTest_OR/Page_Registration Apparel Site UK/select_Please select                MrMissMrsMsDrRev'), 
    'dr', true)

WebUI.selectOptionByValue(findTestObject('RegistrationTest_OR/Page_Registration Apparel Site UK/select_Please select                MrMissMrsMsDrRev'), 
    'rev', true)

WebUI.setText(findTestObject('RegistrationTest_OR/Page_Registration Apparel Site UK/input_First Name_firstName'), 'Robert')

WebUI.setText(findTestObject('RegistrationTest_OR/Page_Registration Apparel Site UK/input_Last Name_lastName'), 'Joseph')

WebUI.setText(findTestObject('RegistrationTest_OR/Page_Registration Apparel Site UK/input_Email Address_email'), "$GlobalVariable.Email")

WebUI.setEncryptedText(findTestObject('RegistrationTest_OR/Page_Registration Apparel Site UK/input_Password_pwd'), "$GlobalVariable.Password")

WebUI.setEncryptedText(findTestObject('RegistrationTest_OR/Page_Registration Apparel Site UK/input_Confirm Password_checkPwd'), 
    "$GlobalVariable.Password")

WebUI.click(findTestObject('RegistrationTest_OR/Page_Registration Apparel Site UK/input_Confirm Password_consentFormconsentGiven'))

WebUI.click(findTestObject('RegistrationTest_OR/Page_Registration Apparel Site UK/input_I am confirming that I have read and agreed with the_termsCheck'))

WebUI.delay(2)

WebUI.click(findTestObject('RegistrationTest_OR/Page_Registration Apparel Site UK/button_Register'))

WebUI.delay(2)

//WebUI.click(findTestObject('Object Repository/RegistrationTest_OR/Page_Apparel Site UK  Homepage/button_'))
//WebUI.click(findTestObject('Object Repository/RegistrationTest_OR/Page_Apparel Site UK  Homepage/a_Sign Out'))
//if (WebUI.verifyElementClickable(findTestObject('Object Repository/ExcelRegistrationTest_OR/Page_Apparel Site UK  Homepage/div_My Account'))) {
//WebUI.delay(3)
//	WebUI.click(findTestObject('Object Repository/ExcelRegistrationTest_OR/Page_Apparel Site UK  Homepage/div_Thank you for registering'))
//WebUI.delay(3)
//WebUI.click(findTestObject('Object Repository/ExcelRegistrationTest_OR/Page_Apparel Site UK  Homepage/div_My Account'))
//	WebUI.delay(3)
//WebUI.click(findTestObject('Object Repository/ExcelRegistrationTest_OR/Page_Apparel Site UK  Homepage/a_Sign Out'))
//	WebUI.delay(3)
//}
WebUI.delay(2)

