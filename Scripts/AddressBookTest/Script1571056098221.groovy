import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import static com.yourcompany.ConnectionNotPrivate.*
import com.yourcompany.ConnectionNotPrivate as ConnectionNotPrivate

//ConnectionNotPrivate.yourMethod()
WebUI.openBrowser('')

//WebUI.navigateToUrl('https://qa-omnibus.hybrisdemo.sbpcorp.com/')
WebUI.navigateToUrl("http://$GlobalVariable.Hostname")

WebUI.maximizeWindow()

WebUI.delay(2)

WebUI.click(findTestObject('CheckOutTest_OR/Page_Apparel Site UK  Homepage/span_Quick Order_far fa-user'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/AddressBookTest_OR/Page_Login  Apparel Site UK/input_Email Address_j_username'), 
    "$GlobalVariable.Email")

WebUI.delay(2)

WebUI.setEncryptedText(findTestObject('Object Repository/AddressBookTest_OR/Page_Login  Apparel Site UK/input_Password_j_password'), 
    "$GlobalVariable.Password")

WebUI.delay(2)

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/AddressBookTest_OR/Page_Login  Apparel Site UK/button_Log In'))

WebUI.delay(2)

WebUI.click(findTestObject('CheckOutTest_OR/Page_Apparel Site UK  Homepage/span_Quick Order_far fa-user'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/AddressBookTest_OR/Page_Apparel Site UK  Homepage/a_Address Book'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/AddressBookTest_OR/Page_Address Book  Apparel Site UK/span_United Kingdom522212_glyphicon glyphicon-pencil'))

WebUI.scrollToPosition(0, 500)

WebUI.click(findTestObject('Object Repository/AddressBookTest_OR/Page_AddEdit Address  Apparel Site UK/button_Save'))

WebUI.delay(2)

WebUI.click(findTestObject('AddressBookTest_OR/Page_AddEdit Address  Apparel Site UK/button_'))

WebUI.click(findTestObject('AddressBookTest_OR/Page_Address Book  Apparel Site UK/span_United Kingdom522212_glyphicon glyphicon-remove'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/AddressBookTest_OR/Page_Address Book  Apparel Site UK/a_Delete'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/AddressBookTest_OR/Page_Address Book  Apparel Site UK/a_Add Address'))

WebUI.selectOptionByValue(findTestObject('Object Repository/AddressBookTest_OR/Page_AddEdit Address  Apparel Site UK/select_Country                GuernseyIsle of ManJerseyUnited Kingdom'), 
    'GG', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/AddressBookTest_OR/Page_AddEdit Address  Apparel Site UK/select_Country                GuernseyIsle of ManJerseyUnited Kingdom'), 
    'IM', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/AddressBookTest_OR/Page_AddEdit Address  Apparel Site UK/select_Country                GuernseyIsle of ManJerseyUnited Kingdom'), 
    'JE', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/AddressBookTest_OR/Page_AddEdit Address  Apparel Site UK/select_Country                GuernseyIsle of ManJerseyUnited Kingdom'), 
    'GB', true)

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_AddEdit Address  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Company', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_AddEdit Address  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Mr', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_AddEdit Address  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Miss', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_AddEdit Address  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Mrs', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_AddEdit Address  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Ms', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_AddEdit Address  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Rev.', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_AddEdit Address  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Prof.', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_AddEdit Address  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Dr.', true)

WebUI.setText(findTestObject('Object Repository/AddressBookTest_OR/Page_AddEdit Address  Apparel Site UK/input_First Name_firstName'), 
    'prakashaddress')

WebUI.setText(findTestObject('Object Repository/AddressBookTest_OR/Page_AddEdit Address  Apparel Site UK/input_Last Name_lastName'), 
    'velineniaddress')

WebUI.setText(findTestObject('Object Repository/AddressBookTest_OR/Page_AddEdit Address  Apparel Site UK/input_Address Line 1_line1'), 
    'mutluruaddress')

WebUI.setText(findTestObject('Object Repository/AddressBookTest_OR/Page_AddEdit Address  Apparel Site UK/input_(optional)_line2'), 
    'gunturaddress')

WebUI.setText(findTestObject('Object Repository/AddressBookTest_OR/Page_AddEdit Address  Apparel Site UK/input_City_townCity'), 
    'andhrapradeshaddress')

WebUI.setText(findTestObject('Object Repository/AddressBookTest_OR/Page_AddEdit Address  Apparel Site UK/input_Post Code_postcode'), 
    '522212')

WebUI.setText(findTestObject('Object Repository/AddressBookTest_OR/Page_AddEdit Address  Apparel Site UK/input_(optional)_phone'), 
    '9999901234')

WebUI.click(findTestObject('Object Repository/AddressBookTest_OR/Page_AddEdit Address  Apparel Site UK/input_Make this my default address_defaultAddress'))

WebUI.click(findTestObject('Object Repository/AddressBookTest_OR/Page_AddEdit Address  Apparel Site UK/button_Save'))

WebUI.verifyElementPresent(findTestObject('AddressBookTest_OR/Page_AddEdit Address  Apparel Site UK/div_Address created successfully'), 
    0)

String address = WebUI.getText(findTestObject('Object Repository/AddressBookTest_OR/Page_AddEdit Address  Apparel Site UK/div_Address created successfully'))

KeywordLogger logger = new KeywordLogger()

logger.logInfo(address)

WebUI.click(findTestObject('Object Repository/AddressBookTest_OR/Page_AddEdit Address  Apparel Site UK/button_'))

