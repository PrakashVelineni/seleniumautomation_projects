import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys




import static com.yourcompany.ConnectionNotPrivate.*
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.yourcompany.ConnectionNotPrivate as ConnectionNotPrivate




//ConnectionNotPrivate.yourMethod()





WebUI.openBrowser('')

//WebUI.navigateToUrl('https://qa-omnibus.hybrisdemo.sbpcorp.com/')

import internal.GlobalVariable as GlobalVariable

WebUI.navigateToUrl("http://${GlobalVariable.Hostname}")

WebUI.maximizeWindow()

WebUI.delay(2)

WebUI.click(findTestObject('CheckOutTest_OR/Page_Apparel Site UK  Homepage/span_Quick Order_far fa-user'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CartTest_WithLogin_OR/Page_Login  Apparel Site UK/input_Email Address_j_username'), 
     "$GlobalVariable.Email")

WebUI.delay(2)

WebUI.setEncryptedText(findTestObject('Object Repository/CartTest_WithLogin_OR/Page_Login  Apparel Site UK/input_Password_j_password'), 
     "$GlobalVariable.Password")

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartTest_WithLogin_OR/Page_Login  Apparel Site UK/button_Log In'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartTest_WithLogin_OR/Page_Apparel Site UK  Homepage/a_Accessories'))

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartTest_WithLogin_OR/Page_Accessories  Categories  Apparel Site UK/button_2021_fas fa-cart-plus custom-cart js-enable-btn'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartTest_WithLogin_OR/Page_Accessories  Categories  Apparel Site UK/a_Continue Shopping'))

WebUI.delay(2)

WebUI.mouseOver(findTestObject('Object Repository/CartTest_WithLogin_OR/Page_Accessories  Categories  Apparel Site UK/span_Quick Order_glyphicon fas fa-cart-plus'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartTest_WithLogin_OR/Page_Your Shopping Bag  Apparel Site UK/a_Check Out'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartTest_WithLogin_OR/Page_Your Shopping Bag  Apparel Site UK/button_Check Out'))

WebUI.delay(2)

