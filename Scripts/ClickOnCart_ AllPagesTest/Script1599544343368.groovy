import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import static com.yourcompany.ConnectionNotPrivate.*
import com.yourcompany.ConnectionNotPrivate as ConnectionNotPrivate
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import java.util.concurrent.TimeUnit as TimeUnit
import org.openqa.selenium.WebDriver as WebDriver
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent

//ConnectionNotPrivate.yourMethod()
WebUI.openBrowser('')

WebUI.waitForPageLoad(30)

WebDriver driver = DriverFactory.getWebDriver()

driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS)

WebUI.maximizeWindow()

WebUI.navigateToUrl("https://$GlobalVariable.Hostname")

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Rapid Hybris  Homepage/span_Quick Order_glyphicon fas fa-cart-plus'))

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Your Shopping Bag  Rapid Hybris/a_Accessories'))

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Accessories  Categories  Rapid Hybris/span_Quick Order_glyphicon fas fa-cart-plus'))

WebUI.back()

WebUI.scrollToPosition(0, 250)

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Accessories  Categories  Rapid Hybris/button_12956_fas fa-cart-plus custom-cart js-enable-btn'))

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Accessories  Categories  Rapid Hybris/span_Added to Your Shopping Bag_glyphicon glyphicon-remove'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Accessories  Categories  Rapid Hybris/span_The Sentry gunship'))

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_The Sentry gunship  Rapid Hybris/span_Quick Order_glyphicon fas fa-cart-plus'))

WebUI.back()

WebUI.scrollToPosition(0, 500)

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_The Sentry gunship  Rapid Hybris/button_Add to bag'))

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_The Sentry gunship  Rapid Hybris/span_Added to Your Shopping Bag_glyphicon glyphicon-remove'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_The Sentry gunship  Rapid Hybris/input_Quick Order_text'), 
    'brands')

WebUI.delay(5)

Robot robot = new Robot()

robot.keyPress(KeyEvent.VK_ENTER)

robot.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Search brands  Rapid Hybris/span_Quick Order_glyphicon fas fa-cart-plus'))

WebUI.back()

WebUI.scrollToPosition(0, 250)

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Search brands  Rapid Hybris/button_4046_fas fa-cart-plus custom-cart js-enable-btn'))

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Search brands  Rapid Hybris/button_Added to Your Shopping Bag_cboxClose'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Search brands  Rapid Hybris/span_Granda Belt white L'))

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Granda Belt white  Rapid Hybris/span_Quick Order_glyphicon fas fa-cart-plus'))

WebUI.back()

WebUI.scrollToPosition(0, 500)

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Granda Belt white  Rapid Hybris/button_Add to bag'))

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Granda Belt white  Rapid Hybris/span_Added to Your Shopping Bag_glyphicon glyphicon-remove'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Granda Belt white  Rapid Hybris/input_Quick Order_text'), 
    'streetwear')

WebUI.delay(5)

Robot robot1 = new Robot()

robot1.keyPress(KeyEvent.VK_ENTER)

robot1.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Search streetwear  Rapid Hybris/span_Quick Order_glyphicon fas fa-cart-plus'))

WebUI.back()

WebUI.scrollToPosition(0, 250)

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Search streetwear  Rapid Hybris/button_4046_fas fa-cart-plus custom-cart js-enable-btn'))

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Search streetwear  Rapid Hybris/span_Added to Your Shopping Bag_glyphicon glyphicon-remove'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Search streetwear  Rapid Hybris/span_Frickin Too Short 22 black S'))

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Frickin Too Short 22 black  Shorts  Streetwear men  Categories  Rapid Hybris/span_Quick Order_glyphicon fas fa-cart-plus'))

WebUI.back()

WebUI.scrollToPosition(0, 500)

//WebUI.selectOptionByValue(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Frickin Too Short 22 black  Shorts  Streetwear men  Categories  Rapid Hybris/select_Select 463'), 
//   '300606761', true)
WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Frickin Too Short 22 black  Shorts  Streetwear men  Categories  Rapid Hybris/button_Add to bag'))

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Frickin Too Short 22 black  Shorts  Streetwear men  Categories  Rapid Hybris/span_Added to Your Shopping Bag_glyphicon glyphicon-remove'))

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Frickin Too Short 22 black  Shorts  Streetwear men  Categories  Rapid Hybris/span_Quick Order_far fa-user'))

WebUI.setText(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Login  Rapid Hybris/input_Email Address_j_username'), 
    "$GlobalVariable.Email")

WebUI.setEncryptedText(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Login  Rapid Hybris/input_Password_j_password'), 
    "$GlobalVariable.Password")

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Login  Rapid Hybris/button_Log In'))

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Frickin Too Short 22 black  Shorts  Streetwear men  Categories  Rapid Hybris/img_Quick Order_lazy'))

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Rapid Hybris  Homepage/span_Welcome prakash_glyphicon fas fa-cart-plus'))

WebUI.back()

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Rapid Hybris  Homepage/a_Accessories'))

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Accessories  Categories  Rapid Hybris/span_Welcome prakash_glyphicon fas fa-cart-plus'))

WebUI.back()

WebUI.scrollToPosition(0, 250)

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Accessories  Categories  Rapid Hybris/button_12956_fas fa-cart-plus custom-cart js-enable-btn'))

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Accessories  Categories  Rapid Hybris/span_Added to Your Shopping Bag_glyphicon glyphicon-remove'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Accessories  Categories  Rapid Hybris/span_The Sentry gunship'))

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_The Sentry gunship  Rapid Hybris/span_Welcome prakash_glyphicon fas fa-cart-plus'))

WebUI.back()

WebUI.scrollToPosition(0, 500)

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_The Sentry gunship  Rapid Hybris/button_Add to bag'))

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_The Sentry gunship  Rapid Hybris/span_Added to Your Shopping Bag_glyphicon glyphicon-remove'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_The Sentry gunship  Rapid Hybris/input_Quick Order_text'), 
    'youth')

WebUI.delay(5)

Robot robot2 = new Robot()

robot2.keyPress(KeyEvent.VK_ENTER)

robot2.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Search youth  Rapid Hybris/span_Welcome prakash_glyphicon fas fa-cart-plus'))

WebUI.back()

WebUI.scrollToPosition(0, 250)

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Search youth  Rapid Hybris/button_3641_fas fa-cart-plus custom-cart js-enable-btn'))

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Search youth  Rapid Hybris/span_Added to Your Shopping Bag_glyphicon glyphicon-remove'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Search youth  Rapid Hybris/span_Boardshort youth summer M'))

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Boardshort youth summer  Kids  Collections  Rapid Hybris/span_Welcome prakash_glyphicon fas fa-cart-plus'))

WebUI.back()

WebUI.scrollToPosition(0, 250)

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Boardshort youth summer  Kids  Collections  Rapid Hybris/button_Add to bag'))

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Boardshort youth summer  Kids  Collections  Rapid Hybris/span'))

