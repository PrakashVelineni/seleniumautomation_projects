import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import static com.yourcompany.ConnectionNotPrivate.*
import com.yourcompany.ConnectionNotPrivate as ConnectionNotPrivate
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import java.util.concurrent.TimeUnit as TimeUnit
import org.openqa.selenium.WebDriver as WebDriver
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent

//ConnectionNotPrivate.yourMethod()
WebUI.openBrowser('')

WebUI.waitForPageLoad(30)

WebDriver driver = DriverFactory.getWebDriver()

driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS)

WebUI.maximizeWindow()

WebUI.navigateToUrl("https://$GlobalVariable.Hostname")

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris  Homepage/a_Streetwear'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.mouseOver(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Streetwear  Collections  Rapid Hybris/div_Shop by Price'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Streetwear  Collections  Rapid Hybris/div_Shop by Price'))

WebUI.delay(2)

WebUI.mouseOver(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Streetwear  Collections  Rapid Hybris/div_Shop by Price'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Streetwear  Collections  Rapid Hybris/div_Shop by Price'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Streetwear  Collections  Rapid Hybris/div_Shop by Size'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Streetwear  Collections  Rapid Hybris/div_Shop by Size'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Streetwear  Collections  Rapid Hybris/div_Shop by gender'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Streetwear  Collections  Rapid Hybris/div_Shop by gender'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Streetwear  Collections  Rapid Hybris/div_Shop by Brand'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Streetwear  Collections  Rapid Hybris/div_Shop by Brand'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Streetwear  Collections  Rapid Hybris/div_Shop by Collection'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Streetwear  Collections  Rapid Hybris/div_Shop by Collection'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Streetwear  Collections  Rapid Hybris/div_Shop by Category'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Streetwear  Collections  Rapid Hybris/div_Shop by Category'))

WebUI.delay(2)

WebUI.scrollToPosition(750, 0)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Streetwear  Collections  Rapid Hybris/button_List'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Streetwear  Collections  Rapid Hybris/button_Grid'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 4000)

WebUI.delay(2)

WebUI.mouseOver(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Streetwear  Collections  Rapid Hybris/button_List'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Streetwear  Collections  Rapid Hybris/button_List'))

WebUI.delay(2)

WebUI.mouseOver(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Streetwear  Collections  Rapid Hybris/button_Grid'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Streetwear  Collections  Rapid Hybris/button_Grid'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Frickin Too Short 22 black  Shorts  Streetwear men  Categories  Rapid Hybris/img_Quick Order_lazy'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Streetwear  Collections  Rapid Hybris/a_Accessories'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Accessories  Categories  Rapid Hybris/div_Shop by Price'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Accessories  Categories  Rapid Hybris/div_Shop by Price'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Accessories  Categories  Rapid Hybris/div_Shop by Colour'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Accessories  Categories  Rapid Hybris/div_Shop by Colour'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Accessories  Categories  Rapid Hybris/div_Shop by Size'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Accessories  Categories  Rapid Hybris/div_Shop by Size'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Accessories  Categories  Rapid Hybris/div_Shop by Brand'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Accessories  Categories  Rapid Hybris/div_Shop by Brand'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Accessories  Categories  Rapid Hybris/div_Shop by Collection'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Accessories  Categories  Rapid Hybris/div_Shop by Collection'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Accessories  Categories  Rapid Hybris/div_Shop by Category'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Accessories  Categories  Rapid Hybris/div_Shop by Category'))

WebUI.delay(2)

WebUI.scrollToPosition(750, 0)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Accessories  Categories  Rapid Hybris/button_List'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Accessories  Categories  Rapid Hybris/button_Grid'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 4000)

WebUI.delay(2)

WebUI.mouseOver(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Accessories  Categories  Rapid Hybris/button_List'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Accessories  Categories  Rapid Hybris/button_List'))

WebUI.delay(2)

WebUI.mouseOver(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Accessories  Categories  Rapid Hybris/button_Grid'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Accessories  Categories  Rapid Hybris/button_Grid'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Frickin Too Short 22 black  Shorts  Streetwear men  Categories  Rapid Hybris/img_Quick Order_lazy'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Accessories  Categories  Rapid Hybris/a_Snow'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Snow  Collections  Rapid Hybris/div_Shop by Category'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Snow  Collections  Rapid Hybris/div_Shop by Category'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Frickin Too Short 22 black  Shorts  Streetwear men  Categories  Rapid Hybris/img_Quick Order_lazy'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Snow  Collections  Rapid Hybris/a_Brands'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Brands  Rapid Hybris/div_Shop by Price'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Brands  Rapid Hybris/div_Shop by Price'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Brands  Rapid Hybris/div_Shop by Colour'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Brands  Rapid Hybris/div_Shop by Colour'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Brands  Rapid Hybris/div_Shop by Size'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Brands  Rapid Hybris/div_Shop by Size'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Brands  Rapid Hybris/div_Shop by gender'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Brands  Rapid Hybris/div_Shop by gender'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Brands  Rapid Hybris/div_Shop by Brand'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Brands  Rapid Hybris/div_Shop by Brand'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Brands  Rapid Hybris/div_Shop by Collection'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Brands  Rapid Hybris/div_Shop by Collection'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Brands  Rapid Hybris/div_Shop by Category'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Brands  Rapid Hybris/div_Shop by Category'))

WebUI.delay(2)

WebUI.scrollToPosition(750, 0)

WebUI.delay(2)

WebUI.mouseOver(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Brands  Rapid Hybris/button_List'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Brands  Rapid Hybris/button_List'))

WebUI.delay(2)

WebUI.mouseOver(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Brands  Rapid Hybris/button_Grid'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Brands  Rapid Hybris/button_Grid'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 4000)

WebUI.delay(2)

WebUI.mouseOver(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Brands  Rapid Hybris/button_List'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Brands  Rapid Hybris/button_List'))

WebUI.delay(2)

WebUI.mouseOver(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Brands  Rapid Hybris/button_Grid'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Brands  Rapid Hybris/button_Grid'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Frickin Too Short 22 black  Shorts  Streetwear men  Categories  Rapid Hybris/img_Quick Order_lazy'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Brands  Rapid Hybris/a_Youth'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Streetwear youth  Categories  Rapid Hybris/div_Shop by Price'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Streetwear youth  Categories  Rapid Hybris/div_Shop by Price'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Streetwear youth  Categories  Rapid Hybris/div_Shop by Colour'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Streetwear youth  Categories  Rapid Hybris/div_Shop by Colour'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Streetwear youth  Categories  Rapid Hybris/div_Shop by Size'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Streetwear youth  Categories  Rapid Hybris/div_Shop by Size'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Streetwear youth  Categories  Rapid Hybris/div_Shop by Brand'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Streetwear youth  Categories  Rapid Hybris/div_Shop by Brand'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Streetwear youth  Categories  Rapid Hybris/div_Shop by Collection'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Streetwear youth  Categories  Rapid Hybris/div_Shop by Collection'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Streetwear youth  Categories  Rapid Hybris/div_Shop by Category'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Streetwear youth  Categories  Rapid Hybris/div_Shop by Category'))

WebUI.delay(2)

WebUI.scrollToPosition(750, 0)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Streetwear youth  Categories  Rapid Hybris/button_List'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Streetwear youth  Categories  Rapid Hybris/button_Grid'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 4000)

WebUI.delay(2)

WebUI.mouseOver(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Streetwear youth  Categories  Rapid Hybris/button_List'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Streetwear youth  Categories  Rapid Hybris/button_List'))

WebUI.delay(2)

WebUI.mouseOver(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Streetwear youth  Categories  Rapid Hybris/button_Grid'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Streetwear youth  Categories  Rapid Hybris/button_Grid'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Frickin Too Short 22 black  Shorts  Streetwear men  Categories  Rapid Hybris/img_Quick Order_lazy'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Streetwear youth  Categories  Rapid Hybris/a_Youth'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Streetwear youth  Categories  Rapid Hybris/input_Quick Order_text'), 
    'ACCESSORIES')

WebUI.delay(2)

WebUI.click(findTestObject('ClickFilterArrowsListGridTest_OR/Page_Streetwear  Collections  Rapid Hybris/div_Shop by Price'))

WebUI.delay(2)

WebUI.click(findTestObject('ClickFilterArrowsListGridTest_OR/Page_Streetwear  Collections  Rapid Hybris/div_Shop by Price'))

WebUI.delay(2)

WebUI.click(findTestObject('ClickFilterArrowsListGridTest_OR/Page_Streetwear  Collections  Rapid Hybris/div_Shop by Size'))

WebUI.delay(2)

WebUI.click(findTestObject('ClickFilterArrowsListGridTest_OR/Page_Streetwear  Collections  Rapid Hybris/div_Shop by Size'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Search ACCESSORIES  Rapid Hybris/div_Shop by gender'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Search ACCESSORIES  Rapid Hybris/div_Shop by gender'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Search ACCESSORIES  Rapid Hybris/div_Shop by Brand'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Search ACCESSORIES  Rapid Hybris/div_Shop by Brand'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Search ACCESSORIES  Rapid Hybris/div_Shop by Collection'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Search ACCESSORIES  Rapid Hybris/div_Shop by Collection'))

WebUI.delay(2)

WebUI.mouseOver(findTestObject('ClickFilterArrowsListGridTest_OR/Page_Search ACCESSORIES  Rapid Hybris/div_Shop by Category'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Search ACCESSORIES  Rapid Hybris/div_Shop by Category'))

WebUI.delay(2)

WebUI.mouseOver(findTestObject('ClickFilterArrowsListGridTest_OR/Page_Search ACCESSORIES  Rapid Hybris/div_Shop by Category'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Search ACCESSORIES  Rapid Hybris/div_Shop by Category'))

WebUI.delay(2)

WebUI.scrollToPosition(750, 0)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Search ACCESSORIES  Rapid Hybris/button_Grid'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Search ACCESSORIES  Rapid Hybris/button_List'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Search ACCESSORIES  Rapid Hybris/button_List'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Search ACCESSORIES  Rapid Hybris/button_Grid'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 4000)

WebUI.delay(2)

WebUI.mouseOver(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Search ACCESSORIES  Rapid Hybris/button_List'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Search ACCESSORIES  Rapid Hybris/button_List'))

WebUI.delay(2)

WebUI.mouseOver(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Search ACCESSORIES  Rapid Hybris/button_Grid'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Search ACCESSORIES  Rapid Hybris/button_Grid'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Search ACCESSORIES  Rapid Hybris/a_2'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/div_Shop by Price'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/div_Shop by Price'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/div_Shop by Colour'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/div_Shop by Colour'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/div_Shop by Size'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/div_Shop by Size'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/div_Shop by gender'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/div_Shop by gender'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/div_Shop by Brand'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/div_Shop by Brand'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/div_Shop by Collection'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/div_Shop by Collection'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/div_Shop by Category'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/div_Shop by Category'))

WebUI.delay(2)

WebUI.scrollToPosition(750, 0)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/button_List'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/button_Grid'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 4000)

WebUI.delay(2)

WebUI.mouseOver(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/button_List'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/button_List'))

WebUI.delay(2)

WebUI.mouseOver(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/button_Grid'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/button_Grid'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/span_Quick Order_far fa-user'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Login  Rapid Hybris/input_Email Address_j_username'), 
    'prakashdev@test.com')

WebUI.delay(2)

WebUI.setEncryptedText(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Login  Rapid Hybris/input_Password_j_password'), 
    'zXMD8VQinzNakze1o1dv8w==')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Login  Rapid Hybris/button_Log In'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/div_Shop by Price'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/div_Shop by Price'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/div_Shop by Colour'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/div_Shop by Colour'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/div_Shop by Size'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/div_Shop by Size'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/div_Shop by gender'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/div_Shop by gender'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/div_Shop by Brand'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/div_Shop by Brand'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/div_Shop by Collection'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/div_Shop by Collection'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/div_Shop by Category'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/div_Shop by Category'))

WebUI.delay(2)

WebUI.scrollToPosition(750, 0)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/button_List'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/button_Grid'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 4000)

WebUI.delay(2)

WebUI.mouseOver(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/button_List'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/button_List'))

WebUI.delay(2)

WebUI.mouseOver(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/button_Grid'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/button_Grid'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickOnCart_AllPagesTest_OR1/Page_Frickin Too Short 22 black  Shorts  Streetwear men  Categories  Rapid Hybris/img_Quick Order_lazy'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/a_Brands'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Brands  Rapid Hybris/div_Shop by Price'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Brands  Rapid Hybris/div_Shop by Price'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Brands  Rapid Hybris/div_Shop by Colour'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Brands  Rapid Hybris/div_Shop by Colour'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Brands  Rapid Hybris/div_Shop by Size'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Brands  Rapid Hybris/div_Shop by Size'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Brands  Rapid Hybris/div_Shop by gender'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Brands  Rapid Hybris/div_Shop by gender'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Brands  Rapid Hybris/div_Shop by Brand'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Brands  Rapid Hybris/div_Shop by Brand'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Brands  Rapid Hybris/div_Shop by Collection'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Brands  Rapid Hybris/div_Shop by Collection'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Brands  Rapid Hybris/div_Shop by Category'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Brands  Rapid Hybris/div_Shop by Category'))

WebUI.delay(2)

WebUI.scrollToPosition(750, 0)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Brands  Rapid Hybris/button_List'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Brands  Rapid Hybris/button_Grid'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 4000)

WebUI.delay(2)

WebUI.mouseOver(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Brands  Rapid Hybris/button_List'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Brands  Rapid Hybris/button_List'))

WebUI.delay(2)

WebUI.mouseOver(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Brands  Rapid Hybris/button_Grid'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Brands  Rapid Hybris/button_Grid'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Brands  Rapid Hybris/input_Quick Order_text'), 
    'youth')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Search youth  Rapid Hybris/div_Shop by Price'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Search youth  Rapid Hybris/div_Shop by Price'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Search youth  Rapid Hybris/div_Shop by Colour'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Search youth  Rapid Hybris/div_Shop by Colour'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Search youth  Rapid Hybris/div_Shop by Size'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Search youth  Rapid Hybris/div_Shop by Size'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Search youth  Rapid Hybris/div_Shop by gender'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Search youth  Rapid Hybris/div_Shop by gender'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Search youth  Rapid Hybris/div_Shop by Brand'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Search youth  Rapid Hybris/div_Shop by Brand'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Search youth  Rapid Hybris/div_Shop by Collection'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Search youth  Rapid Hybris/div_Shop by Collection'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Search youth  Rapid Hybris/div_Shop by Category'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Search youth  Rapid Hybris/div_Shop by Category'))

WebUI.delay(2)

WebUI.scrollToPosition(750, 0)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Search youth  Rapid Hybris/button_List'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Search youth  Rapid Hybris/button_Grid'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 4000)

WebUI.delay(2)

WebUI.mouseOver(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Search youth  Rapid Hybris/button_List'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Search youth  Rapid Hybris/button_List'))

WebUI.delay(2)

WebUI.mouseOver(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Search youth  Rapid Hybris/button_Grid'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Search youth  Rapid Hybris/button_Grid'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Search youth  Rapid Hybris/a_(current)_fas fa-long-arrow-alt-right'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/div_Shop by Price'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/div_Shop by Price'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/div_Shop by Colour'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/div_Shop by Colour'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/div_Shop by Size'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/div_Shop by Size'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/div_Shop by gender'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/div_Shop by gender'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/div_Shop by Brand'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/div_Shop by Brand'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/div_Shop by Collection'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/div_Shop by Collection'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/div_Shop by Category'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/div_Shop by Category'))

WebUI.delay(2)

WebUI.scrollToPosition(750, 0)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/button_List'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/button_Grid'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 4000)

WebUI.delay(2)

WebUI.mouseOver(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/button_List'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/button_List'))

WebUI.delay(2)

WebUI.mouseOver(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/button_Grid'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ClickFilterArrowsListGridTest_OR/Page_Rapid Hybris/button_Grid'))

WebUI.delay(2)

