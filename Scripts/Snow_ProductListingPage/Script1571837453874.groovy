import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint

import static com.yourcompany.YourClass1.*
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.yourcompany.YourClass1 as YourClass1

import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import static com.yourcompany.ConsoleRobot.*
import com.yourcompany.ConsoleRobot as ConsoleRobot



WebUI.openBrowser('')

//WebUI.navigateToUrl('https://apparel-uk.hybrisdemo.sbpcorp.com/')

import internal.GlobalVariable as GlobalVariable

WebUI.navigateToUrl("http://${GlobalVariable.Hostname}")

WebUI.waitForPageLoad(30)

WebUI.maximizeWindow()

WebUI.click(findTestObject('CheckOutTest_OR/Page_Apparel Site UK  Homepage/span_Quick Order_far fa-user'))

WebUI.setText(findTestObject('Object Repository/ReturnsHistoryTest_OR/Page_Login  Apparel Site UK/input_Email Address_j_username'),
	"$GlobalVariable.Email")

WebUI.setEncryptedText(findTestObject('Object Repository/ReturnsHistoryTest_OR/Page_Login  Apparel Site UK/input_Password_j_password'),
	"$GlobalVariable.Password")

WebUI.scrollToPosition(0, 250)

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Login  Apparel Site UK/button_Log In'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Snow_ProductListingpage_OR/Page_Apparel Site UK  Homepage/a_Snow'))

WebUI.delay(3)

WebUI.verifyElementPresent(findTestObject('Object Repository/Snow_ProductListingpage_OR/Page_Snow  Collections  Apparel Site UK/img_Ski Gear_js-responsive-image'), 
    0)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Snow_ProductListingpage_OR/Page_Snow  Collections  Apparel Site UK/img_Ski Gear_js-responsive-image_1'))

WebUI.delay(3)

WebUI.scrollToPosition(0, 500)

WebUI.delay(3)

WebUI.scrollToPosition(500, 1000)

WebUI.delay(3)

WebUI.back()

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Snow_ProductListingpage_OR/Page_Snow  Collections  Apparel Site UK/img_Refine_js-responsive-image'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Snow_ProductListingpage_OR/Page_Snow  Collections  Apparel Site UK/img_Refine_js-responsive-image_1'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Snow_ProductListingpage_OR/Page_Snow  Collections  Apparel Site UK/img_Refine_js-responsive-image_2'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Snow_ProductListingpage_OR/Page_Snow  Collections  Apparel Site UK/img_Refine_js-responsive-image_3'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Snow_ProductListingpage_OR/Page_Snow  Collections  Apparel Site UK/img_Refine_js-responsive-image_4'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Snow_ProductListingpage_OR/Page_Snow  Collections  Apparel Site UK/img_Refine_js-responsive-image_5'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/Snow_ProductListingpage_OR/Page_Snow  Collections  Apparel Site UK/img_Refine_js-responsive-image_6'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(1000, 0)

WebUI.click(findTestObject('Object Repository/Snow_ProductListingpage_OR/Page_Snow  Collections  Apparel Site UK/a_Ski Gear'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

