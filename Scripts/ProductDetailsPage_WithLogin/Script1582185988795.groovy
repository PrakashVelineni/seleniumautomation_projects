import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl("http://$GlobalVariable.Hostname")

WebUI.maximizeWindow()

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ProductDetailsPage_WithLogin_OR/Page_Rapid Hybris  Homepage/span_Quick Order_far fa-user'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/ProductDetailsPage_WithLogin_OR/Page_Login  Rapid Hybris/input_Email Address_j_username'), 
    "$GlobalVariable.Email")

WebUI.delay(2)

WebUI.setEncryptedText(findTestObject('Object Repository/ProductDetailsPage_WithLogin_OR/Page_Login  Rapid Hybris/input_Password_j_password'), 
    "$GlobalVariable.Password")

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ProductDetailsPage_WithLogin_OR/Page_Login  Rapid Hybris/button_Log In'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 2000)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ProductDetailsPage_WithLogin_OR/Page_Rapid Hybris  Homepage/img_846_js-responsive-image lazy'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ProductDetailsPage_WithLogin_OR/Page_Maguro Pu Belt plaid  Streetwear  Collections  Rapid Hybris/button_Add to bag'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ProductDetailsPage_WithLogin_OR/Page_Maguro Pu Belt plaid  Streetwear  Collections  Rapid Hybris/button_Added to Your Shopping Bag_cboxClose'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ProductDetailsPage_WithLogin_OR/Page_Maguro Pu Belt plaid  Streetwear  Collections  Rapid Hybris/i_Added to Your Shopping Bag_far fa-heart'))

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/ProductDetailsPage_WithLogin_OR/Page_Maguro Pu Belt plaid  Streetwear  Collections  Rapid Hybris/select_Select a size  SizeLXL242610  SizeSM24262'), 
    '300785815', true)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ProductDetailsPage_WithLogin_OR/Page_Maguro Pu Belt plaid  Streetwear  Collections  Rapid Hybris/button_Add to bag'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ProductDetailsPage_WithLogin_OR/Page_Maguro Pu Belt plaid  Streetwear  Collections  Rapid Hybris/button_Added to Your Shopping Bag_cboxClose'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ProductDetailsPage_WithLogin_OR/Page_Maguro Pu Belt plaid  Streetwear  Collections  Rapid Hybris/i_Added to Your Shopping Bag_fa-heart fas'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ProductDetailsPage_WithLogin_OR/Page_Maguro Pu Belt plaid  Streetwear  Collections  Rapid Hybris/i_Added to Your Shopping Bag_fa-heart far'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ProductDetailsPage_WithLogin_OR/Page_Maguro Pu Belt plaid  Streetwear  Collections  Rapid Hybris/span_4'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ProductDetailsPage_WithLogin_OR/Page_Wish List Page  Rapid Hybris/a_View Details'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ProductDetailsPage_WithLogin_OR/Page_Maguro Pu Belt plaid  Streetwear  Collections  Rapid Hybris/button_Add to bag'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ProductDetailsPage_WithLogin_OR/Page_Maguro Pu Belt plaid  Streetwear  Collections  Rapid Hybris/span_Added to Your Shopping Bag_glyphicon glyphicon-remove'))

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/ProductDetailsPage_WithLogin_OR/Page_Maguro Pu Belt plaid  Streetwear  Collections  Rapid Hybris/select_Select a size  SizeLXL242610  SizeSM24262'), 
    '300785815', true)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ProductDetailsPage_WithLogin_OR/Page_Maguro Pu Belt plaid  Streetwear  Collections  Rapid Hybris/button_Add to bag'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ProductDetailsPage_WithLogin_OR/Page_Maguro Pu Belt plaid  Streetwear  Collections  Rapid Hybris/span_Added to Your Shopping Bag_glyphicon glyphicon-remove'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ProductDetailsPage_WithLogin_OR/Page_Maguro Pu Belt plaid  Streetwear  Collections  Rapid Hybris/i_Added to Your Shopping Bag_fa-heart fas'))

WebUI.delay(2)

WebUI.back()

WebUI.delay(2)

WebUI.back()

WebUI.scrollToPosition(500, 0)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ProductDetailsPage_WithLogin_OR/Page_Maguro Pu Belt plaid  Streetwear  Collections  Rapid Hybris/a_Write a Review'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/ProductDetailsPage_WithLogin_OR/Page_Maguro Pu Belt plaid  Streetwear  Collections  Rapid Hybris/input_Review Title_headline'), 
    'testpdp')

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/ProductDetailsPage_WithLogin_OR/Page_Maguro Pu Belt plaid  Streetwear  Collections  Rapid Hybris/textarea_Review Description_comment'), 
    'testpdp')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ProductDetailsPage_WithLogin_OR/Page_Maguro Pu Belt plaid  Streetwear  Collections  Rapid Hybris/span_Your Rating_js-ratingIcon glyphicon glyphicon-star lh active'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/ProductDetailsPage_WithLogin_OR/Page_Maguro Pu Belt plaid  Streetwear  Collections  Rapid Hybris/input_(optional)_alias'), 
    'testpdp')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ProductDetailsPage_WithLogin_OR/Page_Maguro Pu Belt plaid  Streetwear  Collections  Rapid Hybris/button_Submit Review'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/ProductDetailsPage_WithLogin_OR/Page_Maguro Pu Belt plaid  Streetwear  Collections  Rapid Hybris/button_'))

WebUI.delay(2)

WebUI.delay(2)

WebUI.scrollToPosition(0, 500)

WebUI.click(findTestObject('Object Repository/ProductDetailsPage_WithLogin_OR/Page_Maguro Pu Belt plaid  Streetwear  Collections  Rapid Hybris/a_Product Details'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ProductDetailsPage_WithLogin_OR/Page_Maguro Pu Belt plaid  Streetwear  Collections  Rapid Hybris/a_Specs'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ProductDetailsPage_WithLogin_OR/Page_Maguro Pu Belt plaid  Streetwear  Collections  Rapid Hybris/a_Reviews'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ProductDetailsPage_WithLogin_OR/Page_Maguro Pu Belt plaid  Streetwear  Collections  Rapid Hybris/button_Write a Review'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ProductDetailsPage_WithLogin_OR/Page_Maguro Pu Belt plaid  Streetwear  Collections  Rapid Hybris/button_Write a Review'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ProductDetailsPage_WithLogin_OR/Page_Maguro Pu Belt plaid  Streetwear  Collections  Rapid Hybris/a_Shipping'))

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('Object Repository/ImportSavedCart_Test_OR/Page_Apparel Site UK  Homepage/div_Import Saved Cart                                        Quick Order'), 
    0)

WebUI.mouseOver(findTestObject('Object Repository/ImportSavedCart_Test_OR/Page_Apparel Site UK  Homepage/div_Import Saved Cart                                        Quick Order'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ImportSavedCart_Test_OR/Page_Apparel Site UK  Homepage/a_Import Saved Cart'))

WebUI.delay(2)

WebUI.back()

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('Object Repository/QuickOrderTest_OR1/Page_Apparel Site UK  Homepage/div_Import Saved Cart                                        Quick Order'), 
    0)

WebUI.click(findTestObject('Object Repository/QuickOrderTest_OR1/Page_Apparel Site UK  Homepage/div_Import Saved Cart                                        Quick Order'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/QuickOrderTest_OR1/Page_Apparel Site UK  Homepage/a_Quick Order'))

WebUI.delay(2)

WebUI.back()

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ProductDetailsPage_WithLogin_OR/Page_Maguro Pu Belt plaid  Streetwear  Collections  Rapid Hybris/span_Welcome prakash_glyphicon fas fa-cart-plus'))

WebUI.delay(2)

WebUI.back()

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ProductDetailsPage_WithLogin_OR/Page_Maguro Pu Belt plaid  Streetwear  Collections  Rapid Hybris/span_3'))

WebUI.delay(2)

WebUI.back()

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ProductDetailsPage_WithLogin_OR/Page_Maguro Pu Belt plaid  Streetwear  Collections  Rapid Hybris/span_Quick Order_far fa-user'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ProductDetailsPage_WithLogin_OR/Page_Maguro Pu Belt plaid  Streetwear  Collections  Rapid Hybris/a_Sign Out'))

WebUI.delay(2)

