import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent


import static com.yourcompany.ConnectionNotPrivate.*
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.yourcompany.ConnectionNotPrivate as ConnectionNotPrivate




//ConnectionNotPrivate.yourMethod()


WebUI.openBrowser('')

//WebUI.navigateToUrl('https://apparel-uk.hybrisdemo.sbpcorp.com/')

import internal.GlobalVariable as GlobalVariable

WebUI.navigateToUrl("http://$GlobalVariable.Hostname")

WebUI.maximizeWindow()

WebUI.click(findTestObject('LoginTest_OR/Page_Apparel Site UK  Homepage/span_Quick Order_far fa-user'))

WebUI.click(findTestObject('Object Repository/HomePage_TopNavigationTest_OR/Page_Login  Apparel Site UK/img'))

WebUI.click(findTestObject('Object Repository/HomePage_TopNavigationTest_OR/Page_Apparel Site UK  Homepage/span_Quick Order_far fa-user'))

WebUI.click(findTestObject('Object Repository/HomePage_TopNavigationTest_OR/Page_Login  Apparel Site UK/span_0'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/HomePage_TopNavigationTest_OR/Page_Login  Apparel Site UK/span_Quick Order_glyphicon fas fa-cart-plus'))

WebUI.delay(2)


WebUI.mouseOver(findTestObject('Object Repository/ImportSavedCart_Test_OR/Page_Login  Apparel Site UK/div_Import Saved Cart                                        Quick Order'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ImportSavedCart_Test_OR/Page_Login  Apparel Site UK/a_Import Saved Cart'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/HomePage_TopNavigationTest_OR/Page_Quick Order Page  Apparel Site UK/img'))

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('Object Repository/QuickOrderTest_OR1/Page_Apparel Site UK  Homepage/div_Import Saved Cart                                        Quick Order'), 0)

WebUI.click(findTestObject('Object Repository/QuickOrderTest_OR1/Page_Apparel Site UK  Homepage/div_Import Saved Cart                                        Quick Order'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/QuickOrderTest_OR1/Page_Apparel Site UK  Homepage/a_Quick Order'))

WebUI.delay(2)


WebUI.click(findTestObject('Object Repository/HomePage_TopNavigationTest_OR/Page_Quick Order Page  Apparel Site UK/img'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/HomePage_TopNavigationTest_OR/Page_Apparel Site UK  Homepage/input_Quick Order_text'), 
    'accessories')

WebUI.delay(3)

Robot robot = new Robot()

robot.keyPress(KeyEvent.VK_ENTER)

robot.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/HomePage_TopNavigationTest_OR/Page_Search accessories  Apparel Site UK/img'))

WebUI.click(findTestObject('Object Repository/HomePage_TopNavigationTest_OR/Page_Apparel Site UK  Homepage/a_Snow'))

WebUI.click(findTestObject('Object Repository/HomePage_TopNavigationTest_OR/Page_Snow  Collections  Apparel Site UK/a_Accessories'))

WebUI.click(findTestObject('Object Repository/HomePage_TopNavigationTest_OR/Page_Accessories  Categories  Apparel Site UK/a_Streetwear'))

WebUI.click(findTestObject('Object Repository/HomePage_TopNavigationTest_OR/Page_Streetwear  Collections  Apparel Site UK/a_Brands'))

WebUI.click(findTestObject('Object Repository/HomePage_TopNavigationTest_OR/Page_Brands  Apparel Site UK/a_Youth'))

WebUI.click(findTestObject('Object Repository/HomePage_TopNavigationTest_OR/Page_Streetwear youth  Categories  Apparel Site UK/img'))

WebUI.click(findTestObject('Object Repository/HomePage_TopNavigationTest_OR/Page_Apparel Site UK  Homepage/img_Browse All _js-responsive-image'))

WebUI.click(findTestObject('Object Repository/HomePage_TopNavigationTest_OR/Page_Streetwear  Collections  Apparel Site UK/img'))

WebUI.click(findTestObject('Object Repository/HomePage_TopNavigationTest_OR/Page_Apparel Site UK  Homepage/img_Browse All _js-responsive-image_1'))

WebUI.click(findTestObject('Object Repository/HomePage_TopNavigationTest_OR/Page_Snow  Collections  Apparel Site UK/img'))

WebUI.click(findTestObject('Object Repository/HomePage_TopNavigationTest_OR/Page_Apparel Site UK  Homepage/img_Browse All _js-responsive-image_2'))

WebUI.click(findTestObject('Object Repository/HomePage_TopNavigationTest_OR/Page_Apparel Site UK/img'))

WebUI.click(findTestObject('Object Repository/HomePage_TopNavigationTest_OR/Page_Apparel Site UK  Homepage/img_Browse All _js-responsive-image_3'))

WebUI.click(findTestObject('Object Repository/HomePage_TopNavigationTest_OR/Page_Apparel Site UK/img'))

WebUI.click(findTestObject('Object Repository/HomePage_TopNavigationTest_OR/Page_Apparel Site UK  Homepage/img_Browse All _js-responsive-image_4'))

WebUI.click(findTestObject('Object Repository/HomePage_TopNavigationTest_OR/Page_Streetwear youth  Categories  Apparel Site UK/img'))

WebUI.click(findTestObject('Object Repository/HomePage_TopNavigationTest_OR/Page_Apparel Site UK  Homepage/img_Browse All _js-responsive-image_5'))

WebUI.click(findTestObject('Object Repository/HomePage_TopNavigationTest_OR/Page_Brands  Apparel Site UK/img'))

