import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.yourcompany.YourClass1.*
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.yourcompany.YourClass1 as YourClass1
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import static com.yourcompany.ConsoleRobot.*
import com.yourcompany.ConsoleRobot as ConsoleRobot

WebUI.openBrowser('')

//WebUI.navigateToUrl('https://apparel-uk.hybrisdemo.sbpcorp.com/')
WebUI.navigateToUrl("http://$GlobalVariable.Hostname")

WebUI.waitForPageLoad(30)

WebUI.maximizeWindow()

WebUI.click(findTestObject('CheckOutTest_OR/Page_Apparel Site UK  Homepage/span_Quick Order_far fa-user'))

WebUI.setText(findTestObject('Object Repository/ReturnsHistoryTest_OR/Page_Login  Apparel Site UK/input_Email Address_j_username'), 
    "$GlobalVariable.Email")

WebUI.setEncryptedText(findTestObject('Object Repository/ReturnsHistoryTest_OR/Page_Login  Apparel Site UK/input_Password_j_password'), 
    "$GlobalVariable.Password")

WebUI.scrollToPosition(0, 250)

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Login  Apparel Site UK/button_Log In'))

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Apparel Site UK  Homepage/a_Streetwear'))

WebUI.scrollToPosition(0, 500)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Thort SS youth white S'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Del Chaos Tee SS Youth white L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Boxed Out Boardshort youth multi XL'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(500, 1000)

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Alex Boardshort Youth neo orange S'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Supremacy Boardshort Youth charcoal XS'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Wrappers Delight Tote Women fire red Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(1000, 1500)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Purma Backpack red combo Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Airline Bag fairwaywhite Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Airline Bag bluebird Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(1500, 2000)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Assortment Web Belt drip red Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Maguro Pu Belt print LXL'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Star SS naturalathletic red M'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(2000, 2500)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Crammed SS kelly S'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Up Country Floral Dress Women country foral L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Strybal Dress Women black L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(2500, 3000)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Eclipse SS youth red fire XS'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Asterisk SS youth dirty plum M'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Gary Polo SS Youth lime S'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(3000, 3500)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Stella Dress Women blue checked L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Nature 19 Boardshort youth girls white M'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(3500, 4000)

WebUI.delay(3)

WebUI.click(findTestObject('SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/a_2'))

WebUI.scrollToPosition(0, 500)

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Blocked Boardshort youth quik red XL'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Boardshort youth other square M'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Wind Jammer Tote Beach Bag Women sand Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(500, 1000)

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Baby II Bag Women cloud Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Dream Team II Skate Backpack black Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Seizure Bag clay court Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(1000, 1500)

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Incision Leather Belt shadow grey SM'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Granda Belt white L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Bike Bones SS Youth black L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(1500, 2000)

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Eat Sleep Dream SS F black L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Strybal Scoop Tee SS Women blue L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Panorama Pants Women beet red S'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(2000, 2500)

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Shield Tee SS Women onyx L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Nightlife T-Shirt Women violet M'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Eclipse Tee SS black L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(2500, 3000)

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Skull 80 SS youth navy S'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Frickin Too Short 22 black XXS'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Laundry Chino Short gold M'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(3000, 3500)

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Sixo Cargo Short cool grey XS'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Quad Tee SS black L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.delay(3)

WebUI.scrollToPosition(3500, 4000)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/a_3'))

WebUI.scrollToPosition(0, 500)

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Rollercoaster SS whitemulticolor L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Acid SS white L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_New Standard SS black L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(500, 1000)

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Lola Tee SS Women paradise pink L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Her Logo SS Women true black L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Girl On Feather Tank Top Women coco L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(1000, 1500)

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Multilogo Good Looking SS Women medieval blue L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_System Tee SS black M'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Because Tee SS white L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(1500, 2000)

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Jackson Tee SS marine heather L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Solo Parenth SS black L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Monster SS Youth Girls fuchsiapink L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(2000, 2500)

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Flow Tee SS Youth royal L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Selva Dress Women island greenpink lemonad XL'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Spaghetti SS Women black S'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(2500, 3000)

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Studded Belt cardinal M'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Pennant SS royal S'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Handshake Belt white M'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(3000, 3500)

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_I Love Tee SS Women grey heather XS'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Stratus Dress Women seaweed L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(3500, 4000)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/a_4'))

WebUI.scrollToPosition(0, 500)

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Control Short gold L'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Pledge Short black XS'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Maguro Print Boardshort Youth shadow grey XS'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(500, 1000)

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Gusty Tote Ball Bag Women desert point Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Rachel Women black Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Seizure Satchel brown Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(1000, 1500)

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_AC Trefoil Shop Bag white whitebluebird Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Airline Bag white Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Nava-Roll the World Beach Tote Women fog Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(1500, 2000)

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_BT Shopping Bag blue Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Lenus Belt kelly Uni'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Mountain SS Basic Tee Logo youth trigo XL'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(2000, 2500)

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Kompass Tee SS Women light green S'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/span_Torbacula Belt white SM'))

YourClass1.yourMethod()

ConsoleRobot.yourMethod()

WebUI.scrollToPosition(2500, 3000)

WebUI.click(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/button_List'))

WebUI.delay(3)

WebUI.click(findTestObject('SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Rapid Hybris/button_Gridbottom'))

WebUI.selectOptionByValue(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/select_Sort by  Relevance  Top Rated'), 
    'topRated', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/select_Sort by  Relevance  Top Rated'), 
    'name-asc', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/select_Sort by  Relevance  Top Rated'), 
    'name-desc', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/select_Sort by  Relevance  Top Rated'), 
    'price-asc', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/SnowWear_ProductListingPage_OR/Page_Streetwear  Collections  Apparel Site UK/select_Sort by  Relevance  Top Rated'), 
    'price-desc', true)

