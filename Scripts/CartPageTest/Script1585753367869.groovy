import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent
import static com.yourcompany.ConnectionNotPrivate.*
import com.yourcompany.ConnectionNotPrivate as ConnectionNotPrivate

//ConnectionNotPrivate.yourMethod()
WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl("http://$GlobalVariable.Hostname")

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartpageTest_OR/Page_Rapid Hybris  Homepage/img_Quick Order_lazy'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartpageTest_OR/Page_Rapid Hybris  Homepage/span_Quick Order_glyphicon fas fa-cart-plus'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartpageTest_OR/Page_Your Shopping Bag  Rapid Hybris/a_New Cart'))

WebUI.delay(2)

WebUI.back()

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('Object Repository/ImportSavedCart_Test_OR/Page_Apparel Site UK  Homepage/div_Import Saved Cart                                        Quick Order'), 
    0)

WebUI.mouseOver(findTestObject('Object Repository/ImportSavedCart_Test_OR/Page_Apparel Site UK  Homepage/div_Import Saved Cart                                        Quick Order'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ImportSavedCart_Test_OR/Page_Apparel Site UK  Homepage/a_Import Saved Cart'))

WebUI.delay(2)

WebUI.back()

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('Object Repository/QuickOrderTest_OR1/Page_Apparel Site UK  Homepage/div_Import Saved Cart                                        Quick Order'), 
    0)

WebUI.click(findTestObject('Object Repository/QuickOrderTest_OR1/Page_Apparel Site UK  Homepage/div_Import Saved Cart                                        Quick Order'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/QuickOrderTest_OR1/Page_Apparel Site UK  Homepage/a_Quick Order'))

WebUI.delay(2)

WebUI.back()

WebUI.click(findTestObject('Object Repository/CartpageTest_OR/Page_Your Shopping Bag  Rapid Hybris/span_Quick Order_glyphicon fas fa-cart-plus'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartpageTest_OR/Page_Rapid Hybris  Homepage/span_0'))

WebUI.delay(2)

WebUI.back()

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartpageTest_OR/Page_Rapid Hybris  Homepage/span_Quick Order_far fa-user'))

WebUI.delay(2)

WebUI.back()

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CartpageTest_OR/Page_Rapid Hybris  Homepage/input_Quick Order_text'), 'acce')

WebUI.delay(2)

WebUI.mouseOver(findTestObject('Object Repository/CartpageTest_OR/Page_Rapid Hybris  Homepage/div_Acid SS white L'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartpageTest_OR/Page_Rapid Hybris  Homepage/div_Acid SS white L'))

WebUI.delay(2)

WebUI.back()

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/WishlistTest_WithLogin_OR/Page_Accessories  Categories  Apparel Site UK/input_Quick Order_text'), 
    'accessories')

WebUI.delay(2)

Robot robot = new Robot()

robot.keyPress(KeyEvent.VK_ENTER)

robot.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartpageTest_OR/Page_Rapid Hybris  Homepage/img_Quick Order_lazy'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartpageTest_OR/Page_Rapid Hybris  Homepage/a_Accessories'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartpageTest_OR/Page_Accessories  Categories  Rapid Hybris/button_28346_fas fa-cart-plus custom-cart js-enable-btn'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartpageTest_OR/Page_Accessories  Categories  Rapid Hybris/span_Added to Your Shopping Bag_glyphicon glyphicon-remove'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartpageTest_OR/Page_Accessories  Categories  Rapid Hybris/button_1616_fas fa-cart-plus custom-cart js-enable-btn'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartpageTest_OR/Page_Accessories  Categories  Rapid Hybris/a_Continue Shopping'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartpageTest_OR/Page_Accessories  Categories  Rapid Hybris/span_The Reservoir Beanie caribbean blue Uni'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartpageTest_OR/Page_The Reservoir Beanie caribbean blue  Beanies  Accessories  Categories  Rapid Hybris/button_Add to bag'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartpageTest_OR/Page_The Reservoir Beanie caribbean blue  Beanies  Accessories  Categories  Rapid Hybris/a_Check Out'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartpageTest_OR/Page_Your Shopping Bag  Rapid Hybris/span_1454_glyphicon glyphicon-option-vertical'))

WebUI.delay(2)

WebUI.click(findTestObject('CartpageTest_OR/Page_Your Shopping Bag  Rapid Hybris/a_Removenew'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartpageTest_OR/Page_Your Shopping Bag  Rapid Hybris/button_'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartpageTest_OR/Page_Your Shopping Bag  Rapid Hybris/span_1616_glyphicon glyphicon-option-vertical'))

WebUI.delay(2)

WebUI.click(findTestObject('CartpageTest_OR/Page_Your Shopping Bag  Rapid Hybris/a_Removenew'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartpageTest_OR/Page_Your Shopping Bag  Rapid Hybris/button_'))

not_run: WebUI.delay(2)

not_run: WebUI.click(findTestObject('Object Repository/CartpageTest_OR/Page_Your Shopping Bag  Rapid Hybris/span_28346_glyphicon glyphicon-option-vertical'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartpageTest_OR/Page_Your Shopping Bag  Rapid Hybris/a_Accessories'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 250)

WebUI.click(findTestObject('Object Repository/CartpageTest_OR/Page_Accessories  Categories  Rapid Hybris/button_28346_fas fa-cart-plus custom-cart js-enable-btn'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartpageTest_OR/Page_Accessories  Categories  Rapid Hybris/a_Check Out'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartpageTest_OR/Page_Your Shopping Bag  Rapid Hybris/a_Accessories'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 750)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartpageTest_OR/Page_Accessories  Categories  Rapid Hybris/span_The Life Beanie black Uni'))

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartpageTest_OR/Page_The Life Beanie black  Beanies  Accessories  Categories  Rapid Hybris/button_Add to bag'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartpageTest_OR/Page_The Life Beanie black  Beanies  Accessories  Categories  Rapid Hybris/a_Check Out'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartpageTest_OR/Page_Your Shopping Bag  Rapid Hybris/span_1535_glyphicon glyphicon-option-vertical'))

WebUI.delay(2)

WebUI.click(findTestObject('CartpageTest_OR/Page_Your Shopping Bag  Rapid Hybris/a_Removenew'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartpageTest_OR/Page_Your Shopping Bag  Rapid Hybris/a_Export CSV'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartpageTest_OR/Page_Your Shopping Bag  Rapid Hybris/a_Export CSV'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CartpageTest_OR/Page_Your Shopping Bag  Rapid Hybris/input_Coupon code_voucherCode'), 
    '1234qwert!@##$$%')

WebUI.delay(3)

WebUI.scrollToPosition(500, 250)

WebUI.click(findTestObject('CartpageTest_OR/Page_Your Shopping Bag  Rapid Hybris/button_ApplyN'))

WebUI.delay(3)

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartpageTest_OR/Page_Your Shopping Bag  Rapid Hybris/button_Continue Shopping'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartpageTest_OR/Page_Accessories  Categories  Rapid Hybris/span_Quick Order_glyphicon fas fa-cart-plus'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartpageTest_OR/Page_Your Shopping Bag  Rapid Hybris/button_Check Out'))

WebUI.delay(2)

