import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent
import static com.yourcompany.ConnectionNotPrivate.*
import com.yourcompany.ConnectionNotPrivate as ConnectionNotPrivate

//ConnectionNotPrivate.yourMethod()
WebUI.openBrowser('')

//WebUI.navigateToUrl('https://qa-omnibus.hybrisdemo.sbpcorp.com/')
WebUI.navigateToUrl("http://$GlobalVariable.Hostname")

WebUI.maximizeWindow()

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('Object Repository/QuickOrderTest_OR1/Page_Apparel Site UK  Homepage/div_Import Saved Cart                                        Quick Order'), 
    0)

WebUI.click(findTestObject('Object Repository/QuickOrderTest_OR1/Page_Apparel Site UK  Homepage/div_Import Saved Cart                                        Quick Order'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/QuickOrderTest_OR1/Page_Apparel Site UK  Homepage/a_Quick Order'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/QuickOrderTest_OR1/Page_Quick Order Page  Apparel Site UK/input_Total_js-sku-input-field form-control'), 
    'prakash1234!@#$')

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/QuickOrderTest_OR1/Page_Quick Order Page  Apparel Site UK/input_Product not found_js-sku-input-field form-control'), 
    'prakash1234!@#$')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/QuickOrderTest_OR1/Page_Quick Order Page  Apparel Site UK/ul_ProductPriceQtyTotal                                                                Product not found'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/QuickOrderTest_OR1/Page_Quick Order Page  Apparel Site UK/input_Sku already exists in the form_js-sku-input-field form-control'), 
    '300785802')

WebUI.delay(2)

Robot robot = new Robot()

robot.keyPress(KeyEvent.VK_ENTER)

robot.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/QuickOrderTest_OR1/Page_Quick Order Page  Apparel Site UK/input_Item price_js-quick-order-qty form-control'), 
    '10')

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

Robot robot1 = new Robot()

robot1.keyPress(KeyEvent.VK_ENTER)

robot1.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/QuickOrderTest_OR1/Page_Quick Order Page  Apparel Site UK/button_Add to bag'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/QuickOrderTest_OR1/Page_Quick Order Page  Apparel Site UK/span_Added to Your Shopping Bag_glyphicon glyphicon-remove'))

WebUI.delay(2)

WebUI.scrollToPosition(250, 0)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/QuickOrderTest_OR1/Page_Quick Order Page  Apparel Site UK/button_Reset Form'))

WebUI.acceptAlert()

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/QuickOrderTest_OR1/Page_Quick Order Page  Apparel Site UK/input_Sku already exists in the form_js-sku-input-field form-control'), 
    '300785802')

WebUI.delay(2)

Robot robot2 = new Robot()

robot2.keyPress(KeyEvent.VK_ENTER)

robot2.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/QuickOrderTest_OR1/Page_Quick Order Page  Apparel Site UK/span_Assortment Web Belt blue combo Uni'))

WebUI.acceptAlert()

WebUI.delay(2)

WebUI.back()

WebUI.delay(2)

