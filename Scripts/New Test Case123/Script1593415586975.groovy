import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://dev-omnibus.hybrisdemo.sbpcorp.com/')

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_Rapid Hybris  Homepage/span_Quick Order_far fa-user'))

WebUI.selectOptionByValue(findTestObject('Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Company', true)

WebUI.selectOptionByValue(findTestObject('Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Mr', true)

WebUI.selectOptionByValue(findTestObject('Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Miss', true)

WebUI.selectOptionByValue(findTestObject('Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Mrs', true)

WebUI.selectOptionByValue(findTestObject('Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Ms', true)

WebUI.selectOptionByValue(findTestObject('Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Rev.', true)

WebUI.selectOptionByValue(findTestObject('Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Prof.', true)

WebUI.selectOptionByValue(findTestObject('Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Dr.', true)

WebUI.openBrowser('')

WebUI.navigateToUrl('https://dev-omnibus.hybrisdemo.sbpcorp.com/')

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_Rapid Hybris  Homepage/span_Quick Order_far fa-user'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Company', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Mr', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Miss', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Mrs', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Ms', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Rev.', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Prof.', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Dr.', true)

WebUI.openBrowser('')

WebUI.navigateToUrl('https://dev-omnibus.hybrisdemo.sbpcorp.com/')

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_Rapid Hybris  Homepage/span_Quick Order_far fa-user'))

WebUI.setText(findTestObject('Object Repository/Titles_OR/Page_Login  Rapid Hybris/input_Email Address_j_username'), 'prakashdev1@test.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Titles_OR/Page_Login  Rapid Hybris/input_Password_j_password'), 
    'zXMD8VQinzNakze1o1dv8w==')

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_Login  Rapid Hybris/button_Log In'))

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_Rapid Hybris  Homepage/span_Welcome prakash_glyphicon fas fa-cart-plus'))

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_Your Shopping Bag  Rapid Hybris/button_Check Out'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_Country                GuernseyIsle of ManJerseyUnited Kingdom'), 
    'GG', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Company', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Mr', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Miss', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Mrs', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Ms', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Rev.', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Prof.', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Dr.', true)

WebUI.openBrowser('')

WebUI.navigateToUrl('https://dev-omnibus.hybrisdemo.sbpcorp.com/')

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_Rapid Hybris  Homepage/span_Quick Order_far fa-user'))

WebUI.setText(findTestObject('Object Repository/Titles_OR/Page_Login  Rapid Hybris/input_Email Address_j_username'), 'prakashdev1@test.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Titles_OR/Page_Login  Rapid Hybris/input_Password_j_password'), 
    'zXMD8VQinzNakze1o1dv8w==')

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_Rapid Hybris  Homepage/a_Streetwear'))

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_Streetwear  Collections  Rapid Hybris/button_15386_fas fa-cart-plus custom-cart js-enable-btn'))

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_Streetwear  Collections  Rapid Hybris/a_Check Out'))

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_Your Shopping Bag  Rapid Hybris/button_Check Out'))

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/button_Address Book'))

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/button_Use this Address'))

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/button_Next'))

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/input_Use my Delivery Address_useDeliveryAddress'))

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/div_Use my Delivery Address'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr_1'), 
    'Company', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr_2'), 
    'Mr', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr_3'), 
    'Miss', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr_4'), 
    'Mrs', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr_5'), 
    'Ms', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr_6'), 
    'Rev.', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr_7'), 
    'Prof.', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr_8'), 
    'Dr.', true)

WebUI.openBrowser('')

WebUI.navigateToUrl('https://dev-omnibus.hybrisdemo.sbpcorp.com/b2b/?site=powertools')

WebUI.click(findTestObject('Page_Powertools Site  Homepage/span_Quick Order_far fa-user'))

WebUI.setText(findTestObject('Page_Login  Powertools Site/input_Email Address_j_username'), 'powerdrillslover@pronto-hw.com')

WebUI.setEncryptedText(findTestObject('Page_Login  Powertools Site/input_Password_j_password'), 'aeHFOx8jV/A=')

WebUI.click(findTestObject('Page_Login  Powertools Site/button_Log In'))

WebUI.setText(findTestObject('Object Repository/Titles_OR/Page_Powertools Site  Homepage/input_Quick Order_text'), 'megamachines')

WebUI.click(findTestObject('Page_Search megamachines  Powertools Site/button_Configure'))

WebUI.click(findTestObject('Page_MegaMachines Excavator Model 500  Powertools Site/div_03OPTIONS'))

WebUI.click(findTestObject('Page_MegaMachines Excavator Model 500  Powertools Site/span_Please select'))

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_MegaMachines Excavator Model 500  Powertools Site/div_ArmlengthPlease select810121416Please select'))

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_MegaMachines Excavator Model 500  Powertools Site/span_Please select_1'))

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_MegaMachines Excavator Model 500  Powertools Site/div_ArmlengthPlease select810121416Please select'))

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_MegaMachines Excavator Model 500  Powertools Site/div_Please select'))

WebUI.click(findTestObject('Page_MegaMachines Excavator Model 500  Powertools Site/div_8'))

WebUI.click(findTestObject('Page_MegaMachines Excavator Model 500  Powertools Site/span_8'))

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_MegaMachines Excavator Model 500  Powertools Site/span_8_1'))

WebUI.click(findTestObject('Page_MegaMachines Excavator Model 500  Powertools Site/span_8'))

WebUI.click(findTestObject('Page_MegaMachines Excavator Model 500  Powertools Site/div_10'))

WebUI.click(findTestObject('Page_MegaMachines Excavator Model 500  Powertools Site/span_10'))

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_MegaMachines Excavator Model 500  Powertools Site/span_10_1'))

WebUI.click(findTestObject('Page_MegaMachines Excavator Model 500  Powertools Site/span_10'))

WebUI.click(findTestObject('Page_MegaMachines Excavator Model 500  Powertools Site/div_12'))

WebUI.click(findTestObject('Page_MegaMachines Excavator Model 500  Powertools Site/span_12'))

WebUI.click(findTestObject('Page_MegaMachines Excavator Model 500  Powertools Site/span_12_1'))

WebUI.click(findTestObject('Page_MegaMachines Excavator Model 500  Powertools Site/span_12'))

WebUI.click(findTestObject('Page_MegaMachines Excavator Model 500  Powertools Site/div_14'))

WebUI.click(findTestObject('Page_MegaMachines Excavator Model 500  Powertools Site/span_14'))

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_MegaMachines Excavator Model 500  Powertools Site/span_14_1'))

WebUI.click(findTestObject('Page_MegaMachines Excavator Model 500  Powertools Site/span_14'))

WebUI.click(findTestObject('Page_MegaMachines Excavator Model 500  Powertools Site/div_16'))

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_MegaMachines Excavator Model 500  Powertools Site/span_16'))

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_MegaMachines Excavator Model 500  Powertools Site/span_16_1'))

WebUI.openBrowser('')

WebUI.navigateToUrl('https://dev-omnibus.hybrisdemo.sbpcorp.com/b2b/?site=powertools')

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_Powertools Site  Homepage/span_Quick Order_far fa-user'))

WebUI.closeBrowser()

WebUI.openBrowser('')

WebUI.navigateToUrl('https://dev-omnibus.hybrisdemo.sbpcorp.com/')

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_Rapid Hybris  Homepage/span_Quick Order_far fa-user'))

WebUI.setText(findTestObject('Object Repository/Titles_OR/Page_Login  Rapid Hybris/input_Email Address_j_username'), 'prakashdev1@test.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Titles_OR/Page_Login  Rapid Hybris/input_Password_j_password'), 
    'zXMD8VQinzNakze1o1dv8w==')

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_Login  Rapid Hybris/button_Log In'))

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_Rapid Hybris  Homepage/span_Quick Order_far fa-user_1'))

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_Rapid Hybris  Homepage/a_Address Book'))

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_Address Book  Rapid Hybris/a_Add Address'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_AddEdit Address  Rapid Hybris/select_Country                GuernseyIsle of ManJerseyUnited Kingdom'), 
    'GG', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_AddEdit Address  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Company', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_AddEdit Address  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Mr', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_AddEdit Address  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Miss', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_AddEdit Address  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Mrs', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_AddEdit Address  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Ms', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_AddEdit Address  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Rev.', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_AddEdit Address  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Prof.', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_AddEdit Address  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Dr.', true)

WebUI.openBrowser('')

WebUI.navigateToUrl('https://qa-omnibus.hybrisdemo.sbpcorp.com/')

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_Rapid Hybris  Homepage/span_Quick Order_far fa-user'))

WebUI.setText(findTestObject('Object Repository/Titles_OR/Page_Login  Rapid Hybris/input_Email Address_j_username'), 'prakashqatest@test.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Titles_OR/Page_Login  Rapid Hybris/input_Password_j_password'), 
    'zXMD8VQinzNakze1o1dv8w==')

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_Login  Rapid Hybris/button_Log In'))

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_Rapid Hybris  Homepage/span_Quick Order_far fa-user_2'))

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_Rapid Hybris  Homepage/a_Personal Details'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Update Profile Page  Rapid Hybris/select_Please select                CompanyMrMrsMissDrMsRevProf'), 
    'Company', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Update Profile Page  Rapid Hybris/select_Please select                CompanyMrMrsMissDrMsRevProf'), 
    'Mr', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Update Profile Page  Rapid Hybris/select_Please select                CompanyMrMrsMissDrMsRevProf'), 
    'Mrs', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Update Profile Page  Rapid Hybris/select_Please select                CompanyMrMrsMissDrMsRevProf'), 
    'Miss', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Update Profile Page  Rapid Hybris/select_Please select                CompanyMrMrsMissDrMsRevProf'), 
    'Dr.', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Update Profile Page  Rapid Hybris/select_Please select                CompanyMrMrsMissDrMsRevProf'), 
    'Ms', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Update Profile Page  Rapid Hybris/select_Please select                CompanyMrMrsMissDrMsRevProf'), 
    'Rev.', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Update Profile Page  Rapid Hybris/select_Please select                CompanyMrMrsMissDrMsRevProf'), 
    'Prof.', true)

WebUI.openBrowser('')

WebUI.navigateToUrl('https://uat-omnibus.hybrisdemo.sbpcorp.com/b2b/?site=powertools')

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_Rapid Hybris B2B  Homepage/span_Import Saved Cart_far fa-user'))

WebUI.setText(findTestObject('Object Repository/Titles_OR/Page_Login  Rapid Hybris B2B/input_Email Address_j_username'), 
    'powerdrillslover@pronto-hw.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Titles_OR/Page_Login  Rapid Hybris B2B/input_Password_j_password'), 
    'zXMD8VQinzNakze1o1dv8w==')

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_Login  Rapid Hybris B2B/button_Log In'))

WebUI.setEncryptedText(findTestObject('Object Repository/Titles_OR/Page_Login  Rapid Hybris B2B/input_Password_j_password'), 
    'aeHFOx8jV/A=')

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_Login  Rapid Hybris B2B/button_Log In'))

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_Rapid Hybris B2B  Homepage/a_Power Drills'))

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_Power Drills  Tools  Open Catalogue  Rapid Hybris B2B/button_7900_fas fa-cart-plus custom-cart js-enable-btn rh_cart'))

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_Power Drills  Tools  Open Catalogue  Rapid Hybris B2B/a_Check Out'))

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_Your Shopping Cart  Rapid Hybris B2B/button_Request a Quote'))

WebUI.setText(findTestObject('Object Repository/Titles_OR/Page_Quote Edit  Rapid Hybris B2B/input_Item price_quantity'), 
    '')

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_Quote Edit  Rapid Hybris B2B/div_Export CSV                            1 item                7950'))

WebUI.setText(findTestObject('Object Repository/Titles_OR/Page_Quote Edit  Rapid Hybris B2B/input_Item price_quantity'), 
    '999')

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_Quote Edit  Rapid Hybris B2B/form_Item price_updateCartForm0'))

WebUI.setText(findTestObject('Object Repository/Titles_OR/Page_Quote Edit  Rapid Hybris B2B/input_Item price_quantity'), 
    '171999')

WebUI.click(findTestObject('Object Repository/Titles_OR/Page_Quote Edit  Rapid Hybris B2B/div_Buy 20 power drills get 5 discount'))

