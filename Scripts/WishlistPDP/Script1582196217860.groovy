import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import static com.yourcompany.ConnectionNotPrivate.*
import com.yourcompany.ConnectionNotPrivate as ConnectionNotPrivate

//ConnectionNotPrivate.yourMethod()
WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl("http://$GlobalVariable.Hostname")

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Rapid Hybris  Homepage/span_Quick Order_far fa-user'))

WebUI.selectOptionByValue(findTestObject('Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Company', true)

WebUI.selectOptionByValue(findTestObject('Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Mr', true)

WebUI.selectOptionByValue(findTestObject('Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Miss', true)

WebUI.selectOptionByValue(findTestObject('Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Mrs', true)

WebUI.selectOptionByValue(findTestObject('Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Ms', true)

WebUI.selectOptionByValue(findTestObject('Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Rev.', true)

WebUI.selectOptionByValue(findTestObject('Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Prof.', true)

WebUI.selectOptionByValue(findTestObject('Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Dr.', true)

WebUI.setText(findTestObject('Object Repository/WishlistPDP1_OR/Page_Login  Rapid Hybris/input_First Name_firstName'), 'robert')

WebUI.setText(findTestObject('Object Repository/WishlistPDP1_OR/Page_Login  Rapid Hybris/input_Last Name_lastName'), 'reviewer')

WebUI.setText(findTestObject('RegistrationTest_OR/Page_Registration Apparel Site UK/input_Email Address_email'), "$GlobalVariable.Email")

WebUI.setEncryptedText(findTestObject('RegistrationTest_OR/Page_Registration Apparel Site UK/input_Password_pwd'), "$GlobalVariable.Password")

WebUI.setEncryptedText(findTestObject('RegistrationTest_OR/Page_Registration Apparel Site UK/input_Confirm Password_checkPwd'), 
    "$GlobalVariable.Password")

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Login  Rapid Hybris/input_Confirm Password_consentFormconsentGiven'))

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Login  Rapid Hybris/input_I am confirming that I have read and agreed with the_termsCheck'))

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Login  Rapid Hybris/button_Register'))

WebUI.delay(2)

WebUI.click(findTestObject('WishlistPDP1_OR/Page_Rapid Hybris  Homepage/button_'))

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Rapid Hybris  Homepage/img_Quick Order_lazy'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 2000)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Rapid Hybris  Homepage/img_846_js-responsive-image lazy'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 750)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Maguro Pu Belt plaid  Belts  Accessories  Categories  Rapid Hybris/button_Add to bag'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Maguro Pu Belt plaid  Belts  Accessories  Categories  Rapid Hybris/span_Added to Your Shopping Bag_glyphicon glyphicon-remove'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Maguro Pu Belt plaid  Belts  Accessories  Categories  Rapid Hybris/i_Added to Your Shopping Bag_far fa-heart'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Maguro Pu Belt plaid  Belts  Accessories  Categories  Rapid Hybris/span_1'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Wish List Page  Rapid Hybris/a_View Details'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 750)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Maguro Pu Belt plaid  Belts  Accessories  Categories  Rapid Hybris/button_Add to bag'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Maguro Pu Belt plaid  Belts  Accessories  Categories  Rapid Hybris/span_Added to Your Shopping Bag_glyphicon glyphicon-remove'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Maguro Pu Belt plaid  Belts  Accessories  Categories  Rapid Hybris/i_Added to Your Shopping Bag_fa-heart fas'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Maguro Pu Belt plaid  Belts  Accessories  Categories  Rapid Hybris/i_Added to Your Shopping Bag_fa-heart far'))

WebUI.delay(2)

WebUI.scrollToPosition(750, 500)

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/WishlistPDP1_OR/Page_Maguro Pu Belt plaid  Belts  Accessories  Categories  Rapid Hybris/select_Select a size  SizeLXL242610  SizeSM242622'), 
    '300785815', true)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Maguro Pu Belt plaid  Belts  Accessories  Categories  Rapid Hybris/button_Add to bag'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Maguro Pu Belt plaid  Belts  Accessories  Categories  Rapid Hybris/a_Continue Shopping'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Maguro Pu Belt plaid  Belts  Accessories  Categories  Rapid Hybris/i_Added to Your Shopping Bag_fa-heart fas'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Maguro Pu Belt plaid  Belts  Accessories  Categories  Rapid Hybris/i_Added to Your Shopping Bag_fa-heart far'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Maguro Pu Belt plaid  Belts  Accessories  Categories  Rapid Hybris/img_ plaid_lazy'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Maguro Pu Belt plaid  Belts  Accessories  Categories  Rapid Hybris/button_Add to bag'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Maguro Pu Belt plaid  Belts  Accessories  Categories  Rapid Hybris/span_Added to Your Shopping Bag_glyphicon glyphicon-remove'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Maguro Pu Belt plaid  Belts  Accessories  Categories  Rapid Hybris/i_Added to Your Shopping Bag_fa-heart far'))

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/WishlistPDP1_OR/Page_Maguro Pu Belt plaid  Belts  Accessories  Categories  Rapid Hybris/select_Size LXL 2426 37 Size SM 2426 24'), 
    '300608278', true)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Maguro Pu Belt plaid  Belts  Accessories  Categories  Rapid Hybris/button_Add to bag'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Maguro Pu Belt plaid  Belts  Accessories  Categories  Rapid Hybris/span_Added to Your Shopping Bag_glyphicon glyphicon-remove'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Maguro Pu Belt plaid  Belts  Accessories  Categories  Rapid Hybris/i_Added to Your Shopping Bag_fa-heart fas'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Maguro Pu Belt plaid  Belts  Accessories  Categories  Rapid Hybris/i_Added to Your Shopping Bag_fa-heart far'))

WebUI.delay(2)

WebUI.scrollToPosition(500, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Maguro Pu Belt plaid  Belts  Accessories  Categories  Rapid Hybris/a_Write a Review'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/WishlistPDP1_OR/Page_Maguro Pu Belt plaid  Belts  Accessories  Categories  Rapid Hybris/input_Review Title_headline'), 
    'test')

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/WishlistPDP1_OR/Page_Maguro Pu Belt plaid  Belts  Accessories  Categories  Rapid Hybris/textarea_Review Description_comment'), 
    ' test')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Maguro Pu Belt plaid  Belts  Accessories  Categories  Rapid Hybris/span_Your Rating_js-ratingIcon glyphicon glyphicon-star lh active'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/WishlistPDP1_OR/Page_Maguro Pu Belt plaid  Belts  Accessories  Categories  Rapid Hybris/input_(optional)_alias'), 
    'test')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Maguro Pu Belt plaid  Belts  Accessories  Categories  Rapid Hybris/button_Submit Review'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Maguro Pu Belt plaid  Belts  Accessories  Categories  Rapid Hybris/button_'))

WebUI.scrollToPosition(250, 800)

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Maguro Pu Belt plaid  Belts  Accessories  Categories  Rapid Hybris/a_Product Details'))

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Maguro Pu Belt plaid  Belts  Accessories  Categories  Rapid Hybris/a_Specs'))

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Maguro Pu Belt plaid  Belts  Accessories  Categories  Rapid Hybris/a_Reviews'))

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Maguro Pu Belt plaid  Belts  Accessories  Categories  Rapid Hybris/button_Write a Review'))

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Maguro Pu Belt plaid  Belts  Accessories  Categories  Rapid Hybris/button_Write a Review'))

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Maguro Pu Belt plaid  Belts  Accessories  Categories  Rapid Hybris/a_Shipping'))

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('Object Repository/ImportSavedCart_Test_OR/Page_Apparel Site UK  Homepage/div_Import Saved Cart                                        Quick Order'), 
    0)

WebUI.mouseOver(findTestObject('Object Repository/ImportSavedCart_Test_OR/Page_Apparel Site UK  Homepage/div_Import Saved Cart                                        Quick Order'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/ImportSavedCart_Test_OR/Page_Apparel Site UK  Homepage/a_Import Saved Cart'))

WebUI.delay(2)

WebUI.back()

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('Object Repository/QuickOrderTest_OR1/Page_Apparel Site UK  Homepage/div_Import Saved Cart                                        Quick Order'), 
    0)

WebUI.click(findTestObject('Object Repository/QuickOrderTest_OR1/Page_Apparel Site UK  Homepage/div_Import Saved Cart                                        Quick Order'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/QuickOrderTest_OR1/Page_Apparel Site UK  Homepage/a_Quick Order'))

WebUI.delay(2)

WebUI.back()

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Maguro Pu Belt plaid  Belts  Accessories  Categories  Rapid Hybris/span_Welcome robert_glyphicon fas fa-cart-plus'))

WebUI.delay(2)

WebUI.back()

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Maguro Pu Belt plaid  Belts  Accessories  Categories  Rapid Hybris/span_2'))

WebUI.delay(2)

WebUI.back()

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Maguro Pu Belt plaid  Belts  Accessories  Categories  Rapid Hybris/span_Quick Order_far fa-user'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Maguro Pu Belt plaid  Belts  Accessories  Categories  Rapid Hybris/span_Quick Order_far fa-user_1'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Maguro Pu Belt plaid  Belts  Accessories  Categories  Rapid Hybris/span_2'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Wish List Page  Rapid Hybris/span_X'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Wish List Page  Rapid Hybris/a_View Details'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 500)

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Maguro Pu Belt plaid  Belts  Accessories  Categories  Rapid Hybris/img_ plaid_lazy'))

WebUI.click(findTestObject('Object Repository/WishlistPDP1_OR/Page_Maguro Pu Belt plaid  Belts  Accessories  Categories  Rapid Hybris/img_Quick Order_lazy'))

