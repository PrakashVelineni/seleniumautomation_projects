import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import static com.yourcompany.ConnectionNotPrivate.*
import com.yourcompany.ConnectionNotPrivate as ConnectionNotPrivate

//ConnectionNotPrivate.yourMethod()
WebUI.openBrowser('')

WebUI.navigateToUrl("https://$GlobalVariable.Hostname")

WebUI.maximizeWindow()

WebUI.click(findTestObject('CheckOutTest_OR/Page_Apparel Site UK  Homepage/span_Quick Order_far fa-user'))

WebUI.setText(findTestObject('Object Repository/CheckOutTest_OR/Page_Login  Apparel Site UK/input_Email Address_j_username'), 
    "$GlobalVariable.Email")

WebUI.setEncryptedText(findTestObject('Object Repository/CheckOutTest_OR/Page_Login  Apparel Site UK/input_Password_j_password'), 
    "$GlobalVariable.Password")

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CheckOutTest_OR/Page_Login  Apparel Site UK/button_Log In'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CheckOutTest_OR/Page_Apparel Site UK  Homepage/a_Accessories'))

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CheckOutTest_OR/Page_Accessories  Categories  Apparel Site UK/span_The Sentry gunship'))

//WebUI.click(findTestObject('Checkout_Guest_OR1/Page_Accessories  Categories  Apparel Site UK/span_Granda Belt white L'))
WebUI.scrollToPosition(0, 300)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/CheckOutTest_OR/Page_The Sentry gunship  Watches  Accessories  Categories  Apparel Site UK/button_Add to bag'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CheckOutTest_OR/Page_The Sentry gunship  Watches  Accessories  Categories  Apparel Site UK/a_Check Out'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CheckOutTest_OR/Page_Your Shopping Bag  Apparel Site UK/button_Check Out'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/select_Country                GuernseyIsle of ManJerseyUnited Kingdom'), 
    'GG', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/select_Country                GuernseyIsle of ManJerseyUnited Kingdom'), 
    'IM', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/select_Country                GuernseyIsle of ManJerseyUnited Kingdom'), 
    'JE', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/select_Country                GuernseyIsle of ManJerseyUnited Kingdom'), 
    'GB', true)

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Company', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Mr', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Miss', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Mrs', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Ms', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Rev.', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Prof.', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Dr.', true)

WebUI.setText(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/input_First Name_firstName'), 
    'prakashkatalon')

WebUI.setText(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/input_Last Name_lastName'), 
    'velinenikatalon')

WebUI.setText(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/input_Address Line 1_line1'), 
    'guntur')

WebUI.setText(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/input_(optional)_line2'), 
    'andhrapradesh')

WebUI.setText(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/input_City_townCity'), 'hyderabad')

WebUI.setText(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/input_Post Code_postcode'), 
    '522212')

WebUI.setText(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/input_(optional)_phone'), 
    '9999999999')

WebUI.click(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/input_Save Shipping Address_saveInAddressBook'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/button_Next'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/select_Standard Delivery-3-5 business days-599Premium Delivery-1-2 business days-1099'), 
    'premium-gross', true)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/button_Next_1'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/select_Please select a card type                American ExpressMaestroVisaMastercardDiners Club'), 
    '003', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/select_Please select a card type                American ExpressMaestroVisaMastercardDiners Club'), 
    '024', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/select_Please select a card type                American ExpressMaestroVisaMastercardDiners Club'), 
    '001', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/select_Please select a card type                American ExpressMaestroVisaMastercardDiners Club'), 
    '002', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/select_Please select a card type                American ExpressMaestroVisaMastercardDiners Club'), 
    '005', true)

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/input_(optional)_card_nameOnCard'), 
    'prakashkatalon')

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/input_Card number_card_accountNumber'), 
    '9999999999123456')

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/select_Month                010203040506070809101112'), 
    '1', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/select_Month                010203040506070809101112'), 
    '2', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/select_Month                010203040506070809101112'), 
    '3', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/select_Month                010203040506070809101112'), 
    '5', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/select_Month                010203040506070809101112'), 
    '4', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/select_Month                010203040506070809101112'), 
    '6', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/select_Month                010203040506070809101112'), 
    '7', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/select_Month                010203040506070809101112'), 
    '8', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/select_Month                010203040506070809101112'), 
    '9', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/select_Month                010203040506070809101112'), 
    '10', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/select_Month                010203040506070809101112'), 
    '11', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/select_Month                010203040506070809101112'), 
    '12', true)

WebUI.delay(5)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/select_Year                20192020202120222023202420252026202720282029'), 
    '2020', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/select_Year                20192020202120222023202420252026202720282029'), 
    '2021', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/select_Year                20192020202120222023202420252026202720282029'), 
    '2022', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/select_Year                20192020202120222023202420252026202720282029'), 
    '2023', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/select_Year                20192020202120222023202420252026202720282029'), 
    '2024', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/select_Year                20192020202120222023202420252026202720282029'), 
    '2025', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/select_Year                20192020202120222023202420252026202720282029'), 
    '2026', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/select_Year                20192020202120222023202420252026202720282029'), 
    '2027', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/select_Year                20192020202120222023202420252026202720282029'), 
    '2028', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/select_Year                20192020202120222023202420252026202720282029'), 
    '2029', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/select_Year                20192020202120222023202420252026202720282029'), 
    '2030', true)

WebUI.setText(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/input_Card Verification Number_card_cvNumber'), 
    '123')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/input_Save Payment Info_savePaymentInfo'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/button_Next_2'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.scrollToPosition(0, 250)

WebUI.click(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/input_By placing the order I am confirming that I have read and agree with the_termsCheck'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CheckOutTest_OR/Page_Checkout  Apparel Site UK/button_Place Order'))

WebUI.delay(2)

WebUI.click(findTestObject('CheckOutTest_OR/Page_Order Confirmation  Rapid Hybris/div_Thank you for your Order'))

WebUI.delay(2)

WebUI.verifyElementPresent(findTestObject('CheckOutTest_OR/Page_Order Confirmation  Rapid Hybris/div_Thank you for your Order'), 
    0)

String order = WebUI.getText(findTestObject('CheckOutTest_OR/Page_Order Confirmation  Rapid Hybris/div_Thank you for your Order'))

KeywordLogger logger = new KeywordLogger()

logger.logInfo(order)

