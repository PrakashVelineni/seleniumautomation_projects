import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import static com.yourcompany.ConnectionNotPrivate.*
import com.yourcompany.ConnectionNotPrivate as ConnectionNotPrivate

//ConnectionNotPrivate.yourMethod()
WebUI.openBrowser('')

WebUI.navigateToUrl("http://$GlobalVariable.Hostname")

WebUI.maximizeWindow()

WebUI.click(findTestObject('Object Repository/DeleteSavedCartsTest_OR/Page_Rapid Hybris  Homepage/span_Quick Order_far fa-user'))

WebUI.setText(findTestObject('Object Repository/DeleteSavedCartsTest_OR/Page_Login  Rapid Hybris/input_Email Address_j_username'), 
    "$GlobalVariable.Email")

WebUI.setEncryptedText(findTestObject('Object Repository/DeleteSavedCartsTest_OR/Page_Login  Rapid Hybris/input_Password_j_password'), 
    "$GlobalVariable.Password")

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/DeleteSavedCartsTest_OR/Page_Login  Rapid Hybris/button_Log In'))

WebUI.click(findTestObject('Object Repository/DeleteSavedCartsTest_OR/Page_Rapid Hybris  Homepage/a_Accessories'))

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/DeleteSavedCartsTest_OR/Page_Accessories  Categories  Rapid Hybris/button_1616_fas fa-cart-plus custom-cart js-enable-btn'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/DeleteSavedCartsTest_OR/Page_Accessories  Categories  Rapid Hybris/a_Continue Shopping'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/DeleteSavedCartsTest_OR/Page_Accessories  Categories  Rapid Hybris/span_Welcome kenneth_glyphicon fas fa-cart-plus'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/DeleteSavedCartsTest_OR/Page_Your Shopping Bag  Rapid Hybris/a_New Cart'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/DeleteSavedCartsTest_OR/Page_Your Shopping Bag  Rapid Hybris/input_Name_name'), 
    'prakashtest')

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/DeleteSavedCartsTest_OR/Page_Your Shopping Bag  Rapid Hybris/textarea_Description_description'), 
    'prakashtest')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/DeleteSavedCartsTest_OR/Page_Your Shopping Bag  Rapid Hybris/button_Save'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/DeleteSavedCartsTest_OR/Page_Your Shopping Bag  Rapid Hybris/a_Streetwear'))

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/DeleteSavedCartsTest_OR/Page_Streetwear  Collections  Rapid Hybris/button_2021_fas fa-cart-plus custom-cart js-enable-btn'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/DeleteSavedCartsTest_OR/Page_Streetwear  Collections  Rapid Hybris/a_Check Out'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/DeleteSavedCartsTest_OR/Page_Your Shopping Bag  Rapid Hybris/a_New Cart'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/DeleteSavedCartsTest_OR/Page_Your Shopping Bag  Rapid Hybris/input_Name_name'), 
    'prakashtestdelete')

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/DeleteSavedCartsTest_OR/Page_Your Shopping Bag  Rapid Hybris/textarea_Description_description'), 
    'prakashtestdelete')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/DeleteSavedCartsTest_OR/Page_Your Shopping Bag  Rapid Hybris/button_Save'))

WebUI.delay(2)

WebUI.click(findTestObject('DeleteSavedCartsTest_OR/Page_Your Shopping Bag  Rapid Hybris/a_Saved Carts (2)'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/DeleteSavedCartsTest_OR/Page_Saved Carts  Rapid Hybris/span_Restore_glyphicon glyphicon-remove'))

WebUI.delay(2)

WebUI.click(findTestObject('DeleteSavedCartsTest_OR/Page_Saved Carts  Rapid Hybris/button_Cancel'))

WebUI.delay(2)

/*WebUI.click(findTestObject('DeleteSavedCartsTest_OR/Page_Saved Carts  Rapid Hybris/span_Delete Saved Cart_glyphicon glyphicon-remove'))

WebUI.delay(2)

WebUI.click(findTestObject('DeleteSavedCartsTest_OR/Page_Saved Carts  Rapid Hybris/span_Delete Saved Cart_glyphicon glyphicon-remove'))*/
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/DeleteSavedCartsTest_OR/Page_Saved Carts  Rapid Hybris/a_prakashtestdelete'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/DeleteSavedCartsTest_OR/Page_Saved Cart Details  Rapid Hybris/a_Delete Cart'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/DeleteSavedCartsTest_OR/Page_Saved Cart Details  Rapid Hybris/button_Cancel'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/DeleteSavedCartsTest_OR/Page_Saved Cart Details  Rapid Hybris/a_Delete Cart'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/DeleteSavedCartsTest_OR/Page_Saved Cart Details  Rapid Hybris/span_Delete Saved Cart_glyphicon glyphicon-remove'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/DeleteSavedCartsTest_OR/Page_Saved Cart Details  Rapid Hybris/a_Delete Cart'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/DeleteSavedCartsTest_OR/Page_Saved Cart Details  Rapid Hybris/button_Delete'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/DeleteSavedCartsTest_OR/Page_Saved Carts  Rapid Hybris/a_Brands'))

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/DeleteSavedCartsTest_OR/Page_Brands  Rapid Hybris/button_401_fas fa-cart-plus custom-cart js-enable-btn'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/DeleteSavedCartsTest_OR/Page_Brands  Rapid Hybris/a_Check Out'))

WebUI.delay(2)

WebUI.click(findTestObject('DeleteSavedCartsTest_OR/Page_Your Shopping Bag  Rapid Hybris/a_Saved Carts (1)'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/DeleteSavedCartsTest_OR/Page_Saved Carts  Rapid Hybris/span_Restore'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/DeleteSavedCartsTest_OR/Page_Saved Carts  Rapid Hybris/input_Keep a copy of this cart on saved list_keepRestoredCart'))

WebUI.delay(2)

WebUI.click(findTestObject('DeleteSavedCartsTest_OR/Page_Saved Carts  Rapid Hybris/button_Restore'))

WebUI.delay(3)

WebUI.click(findTestObject('DeleteSavedCartsTest_OR/Page_Rapid Hybris  Homepage/span_Quick Order_far fa-user'))

WebUI.delay(3)

WebUI.click(findTestObject('DeleteSavedCartsTest_OR/Page_Rapid Hybris  Homepage/a_Saved Carts'))

WebUI.delay(2)

