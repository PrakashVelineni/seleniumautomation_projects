import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://dev-omnibus.hybrisdemo.sbpcorp.com/')

WebUI.click(findTestObject('Object Repository/RegistrationDemo_OR/Page_Rapid Hybris  Homepage/span_Quick Order_far fa-user'))

WebUI.selectOptionByValue(findTestObject('Object Repository/RegistrationDemo_OR/Page_Login  Rapid Hybris/select_Please select'), 
    'Mr', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/RegistrationDemo_OR/Page_Login  Rapid Hybris/select_Please select'), 
    'Mrs', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/RegistrationDemo_OR/Page_Login  Rapid Hybris/select_Please select'), 
    'Ms', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/RegistrationDemo_OR/Page_Login  Rapid Hybris/select_Please select'), 
    'Miss', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/RegistrationDemo_OR/Page_Login  Rapid Hybris/select_Please select'), 
    'Company', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/RegistrationDemo_OR/Page_Login  Rapid Hybris/select_Please select'), 
    'Dr.', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/RegistrationDemo_OR/Page_Login  Rapid Hybris/select_Please select'), 
    'Rev.', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/RegistrationDemo_OR/Page_Login  Rapid Hybris/select_Please select'), 
    'Prof.', true)

WebUI.setText(findTestObject('Object Repository/RegistrationDemo_OR/Page_Login  Rapid Hybris/input_First Name_firstName'), 
    'test')

WebUI.click(findTestObject('Object Repository/RegistrationDemo_OR/Page_Login  Rapid Hybris/form_Title Please select'))

WebUI.setText(findTestObject('Object Repository/RegistrationDemo_OR/Page_Login  Rapid Hybris/input_Last Name_lastName'), 
    'test')

WebUI.setText(findTestObject('Object Repository/RegistrationDemo_OR/Page_Login  Rapid Hybris/input_Email Address_email'), 
    'regtest1@test.com')

WebUI.setEncryptedText(findTestObject('Object Repository/RegistrationDemo_OR/Page_Login  Rapid Hybris/input_Password_pwd'), 
    'zXMD8VQinzNakze1o1dv8w==')

WebUI.setEncryptedText(findTestObject('Object Repository/RegistrationDemo_OR/Page_Login  Rapid Hybris/input_Confirm Password_checkPwd'), 
    'zXMD8VQinzNakze1o1dv8w==')

WebUI.click(findTestObject('Object Repository/RegistrationDemo_OR/Page_Login  Rapid Hybris/input_Confirm Password_consent'))

WebUI.click(findTestObject('Object Repository/RegistrationDemo_OR/Page_Login  Rapid Hybris/input_I am confirming'))

WebUI.click(findTestObject('Object Repository/RegistrationDemo_OR/Page_Login  Rapid Hybris/button_Register'))

