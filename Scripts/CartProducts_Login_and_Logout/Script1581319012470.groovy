import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import static com.yourcompany.ConnectionNotPrivate.*
import com.yourcompany.ConnectionNotPrivate as ConnectionNotPrivate

//ConnectionNotPrivate.yourMethod()
WebUI.openBrowser('')

WebUI.navigateToUrl("http://$GlobalVariable.Hostname")

WebUI.maximizeWindow()

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Rapid Hybris  Homepage/span_Quick Order_far fa-user'))

WebUI.selectOptionByValue(findTestObject('Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Company', true)

WebUI.selectOptionByValue(findTestObject('Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Mr', true)

WebUI.selectOptionByValue(findTestObject('Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Miss', true)

WebUI.selectOptionByValue(findTestObject('Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Mrs', true)

WebUI.selectOptionByValue(findTestObject('Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Ms', true)

WebUI.selectOptionByValue(findTestObject('Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Rev.', true)

WebUI.selectOptionByValue(findTestObject('Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Prof.', true)

WebUI.selectOptionByValue(findTestObject('Titles_OR/Page_Login  Rapid Hybris/select_Please select CompanyMrMissMrsMsRevProfDr'), 
    'Dr.', true)

WebUI.setText(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Login  Rapid Hybris/input_First Name_firstName'), 
    'kenneth')

WebUI.setText(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Login  Rapid Hybris/input_Last Name_lastName'), 
    'sarah')

WebUI.setText(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Login  Rapid Hybris/input_Email Address_email'), 
    "$GlobalVariable.Email")

WebUI.setEncryptedText(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Login  Rapid Hybris/input_Password_pwd'), 
    "$GlobalVariable.Password")

WebUI.setEncryptedText(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Login  Rapid Hybris/input_Confirm Password_checkPwd'), 
    "$GlobalVariable.Password")

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Login  Rapid Hybris/input_Confirm Password_consentFormconsentGiven'))

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Login  Rapid Hybris/input_I am confirming that I have read and agreed with the_termsCheck'))

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Login  Rapid Hybris/button_Register'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Rapid Hybris  Homepage/button_'))

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Rapid Hybris  Homepage/a_Accessories'))

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Accessories  Categories  Rapid Hybris/button_1616_fas fa-cart-plus custom-cart js-enable-btn'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Accessories  Categories  Rapid Hybris/a_Continue Shopping'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Accessories  Categories  Rapid Hybris/button_1454_fas fa-cart-plus custom-cart js-enable-btn'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Accessories  Categories  Rapid Hybris/a_Continue Shopping'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Accessories  Categories  Rapid Hybris/span_Quick Order_far fa-user'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Accessories  Categories  Rapid Hybris/a_Sign Out'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Rapid Hybris  Homepage/span_Quick Order_far fa-user'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Login  Rapid Hybris/input_Email Address_j_username'), 
    "$GlobalVariable.Email")

WebUI.delay(2)

WebUI.setEncryptedText(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Login  Rapid Hybris/input_Password_j_password'), 
    "$GlobalVariable.Password")

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Login  Rapid Hybris/button_Log In'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Rapid Hybris  Homepage/span_Quick Order_far fa-user_1'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Rapid Hybris  Homepage/a_Sign Out'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Rapid Hybris  Homepage/span_Quick Order_far fa-user'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Login  Rapid Hybris/input_Email Address_j_username'), 
    "$GlobalVariable.Email")

WebUI.delay(2)

WebUI.setEncryptedText(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Login  Rapid Hybris/input_Password_j_password'), 
    "$GlobalVariable.Password")

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Login  Rapid Hybris/button_Log In'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Rapid Hybris  Homepage/span_Welcome Robert_glyphicon fas fa-cart-plus'))

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('CartProducts_Login_and_Logout/Page_Your Shopping Bag  Rapid Hybris/span_1454_glyphicon glyphicon-option-vertical'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Your Shopping Bag  Rapid Hybris/a_Remove'))

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

//WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Your Shopping Bag  Rapid Hybris/span_1454_glyphicon glyphicon-option-vertical'))
WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Your Shopping Bag  Rapid Hybris/span_1616_glyphicon glyphicon-option-vertical'))

WebUI.delay(2)

//WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Your Shopping Bag  Rapid Hybris/a_Remove'))
WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Your Shopping Bag  Rapid Hybris/a_Remove'))

WebUI.delay(2)

WebUI.click(findTestObject('CartProducts_Login_and_Logout/Page_Your Shopping Bag  Rapid Hybris/buttonCartmessage_'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Your Shopping Bag  Rapid Hybris/span_Quick Order_far fa-user'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Your Shopping Bag  Rapid Hybris/a_Sign Out'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Rapid Hybris  Homepage/span_Quick Order_far fa-user'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Login  Rapid Hybris/input_Email Address_j_username'), 
    "$GlobalVariable.Email")

WebUI.delay(2)

WebUI.setEncryptedText(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Login  Rapid Hybris/input_Password_j_password'), 
    "$GlobalVariable.Password")

WebUI.delay(2)

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Login  Rapid Hybris/button_Log In'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Rapid Hybris  Homepage/span_Quick Order_far fa-user_2'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Rapid Hybris  Homepage/a_Sign Out'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Rapid Hybris  Homepage/span_Quick Order_far fa-user'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Login  Rapid Hybris/input_Email Address_j_username'), 
    "$GlobalVariable.Email")

WebUI.delay(2)

WebUI.setEncryptedText(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Login  Rapid Hybris/input_Password_j_password'), 
    "$GlobalVariable.Password")

WebUI.delay(2)

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Login  Rapid Hybris/button_Log In'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Rapid Hybris  Homepage/a_Accessories'))

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Accessories  Categories  Rapid Hybris/span_Easy Beanie black Uni'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Easy Beanie black Uni  Beanies  Accessories  Categories  Rapid Hybris/button_Add to bag'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Easy Beanie black Uni  Beanies  Accessories  Categories  Rapid Hybris/a_Continue Shopping'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Images_AllCategoriesTest/Page_Apparel Site UK  Homepage/img_Quick Order_lazy'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Easy Beanie black Uni  Beanies  Accessories  Categories  Rapid Hybris/a_Accessories'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Accessories  Categories  Rapid Hybris/span_The Reservoir Beanie caribbean blue Uni'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_The Reservoir Beanie caribbean blue Uni  Beanies  Accessories  Categories  Rapid Hybris/button_Add to bag'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_The Reservoir Beanie caribbean blue Uni  Beanies  Accessories  Categories  Rapid Hybris/a_Check Out'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Your Shopping Bag  Rapid Hybris/span_Quick Order_far fa-user'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Your Shopping Bag  Rapid Hybris/a_Sign Out'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Rapid Hybris  Homepage/span_Quick Order_far fa-user'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Login  Rapid Hybris/input_Email Address_j_username'), 
    "$GlobalVariable.Email")

WebUI.delay(2)

WebUI.setEncryptedText(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Login  Rapid Hybris/input_Password_j_password'), 
    "$GlobalVariable.Password")

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Login  Rapid Hybris/button_Log In'))

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('Object Repository/CartTest_OR/Page_Accessories  Categories  Apparel Site UK/span_Quick Order_glyphicon fas fa-cart-plus'), 
    0)

WebUI.mouseOver(findTestObject('Object Repository/CartTest_OR/Page_Accessories  Categories  Apparel Site UK/span_Quick Order_glyphicon fas fa-cart-plus'))

WebUI.waitForElementClickable(findTestObject('Object Repository/CartTest_OR/Page_Accessories  Categories  Apparel Site UK/a_Check Out'), 
    30)

WebUI.scrollToElement(findTestObject('Object Repository/CartTest_OR/Page_Accessories  Categories  Apparel Site UK/a_Check Out'), 
    0)

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartTest_OR/Page_Accessories  Categories  Apparel Site UK/a_Check Out'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Your Shopping Bag  Rapid Hybris/span_1454_glyphicon glyphicon-option-vertical'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Your Shopping Bag  Rapid Hybris/a_Remove'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Your Shopping Bag  Rapid Hybris/span_Quick Order_far fa-user'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Your Shopping Bag  Rapid Hybris/a_Sign Out'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Rapid Hybris  Homepage/span_Quick Order_far fa-user'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Login  Rapid Hybris/input_Email Address_j_username'), 
    "$GlobalVariable.Email")

WebUI.delay(2)

WebUI.setEncryptedText(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Login  Rapid Hybris/input_Password_j_password'), 
    "$GlobalVariable.Password")

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Login  Rapid Hybris/button_Log In'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Rapid Hybris  Homepage/span_Welcome Robert_glyphicon fas fa-cart-plus'))

WebUI.delay(2)

WebUI.scrollToPosition(0, 150)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Your Shopping Bag  Rapid Hybris/span_1616_glyphicon glyphicon-option-vertical'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Your Shopping Bag  Rapid Hybris/a_Remove'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Your Shopping Bag  Rapid Hybris/span_Quick Order_far fa-user'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Your Shopping Bag  Rapid Hybris/a_Sign Out'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Rapid Hybris  Homepage/span_Quick Order_far fa-user'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Login  Rapid Hybris/input_Email Address_j_username'), 
    "$GlobalVariable.Email")

WebUI.delay(2)

WebUI.setEncryptedText(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Login  Rapid Hybris/input_Password_j_password'), 
    "$GlobalVariable.Password")

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CartProducts_Login_and_Logout/Page_Login  Rapid Hybris/button_Log In'))

