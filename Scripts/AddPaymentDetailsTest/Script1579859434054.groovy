import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import static com.yourcompany.ConnectionNotPrivate.*
import com.yourcompany.ConnectionNotPrivate as ConnectionNotPrivate

//ConnectionNotPrivate.yourMethod()

WebUI.openBrowser('')

import internal.GlobalVariable as GlobalVariable

WebUI.navigateToUrl("http://${GlobalVariable.Hostname}")

WebUI.maximizeWindow()
WebUI.click(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Apparel Site UK  Homepage/span_Quick Order_far fa-user'))

WebUI.setText(findTestObject('Page_Login  Apparel Site UK/input_Email Address_j_username'), "$GlobalVariable.Email")

WebUI.setEncryptedText(findTestObject('Page_Login  Apparel Site UK/input_Password_j_password'), "$GlobalVariable.Password")

WebUI.click(findTestObject('Page_Login  Apparel Site UK/button_Log In'))

WebUI.click(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Apparel Site UK  Homepage/span_Quick Order_far fa-user_1'))

WebUI.click(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Apparel Site UK  Homepage/a_Payment Details'))

WebUI.click(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Payment Details  Apparel Site UK/a_Add Payment Details'))

WebUI.selectOptionByValue(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/select_State  Province GuernseyIsle of ManJerseyUnited Kingdom'), 
    'GG', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/select_State  Province GuernseyIsle of ManJerseyUnited Kingdom'), 
    'IM', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/select_State  Province GuernseyIsle of ManJerseyUnited Kingdom'), 
    'JE', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/select_State  Province GuernseyIsle of ManJerseyUnited Kingdom'), 
    'GB', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_AddEdit Address  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Company', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_AddEdit Address  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Mr', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_AddEdit Address  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Miss', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_AddEdit Address  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Mrs', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_AddEdit Address  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Ms', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_AddEdit Address  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Rev.', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_AddEdit Address  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Prof.', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_AddEdit Address  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Dr.', true)

WebUI.setText(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/input_First Name_firstName'), 
    'prakash')

WebUI.setText(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/input_Last Name_lastName'), 
    'velineni')

WebUI.setText(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/input_Address Line 1_line1'), 
    'krishnanagar')

WebUI.setText(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/input_(optional)_line2'), 
    'yousufguda')

WebUI.setText(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/input_City_townCity'), 
    'hyderabad')

WebUI.setText(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/input_Zip  Postal Code_postcode'), 
    '522212')

WebUI.setText(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/input_(optional)_phone'), 
    '9999912345')

WebUI.click(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/input_Make this my default address_defaultAddress'))

WebUI.selectOptionByValue(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/select_Please select a card type American Express'), 
    'amex', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/select_Please select a card type American Express'), 
    'maestro', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/select_Please select a card type American Express'), 
    'switch', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/select_Please select a card type American Express'), 
    'visa', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/select_Please select a card type American Express'), 
    'master', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/select_Please select a card type American Express'), 
    'mastercard_eurocard', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/select_Please select a card type American Express'), 
    'diners', true)

WebUI.setText(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/input_(optional)_nameOnCard'), 
    'prakash velineni')

WebUI.setText(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/input_cardNumber_cardNumber'), 
    '1234567890123456')

WebUI.selectOptionByValue(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/select_Month 010203040506070809101112'), 
    '1', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/select_Month 010203040506070809101112'), 
    '2', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/select_Month 010203040506070809101112'), 
    '3', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/select_Month 010203040506070809101112'), 
    '4', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/select_Month 010203040506070809101112'), 
    '5', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/select_Month 010203040506070809101112'), 
    '6', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/select_Month 010203040506070809101112'), 
    '7', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/select_Month 010203040506070809101112'), 
    '8', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/select_Month 010203040506070809101112'), 
    '9', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/select_Month 010203040506070809101112'), 
    '10', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/select_Month 010203040506070809101112'), 
    '11', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/select_Month 010203040506070809101112'), 
    '12', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/select_Year 20202021202220232024202520262027202820292030'), 
    '2020', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/select_Year 20202021202220232024202520262027202820292030'), 
    '2021', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/select_Year 20202021202220232024202520262027202820292030'), 
    '2022', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/select_Year 20202021202220232024202520262027202820292030'), 
    '2023', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/select_Year 20202021202220232024202520262027202820292030'), 
    '2024', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/select_Year 20202021202220232024202520262027202820292030'), 
    '2025', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/select_Year 20202021202220232024202520262027202820292030'), 
    '2026', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/select_Year 20202021202220232024202520262027202820292030'), 
    '2027', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/select_Year 20202021202220232024202520262027202820292030'), 
    '2028', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/select_Year 20202021202220232024202520262027202820292030'), 
    '2029', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/select_Year 20202021202220232024202520262027202820292030'), 
    '2030', true)

WebUI.setText(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/input_(optional)_issueNumber'), 
    '123')

WebUI.click(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Add Payment Details  Apparel Site UK/button_Save'))

WebUI.click(findTestObject('AddPaymentDetailsTest_OR/Page_Payment Details  Apparel Site UK/span_United Kingdom522212_glyphicon glyphicon-pencil'))

WebUI.selectOptionByValue(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Edit Payment Details  Apparel Site UK/select_12010203040506070809101112'), 
    '1', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Edit Payment Details  Apparel Site UK/select_203020202021202220232024202520262027202820292030'), 
    '2029', true)

WebUI.click(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Edit Payment Details  Apparel Site UK/button_Update'))

WebUI.click(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Payment Details  Apparel Site UK/span_United Kingdom522212_glyphicon glyphicon-pencil'))

WebUI.click(findTestObject('Object Repository/AddPaymentDetailsTest_OR/Page_Edit Payment Details  Apparel Site UK/button_Cancel'))

