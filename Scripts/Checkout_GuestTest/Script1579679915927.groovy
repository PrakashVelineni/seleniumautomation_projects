import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger



import static com.yourcompany.ConnectionNotPrivate.*
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.yourcompany.ConnectionNotPrivate as ConnectionNotPrivate




//ConnectionNotPrivate.yourMethod()

WebUI.openBrowser('')

WebUI.maximizeWindow()

import internal.GlobalVariable as GlobalVariable

WebUI.navigateToUrl("http://${GlobalVariable.Hostname}")

WebUI.click(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Apparel Site UK  Homepage/a_Accessories'))

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/CheckOutTest_OR/Page_Accessories  Categories  Apparel Site UK/span_The Sentry gunship'))

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Checkout_Guest_OR1/Page_HD Hero2 Outdoor Edition uni/button_Add to bag'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Checkout_Guest_OR1/Page_HD Hero2 Outdoor Edition uni/a_Check Out'))

WebUI.click(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Your Shopping Bag  Apparel Site UK/button_Check Out'))

WebUI.setText(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Proceed to Checkout  Apparel Site UK/input_Email Address_email'), 
    "$GlobalVariable.Email")

WebUI.setText(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Proceed to Checkout  Apparel Site UK/input_Confirm Email Address_guestconfirmemail'), 
    "$GlobalVariable.Email")

WebUI.click(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Proceed to Checkout  Apparel Site UK/button_Check Out as a Guest'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/select_Country  GuernseyIsle of ManJerseyUnited Kingdom'), 
    'GG', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/select_Country  GuernseyIsle of ManJerseyUnited Kingdom'), 
    'IM', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/select_Country  GuernseyIsle of ManJerseyUnited Kingdom'), 
    'JE', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/select_Country  GuernseyIsle of ManJerseyUnited Kingdom'), 
    'GB', true)

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Company', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Mr', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Miss', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Mrs', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Ms', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Rev.', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Prof.', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Dr.', true)

WebUI.setText(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/input_First Name_firstName'), 
    'prakash')

WebUI.setText(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/input_Last Name_lastName'), 
    'velineni')

WebUI.click(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/div_Address Line 1'))

WebUI.setText(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/input_Address Line 1_line1'), 
    'krishnanagar')

WebUI.click(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/div_Title title MrMissMrsMsDrRev'))

WebUI.setText(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/input_(optional)_line2'), 
    'yousufguda')

WebUI.setText(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/input_City_townCity'), 
    'hyderabad')

WebUI.setText(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/input_Post Code_postcode'), 
    '522212')

WebUI.setText(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/input_(optional)_phone'), 
    '9090909191')

WebUI.click(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/button_Next'))

WebUI.click(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/button_Next_1'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/select_Please select a card type'), 
    '003', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/select_Please select a card type'), 
    '024', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/select_Please select a card type'), 
    '001', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/select_Please select a card type'), 
    '002', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/select_Please select a card type'), 
    '005', true)

WebUI.setText(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/input_(optional)_card_nameOnCard'), 
    'prakash velineni')

WebUI.setText(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/input_Card number_card_accountNumber'), 
    '1234567890123456')

WebUI.selectOptionByValue(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/select_Month                010203040506070809101112'), 
    '1', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/select_Month                010203040506070809101112'), 
    '2', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/select_Month                010203040506070809101112'), 
    '3', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/select_Month                010203040506070809101112'), 
    '4', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/select_Month                010203040506070809101112'), 
    '5', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/select_Month                010203040506070809101112'), 
    '6', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/select_Month                010203040506070809101112'), 
    '7', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/select_Month                010203040506070809101112'), 
    '8', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/select_Month                010203040506070809101112'), 
    '9', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/select_Month                010203040506070809101112'), 
    '10', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/select_Month                010203040506070809101112'), 
    '11', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/select_Month                010203040506070809101112'), 
    '12', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/select_Year 20202021202220232024202520262027202820292030'), 
    '2020', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/select_Year 20202021202220232024202520262027202820292030'), 
    '2021', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/select_Year 20202021202220232024202520262027202820292030'), 
    '2022', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/select_Year 20202021202220232024202520262027202820292030'), 
    '2023', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/select_Year 20202021202220232024202520262027202820292030'), 
    '2024', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/select_Year 20202021202220232024202520262027202820292030'), 
    '2025', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/select_Year 20202021202220232024202520262027202820292030'), 
    '2026', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/select_Year 20202021202220232024202520262027202820292030'), 
    '2027', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/select_Year 20202021202220232024202520262027202820292030'), 
    '2028', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/select_Year 20202021202220232024202520262027202820292030'), 
    '2029', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/select_Year 20202021202220232024202520262027202820292030'), 
    '2030', true)

WebUI.setText(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/input_Card Verification Number_card_cvNumber'), 
    '123')

WebUI.click(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/input_Use my Delivery Address_useDeliveryAddress'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/select_Country   Albania'), 
    'AL', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Company', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Mr', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Miss', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Mrs', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Ms', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Rev.', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Prof.', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Titles_OR/Page_Checkout  Rapid Hybris/select_title                CompanyMrMissMrsMsRevProfDr'), 
    'Dr.', true)

WebUI.setText(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/input_First Name_billTo_firstName'), 
    'prakash')

WebUI.setText(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/input_Last Name_billTo_lastName'), 
    'velineni')

WebUI.setText(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/input_Address Line 1_billTo_street1'), 
    'krishnanagar')

WebUI.setText(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/input_(optional)_billTo_street2'), 
    'yousufguda')

WebUI.setText(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/input_City_billTo_city'), 
    'hyderabad')

WebUI.setText(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/input_Post Code_billTo_postalCode'), 
    '522212')

WebUI.setText(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/input_(optional)_billTo_phoneNumber'), 
    '9090909191')

WebUI.click(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/button_Next_2'))

WebUI.scrollToPosition(0, 250)

WebUI.delay(2)

WebUI.scrollToPosition(0, 250)

WebUI.click(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/input_By placing the order'))

WebUI.click(findTestObject('Object Repository/Checkout_Guest_OR1/Page_Checkout  Apparel Site UK/button_Place Order'))

WebUI.delay(2)

WebUI.click(findTestObject('CheckOutTest_OR/Page_Order Confirmation  Rapid Hybris/div_Thank you for your Order'))

WebUI.delay(2)

WebUI.verifyElementPresent(findTestObject('CheckOutTest_OR/Page_Order Confirmation  Rapid Hybris/div_Thank you for your Order'),
	0)

String order = WebUI.getText(findTestObject('CheckOutTest_OR/Page_Order Confirmation  Rapid Hybris/div_Thank you for your Order'))

KeywordLogger logger = new KeywordLogger()

logger.logInfo(order)


